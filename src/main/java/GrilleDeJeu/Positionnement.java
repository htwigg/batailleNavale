/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package GrilleDeJeu;

import java.util.ArrayList;
import java.util.Random;

public class Positionnement {

    private ArrayList<Case> serie = new ArrayList<Case>();

    public Positionnement() {
    }

    /** Place une navire aléatoirement sur la grille du joueur.
     *
     * @param longeur du navire
     * @param grille du joueur
     * @return la position sur laquelle le navire est placé
     */
    public ArrayList<Case> aleatoire(int longeur, GrilleDeJeu grille) {
        boolean trouve = false;

        while(!trouve) {
            serie.clear();
            trouverCases(longeur, grille);
            // verification si les cases sont occupées
            for(int i = 0; i < serie.size(); i++){
                if(serie.get(i).navire != null){
                    trouve = false;
                    serie.clear();
                }
            }
            // si une combinaison de longueur voulu est trouvé
            if(serie.size() == longeur) {
                trouve = true;
            }
        }
        return serie;
    }

    /** Trouve une série de case libre pour le navire.
     *
     * @param longeur du navire
     * @param grille du joueur
     */
    private void trouverCases(int longeur, GrilleDeJeu grille) {
        Random random = new Random();
        int coordX = random.nextInt(10);
        int coordY = random.nextInt(10);
        int x = coordX;
        int y = coordY;
        boolean horizontal = random.nextBoolean();
        boolean inverse = false;

        // trouver cases aleatoires
        for(int i = 0; i < longeur; i++){
            if(estCaseValide(y, x)){
                serie.add(grille.cases[y][x]);
            }else{
                x = coordX;
                y = coordY;
                inverse = true;
            }
            if(!inverse) {
                if(horizontal) {
                    x++;
                } else {
                    y++;
                }
            }else{
                if(horizontal) {
                    x--;
                } else {
                    y--;
                }
            }
        }
    }

    /**
     * @param y ordonnée
     * @param x abscisse
     * @return si la case est valide sur la grille
     */
    private boolean estCaseValide(int y, int x){ return ((y >= 0 && y <= 9) && (x >= 0 && x <= 9)); }

}
