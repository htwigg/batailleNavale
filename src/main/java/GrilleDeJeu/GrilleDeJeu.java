/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package GrilleDeJeu;

public class GrilleDeJeu {

    public Case [][] cases = new Case[10][10];

    public GrilleDeJeu() {
        for(int o = 0; o < 10; o++) {
            for(int a = 0; a < 10; a++) {
                Case c = new Case(o,a);
                cases[o][a] = c;
            }
        }
    }
}
