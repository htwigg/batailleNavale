/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package GrilleDeJeu;

import Navire.*;

public class Case {

	public int abscisse, ordonnee;
	public Boolean occupe, toucher;
	public AbstractNavire navire;

	public Case(int o, int a) {
		ordonnee = o;
        abscisse = a;
        occupe = false;
		toucher = false;
		navire = null;
	}

	/** Si la case est occupée on appel la méthode toucher.
	 *
	 * @return si la case est occupée par un navire
	 */
	public boolean caseOccuper() {
		if(occupe) {
			toucher();
		}
		return occupe;
	}

	/**
	 * Si la case est occupée réduire la vie du navire
	 * et changer l'état toucher de la case.
	 */
	private void toucher() {
		toucher = true;
		navire.reduireVie();
	}

	public void reinitialiser() {
		toucher = false;
	}
}
