/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package Menu;

import GrilleDeJeu.Case;
import Navire.*;
import Partie.Partie;
import java.io.File;
import java.util.ArrayList;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

public class RecuperationPartie {

    /** Lit le fichier xml et restaure l'état de la partie précédement jouer.
     *
     * @param fichier chemin du fichier xml à lire
     * @return une instance de partie
     */
    public static Partie xmlAPartie(File fichier)
    {
        Partie partie = new Partie();
        Document document = creerDocument(fichier);

        Element joueur = document.getRootElement().getChild("joueur");
        Element ai = document.getRootElement().getChild("JoueurArtificiel");

        String nom = joueur.getAttributeValue("nom");
        String niveau = ai.getAttributeValue("niveau");

        ArrayList<AbstractNavire> navires = naviresJoueur(joueur);
        ArrayList<AbstractNavire> naviresAI = naviresJoueur(ai);
        ArrayList<Case> coups = coupsJoueur(joueur);
        ArrayList<Case> coupsAI = coupsJoueur(ai);

        partie.configurer(nom, Integer.parseInt(niveau));
        partie.joueurH.navires = navires;
        partie.joueurH.coupsJouer = coups;
        partie.joueurA.navires = naviresAI;
        partie.joueurA.coupsJouer = coupsAI;

        return partie;
    }

    /** Passe à travers les coups du fichier et les associes à un joueur.
     *
     * @param joueur Element du fichier de type joueur
     * @return une liste de coups associés à un joueur
     */
    private static ArrayList<Case> coupsJoueur(Element joueur){
        ArrayList<Case> coups = new ArrayList<Case>();
        String ordonnee, abscisse;
        int iOrdonnee, iabscisse;

        for(Element coupJouer : joueur.getChildren("coupJouer")) {
            for (Element coup : coupJouer.getChildren("coup")) {
                ordonnee = coup.getAttributeValue("ordonnee");
                abscisse = coup.getAttributeValue("abscisse");
                iOrdonnee = Integer.parseInt(ordonnee);
                iabscisse = Integer.parseInt(abscisse);
                coups.add(new Case(iOrdonnee, iabscisse));
            }
        }
        return coups;
    }

    /** Passer à travers les navires du fichier et les associes à un joueur.
     *
     * @param joueur Element du fichier de type joueur
     * @return une liste de navires associés à un joueur
     */
    private static ArrayList<AbstractNavire> naviresJoueur(Element joueur){
        ArrayList<AbstractNavire> navires = new ArrayList<AbstractNavire>();
        ArrayList<Case> caseNavire;
        AbstractNavire bateau = null;
        String ordonnee, abscisse, longueur, vie, type;
        int iOrdonnee, iabscisse;

        for (Element lesNavires : joueur.getChildren("navires")) {
            for(Element navire : lesNavires.getChildren("navire")) {
                longueur = navire.getAttributeValue("longueur");
                type = navire.getAttributeValue("type");
                vie = navire.getAttributeValue("vie");
                caseNavire = new ArrayList<Case>();
                for (Element caseGrille : navire.getChildren("cases")) {
                    ordonnee = caseGrille.getAttributeValue("ordonnee");
                    abscisse = caseGrille.getAttributeValue("abscisse");
                    iOrdonnee = Integer.parseInt(ordonnee);
                    iabscisse = Integer.parseInt(abscisse);
                    Case uneCase = new Case(iOrdonnee, iabscisse);
                    caseNavire.add(uneCase);
                }
                if(type.equals("Porte-Avion")) {
                    bateau = new PorteAvion();
                }else if(type.equals("Croiseur")) {
                    bateau = new Croiseur();
                }else if(type.equals("Contre-Torpilleurs")) {
                    bateau = new ContreTorpilleurs();
                }else if(type.equals("Sous-Marin")) {
                    bateau = new SousMarin();
                }else if(type.equals("Torpilleur")) {
                    bateau = new Torpilleur();
                }
                bateau.setEmplacement(caseNavire);
                bateau.setLongueur(Integer.parseInt(longueur));
                navires.add(bateau);
            }
        }
        return navires;
    }

    /** Récupère le fichier et en faire un document  que l'on peut traverser.
     *
     * @param fichier chemin du fichier à récupérer
     * @return une instance de document
     */
    private static Document creerDocument(File fichier) {
        SAXBuilder builder = new SAXBuilder();
        try {
            Document doc = builder.build(fichier);
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
