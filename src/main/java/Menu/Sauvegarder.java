/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package Menu;

import Navire.*;
import GrilleDeJeu.Case;
import Partie.Partie;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Sauvegarder {

    /** Crée les informations nécessaire pour la sauvegarde
     * d'une partie dans un fichier XML.
     *
     * @param partie à sauvegarder
     */
    public static void partieAXml(Partie partie) {
        try {
            Document docXml = new Document();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss");
            String nomFichier = formatter.format(new java.util.Date());
            String extension = ".xml";
            String nomFichierAvecExtension = nomFichier.concat(extension);
            File workingDirectory = new File(System.getProperty("user.home"), "partie_sauvegarder");
            String repertoire = System.getProperty("user.home") + "/partie_sauvegarder/";
            String cheminComplet = repertoire.concat(nomFichierAvecExtension);
            formatFichier(docXml,partie);

            XMLOutputter xmlOutput = new XMLOutputter(org.jdom2.output.Format.getPrettyFormat());
            if(!workingDirectory.exists()){
                workingDirectory.mkdir();
            }
            xmlOutput.output(docXml, new FileOutputStream(new File(cheminComplet)));
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    /** Génère le format du fichier XML sauvegardé.
     *
     * @param doc pour la sauvegarde
     * @param partieSauvegarder à sauvegarder
     */
    private static void formatFichier(Document doc, Partie partieSauvegarder)
    {
        //format du fichier XML
        Element partie = new Element("Partie");

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss");
        String dateDebut =   formatter.format(new java.util.Date());

        partie.setAttribute("dateDebut", dateDebut);

        doc.setRootElement(partie);
        // sauvegarde information joueur Humain
        Element joueur =  ecrireJoueurHumain(partieSauvegarder);
        partie.addContent(joueur);
        //sauvegarde information joueur Artificiel
        Element ia = ecrireJoueurArtificiel(partieSauvegarder);
        partie.addContent(ia);
    }

    /** Formate les données du joueur pour la sauvegarde.
     *
     * @param partieSauvegarder partie à sauvegarder
     * @return un element joueur
     */
     private static Element ecrireJoueurHumain(Partie partieSauvegarder){
        Element joueur = new Element ("joueur");
        String nom = partieSauvegarder.joueurH.nom;
        ArrayList<AbstractNavire> listeNavires = partieSauvegarder.joueurH.navires;
        ArrayList<Case> listeCoupsJouer = partieSauvegarder.joueurH.coupsJouer;
        joueur.setAttribute("nom", nom);

        Element navires = ecrireInformationNavires(listeNavires);
        joueur.addContent(navires);

        Element coupJouer = ecrireCoupJoueur(listeCoupsJouer);
        joueur.addContent(coupJouer);

        return joueur;
    }

    /** Formate les données du joueur aritificiel pour la sauvegarde.
     *
     * @param partieSauvegarder partie à sauvegarder
     * @return un element joueur artificiel
     */
    private static Element ecrireJoueurArtificiel(Partie partieSauvegarder){
        Element ia = new Element("JoueurArtificiel");
        int niveau = partieSauvegarder.joueurA.getNiveau();
        ia.setAttribute("niveau",Integer.toString(niveau));

        ArrayList<AbstractNavire> listeNavires = partieSauvegarder.joueurA.navires;
        ArrayList<Case> listeCoupsJouer = partieSauvegarder.joueurA.coupsJouer;
        Element navires = ecrireInformationNavires(listeNavires);
        ia.addContent(navires);

        Element coupJouer = ecrireCoupJoueur(listeCoupsJouer);
        ia.addContent(coupJouer);

        return ia;
    }

    /** Prépare la sauvegarde des navires du joueur.
     *
     * @param listeNavires liste des navires du joueur
     * @return liste des navires
     */
    private static Element ecrireInformationNavires(ArrayList<AbstractNavire> listeNavires){

        Element navires = new Element("navires");
        for(AbstractNavire n : listeNavires ){
            int longueur_ = n.getLongueur();
            String type_ = n.getNom();
            int vie_ = n.getVie();

            Element navire = sauvegarderNavires(n,longueur_,type_,vie_);
            navires.addContent(navire);

            ArrayList<Case> listeDeCase = n.getEmplacement();
            for(Case courante : listeDeCase){
                int ordonne = courante.ordonnee;
                int abscisse = courante.abscisse;
                Element cases = sauvegarderCase(abscisse,ordonne);
                navire.addContent(cases);
            }
        }
        return navires;
    }

    /** Prépare la sauvegarde des coups fait par le joueur.
     *
     * @param listeCoupsJouer que le joueur a effectués
     * @return liste de coups fait
     */
    private static Element ecrireCoupJoueur(ArrayList<Case> listeCoupsJouer){
        Element coupJouer = new Element("coupJouer");
        for(Case courante : listeCoupsJouer){
            int ordonne = courante.ordonnee;
            int abscisse = courante.abscisse;

            Element coup =  sauvegarderCoup(ordonne,abscisse);
            coupJouer.addContent(coup);
        }
        return coupJouer;
    }

    /** Sauvegarder les navires du joueur.
     *
     * @param n un navire
     * @param longueur_ du navire
     * @param type_ du navire
     * @param vie_ du navire
     * @return le navire sauvegarder
     */
   private static Element sauvegarderNavires(AbstractNavire n, int longueur_, String type_, int vie_){
        Element navire = new Element("navire");

        navire.setAttribute("longueur",Integer.toString(longueur_));
        navire.setAttribute("type", type_);
        navire.setAttribute("vie",Integer.toString(vie_));

        return navire;
    }

    /** Sauvegarder de la case.
     *
     * @param ordonne de la case
     * @param abscisse de la case
     * @return la case sauvegarder
     */
    private static Element sauvegarderCase(int ordonne , int abscisse){
        Element cases = new Element("cases");

        cases.setAttribute("ordonnee", Integer.toString(ordonne));
        cases.setAttribute("abscisse", Integer.toString(abscisse));

        return cases;
    }

    /** Sauvegarder du coup.
     *
     * @param ordonne du coup
     * @param abscisse du coup
     * @return le coup fait
     */
    private static Element sauvegarderCoup(int ordonne, int abscisse ){
        Element coup = new Element("coup");

        coup.setAttribute("ordonnee",Integer.toString(ordonne));
        coup.setAttribute("abscisse",Integer.toString(abscisse));

        return coup;
    }
}
