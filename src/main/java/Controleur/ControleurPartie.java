/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package Controleur;

import GrilleDeJeu.*;
import Joueur.AbstractJoueur;
import Joueur.JoueurArtificiel;
import Joueur.JoueurHumain;
import Menu.RecuperationPartie;
import Menu.Sauvegarder;
import Navire.*;
import Partie.Partie;
import java.io.File;
import java.util.ArrayList;

public final class ControleurPartie {

    private static final ControleurPartie CONTROLEUR = new ControleurPartie();
    private Partie partie;

    private ControleurPartie(){}

    /** Retour l'unique instance du contrôleur de la partie.
     *
     * @return controleur : Instance du contrôleur
     */
    public static ControleurPartie getInstance() {
        return CONTROLEUR;
    }

    /** Effectue la création de la partie et la configuration des joueurs.
     *
     * @param nom du joueur humain
     * @param niveau du joueur artificiel
     */
    public void configurer(String nom, int niveau){
        partie = new Partie();
        partie.configurer(nom, niveau);
    }

    /**
     * @return l'instance de la grille du joueur artificiel
     */
    public GrilleDeJeu getGrilleJoueurArtificiel() {
        return partie.joueurA.grille;
    }

    /**
     * @return l'instance du joueur humain
     */
    public JoueurHumain getJoueurHumain() { return partie.joueurH; }

    /**
     * @return l'instance du joueur artificiel
     */
    public JoueurArtificiel getJoueurArtificiel() { return partie.joueurA; }

    /** Vérifi si le coup lancé par le joueur humain est valide.
     *
     * @param o ordonnée du coup
     * @param a abscisse du coup
     * @return la validiter du coup
     */
    public boolean placerUnCoup(int o, int a) {
        return partie.joueurH.coupSuivant(o,a);
    }

    /** Vérifi si le coup lancé à touché l'opposant.
     *
     * @param joueur sur qui le coup est lancé
     * @param o ordonnée du coup
     * @param a abscisse du coup
     * @return si le coup a touché l'opposant
     */
    public boolean aTouchee(AbstractJoueur joueur, int o, int a) {
        return partie.miseAJourTorpille(joueur,o,a);
    }

    /**
     * @param joueur sur qui le coup est lancé
     * @param o ordonnée du coup
     * @param a abscisse du coup
     * @return si le coup a coulé un navire
     */
    public boolean aCoulee(AbstractJoueur joueur, int o, int a) {
        return partie.aCouleeNavire(joueur,o,a);
    }

    /**
     * @param joueur dont le navire a été coulé
     * @param o ordonnée du coup
     * @param a abscisse du coup
     * @return la liste des positions du navire coulé
     */
    public int[][] naviresCoulees(AbstractJoueur joueur, int o, int a) {
        ArrayList<Case> cases = partie.listeCasesCoulees(joueur,o,a);
        int [][] positions = new int[cases.size()][2];
        for(int i = 0; i < cases.size(); i++) {
            Case c = cases.get(i);
            positions[i][0] = c.ordonnee;
            positions[i][1] = c.abscisse;
        }
        return positions;
    }

    /** Appel la fonction de calcul du coup exécuté par le joueur artificiel.
     *
     * @return la position du coup exécuté par le joueur artificiel
     */
    public int [] jouerCoupArtificiel() {
        return partie.joueurA.executerCoup();
    }

    /** Mise à jour de l'état de la case sur laquelle le joueur artificiel a lancé.
     *
     * @param touchee si le dernier coup fait par le joueur artificiel a touché
     */
    public void aIDernierTouchee(boolean touchee) {
        partie.joueurA.dernierToucher = touchee;
    }

    /** Appel la partie pour savoir si le joueur a perdu.
     *
     * @param joueur qui pourrait avoir perdu après le dernier coup lancé
     * @return si le joueur a perdu
     */
    public boolean verifierPerdu(AbstractJoueur joueur) {
        return partie.finPartie(joueur);
    }

    /** Demande au joueur un coup qu'il a précédement effectué.
     *
     * @param joueur pour lequel on veut récupéré le dernier coup
     * @param index du coup dans la liste de coup effectué
     * @return le coup fait
     */
    public int [] rejouerCoup(AbstractJoueur joueur, int index) {
        int [] position = new int[2];
        Case c = partie.getCoupJoueur(joueur, index);
        position[0] = c.ordonnee;
        position[1] = c.abscisse;
        return position;
    }

    /**
     * @return le nombre de coup effetuer par les joueurs
     */
    public int getNombreCoupsJouer() {
        return partie.joueurH.coupsJouer.size();
    }

    public void reinitialiserPartie() {
        partie.reinitialiserNavires();
        partie.reinitialiserCases();
    }

    /**
     * Appel la fonction de la classe Sauvegarder pour
     * effectuer la sauvegarde de la partie en court.
     */
    public void sauvegarderPartie() {
        Sauvegarder.partieAXml(partie);
    }

    /** Reçoit un fichié sélectionné par le joueur d'une partie à reprendre.
     *
     * @param fichier sélectionné par le joueur
     */
    public void chargerPartie(File fichier) {
        partie = RecuperationPartie.xmlAPartie(fichier);
    }

    /**
     * Demande à la partie de réinitialiser l'état de celle-ci.
     */
    public void reprendre() {
        partie.reprendrePartie();
    }

    /**
     * Demande au joueur artificiel de placé ses navires sur la grille.
     */
    public void placerNaviresOrdinateur() {
        partie.joueurA.placerAleatoire();
    }

    /** Traverse la grille reçu par l'interface et construit
     * les listes pour le positionnement de chaque navires.
     *
     * @param navies du joueur humain
     */
    public void placerNavireJoueur(int [][] navies) {
        ArrayList<Case> pa = new ArrayList<Case>();
        ArrayList<Case> cr = new ArrayList<Case>();
        ArrayList<Case> ct = new ArrayList<Case>();
        ArrayList<Case> sm = new ArrayList<Case>();
        ArrayList<Case> to = new ArrayList<Case>();

        for(int o = 0; o < 10; o++) {
            for(int a = 0; a < 10; a++) {
                if(navies[o][a] == 5)
                    pa.add(partie.joueurH.grille.cases[o][a]);
                else if(navies[o][a] == 4)
                    cr.add(partie.joueurH.grille.cases[o][a]);
                else if(navies[o][a] == 3)
                    ct.add(partie.joueurH.grille.cases[o][a]);
                else if(navies[o][a] == 2)
                    sm.add(partie.joueurH.grille.cases[o][a]);
                else if(navies[o][a] == 1)
                    to.add(partie.joueurH.grille.cases[o][a]);
            }
        }
        partie.joueurH.placerNavireJoueur(pa,cr,ct,sm,to);
    }

    /** Place aléatoirement les navires du joueur humain sur la grille.
     *
     * @param grille du joueur humain
     * @return la grille avec les navires positionné aléatoirement
     */
    public int [][] placerNavireAleatoire(int [][] grille) {

        partie.joueurH.placerAleatoire();
        remplirGrilleNavires(grille);
        return grille;
    }

    /** Place sur la grille graphique les navires du joueur humain
     * contenu dans la partie.
     *
     * @param grille du joueur humain
     * @return la grille avec les navires placés
     */
    public int [][] remplirGrilleNavires(int [][] grille) {

        for(int o = 0; o < 10; o++) {
            for(int a = 0; a < 10; a++) {
                if(partie.joueurH.grille.cases[o][a].navire instanceof PorteAvion)
                    grille[o][a] = 5;
                else if(partie.joueurH.grille.cases[o][a].navire instanceof Croiseur)
                    grille[o][a] = 4;
                else if(partie.joueurH.grille.cases[o][a].navire instanceof ContreTorpilleurs)
                    grille[o][a] = 3;
                else if(partie.joueurH.grille.cases[o][a].navire instanceof SousMarin)
                    grille[o][a] = 2;
                else if(partie.joueurH.grille.cases[o][a].navire instanceof Torpilleur)
                    grille[o][a] = 1;
                else
                    grille[o][a] = 0;
            }
        }
        return grille;
    }
}
