/** Titre : Battaille Navale
 *
 *                                      # #  ( )
 *                                   ___#_#___|__
 *                               _  |0__0__0__0__|  _
 *                        _=====| | |            | | |==== _
 *                  =====| |.---------------------------. | |====
 *    <--------------------'   .  .  .  .  .  .  .  .   '--------------/
 *      \                                                             /
 *       \___________________________________________________________/
 *   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package Graphics;

import Controleur.ControleurPartie;
import Joueur.AbstractJoueur;
import java.awt.EventQueue;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.io.File;

public class InterfaceGraphique {

    private int tailleNavire = 0;
    private int navireSelection = 0;
    private int indexVisionnement = 0;
    private int nbNavirePlacer;
    private int [] navirePlacer = new int[5];
    private int [][] gjI;
    private boolean rotation = true;

    private JFrame menu;
    private JButton [][] gj;
    private JButton [][] gp;
    private JButton [][] ga;
    private JButton [][] rga;
    private JButton [][] rgj;
    private Container pages;
    private CardLayout cardLayout;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    InterfaceGraphique window = new InterfaceGraphique();
                    window.menu.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    private InterfaceGraphique() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {

        //variable temporaire qui devrais venir de l'exterieur
        Container gJoueur = new Container();
        gj = creationGrille(gJoueur);
        gjI = new int[10][10];

        for(int i = 0 ; i< 5 ; i++) {
            navirePlacer[i] = 0;
        }

        for(int i = 0; i < 10; i++) {
            for(int y = 0; y < 10; y++) {
                gjI[i][y] = 0;
            }
        }

        // Initialise la fenetre
        menu = new JFrame();
        menu.setTitle("Bataille Naval");
        menu.setIconImage(Toolkit.getDefaultToolkit()
                .getImage("menuBackground.jpg"));
        menu.setBounds(100, 100, 1080, 920);
        menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        menu.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));

        // Initialise Pages
        pages = new Container();
        pages.setLayout(new CardLayout(0, 0));
        cardLayout = (CardLayout) pages.getLayout();

        /**
         * initialise Menu Principale
         */

        Container menuPrincipale = new Container();
        menuPrincipale.setLayout(new GridLayout(0, 1, 0, 0));
        pages.add(menuPrincipale, "menuPrincipale");
        JPanel background = new JPanel();
        menuPrincipale.add(background);

        // Initialise le cadre
        JPanel cadre = new JPanel();
        cadre.setBorder(new EmptyBorder(20, 20, 20, 20));
        cadre.setLayout(new GridLayout(4, 1, 10, 30));

        // initialise le logo
        JLabel logo = new JLabel("Battaille Navale");
        logo.setIcon(null);
        cadre.add(logo);

        final JCheckBox choixNiveau = new JCheckBox("Niveau de Difficulte");

        // Bouton Jouer
        JButton btnJouer = new JButton("Jouer");
        btnJouer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                int niveau = 0;
                if(choixNiveau.isSelected()) {
                    niveau = 1;
                }
                ControleurPartie.getInstance().configurer("Arnold", niveau);
                ControleurPartie.getInstance().placerNaviresOrdinateur();
                cardLayout.show(pages, "placerNavire");
            }
        });
        cadre.add(btnJouer);
        cadre.add(choixNiveau);

        // Bouton Charger Partie
        JButton btnCharger = new JButton("Charger Partie");
        btnCharger.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                clearGrid(gj);
                clearGrid(ga);
                clearGridInt(gjI);
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Bataille naval");
                int returnValue = fileChooser.showOpenDialog(null);
                if(returnValue == JFileChooser.APPROVE_OPTION) {
                    File fichierSelectionner = fileChooser.getSelectedFile();
                    if(fichierSelectionner != null) {
                        ControleurPartie.getInstance().chargerPartie(fichierSelectionner);
                        ControleurPartie.getInstance().reprendre();
                        cardLayout.show(pages, "partie");
                        gjI = ControleurPartie.getInstance().remplirGrilleNavires(gjI);
                        for(int i = 0; i < 10; i++) {
                            for(int y = 0; y < 10; y++) {
                                if(gjI[i][y] != 0)
                                    gj[i][y].setBackground(Color.GREEN);
                            }
                        }
                        for(int c = 0; c < ControleurPartie.getInstance().getNombreCoupsJouer(); c++) {
                            int [] position;
                            position = ControleurPartie.getInstance().rejouerCoup(ControleurPartie.getInstance().getJoueurHumain(),c);
                            repriseCoupHumain(position[0],position[1]);
                            position = ControleurPartie.getInstance().rejouerCoup(ControleurPartie.getInstance().getJoueurArtificiel(),c);
                            repriseCoupArtificiel(position[0],position[1]);
                        }
                    }
                }
            }
        });
        cadre.add(btnCharger);

        // Bouton Quitter
        JButton btnQuitter = new JButton("Quitter");
        btnQuitter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                menu.dispose();
            }
        });
        cadre.add(btnQuitter);

        /*
         * initialisation de la page pour placer les navire
         */
        Container placerNavire = new Container();
        placerNavire.setLayout(new BorderLayout());
        Container navires = new Container();
        placerNavire.add(navires);
        navires.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        JPanel navire = new JPanel();
        navires.add(navire);
        navire.setLayout(new GridLayout(0, 1, 0, 10));

        JLabel navire1 = new JLabel("IMAGE/Navire1");
        navire1.setIcon(null);
        navire.add(navire1);

        /*
         * Initialisation des bateau a placer
         */
        JButton porteAvion = new JButton("Porte Avion");
        porteAvion.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                tailleNavire = 5;
                navireSelection = 5;
            }
        });
        navire.add(porteAvion);

        JLabel navire2 = new JLabel("IMAGE/Navire2");
        navire.add(navire2);

        JButton croiseur = new JButton("Croiseur");
        croiseur.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tailleNavire = 4;
                navireSelection = 4;
            }
        });
        navire.add(croiseur);

        JLabel navire3 = new JLabel("IMAGE/Navire3");
        navire.add(navire3);

        JButton contreTorpilleurs = new JButton("Contre-Torpilleur");
        contreTorpilleurs.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tailleNavire = 3;
                navireSelection = 3;
            }
        });
        navire.add(contreTorpilleurs);

        JLabel navire4 = new JLabel("IMAGE/Navire4");
        navire.add(navire4);

        JButton sousMarin = new JButton("Sous-Marin");
        sousMarin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tailleNavire = 3;
                navireSelection = 2;
            }
        });
        navire.add(sousMarin);

        JLabel navire5 = new JLabel("IMAGE/Navire5");
        navire.add(navire5);

        JButton torpilleur = new JButton("Torpilleur");
        torpilleur.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tailleNavire = 2;
                navireSelection = 1;
            }
        });
        navire.add(torpilleur);

        Container grille = new Container();
        navires.add(grille);
        grille.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        Container gPlacement = new Container();
        grille.add(gPlacement);
        gPlacement.setLayout(new GridLayout(10, 9, 0, 0));

        // Creation Grille Placement
        gp = creationGrille(gPlacement);

        /**
         * La taille du tableau est HARDCODER enlever les commentaire quand Case va etre
         * importer
         *
         */
        for(int i = 0; i < 10; i++) {
            for(int y = 0; y < 10; y++) {
                final int z = i;
                final int s = y;
                gp[z][s].addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent arg0) {
                        ajouterNavire(z,s);
                    }
                });
            }
        }

        for(int i = 0; i < 10; i++) {
            for(int y = 0; y < 10; y++) {
                final int z = i;
                final int s = y;
                gp[z][s].addMouseListener(new MouseAdapter() {
                    public void mouseEntered(MouseEvent evt) {
                        modifierCouleurEntree(z,s);
                    }
                    public void mouseExited(MouseEvent evt) {
                        modifierCouleurSortie(z,s);
                    }
                });
            }
        }

        pages.add(placerNavire, "placerNavire");

        Container options = new Container();
        placerNavire.add(options, BorderLayout.NORTH);
        options.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 20));

        JButton btnRotation = new JButton("Rotation");
        btnRotation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                setRotation(rotation);
            }
        });
        options.add(btnRotation);

        JButton reinitialiser = new JButton("Reinitialiser");
        reinitialiser.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clearGrid(gp);
                clearGridInt(gjI);
                navireSelection = 0;
                tailleNavire = 0;
                nbNavirePlacer = 0;
                for(int i =0; i<5;i++){
                    navirePlacer[i] =0 ;
                }
            }
        });
        options.add(reinitialiser);

        final Container confirmer = new Container();
        placerNavire.add(confirmer, BorderLayout.SOUTH);
        confirmer.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 20));

        final JButton btnConfirmer = new JButton("Confirmer");
        btnConfirmer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if(nbNavirePlacer >= 5) {
                    clearGrid(gj);
                    ControleurPartie.getInstance().placerNavireJoueur(gjI);
                    cardLayout.show(pages, "partie");
                    /**AJOUTER CA APRES UPDATE*/
                    for(int i = 0; i < 10; i++) {
                        for(int y = 0; y < 10; y++) {
                            if(gjI[i][y] != 0)
                                gj[i][y].setBackground(Color.GREEN);
                        }
                    }
                }
            }
        });
        confirmer.add(btnConfirmer);

        final JButton btnAleatoire = new JButton("Placer Aléatoire");
        btnAleatoire.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clearGrid(gj);
                gjI = ControleurPartie.getInstance().placerNavireAleatoire(gjI);
                cardLayout.show(pages, "partie");
                for(int i = 0; i < 10; i++) {
                    for(int y = 0; y < 10; y++) {
                        if(gjI[i][y] != 0)
                            gj[i][y].setBackground(Color.GREEN);
                    }
                }
            }
        });
        confirmer.add(btnAleatoire);

        JButton btnRetour_1 = new JButton("Retour");
        btnRetour_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                cardLayout.show(pages, "menuPrincipale");
                clearGridInt(gjI);
                clearGrid(gp);
                tailleNavire = 0;
                nbNavirePlacer = 0;
                for(int i = 0 ; i < 5 ; i++) {
                    navirePlacer[i] = 0;
                }
                navireSelection = 0;
            }
        });
        confirmer.add(btnRetour_1);

        Container partie = new Container();
        pages.add(partie, "partie");
        partie.setLayout(new GridLayout(0, 1, 0, 0));

        Container grilleAdv = new Container();
        grilleAdv.setBackground(Color.LIGHT_GRAY);
        partie.add(grilleAdv);
        grilleAdv.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        Container gAdversaire = new Container();
        grilleAdv.add(gAdversaire);
        gAdversaire.setLayout(new GridLayout(10, 9, 0, 0));

        // Creation Grille Adversaire
        ga = creationGrille(gAdversaire);

        for(int i = 0; i < 10; i++) {
            for(int y = 0; y < 10; y++) {
                final int z = i;
                final int s = y;
                ga[z][s].addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent arg0) {
                        effectuerCoup(z,s);
                    }
                });
            }
        }

        Container grilleJoueur = new Container();
        partie.add(grilleJoueur);
        grilleJoueur.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        grilleJoueur.add(gJoueur);
        gJoueur.setLayout(new GridLayout(10, 9, 0, 0));

        // Creation Grille Joueur
        Container optionPartie = new Container();
        partie.add(optionPartie);
        optionPartie.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        JButton btnQuitterPartie = new JButton("Quitter Partie");
        btnQuitterPartie.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                cardLayout.show(pages, "menuPrincipale");
                clearGridInt(gjI);
                clearGrid(gp);
                clearGrid(ga);
                tailleNavire = 0;
                nbNavirePlacer = 0;
                for(int i = 0 ; i< 5 ; i++) {
                    navirePlacer[i] = 0;
                }
                navireSelection = 0;
            }
        });

        JButton btnSauvegarder = new JButton("Sauvegarder Partie");
        btnSauvegarder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ControleurPartie.getInstance().sauvegarderPartie();
            }
        });

        optionPartie.add(btnQuitterPartie);
        optionPartie.add(btnSauvegarder);
        background.add(cadre);
        menu.getContentPane().add(pages);

        Container revoirPartie = new Container();
        pages.add(revoirPartie, "revoirPartie");
        revoirPartie.setLayout(new GridLayout(3, 0, 0, 0));

        /**
         * ajouter les deux grille dans rejouer partie
         */

        Container rejouerGA = new Container();
        Container rejouerGJ = new Container();

        Container rejouerGAdversaire = new Container();
        Container rejouerGJoueur = new Container();
        Container rejouerOption = new Container();

        rejouerGAdversaire.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        rejouerGJoueur.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        rejouerGA.setLayout(new GridLayout(10, 9, 0, 0));
        rejouerGJ.setLayout(new GridLayout(10, 9, 0, 0));

        rga = creationGrille(rejouerGA);
        rgj = creationGrille(rejouerGJ);

        rejouerGAdversaire.add(rejouerGJ);
        rejouerGJoueur.add(rejouerGA);
        revoirPartie.add(rejouerGAdversaire);
        revoirPartie.add(rejouerGJoueur);


        revoirPartie.add(rejouerOption);

        JButton retourMenuPrincipale = new JButton("Retour au menu Principale");
        retourMenuPrincipale.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                cardLayout.show(pages, "menuPrincipale");
                clearGridInt(gjI);
                clearGrid(gp);
                clearGrid(ga);
                clearGrid(rga);
                clearGrid(rgj);
                tailleNavire = 0;
                for(int i = 0 ; i< 5 ; i++) {
                    navirePlacer[i] = 0;
                }
                navireSelection = 0;
            }
        });
        retourMenuPrincipale.setBounds(162, 70, 157, 23);
        rejouerOption.add(retourMenuPrincipale);

        final JButton coupSuivant = new JButton("Coup Suivant");
        coupSuivant.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                visionerCoupSuivant();
                if(indexVisionnement >= ControleurPartie.getInstance().getNombreCoupsJouer()) {
                    coupSuivant.setEnabled(false);
                }
            }
        });

        coupSuivant.setBounds(440, 70, 157, 23);
        rejouerOption.add(coupSuivant);

    }

    /** Effectue les coups du joueur humain et du joueur artificier et mes
     * à jour l'état des cases qui ont reçu un coup.
     *
     * @param z ordonnée
     * @param s abscisse
     */
    private void effectuerCoup(int z, int s) {
        boolean valide;
        boolean touche;
        boolean couler;
        boolean perdu;
        int [] position;

        valide = ControleurPartie.getInstance().placerUnCoup(z,s);
        if(valide) {
            touche = ControleurPartie.getInstance().aTouchee(ControleurPartie.getInstance().getJoueurArtificiel(),z,s);
            if(touche) {
                couler = ControleurPartie.getInstance().aCoulee(ControleurPartie.getInstance().getJoueurArtificiel(),z,s);
                if(couler) {
                    graphCouler(ga,ControleurPartie.getInstance().getJoueurArtificiel(),z,s);
                } else {
                    graphToucher(ga[z][s]);
                }
            } else {
                graphALeau(ga[z][s]);
            }
            perdu = ControleurPartie.getInstance().verifierPerdu(ControleurPartie.getInstance().getJoueurArtificiel());
            if(perdu) {
                cardLayout.show(pages, "revoirPartie");
                afficherNavire();
                ControleurPartie.getInstance().reinitialiserPartie();
            }
            position = ControleurPartie.getInstance().jouerCoupArtificiel();
            touche = ControleurPartie.getInstance().aTouchee(ControleurPartie.getInstance().getJoueurHumain(),position[0],position[1]);
            ControleurPartie.getInstance().aIDernierTouchee(touche);
            if(touche) {
                couler = ControleurPartie.getInstance().aCoulee(ControleurPartie.getInstance().getJoueurHumain(),position[0],position[1]);
                if(couler) {
                    graphCouler(gj,ControleurPartie.getInstance().getJoueurHumain(),position[0],position[1]);
                } else {
                    graphToucher(gj[position[0]][position[1]]);
                }
            } else {
                graphALeau(gj[position[0]][position[1]]);
            }
            perdu = ControleurPartie.getInstance().verifierPerdu(ControleurPartie.getInstance().getJoueurHumain());
            if(perdu) {
                cardLayout.show(pages, "revoirPartie");
                afficherNavire();
                ControleurPartie.getInstance().reinitialiserPartie();
            }
        }
    }

    /**
     * Permet le visionnement des coups joueur tour par tour
     */
    private void visionerCoupSuivant() {
        boolean touche;
        boolean couler;
        int [] position;
        // Récupération du coup faite par le joueur
        position = ControleurPartie.getInstance().rejouerCoup(ControleurPartie.getInstance().getJoueurHumain(), indexVisionnement);
        touche = ControleurPartie.getInstance().aTouchee(ControleurPartie.getInstance().getJoueurArtificiel(),position[0],position[1]);
        if(touche) {
            couler = ControleurPartie.getInstance().aCoulee(ControleurPartie.getInstance().getJoueurArtificiel(),position[0],position[1]);
            if(couler) {
                graphCouler(rgj,ControleurPartie.getInstance().getJoueurArtificiel(),position[0],position[1]);
            } else {
                graphToucher(rgj[position[0]][position[1]]);
            }
        } else {
            graphALeau(rgj[position[0]][position[1]]);
        }
        // Récupération du coup fait par l'ordinateur
        position = ControleurPartie.getInstance().rejouerCoup(ControleurPartie.getInstance().getJoueurArtificiel(), indexVisionnement);
        touche = ControleurPartie.getInstance().aTouchee(ControleurPartie.getInstance().getJoueurHumain(),position[0],position[1]);
        if(touche) {
            couler = ControleurPartie.getInstance().aCoulee(ControleurPartie.getInstance().getJoueurHumain(),position[0],position[1]);
            if(couler) {
                graphCouler(rga,ControleurPartie.getInstance().getJoueurHumain(),position[0],position[1]);
            } else {
                graphToucher(rga[position[0]][position[1]]);
            }
        } else {
            graphALeau(rga[position[0]][position[1]]);
        }
        indexVisionnement++;
    }

    /** Ajoute un navire à la position désiré par le joueur sur sa grille de jeux.
     *
     * @param z ordonnée
     * @param s abscisse
     */
    private void ajouterNavire(int z, int s) {
        if(tailleNavire > 0) {
            if(rotation) {
                if(s + tailleNavire < 10) {
                    if(navirePlacer[navireSelection-1] != 1) {
                        if(emplacementValideHorizontal(tailleNavire,z,s,true)) {
                            for(int t = 0; t < tailleNavire; t++) {
                                gp[z][s + t].setBackground(Color.GREEN);
                                gjI[z][s + t] = navireSelection;
                                navirePlacer[navireSelection-1] = 1;
                            }
                        }
                    }
                } else {
                    if(navirePlacer[navireSelection-1] != 1) {
                        if(emplacementValideHorizontal(tailleNavire,z,s,false)) {
                            for(int t = 9; t >= 10 - tailleNavire; t--) {
                                gp[z][t].setBackground(Color.GREEN);
                                gjI[z][t] = navireSelection;
                                navirePlacer[navireSelection-1] = 1;
                            }
                        }
                    }
                }
            } else {
                if(z + tailleNavire < 10) {
                    if(navirePlacer[navireSelection-1] != 1) {
                        if(emplacementValideVertical(tailleNavire,z,s,true)) {
                            for(int t = 0; t < tailleNavire; t++) {
                                gp[z + t][s].setBackground(Color.GREEN);
                                gjI[z + t][s] = navireSelection;
                                navirePlacer[navireSelection-1] = 1;
                            }
                        }
                    }
                } else {
                    if(navirePlacer[navireSelection-1] != 1) {
                        if(emplacementValideVertical(tailleNavire,z,s,false)) {
                            for(int t = 9; t >= 10 - tailleNavire; t--) {
                                gp[t][s].setBackground(Color.GREEN);
                                gjI[t][s] = navireSelection;
                                navirePlacer[navireSelection-1] = 1;
                            }
                        }
                    }
                }
            }
            nbNavirePlacer++;
        }
    }

    /** Vérifi que le navire peur être placé sur les cases désirées.
     *
     * @param taille du navire
     * @param z ordonnée
     * @param s abscisse
     * @param direction du navire
     * @return vrai si possible
     */
    private boolean emplacementValideHorizontal(int taille, int z, int s, boolean direction) {
        boolean valide = true;
        if(direction) {
            for(int i = 0; i < taille; i++) {
                if(gjI[z][s + i] != 0) { return false; }
            }
        } else {
            for(int i = 9; i >= 10 - taille; i--) {
                if(gjI[z][i] != 0) { return false; }
            }
        }
        return valide;
    }

    /** Vérifi que le navire peur être placé sur les cases désirées.
     *
     * @param taille du navire
     * @param z ordonnée
     * @param s abscisse
     * @param direction du navire
     * @return vrai si possible
     */
    private boolean emplacementValideVertical(int taille, int z, int s, boolean direction) {
        boolean valide = true;
        if(direction) {
            for(int i = 0; i < taille; i++) {
                if(gjI[z + i][s] != 0) { return false; }
            }
        } else {
            for(int i = 9; i >= 10 - taille; i--) {
                if(gjI[i][s] != 0) { return false; }
            }
        }
        return valide;
    }

    /** Modifie la couleur des cases sur lequel le navire pourrait être placé.
     *
     * @param z ordonnée
     * @param s abscisse
     */
    private void modifierCouleurEntree(int z, int s) {
        if(rotation) {
            if(s + tailleNavire <= 10) {
                for(int t = 0; t < tailleNavire; t++) { gp[z][s + t].setBackground(Color.GREEN); }
            } else {
                for(int t = 9; t >= 10 - tailleNavire; t--) { gp[z][t].setBackground(Color.GREEN); }
            }
        } else {
            if(z + tailleNavire <= 10) {
                for(int t = 0; t < tailleNavire; t++) { gp[z + t][s].setBackground(Color.GREEN); }
            } else {
                for(int t = 9; t >= 10 - tailleNavire; t--) { gp[t][s].setBackground(Color.GREEN); }
            }
        }
    }

    /** Remet les cases à leur couleur initial lorsqu'un navire n'est pas au dessus.
     *
     * @param z ordonnée
     * @param s abscisse
     */
    private void modifierCouleurSortie(int z, int s) {
        if(rotation) {
            if(s + tailleNavire <= 10) {
                for(int t = 0; t < tailleNavire; t++) {
                    if(gjI[z][s + t] == 0) { gp[z][s + t].setBackground(Color.LIGHT_GRAY); }
                }
            } else {
                for(int t = 9; t >= 10 - tailleNavire; t--) {
                    if(gjI[z][t] == 0) { gp[z][t].setBackground(Color.LIGHT_GRAY); }
                }
            }
        } else {
            if(z + tailleNavire <= 10) {
                for(int t = 0; t < tailleNavire; t++) {
                    if(gjI[z + t][s] == 0) { gp[z + t][s].setBackground(Color.LIGHT_GRAY); }
                }
            } else {
                for(int t = 9; t >= 10 - tailleNavire; t--) {
                    if(gjI[t][s] == 0) { gp[t][s].setBackground(Color.LIGHT_GRAY); }
                }
            }
        }
    }

    /**
     * Affiche les navires des joueur pour le revisionnement de la partie.
     */
    private void afficherNavire() {
        for(int i = 0; i < 10; i++) {
            for(int y = 0; y < 10; y++) {
                if(gjI[i][y] != 0)
                    rga[i][y].setBackground(Color.GREEN);
            }
        }
        for(int i = 0; i < 10; i++) {
            for(int y = 0; y < 10; y++) {
                if(ControleurPartie.getInstance().getGrilleJoueurArtificiel().cases[i][y].navire != null)
                    rgj[i][y].setBackground(Color.GREEN);
            }
        }
    }

    /** Effectue le coup fait par le joueur humain.
     *
     * @param z ordonnée
     * @param s abscisse
     */
    private void repriseCoupHumain(int z, int s) {
        boolean touche;
        boolean couler;
        // Récupération du coup faite par le joueur
        touche = ControleurPartie.getInstance().aTouchee(ControleurPartie.getInstance().getJoueurArtificiel(),z,s);
        if(touche) {
            couler = ControleurPartie.getInstance().aCoulee(ControleurPartie.getInstance().getJoueurArtificiel(),z,s);
            if(couler) {
                graphCouler(ga,ControleurPartie.getInstance().getJoueurArtificiel(),z,s);
            } else {
                graphToucher(ga[z][s]);
            }
        } else {
            graphALeau(ga[z][s]);
        }
    }

    /** Effectue le coup fait par le joueur artificiel.
     *
     * @param z ordonnée
     * @param s abscisse
     */
    private void repriseCoupArtificiel(int z, int s) {
        boolean touche;
        boolean couler;
        touche = ControleurPartie.getInstance().aTouchee(ControleurPartie.getInstance().getJoueurHumain(),z,s);
        if(touche) {
            couler = ControleurPartie.getInstance().aCoulee(ControleurPartie.getInstance().getJoueurHumain(),z,s);
            if(couler) {
                graphCouler(gj,ControleurPartie.getInstance().getJoueurHumain(),z,s);
            } else {
                graphToucher(gj[z][s]);
            }
        } else {
            graphALeau(gj[z][s]);
        }
    }

    JButton[][] creationGrille(Container container) {

        JButton[][] b = new JButton[10][10];

        for(int i = 0; i < 10; i++) {
            for(int y = 0; y < 10; y++) {
                b[i][y] = new JButton(" ");
                b[i][y].setBackground(Color.LIGHT_GRAY);
                container.add(b[i][y]);
            }
        }
        return b;

    }

    /** Change la couleur des cases d'un navire coulé.
     *
     * @param grille du joueur
     * @param joueur à vérifier
     * @param z ordonnée
     * @param s abscisse
     */
    private void graphCouler(JButton [][] grille, AbstractJoueur joueur, int z, int s) {
        int [][] couler = ControleurPartie.getInstance().naviresCoulees(joueur, z,s);
        for(int i = 0; i < couler.length; i++) {
            grille[couler[i][0]][couler[i][1]].setBackground(Color.RED);
        }
    }

    /** Change la couleur de la case.
     *
     * @param laCase touchée
     */
    private void graphToucher(JButton laCase) {
        laCase.setBackground(Color.YELLOW);
    }

    /** Change la couleur de la case.
     *
     * @param laCase touchée
     */
    private void graphALeau(JButton laCase) {
        laCase.setBackground(Color.BLUE);
    }

    /**
     * @param rotation du navire
     */
    private void setRotation(boolean rotation) {
        this.rotation = !rotation;
    }

    /**
     * @param grille à réinitialiser
     */
    private void clearGrid(JButton[][] grille) {
        for(int i = 0; i < 10; i++) {
            for(int y = 0; y < 10; y++) {
                grille[i][y].setBackground(Color.LIGHT_GRAY);
            }
        }
    }

    /**
     * @param grille d'entier à réinitialiser
     */
    private void clearGridInt(int[][] grille) {
        for(int i = 0; i < 10; i++) {
            for(int y = 0; y < 10; y++) {
                grille[i][y] = 0;
            }
        }
    }
}