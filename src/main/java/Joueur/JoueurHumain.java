/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package Joueur;

public class JoueurHumain extends AbstractJoueur {

    public JoueurHumain(String nomJoueur) {
        nom = nomJoueur;
    }
}
