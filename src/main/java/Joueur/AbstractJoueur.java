/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package Joueur;

import GrilleDeJeu.*;
import Navire.*;
import java.util.*;

public abstract class AbstractJoueur {
	private static final int NB_BATEAU = 5;

	public String nom;
	public GrilleDeJeu grille = new GrilleDeJeu();
	public ArrayList<AbstractNavire> navires = new ArrayList<AbstractNavire>(NB_BATEAU);
	public ArrayList<Case> coupsJouer = new ArrayList<Case>();

	public AbstractJoueur() {
	}

	/** Vérifi si le coup a déjà été lancé et l'ajoute
	 * à la liste de coups effectués.
	 *
	 * @param o ordonnée du coup
	 * @param a abscisse du coup
	 * @return la case ou le coup ou null si invalide
	 */
	private Case lancerTorpille(int o, int a) {
        Case coup = grille.cases[o][a];
		if(coupsJouer.contains(coup)) {
            return null;
		}
        coupsJouer.add(coup);
		return coup;
    }

	/** Appel lancerTorpille avec les coordonnées reçu.
	 *
	 * @param o ordonnée du coup
	 * @param a abscisse du coup
	 * @return vrai si le coup est valide
	 */
	public boolean coupSuivant(int o, int a) {
		Case c = lancerTorpille(o,a);
		return c != null;
    }

	/** Vérifi si le joueur a perdu la partie en
	 * regardant si tous les navires sont coulé.
	 *
	 * @return si le joueur a perdu
	 */
	public boolean aPerdu() {
		boolean perdu = true;
		for(int i = 0; i < navires.size() && perdu; i++) {
				perdu = perdu && navires.get(i).getCoulee();
		}
		return perdu;
	}

	/**
	 * Appel la factory de navires avec un placement aléatoire.
	 */
	public void placerAleatoire() {
		FactoryNavires factory = new FactoryNavires();
		navires = factory.construireNaviresAleatoire(grille);
	}

	/** Appel la factory de navires avec un emplacement définit par le joueur.
	 *
	 * @param porteAvion positionnement du Porte-Avion
	 * @param croiseur positionnement du Croiseur
	 * @param contreTropilleurs positionnement du Contre-Torpilleurs
	 * @param sousMarin positionnement du Sous-Marin
	 * @param torpilleur positionnement du Torpilleur
	 */
	public void placerNavireJoueur(ArrayList<Case> porteAvion,
								   ArrayList<Case> croiseur,
								   ArrayList<Case> contreTropilleurs,
								   ArrayList<Case> sousMarin,
								   ArrayList<Case> torpilleur) {

		FactoryNavires factory = new FactoryNavires();
		navires = factory.construireNavires(porteAvion,
											croiseur,
											contreTropilleurs,
											sousMarin,
											torpilleur);
	}
}
