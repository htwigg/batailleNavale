/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package Joueur;

import GrilleDeJeu.Case;
import java.util.*;

public class JoueurArtificiel extends AbstractJoueur {

	private int niveau;
	private ArrayList<Case> coupPossible = new ArrayList<Case>();
	private Case dernierCoup;
	public boolean dernierToucher;
	private Case avDernierCoup;
	private boolean avDernierToucher;

	public int getNiveau(){return niveau;}

	/** Instanciation du joueur artificiel avec une grille de coup
	 * possible ainsi que sont niveau de difficulté
	 *
	 * @param n Niveau de l'ordinateur
	 */
	public JoueurArtificiel(int n){
		for(int o = 0; o < 10; o++) {
			for(int a = 0; a < 10; a++) {
				Case c = grille.cases[o][a];
				coupPossible.add(c);
			}
		}
		nom = "Joueur Artificiel";
		niveau = n;
		dernierToucher = false;
		avDernierToucher = false;
	}

	/** Dépendament du niveau de difficultée l'ordinateur adapte
	 * une stratégie différente pour lancer son coup.
	 *
	 * @return position du coup fait par l'ordinateur
	 */
	public int [] executerCoup() {
		int [] position = {-1, -1};
		Case coup;
		if(niveau == 0)
			coup = coupAleatoire();
		else
			coup = coupCalculer();
		// mise à jour du dernier coup fait
		dernierCoup = coup;
		retirerCoup(coup);
		position[0] = coup.ordonnee;
		position[1] = coup.abscisse;
		return position;
	}

	/** L'ordinateur sélectionne une coup aléatoire dans
	 * la liste de coup qu'il n'a pas encore accompli.
	 *
	 * @return coup fait aléatoirement
	 */
	private Case coupAleatoire() {
		Random r = new Random();
		int position = r.nextInt(coupPossible.size());
		Case coup = coupPossible.get(position);
		// Mise a jour du dernier coup
		// La partie mais a jour si le coup a toucher
		avDernierCoup = null;
		avDernierToucher = false;
		return coup;
	}

	/** Méthode employé par l'ordinateur pour lancé son coup
	 * en fonction des résultats obtenu des coups précédent.
	 *
	 * @return coup fait en fonction du succès des coups précédent
	 */
	private Case coupCalculer() {
		Case coup;
		if(dernierToucher) {
			if(avDernierToucher) {
				coup = toucherCalculer();
			}
			else {
				coup = toucherAleatoire();
			}
		}
		else {
			coup = coupAleatoire();
		}
		return coup;
	}

	/** L'ordinateur sélectionne aléatoirement une entre les quatres
	 * entourant le coup qu'il a précédament fait.
	 *
	 * @return coup fait sur une des position entourant le coup précédent
	 */
	private Case toucherAleatoire() {
		Case coup;
		// recuperation des ordonnee et abscisse du dernier coup
		// pour recupere les positions des prochains possible
		int o = dernierCoup.ordonnee, a = dernierCoup.abscisse;
		ArrayList<Case> possible = new ArrayList<Case>();

		if(a > 0)
			if(coupPossible.contains(grille.cases[o][a - 1]))
				possible.add(grille.cases[o][a - 1]);
		if(o > 0)
			if(coupPossible.contains(grille.cases[o - 1][a]))
				possible.add(grille.cases[o - 1][a]);
		if(a < 9)
			if(coupPossible.contains(grille.cases[o][a + 1]))
				possible.add(grille.cases[o][a + 1]);
		if(o < 9)
			if(coupPossible.contains(grille.cases[o + 1][a]))
				possible.add(grille.cases[o + 1][a]);

		coup = aleatoirePossible(possible);
		return coup;
	}

	/** L'ordinateur lance un coup sur un position aléatoire linéaire
	 * à partir des deux dernier coup effectués.
	 *
	 * @return coup fait un fonction des deux dernier coups joués
	 */
	private Case toucherCalculer() {
		Case coup = null;
		int avO = avDernierCoup.ordonnee, avA = avDernierCoup.abscisse;
		int drO = dernierCoup.ordonnee, drA = dernierCoup.abscisse;
		ArrayList<Case> possible = new ArrayList<Case>();

		if(avO == drO) {
			if(drA > 0 && drA < 9) {
				if(coupPossible.contains(grille.cases[drO][drA - 1]))
					possible.add(grille.cases[drO][drA - 1]);
				if(coupPossible.contains(grille.cases[drO][drA + 1]))
					possible.add(grille.cases[drO][drA + 1]);

				coup = aleatoirePossible(possible);
			}
			else if(drA == 0) {
				if(coupPossible.contains(grille.cases[avO][avA + 1]))
					coup = grille.cases[avO][avA + 1];
				else
					coup = coupAleatoire();
			}
			else {
				if(coupPossible.contains(grille.cases[avO][avA - 1]))
					coup = grille.cases[avO][avA - 1];
				else
					coup = coupAleatoire();
			}
		}
		else if(avA == drA) {
			if(drO > 0 && drO < 9){
				if(coupPossible.contains(grille.cases[drO - 1][drA]))
					possible.add(grille.cases[drO - 1][drA]);
				if(coupPossible.contains(grille.cases[drO + 1][drA]))
					possible.add(grille.cases[drO + 1][drA]);

				coup = aleatoirePossible(possible);
			}
			else if(drO == 0) {
				if(coupPossible.contains(grille.cases[avO + 1][avA]))
					coup = grille.cases[avO + 1][avA];
				else
					coup = coupAleatoire();
			}
			else {
				if(coupPossible.contains(grille.cases[avO - 1][avA]))
					coup = grille.cases[avO - 1][avA];
				else
					coup = coupAleatoire();
			}
		}
		return coup;
	}

	/** Retir le coup fait de la liste de coup possible
	 * et ajout ce dernier à la liste de coups effectués.
	 *
	 * @param coup effectuer
	 */
	private void retirerCoup(Case coup) {
		coupPossible.remove(coup);
		coupsJouer.add(coup);
	}

	/** Choisit un coup au hasard parmis une liste de coup
	 * reçu en paramètre.
	 *
	 * @param possible liste de coup possible
	 * @return coup choisit parmis la liste reçu
	 */
	private Case aleatoirePossible(ArrayList<Case> possible) {
		Case coup;

		if(possible.isEmpty())
			coup = coupAleatoire();
		else {
			Random r = new Random();
			int position = r.nextInt(possible.size());
			coup = possible.get(position);
			// Mise a jour du dernier coup
			// La partie mais a jour si le coup a toucher
			avDernierCoup = dernierCoup;
			avDernierToucher = dernierToucher;

		}
		return coup;
	}
}
