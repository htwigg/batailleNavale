/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package Partie;

import GrilleDeJeu.Case;
import Joueur.AbstractJoueur;
import Joueur.JoueurArtificiel;
import Joueur.JoueurHumain;
import Navire.AbstractNavire;

import java.util.ArrayList;

public class Partie {

    public JoueurHumain joueurH;
    public JoueurArtificiel joueurA;

    public Partie() { }

    /** Instancie les deux joueur qui exécuteront la partie.
     *
     * @param nom du joueur humain
     * @param niveau du joueur artificiel
     */
    public void configurer(String nom, int niveau) {
        String n = nom == null ? "Unknown" : nom;
        joueurA = new JoueurArtificiel(niveau);
        joueurH = new JoueurHumain(n);
    }

    /** Vérifi si le joueur à perdu.
     *
     * @param joueur sur lequel vérifi
     * @return si le joueur a perdu
     */
    public boolean finPartie(AbstractJoueur joueur) {
        return joueur.aPerdu();
    }

    /**
     * Récupère les positions des navires et mes à jour les cases
     * de la grille du joueur humain et du joueur artificiel.
     */
    public void reprendrePartie() {
        for(int i = 0; i < 5; i++) {
            ArrayList<Case> emplacementH = new ArrayList<Case>();
            AbstractNavire navireJ = joueurH.navires.get(i);
            for(int y = 0; y < navireJ.getEmplacement().size(); y++) {
                Case c = navireJ.getEmplacement().get(y);
                emplacementH.add(joueurH.grille.cases[c.ordonnee][c.abscisse]);
            }
            navireJ.placerNavire(emplacementH);

            AbstractNavire navireA = joueurA.navires.get(i);
            ArrayList<Case> emplacementA = new ArrayList<Case>();
            for(int y = 0; y < navireA.getEmplacement().size(); y++) {
                Case c = navireA.getEmplacement().get(y);
                emplacementA.add(joueurA.grille.cases[c.ordonnee][c.abscisse]);
            }
            navireA.placerNavire(emplacementA);
        }
    }

    /** Mes à joueur la grille de l'opposant du joueur qui
     * a effectuer le coup et reçoit un boolean s'il touche
     * un navire.
     *
     * @param opposant joueur à mettre à jour
     * @param o ordonnée
     * @param a abscisse
     * @return si le joueur à été touché
     */
    public boolean miseAJourTorpille(AbstractJoueur opposant, int o, int a) {
        if(opposant.grille.cases[o][a] == null) {
            System.out.println("La case est invalide");
            return false;
        }
        return opposant.grille.cases[o][a].caseOccuper();
    }

    /** Reçoit les coordonnées d'une case et vérifi si le navire
     * sur celle-ci à été coulée.
     *
     * @param opposant joueur à mettre à jour
     * @param o ordonnée
     * @param a abscisse
     * @return si le navire est coulée
     */
    public boolean aCouleeNavire(AbstractJoueur opposant, int o, int a) {
        return opposant.grille.cases[o][a].navire.getCoulee();
    }

    /**
     * @param joueur à traiter
     * @param o ordonnée
     * @param a abscisse
     * @return liste des navire coulée
     */
    public ArrayList<Case> listeCasesCoulees(AbstractJoueur joueur, int o, int a) {
        ArrayList<Case> liste = new ArrayList<Case>();
        AbstractNavire n = joueur.grille.cases[o][a].navire;
        for(int i = 0; i < 10; i++) {
            for(int y = 0; y < 10; y++) {
                if(joueur.grille.cases[i][y].navire == n) { liste.add(joueur.grille.cases[i][y]); }
            }
        }
        return liste;
    }

    /** Récupéré le coup joueur par le joueur dans le temps.
     *
     * @param joueur a traiter
     * @param index du coup
     * @return coup joueur
     */
    public Case getCoupJoueur(AbstractJoueur joueur, int index) {
        return joueur.coupsJouer.get(index);
    }

    /**
     * Remettre à leur état original les navires des deux joueurs.
     */
    public void reinitialiserNavires() {
        for(int i = 0; i < 5; i++) {
            joueurH.navires.get(i).reinitialiser();
            joueurA.navires.get(i).reinitialiser();
        }
    }

    /**
     * Remettre à leur état original les cases des deux joueurs.
     */
    public void reinitialiserCases() {
        for(int o = 0; o < 10; o++) {
            for(int a = 0; a < 10; a++) {
                joueurH.grille.cases[o][a].reinitialiser();
            }
        }
    }
}
