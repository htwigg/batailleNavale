/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package Navire;

import GrilleDeJeu.Case;
import java.util.ArrayList;

public class Croiseur extends AbstractNavire {
    public Croiseur() {
        this.emplacement = null;
        this.nom = "Croiseur";
        this.longueur = 4;
        this.vie = longueur;
        this.coulee = false;
    }

    @Override
    public void placerNavire(ArrayList<Case> positionnement){
        assert (positionnement.size() == 4);
        this.emplacement = positionnement;
        mettreAJourCases(this, positionnement);
    }
}
