/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package Navire;

import GrilleDeJeu.Case;
import java.util.ArrayList;

public abstract class AbstractNavire {

    // Attributs d'un Navire
    protected String nom;
    protected int longueur;
    protected int vie;
    protected boolean coulee;
    protected ArrayList<Case> emplacement;

    /**
     * @param positionnement cases sur lesquelles le navire sera placées
     */
    public abstract void placerNavire(ArrayList<Case> positionnement);

    public AbstractNavire() {
    }

    // Getters

    /**
     * @return le nom du navire
     */
    public String getNom() {
        return nom;
    }

    /**
     * @return la longueur du navire
     */
    public int getLongueur() {
        return longueur;
    }

    /**
     * @return la vie du navire
     */
    public int getVie() { return vie; }

    /**
     * @return l'état coulee du navire
     */
    public boolean getCoulee() {
        return coulee;
    }

    /**
     * @return l'emplacement du navire sur la grille
     */
    public ArrayList<Case> getEmplacement() {
        return emplacement;
    }

    // Setters

    /**
     * @param longueur à attribuer au navire
     */
    public void setLongueur(int longueur) { this.longueur = longueur; }

    /**
     * @param emplacement à attribuer au navire
     */
    public void setEmplacement(ArrayList<Case> emplacement) {
        this.emplacement = emplacement;
    }

    /**
     * Couler un navire pour lequel la vie dessant à zéro
     */
    private void coulerNavire() {
        this.coulee = true;
    }

    /**
     * Réduit la vie du navire s'il est touché
     */
    public void reduireVie() {
        this.vie--;
        if(this.vie <= 0) {
            this.coulerNavire();
        }
    }

    /** Mise à jour de la grille avec le navire qui les occupes.
     *
     * @param navire qui va servir à mettre à jour les cases
     * @param c liste de cases
     */
    protected void mettreAJourCases(AbstractNavire navire, ArrayList<Case> c) {
        for(int i = 0; i < c.size(); i++) {
            Case tmp = c.get(i);
            tmp.navire = navire;
            tmp.occupe = true;
        }
    }

    /**
     * Remise de la vie de navire à sont état original
     * ainsi que coulee à faux
     */
    public void reinitialiser() {
        this.vie = longueur;
        this.coulee = false;
    }
}