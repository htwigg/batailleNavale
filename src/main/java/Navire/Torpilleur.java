/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package Navire;

import GrilleDeJeu.Case;
import java.util.ArrayList;

public class Torpilleur extends AbstractNavire {
    public Torpilleur() {
        this.emplacement = null;
        this.nom = "Torpilleur";
        this.longueur = 2;
        this.vie = longueur;
        this.coulee = false;
    }

    @Override
    public void placerNavire(ArrayList<Case> positionnement){
        assert (positionnement.size() == 2);
        this.emplacement = positionnement;
        mettreAJourCases(this, positionnement);
    }
}
