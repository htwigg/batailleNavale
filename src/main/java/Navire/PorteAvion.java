/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package Navire;

import GrilleDeJeu.Case;
import java.util.ArrayList;

public class PorteAvion extends AbstractNavire {
    public PorteAvion() {
        this.emplacement = null;
        this.nom = "Porte-Avion";
        this.longueur = 5;
        this.vie = longueur;
        this.coulee = false;
    }

    @Override
    public void placerNavire(ArrayList<Case> positionnement){
        assert (positionnement.size() == 5);
        this.emplacement = positionnement;
        mettreAJourCases(this, positionnement);
    }
}
