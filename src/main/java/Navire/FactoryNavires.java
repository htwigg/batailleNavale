/** Titre : Battaille Navale
 *
 * Cour : Génie logiciel : conception
 * Sigle : INF5153
 * Session : Automne 2018
 *
 * Auteur :
 * Emil Flanagan : FLAE24089106
 * Étienne Bélanger : BELE18039185
 * Hugo Twigg-Coté : TWIH25048700
 * Lou-Gomes Neto : NETL14039105
 */
package Navire;

import GrilleDeJeu.Case;
import GrilleDeJeu.GrilleDeJeu;
import GrilleDeJeu.Positionnement;
import java.util.ArrayList;

public class FactoryNavires {

    public FactoryNavires(){
    }

    /** Resoit les listes de positions sur lesquelles les navires seront placés.
     *
     * @param porteAvion cases du positionnement du porte-avion
     * @param croiseur cases du positionnement du croiseur
     * @param contreTorpilleurs cases du positionnement du contre-torpilleurs
     * @param sousMarin cases du positionnement du sous-marin
     * @param torpilleur cases du positionnement du torpilleur
     * @return la liste des navires nouvellement instancié
     */
    public ArrayList<AbstractNavire> construireNavires(ArrayList<Case> porteAvion,
                                                       ArrayList<Case> croiseur,
                                                       ArrayList<Case> contreTorpilleurs,
                                                       ArrayList<Case> sousMarin,
                                                       ArrayList<Case> torpilleur){

        ArrayList<AbstractNavire> navires = new ArrayList<AbstractNavire>();
        AbstractNavire navire;

        navire = new PorteAvion();
        navire.placerNavire(porteAvion);
        navires.add(navire);

        navire = new Croiseur();
        navire.placerNavire(croiseur);
        navires.add(navire);

        navire = new ContreTorpilleurs();
        navire.placerNavire(contreTorpilleurs);
        navires.add(navire);

        navire = new SousMarin();
        navire.placerNavire(sousMarin);
        navires.add(navire);

        navire = new Torpilleur();
        navire.placerNavire(torpilleur);
        navires.add(navire);

        return navires;
    }

    /** Reçoit la grille de jeux et y positionne les navires aléatoirement.
     *
     * @param grille de jeux
     * @return la liste des navires nouvellement instancié
     */
    public ArrayList<AbstractNavire> construireNaviresAleatoire(GrilleDeJeu grille) {

        ArrayList<AbstractNavire> navires = new ArrayList<AbstractNavire>();
        AbstractNavire navire;

        navire = new PorteAvion();
        navire.placerNavire(new Positionnement().aleatoire(5, grille));
        navires.add(navire);

        navire = new Croiseur();
        navire.placerNavire(new Positionnement().aleatoire(4, grille));
        navires.add(navire);

        navire = new ContreTorpilleurs();
        navire.placerNavire(new Positionnement().aleatoire(3, grille));
        navires.add(navire);

        navire = new SousMarin();
        navire.placerNavire(new Positionnement().aleatoire(3, grille));
        navires.add(navire);

        navire = new Torpilleur();
        navire.placerNavire(new Positionnement().aleatoire(2, grille));
        navires.add(navire);

        return navires;

    }
}
