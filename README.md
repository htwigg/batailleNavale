# Bataille Navale

## Description

Ce programme permet de jouer à une partie de bataille navale contre un joueur artificiel.


## Auteurs

* Étienne Bélanger (Bele18039105)
* Emil Flanagan (Flae24089106)
* Lou-Gomes Neto (Netl14039105)
* Hugo Twigg-Côté (Twih25048700)


## Contexte Académique

Dans le cadre du cours
* INF5153: Génie Logiciel: Conception , groupe 40


## Utilisation

Il est possible de jouer au jeu de deux manières:
* en double cliquant sur le fichier batailleNavale.jar
* a partir d'un IDE en utilisant la fonction BUILD puis RUN.

Dans un IDE, une fois le projet construit, il faut démarer le jeu à partir
de la classe InterfaceGraphique.java.


## Specification

* Le mode en ligne n'est pas fonctionnelle.
* Les parties sauvegardées sont automatiquement sauvegarder de le dossier 
"user_home/partie_sauvegarder/". Sous linux, le dossier va se retrouver 
dans le répertoire " /home/${current_user_name}/partie_sauvegarder/ " 
et sous Windows dans le répertoire "c:\Users\${current_user_name}"
 
## Dépendances

- [JDOM](http://www.jdom.org/index.html), une librairie qui permet de faire une représentation en Java d'un document XML. Cette librairie doit être ajouter aux dépendances Maven afin que le projet fonctionne
