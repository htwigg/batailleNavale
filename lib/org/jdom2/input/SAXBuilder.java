package org.jdom2.input;

import org.jdom2.*;
import org.jdom2.input.sax.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map.Entry;













































































































public class SAXBuilder
  implements SAXEngine
{
  private static final SAXHandlerFactory DEFAULTSAXHANDLERFAC = new DefaultSAXHandlerFactory();
  


  private static final JDOMFactory DEFAULTJDOMFAC = new DefaultJDOMFactory();
  







  private XMLReaderJDOMFactory readerfac = null;
  



  private SAXHandlerFactory handlerfac = null;
  



  private JDOMFactory jdomfac = null;
  







  private final HashMap<String, Boolean> features = new HashMap(5);
  

  private final HashMap<String, Object> properties = new HashMap(5);
  

  private ErrorHandler saxErrorHandler = null;
  

  private EntityResolver saxEntityResolver = null;
  

  private DTDHandler saxDTDHandler = null;
  

  private XMLFilter saxXMLFilter = null;
  

  private boolean expand = true;
  

  private boolean ignoringWhite = false;
  

  private boolean ignoringBoundaryWhite = false;
  

  private boolean reuseParser = true;
  

  private SAXEngine engine = null;
  









  public SAXBuilder()
  {
    this(null, null, null);
  }
  

















  @Deprecated
  public SAXBuilder(boolean validate)
  {
    this(validate ? XMLReaders.DTDVALIDATING : XMLReaders.NONVALIDATING, null, null);
  }
  

















  @Deprecated
  public SAXBuilder(String saxDriverClass)
  {
    this(saxDriverClass, false);
  }
  
















  @Deprecated
  public SAXBuilder(String saxDriverClass, boolean validate)
  {
    this(new XMLReaderSAX2Factory(validate, saxDriverClass), null, null);
  }
  















  public SAXBuilder(XMLReaderJDOMFactory readersouce)
  {
    this(readersouce, null, null);
  }
  
























  public SAXBuilder(XMLReaderJDOMFactory xmlreaderfactory, SAXHandlerFactory handlerfactory, JDOMFactory jdomfactory)
  {
    readerfac = (xmlreaderfactory == null ? XMLReaders.NONVALIDATING : xmlreaderfactory);
    

    handlerfac = (handlerfactory == null ? DEFAULTSAXHANDLERFAC : handlerfactory);
    

    jdomfac = (jdomfactory == null ? DEFAULTJDOMFAC : jdomfactory);
  }
  











  @Deprecated
  public String getDriverClass()
  {
    if ((readerfac instanceof XMLReaderSAX2Factory)) {
      return ((XMLReaderSAX2Factory)readerfac).getDriverClassName();
    }
    return null;
  }
  





  @Deprecated
  public JDOMFactory getFactory()
  {
    return getJDOMFactory();
  }
  





  public JDOMFactory getJDOMFactory()
  {
    return jdomfac;
  }
  







  @Deprecated
  public void setFactory(JDOMFactory factory)
  {
    setJDOMFactory(factory);
  }
  






  public void setJDOMFactory(JDOMFactory factory)
  {
    jdomfac = factory;
    engine = null;
  }
  




  public XMLReaderJDOMFactory getXMLReaderFactory()
  {
    return readerfac;
  }
  






  public void setXMLReaderFactory(XMLReaderJDOMFactory rfac)
  {
    readerfac = (rfac == null ? XMLReaders.NONVALIDATING : rfac);
    

    engine = null;
  }
  




  public SAXHandlerFactory getSAXHandlerFactory()
  {
    return handlerfac;
  }
  






  public void setSAXHandlerFactory(SAXHandlerFactory factory)
  {
    handlerfac = (factory == null ? DEFAULTSAXHANDLERFAC : factory);
    engine = null;
  }
  





  @Deprecated
  public boolean getValidation()
  {
    return isValidating();
  }
  





  public boolean isValidating()
  {
    return readerfac.isValidating();
  }
  


































  @Deprecated
  public void setValidation(boolean validate)
  {
    setXMLReaderFactory(validate ? XMLReaders.DTDVALIDATING : XMLReaders.NONVALIDATING);
  }
  













  public ErrorHandler getErrorHandler()
  {
    return saxErrorHandler;
  }
  






  public void setErrorHandler(ErrorHandler errorHandler)
  {
    saxErrorHandler = errorHandler;
    engine = null;
  }
  





  public EntityResolver getEntityResolver()
  {
    return saxEntityResolver;
  }
  





  public void setEntityResolver(EntityResolver entityResolver)
  {
    saxEntityResolver = entityResolver;
    engine = null;
  }
  






  public DTDHandler getDTDHandler()
  {
    return saxDTDHandler;
  }
  







  public void setDTDHandler(DTDHandler dtdHandler)
  {
    saxDTDHandler = dtdHandler;
    engine = null;
  }
  




  public XMLFilter getXMLFilter()
  {
    return saxXMLFilter;
  }
  














  public void setXMLFilter(XMLFilter xmlFilter)
  {
    saxXMLFilter = xmlFilter;
    engine = null;
  }
  







  public boolean getIgnoringElementContentWhitespace()
  {
    return ignoringWhite;
  }
  










  public void setIgnoringElementContentWhitespace(boolean ignoringWhite)
  {
    this.ignoringWhite = ignoringWhite;
    engine = null;
  }
  








  public boolean getIgnoringBoundaryWhitespace()
  {
    return ignoringBoundaryWhite;
  }
  
















  public void setIgnoringBoundaryWhitespace(boolean ignoringBoundaryWhite)
  {
    this.ignoringBoundaryWhite = ignoringBoundaryWhite;
    engine = null;
  }
  






  public boolean getExpandEntities()
  {
    return expand;
  }
  






















  public void setExpandEntities(boolean expand)
  {
    this.expand = expand;
    engine = null;
  }
  






  public boolean getReuseParser()
  {
    return reuseParser;
  }
  












  public void setReuseParser(boolean reuseParser)
  {
    this.reuseParser = reuseParser;
    if (!reuseParser) {
      engine = null;
    }
  }
  




















  @Deprecated
  public void setFastReconfigure(boolean fastReconfigure) {}
  



















  public void setFeature(String name, boolean value)
  {
    features.put(name, value ? Boolean.TRUE : Boolean.FALSE);
    engine = null;
  }
  





















  public void setProperty(String name, Object value)
  {
    properties.put(name, value);
    engine = null;
  }
  















  public SAXEngine buildEngine()
    throws JDOMException
  {
    SAXHandler contentHandler = handlerfac.createSAXHandler(jdomfac);
    
    contentHandler.setExpandEntities(expand);
    contentHandler.setIgnoringElementContentWhitespace(ignoringWhite);
    contentHandler.setIgnoringBoundaryWhitespace(ignoringBoundaryWhite);
    
    XMLReader parser = createParser();
    
    configureParser(parser, contentHandler);
    boolean valid = readerfac.isValidating();
    
    return new SAXBuilderEngine(parser, contentHandler, valid);
  }
  






  protected XMLReader createParser()
    throws JDOMException
  {
    XMLReader parser = readerfac.createXMLReader();
    

    if (saxXMLFilter != null)
    {
      XMLFilter root = saxXMLFilter;
      while ((root.getParent() instanceof XMLFilter)) {
        root = (XMLFilter)root.getParent();
      }
      root.setParent(parser);
      

      parser = saxXMLFilter;
    }
    
    return parser;
  }
  








  private SAXEngine getEngine()
    throws JDOMException
  {
    if (engine != null) {
      return engine;
    }
    
    engine = buildEngine();
    return engine;
  }
  




















  protected void configureParser(XMLReader parser, SAXHandler contentHandler)
    throws JDOMException
  {
    parser.setContentHandler(contentHandler);
    
    if (saxEntityResolver != null) {
      parser.setEntityResolver(saxEntityResolver);
    }
    
    if (saxDTDHandler != null) {
      parser.setDTDHandler(saxDTDHandler);
    } else {
      parser.setDTDHandler(contentHandler);
    }
    
    if (saxErrorHandler != null) {
      parser.setErrorHandler(saxErrorHandler);
    } else {
      parser.setErrorHandler(new BuilderErrorHandler());
    }
    
    boolean success = false;
    try
    {
      parser.setProperty("http://xml.org/sax/properties/lexical-handler", contentHandler);
      
      success = true;
    }
    catch (SAXNotSupportedException e) {}catch (SAXNotRecognizedException e) {}
    




    if (!success) {
      try {
        parser.setProperty("http://xml.org/sax/handlers/LexicalHandler", contentHandler);
        
        success = true;
      }
      catch (SAXNotSupportedException e) {}catch (SAXNotRecognizedException e) {}
    }
    




    for (Map.Entry<String, Boolean> me : features.entrySet()) {
      internalSetFeature(parser, (String)me.getKey(), ((Boolean)me.getValue()).booleanValue(), (String)me.getKey());
    }
    

    for (Map.Entry<String, Object> me : properties.entrySet()) {
      internalSetProperty(parser, (String)me.getKey(), me.getValue(), (String)me.getKey());
    }
    






    try
    {
      if (parser.getFeature("http://xml.org/sax/features/external-general-entities") != expand) {
        parser.setFeature("http://xml.org/sax/features/external-general-entities", expand);
      }
    }
    catch (SAXException e) {}
    

    if (!expand) {
      try {
        parser.setProperty("http://xml.org/sax/properties/declaration-handler", contentHandler);
        
        success = true;
      }
      catch (SAXNotSupportedException e) {}catch (SAXNotRecognizedException e) {}
    }
  }
  






  private void internalSetFeature(XMLReader parser, String feature, boolean value, String displayName)
    throws JDOMException
  {
    try
    {
      parser.setFeature(feature, value);
    } catch (SAXNotSupportedException e) {
      throw new JDOMException(displayName + " feature not supported for SAX driver " + parser.getClass().getName());
    }
    catch (SAXNotRecognizedException e) {
      throw new JDOMException(displayName + " feature not recognized for SAX driver " + parser.getClass().getName());
    }
  }
  





  private void internalSetProperty(XMLReader parser, String property, Object value, String displayName)
    throws JDOMException
  {
    try
    {
      parser.setProperty(property, value);
    } catch (SAXNotSupportedException e) {
      throw new JDOMException(displayName + " property not supported for SAX driver " + parser.getClass().getName());
    }
    catch (SAXNotRecognizedException e) {
      throw new JDOMException(displayName + " property not recognized for SAX driver " + parser.getClass().getName());
    }
  }
  












  public Document build(InputSource in)
    throws JDOMException, IOException
  {
    try
    {
      return getEngine().build(in);
    } finally {
      if (!reuseParser) {
        engine = null;
      }
    }
  }
  













  public Document build(InputStream in)
    throws JDOMException, IOException
  {
    try
    {
      return getEngine().build(in);
    } finally {
      if (!reuseParser) {
        engine = null;
      }
    }
  }
  












  public Document build(File file)
    throws JDOMException, IOException
  {
    try
    {
      return getEngine().build(file);
    } finally {
      if (!reuseParser) {
        engine = null;
      }
    }
  }
  












  public Document build(URL url)
    throws JDOMException, IOException
  {
    try
    {
      return getEngine().build(url);
    } finally {
      if (!reuseParser) {
        engine = null;
      }
    }
  }
  














  public Document build(InputStream in, String systemId)
    throws JDOMException, IOException
  {
    try
    {
      return getEngine().build(in, systemId);
    } finally {
      if (!reuseParser) {
        engine = null;
      }
    }
  }
  















  public Document build(Reader characterStream)
    throws JDOMException, IOException
  {
    try
    {
      return getEngine().build(characterStream);
    } finally {
      if (!reuseParser) {
        engine = null;
      }
    }
  }
  


















  public Document build(Reader characterStream, String systemId)
    throws JDOMException, IOException
  {
    try
    {
      return getEngine().build(characterStream, systemId);
    } finally {
      if (!reuseParser) {
        engine = null;
      }
    }
  }
  























  public Document build(String systemId)
    throws JDOMException, IOException
  {
    if (systemId == null) {
      throw new NullPointerException("Unable to build a URI from a null systemID.");
    }
    try
    {
      return getEngine().build(systemId);





    }
    catch (IOException ioe)
    {





      int len = systemId.length();
      int i = 0;
      while ((i < len) && (Verifier.isXMLWhitespace(systemId.charAt(i)))) {
        i++;
      }
      if ((i < len) && ('<' == systemId.charAt(i)))
      {
        MalformedURLException mx = new MalformedURLException("SAXBuilder.build(String) expects the String to be a systemID, but in this instance it appears to be actual XML data.");
        




        mx.initCause(ioe);
        throw mx;
      }
      
      throw ioe;
    } finally {
      if (!reuseParser) {
        engine = null;
      }
    }
  }
}
