package org.jdom2.input.sax;

import org.jdom2.JDOMException;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;







































































public enum XMLReaders
  implements XMLReaderJDOMFactory
{
  NONVALIDATING(0), 
  



  DTDVALIDATING(1), 
  



  XSDVALIDATING(2);
  
  private final int singletonID;
  
  private static abstract interface FactorySupplier { public abstract SAXParserFactory supply() throws Exception;
    
    public abstract boolean validates();
  }
  
  private static enum NONSingleton implements XMLReaders.FactorySupplier { INSTANCE;
    
    private final SAXParserFactory factory;
    
    private NONSingleton() {
      SAXParserFactory fac = SAXParserFactory.newInstance();
      
      fac.setNamespaceAware(true);
      fac.setValidating(false);
      factory = fac;
    }
    
    public SAXParserFactory supply() throws Exception
    {
      return factory;
    }
    
    public boolean validates()
    {
      return false;
    }
  }
  
  private static enum DTDSingleton implements XMLReaders.FactorySupplier
  {
    INSTANCE;
    
    private final SAXParserFactory factory;
    
    private DTDSingleton() {
      SAXParserFactory fac = SAXParserFactory.newInstance();
      
      fac.setNamespaceAware(true);
      
      fac.setValidating(true);
      factory = fac;
    }
    
    public SAXParserFactory supply() throws Exception
    {
      return factory;
    }
    
    public boolean validates()
    {
      return true;
    }
  }
  
  private static enum XSDSingleton implements XMLReaders.FactorySupplier
  {
    INSTANCE;
    
    private final Exception failcause;
    private final SAXParserFactory factory;
    
    private XSDSingleton()
    {
      SAXParserFactory fac = SAXParserFactory.newInstance();
      Exception problem = null;
      
      fac.setNamespaceAware(true);
      

      fac.setValidating(false);
      try
      {
        SchemaFactory sfac = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
        Schema schema = sfac.newSchema();
        fac.setSchema(schema);
      }
      catch (SAXException se) {
        fac = null;
        problem = se;

      }
      catch (IllegalArgumentException iae)
      {
        fac = null;
        problem = iae;



      }
      catch (UnsupportedOperationException uoe)
      {


        fac = null;
        problem = uoe;
      }
      factory = fac;
      failcause = problem;
    }
    
    public SAXParserFactory supply()
      throws Exception
    {
      if (factory == null) {
        throw failcause;
      }
      return factory;
    }
    
    public boolean validates()
    {
      return true;
    }
  }
  




  private XMLReaders(int singletonID)
  {
    this.singletonID = singletonID;
  }
  
  private FactorySupplier getSupplier() {
    switch (singletonID) {
    case 0: 
      return NONSingleton.INSTANCE;
    case 1: 
      return DTDSingleton.INSTANCE;
    case 2: 
      return XSDSingleton.INSTANCE;
    }
    throw new IllegalStateException("Unknown singletonID: " + singletonID);
  }
  






  public XMLReader createXMLReader()
    throws JDOMException
  {
    try
    {
      FactorySupplier supplier = getSupplier();
      return supplier.supply().newSAXParser().getXMLReader();
    } catch (SAXException e) {
      throw new JDOMException("Unable to create a new XMLReader instance", e);
    }
    catch (ParserConfigurationException e) {
      throw new JDOMException("Unable to create a new XMLReader instance", e);
    }
    catch (Exception e) {
      throw new JDOMException("It was not possible to configure a suitable XMLReader to support " + this, e);
    }
  }
  

  public boolean isValidating()
  {
    return getSupplier().validates();
  }
}
