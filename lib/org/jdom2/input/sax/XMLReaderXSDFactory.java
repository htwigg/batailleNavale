package org.jdom2.input.sax;

import org.jdom2.JDOMException;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.net.URL;













































































public class XMLReaderXSDFactory
  extends AbstractReaderXSDFactory
{
  private static final AbstractReaderXSDFactory.SchemaFactoryProvider xsdschemas = new AbstractReaderXSDFactory.SchemaFactoryProvider()
  {
    public SchemaFactory getSchemaFactory() {
      return SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
    }
  };
  












  public XMLReaderXSDFactory(String... systemid)
    throws JDOMException
  {
    super(SAXParserFactory.newInstance(), xsdschemas, systemid);
  }
  













  public XMLReaderXSDFactory(String factoryClassName, ClassLoader classloader, String... systemid)
    throws JDOMException
  {
    super(SAXParserFactory.newInstance(factoryClassName, classloader), xsdschemas, systemid);
  }
  









  public XMLReaderXSDFactory(URL... systemid)
    throws JDOMException
  {
    super(SAXParserFactory.newInstance(), xsdschemas, systemid);
  }
  













  public XMLReaderXSDFactory(String factoryClassName, ClassLoader classloader, URL... systemid)
    throws JDOMException
  {
    super(SAXParserFactory.newInstance(factoryClassName, classloader), xsdschemas, systemid);
  }
  









  public XMLReaderXSDFactory(File... systemid)
    throws JDOMException
  {
    super(SAXParserFactory.newInstance(), xsdschemas, systemid);
  }
  













  public XMLReaderXSDFactory(String factoryClassName, ClassLoader classloader, File... systemid)
    throws JDOMException
  {
    super(SAXParserFactory.newInstance(factoryClassName, classloader), xsdschemas, systemid);
  }
  









  public XMLReaderXSDFactory(Source... sources)
    throws JDOMException
  {
    super(SAXParserFactory.newInstance(), xsdschemas, sources);
  }
  













  public XMLReaderXSDFactory(String factoryClassName, ClassLoader classloader, Source... sources)
    throws JDOMException
  {
    super(SAXParserFactory.newInstance(factoryClassName, classloader), xsdschemas, sources);
  }
}
