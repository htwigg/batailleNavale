package org.jdom2.input.sax;

import org.jdom2.*;
import org.xml.sax.Attributes;
import org.xml.sax.DTDHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.ext.Attributes2;
import org.xml.sax.ext.DeclHandler;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;































































































public class SAXHandler
  extends DefaultHandler
  implements LexicalHandler, DeclHandler, DTDHandler
{
  private final JDOMFactory factory;
  private final List<Namespace> declaredNamespaces = new ArrayList(32);
  


  private final StringBuilder internalSubset = new StringBuilder();
  

  private final TextBuffer textBuffer = new TextBuffer();
  

  private final Map<String, String[]> externalEntities = new HashMap();
  

  private Document currentDocument = null;
  

  private Element currentElement = null;
  

  private Locator currentLocator = null;
  

  private boolean atRoot = true;
  





  private boolean inDTD = false;
  

  private boolean inInternalSubset = false;
  

  private boolean previousCDATA = false;
  

  private boolean inCDATA = false;
  

  private boolean expand = true;
  




  private boolean suppress = false;
  

  private int entityDepth = 0;
  

  private boolean ignoringWhite = false;
  

  private boolean ignoringBoundaryWhite = false;
  
  private int lastline = 0; private int lastcol = 0;
  




  public SAXHandler()
  {
    this(null);
  }
  







  public SAXHandler(JDOMFactory factory)
  {
    this.factory = (factory != null ? factory : new DefaultJDOMFactory());
    reset();
  }
  






  protected void resetSubCLass() {}
  






  public final void reset()
  {
    currentLocator = null;
    currentDocument = factory.document(null);
    currentElement = null;
    atRoot = true;
    inDTD = false;
    inInternalSubset = false;
    previousCDATA = false;
    inCDATA = false;
    expand = true;
    suppress = false;
    entityDepth = 0;
    declaredNamespaces.clear();
    internalSubset.setLength(0);
    textBuffer.clear();
    externalEntities.clear();
    ignoringWhite = false;
    ignoringBoundaryWhite = false;
    resetSubCLass();
  }
  







  protected void pushElement(Element element)
  {
    if (atRoot) {
      currentDocument.setRootElement(element);
      
      atRoot = false;
    } else {
      factory.addContent(currentElement, element);
    }
    currentElement = element;
  }
  




  public Document getDocument()
  {
    return currentDocument;
  }
  






  public JDOMFactory getFactory()
  {
    return factory;
  }
  









  public void setExpandEntities(boolean expand)
  {
    this.expand = expand;
  }
  






  public boolean getExpandEntities()
  {
    return expand;
  }
  










  public void setIgnoringElementContentWhitespace(boolean ignoringWhite)
  {
    this.ignoringWhite = ignoringWhite;
  }
  








  public void setIgnoringBoundaryWhitespace(boolean ignoringBoundaryWhite)
  {
    this.ignoringBoundaryWhite = ignoringBoundaryWhite;
  }
  







  public boolean getIgnoringBoundaryWhitespace()
  {
    return ignoringBoundaryWhite;
  }
  








  public boolean getIgnoringElementContentWhitespace()
  {
    return ignoringWhite;
  }
  
  public void startDocument()
  {
    if (currentLocator != null) {
      currentDocument.setBaseURI(currentLocator.getSystemId());
    }
  }
  













  public void externalEntityDecl(String name, String publicID, String systemID)
    throws SAXException
  {
    externalEntities.put(name, new String[] { publicID, systemID });
    
    if (!inInternalSubset) {
      return;
    }
    internalSubset.append("  <!ENTITY ").append(name);
    appendExternalId(publicID, systemID);
    internalSubset.append(">\n");
  }
  
















  public void attributeDecl(String eName, String aName, String type, String valueDefault, String value)
  {
    if (!inInternalSubset) {
      return;
    }
    internalSubset.append("  <!ATTLIST ").append(eName).append(' ').append(aName).append(' ').append(type).append(' ');
    
    if (valueDefault != null) {
      internalSubset.append(valueDefault);
    } else {
      internalSubset.append('"').append(value).append('"');
    }
    if ((valueDefault != null) && (valueDefault.equals("#FIXED"))) {
      internalSubset.append(" \"").append(value).append('"');
    }
    internalSubset.append(">\n");
  }
  









  public void elementDecl(String name, String model)
  {
    if (!inInternalSubset) {
      return;
    }
    internalSubset.append("  <!ELEMENT ").append(name).append(' ').append(model).append(">\n");
  }
  











  public void internalEntityDecl(String name, String value)
  {
    if (!inInternalSubset) {
      return;
    }
    internalSubset.append("  <!ENTITY ");
    if (name.startsWith("%")) {
      internalSubset.append("% ").append(name.substring(1));
    } else {
      internalSubset.append(name);
    }
    internalSubset.append(" \"").append(value).append("\">\n");
  }
  














  public void processingInstruction(String target, String data)
    throws SAXException
  {
    if (suppress) {
      return;
    }
    flushCharacters();
    
    ProcessingInstruction pi = currentLocator == null ? factory.processingInstruction(target, data) : factory.processingInstruction(currentLocator.getLineNumber(), currentLocator.getColumnNumber(), target, data);
    



    if (atRoot) {
      factory.addContent(currentDocument, pi);
    } else {
      factory.addContent(getCurrentElement(), pi);
    }
  }
  










  public void skippedEntity(String name)
    throws SAXException
  {
    if (name.startsWith("%")) {
      return;
    }
    flushCharacters();
    
    EntityRef er = currentLocator == null ? factory.entityRef(name) : factory.entityRef(currentLocator.getLineNumber(), currentLocator.getColumnNumber(), name);
    


    factory.addContent(getCurrentElement(), er);
  }
  










  public void startPrefixMapping(String prefix, String uri)
    throws SAXException
  {
    if (suppress) {
      return;
    }
    Namespace ns = Namespace.getNamespace(prefix, uri);
    declaredNamespaces.add(ns);
  }
  




















  public void startElement(String namespaceURI, String localName, String qName, Attributes atts)
    throws SAXException
  {
    if (suppress) {
      return;
    }
    String prefix = "";
    

    if (!"".equals(qName)) {
      int colon = qName.indexOf(':');
      
      if (colon > 0) {
        prefix = qName.substring(0, colon);
      }
      

      if ((localName == null) || (localName.equals(""))) {
        localName = qName.substring(colon + 1);
      }
    }
    


    Namespace namespace = Namespace.getNamespace(prefix, namespaceURI);
    
    Element element = currentLocator == null ? factory.element(localName, namespace) : factory.element(currentLocator.getLineNumber(), currentLocator.getColumnNumber(), localName, namespace);
    





    if (declaredNamespaces.size() > 0) {
      transferNamespaces(element);
    }
    
    flushCharacters();
    
    if (atRoot) {
      factory.setRoot(currentDocument, element);
      
      atRoot = false;
    } else {
      factory.addContent(getCurrentElement(), element);
    }
    currentElement = element;
    

    int i = 0; for (int len = atts.getLength(); i < len; i++)
    {
      String attPrefix = "";
      String attLocalName = atts.getLocalName(i);
      String attQName = atts.getQName(i);
      boolean specified = (atts instanceof Attributes2) ? ((Attributes2)atts).isSpecified(i) : true;
      


      if (!attQName.equals(""))
      {



        if ((attQName.startsWith("xmlns:")) || (attQName.equals("xmlns"))) {
          continue;
        }
        
        int attColon = attQName.indexOf(':');
        
        if (attColon > 0) {
          attPrefix = attQName.substring(0, attColon);
        }
        

        if ("".equals(attLocalName)) {
          attLocalName = attQName.substring(attColon + 1);
        }
      }
      
      AttributeType attType = AttributeType.getAttributeType(atts.getType(i));
      
      String attValue = atts.getValue(i);
      String attURI = atts.getURI(i);
      
      if ((!"xmlns".equals(attLocalName)) && (!"xmlns".equals(attPrefix)) && (!"http://www.w3.org/2000/xmlns/".equals(attURI)))
      {











        if ((!"".equals(attURI)) && ("".equals(attPrefix)))
        {










          HashMap<String, Namespace> tmpmap = new HashMap();
          for (Namespace nss : element.getNamespacesInScope()) {
            if ((nss.getPrefix().length() > 0) && (nss.getURI().equals(attURI)))
            {
              attPrefix = nss.getPrefix();
              break;
            }
            tmpmap.put(nss.getPrefix(), nss);
          }
          
          if ("".equals(attPrefix))
          {










            int cnt = 0;
            String base = "attns";
            String pfx = "attns" + cnt;
            while (tmpmap.containsKey(pfx)) {
              cnt++;
              pfx = "attns" + cnt;
            }
            attPrefix = pfx;
          }
        }
        Namespace attNs = Namespace.getNamespace(attPrefix, attURI);
        
        Attribute attribute = factory.attribute(attLocalName, attValue, attType, attNs);
        
        if (!specified)
        {
          attribute.setSpecified(false);
        }
        factory.setAttribute(element, attribute);
      }
    }
  }
  






  private void transferNamespaces(Element element)
  {
    for (Namespace ns : declaredNamespaces) {
      if (ns != element.getNamespace()) {
        element.addNamespaceDeclaration(ns);
      }
    }
    declaredNamespaces.clear();
  }
  











  public void characters(char[] ch, int start, int length)
    throws SAXException
  {
    if ((suppress) || ((length == 0) && (!inCDATA))) {
      return;
    }
    if (previousCDATA != inCDATA) {
      flushCharacters();
    }
    
    textBuffer.append(ch, start, length);
    
    if (currentLocator != null) {
      lastline = currentLocator.getLineNumber();
      lastcol = currentLocator.getColumnNumber();
    }
  }
  














  public void ignorableWhitespace(char[] ch, int start, int length)
    throws SAXException
  {
    if (!ignoringWhite) {
      characters(ch, start, length);
    }
  }
  





  protected void flushCharacters()
    throws SAXException
  {
    if (ignoringBoundaryWhite) {
      if (!textBuffer.isAllWhitespace()) {
        flushCharacters(textBuffer.toString());
      }
    } else {
      flushCharacters(textBuffer.toString());
    }
    textBuffer.clear();
  }
  








  protected void flushCharacters(String data)
    throws SAXException
  {
    if ((data.length() == 0) && (!inCDATA)) {
      previousCDATA = inCDATA;
      return;
    }
    







    if (previousCDATA) {
      CDATA cdata = currentLocator == null ? factory.cdata(data) : factory.cdata(lastline, lastcol, data);
      
      factory.addContent(getCurrentElement(), cdata);
    } else {
      Text text = currentLocator == null ? factory.text(data) : factory.text(lastline, lastcol, data);
      
      factory.addContent(getCurrentElement(), text);
    }
    
    previousCDATA = inCDATA;
  }
  
















  public void endElement(String namespaceURI, String localName, String qName)
    throws SAXException
  {
    if (suppress) {
      return;
    }
    flushCharacters();
    
    if (!atRoot) {
      Parent p = currentElement.getParent();
      if ((p instanceof Document)) {
        atRoot = true;
      } else {
        currentElement = ((Element)p);
      }
    } else {
      throw new SAXException("Ill-formed XML document (missing opening tag for " + localName + ")");
    }
  }
  















  public void startDTD(String name, String publicID, String systemID)
    throws SAXException
  {
    flushCharacters();
    
    DocType doctype = currentLocator == null ? factory.docType(name, publicID, systemID) : factory.docType(currentLocator.getLineNumber(), currentLocator.getColumnNumber(), name, publicID, systemID);
    


    factory.addContent(currentDocument, doctype);
    inDTD = true;
    inInternalSubset = true;
  }
  




  public void endDTD()
  {
    currentDocument.getDocType().setInternalSubset(internalSubset.toString());
    
    inDTD = false;
    inInternalSubset = false;
  }
  
  public void startEntity(String name) throws SAXException
  {
    entityDepth += 1;
    
    if ((expand) || (entityDepth > 1))
    {
      return;
    }
    

    if (name.equals("[dtd]")) {
      inInternalSubset = false;
      return;
    }
    

    if ((!inDTD) && (!name.equals("amp")) && (!name.equals("lt")) && (!name.equals("gt")) && (!name.equals("apos")) && (!name.equals("quot")))
    {


      if (!expand) {
        String pub = null;
        String sys = null;
        String[] ids = (String[])externalEntities.get(name);
        if (ids != null) {
          pub = ids[0];
          sys = ids[1];
        }
        







        if (!atRoot) {
          flushCharacters();
          EntityRef entity = currentLocator == null ? factory.entityRef(name, pub, sys) : factory.entityRef(currentLocator.getLineNumber(), currentLocator.getColumnNumber(), name, pub, sys);
          





          factory.addContent(getCurrentElement(), entity);
        }
        suppress = true;
      }
    }
  }
  
  public void endEntity(String name) throws SAXException
  {
    entityDepth -= 1;
    if (entityDepth == 0)
    {

      suppress = false;
    }
    if (name.equals("[dtd]")) {
      inInternalSubset = true;
    }
  }
  



  public void startCDATA()
  {
    if (suppress) {
      return;
    }
    inCDATA = true;
  }
  


  public void endCDATA()
    throws SAXException
  {
    if (suppress) {
      return;
    }
    previousCDATA = true;
    flushCharacters();
    previousCDATA = false;
    inCDATA = false;
  }
  















  public void comment(char[] ch, int start, int length)
    throws SAXException
  {
    if (suppress) {
      return;
    }
    flushCharacters();
    
    String commentText = new String(ch, start, length);
    if ((inDTD) && (inInternalSubset) && (!expand)) {
      internalSubset.append("  <!--").append(commentText).append("-->\n");
      return;
    }
    if ((!inDTD) && (!commentText.equals(""))) {
      Comment comment = currentLocator == null ? factory.comment(commentText) : factory.comment(currentLocator.getLineNumber(), currentLocator.getColumnNumber(), commentText);
      


      if (atRoot) {
        factory.addContent(currentDocument, comment);
      } else {
        factory.addContent(getCurrentElement(), comment);
      }
    }
  }
  











  public void notationDecl(String name, String publicID, String systemID)
    throws SAXException
  {
    if (!inInternalSubset) {
      return;
    }
    internalSubset.append("  <!NOTATION ").append(name);
    appendExternalId(publicID, systemID);
    internalSubset.append(">\n");
  }
  














  public void unparsedEntityDecl(String name, String publicID, String systemID, String notationName)
  {
    if (!inInternalSubset) {
      return;
    }
    internalSubset.append("  <!ENTITY ").append(name);
    appendExternalId(publicID, systemID);
    internalSubset.append(" NDATA ").append(notationName);
    internalSubset.append(">\n");
  }
  








  private void appendExternalId(String publicID, String systemID)
  {
    if (publicID != null) {
      internalSubset.append(" PUBLIC \"").append(publicID).append('"');
    }
    if (systemID != null) {
      if (publicID == null) {
        internalSubset.append(" SYSTEM ");
      } else {
        internalSubset.append(' ');
      }
      internalSubset.append('"').append(systemID).append('"');
    }
  }
  





  public Element getCurrentElement()
    throws SAXException
  {
    if (currentElement == null) {
      throw new SAXException("Ill-formed XML document (multiple root elements detected)");
    }
    
    return currentElement;
  }
  













  public void setDocumentLocator(Locator locator)
  {
    currentLocator = locator;
  }
  





  public Locator getDocumentLocator()
  {
    return currentLocator;
  }
}
