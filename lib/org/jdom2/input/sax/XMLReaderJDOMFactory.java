package org.jdom2.input.sax;

import org.jdom2.JDOMException;
import org.xml.sax.XMLReader;

public abstract interface XMLReaderJDOMFactory
{
  public abstract XMLReader createXMLReader()
    throws JDOMException;
  
  public abstract boolean isValidating();
}
