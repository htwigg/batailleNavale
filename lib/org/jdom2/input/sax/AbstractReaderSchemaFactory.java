package org.jdom2.input.sax;

import org.jdom2.JDOMException;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;















































































public abstract class AbstractReaderSchemaFactory
  implements XMLReaderJDOMFactory
{
  private final SAXParserFactory saxfac;
  
  public AbstractReaderSchemaFactory(SAXParserFactory fac, Schema schema)
  {
    if (schema == null) {
      throw new NullPointerException("Cannot create a SchemaXMLReaderFactory with a null schema");
    }
    
    saxfac = fac;
    if (saxfac != null) {
      saxfac.setNamespaceAware(true);
      saxfac.setValidating(false);
      saxfac.setSchema(schema);
    }
  }
  
  public XMLReader createXMLReader() throws JDOMException
  {
    if (saxfac == null) {
      throw new JDOMException("It was not possible to configure a suitable XMLReader to support " + this);
    }
    
    try
    {
      return saxfac.newSAXParser().getXMLReader();
    } catch (SAXException e) {
      throw new JDOMException("Could not create a new Schema-Validating XMLReader.", e);
    }
    catch (ParserConfigurationException e) {
      throw new JDOMException("Could not create a new Schema-Validating XMLReader.", e);
    }
  }
  

  public boolean isValidating()
  {
    return true;
  }
}
