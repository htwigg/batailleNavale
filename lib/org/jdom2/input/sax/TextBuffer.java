package org.jdom2.input.sax;

import org.jdom2.Verifier;
import org.jdom2.internal.ArrayCopy;











































































final class TextBuffer
{
  private char[] array = new char['Ѐ'];
  

  private int arraySize = 0;
  






  TextBuffer() {}
  





  void append(char[] source, int start, int count)
  {
    if (count + arraySize > array.length)
    {
      array = ArrayCopy.copyOf(array, count + arraySize + (array.length >> 2));
    }
    System.arraycopy(source, start, array, arraySize, count);
    arraySize += count;
  }
  


  void clear()
  {
    arraySize = 0;
  }
  




  boolean isAllWhitespace()
  {
    int i = arraySize;
    do { i--; if (i < 0) break;
    } while (Verifier.isXMLWhitespace(array[i]));
    return false;
    

    return true;
  }
  

  public String toString()
  {
    if (arraySize == 0) {
      return "";
    }
    return String.valueOf(array, 0, arraySize);
  }
}
