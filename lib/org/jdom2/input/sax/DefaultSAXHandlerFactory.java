package org.jdom2.input.sax;

import org.jdom2.JDOMFactory;






























































public final class DefaultSAXHandlerFactory
  implements SAXHandlerFactory
{
  public DefaultSAXHandlerFactory() {}
  
  private static final class DefaultSAXHandler
    extends SAXHandler
  {
    public DefaultSAXHandler(JDOMFactory factory)
    {
      super();
    }
  }
  
  public SAXHandler createSAXHandler(JDOMFactory factory)
  {
    return new DefaultSAXHandler(factory);
  }
}
