package org.jdom2.input.sax;

import org.jdom2.JDOMException;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;













































































public class XMLReaderJAXPFactory
  implements XMLReaderJDOMFactory
{
  private final SAXParserFactory instance;
  private final boolean validating;
  
  public XMLReaderJAXPFactory(String factoryClassName, ClassLoader classLoader, boolean dtdvalidate)
  {
    instance = SAXParserFactory.newInstance(factoryClassName, classLoader);
    instance.setNamespaceAware(true);
    instance.setValidating(dtdvalidate);
    validating = dtdvalidate;
  }
  
  public XMLReader createXMLReader() throws JDOMException
  {
    try {
      return instance.newSAXParser().getXMLReader();
    } catch (SAXException e) {
      throw new JDOMException("Unable to create a new XMLReader instance", e);
    }
    catch (ParserConfigurationException e) {
      throw new JDOMException("Unable to create a new XMLReader instance", e);
    }
  }
  

  public boolean isValidating()
  {
    return validating;
  }
}
