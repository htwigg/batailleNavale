package org.jdom2.input.sax;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.JDOMFactory;
import org.jdom2.input.JDOMParseException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;




























































































public class SAXBuilderEngine
  implements SAXEngine
{
  private final XMLReader saxParser;
  private final SAXHandler saxHandler;
  private final boolean validating;
  
  public SAXBuilderEngine(XMLReader reader, SAXHandler handler, boolean validating)
  {
    saxParser = reader;
    saxHandler = handler;
    this.validating = validating;
  }
  





  public JDOMFactory getJDOMFactory()
  {
    return saxHandler.getFactory();
  }
  





  public boolean isValidating()
  {
    return validating;
  }
  





  public ErrorHandler getErrorHandler()
  {
    return saxParser.getErrorHandler();
  }
  





  public EntityResolver getEntityResolver()
  {
    return saxParser.getEntityResolver();
  }
  





  public DTDHandler getDTDHandler()
  {
    return saxParser.getDTDHandler();
  }
  





  public boolean getIgnoringElementContentWhitespace()
  {
    return saxHandler.getIgnoringElementContentWhitespace();
  }
  





  public boolean getIgnoringBoundaryWhitespace()
  {
    return saxHandler.getIgnoringBoundaryWhitespace();
  }
  





  public boolean getExpandEntities()
  {
    return saxHandler.getExpandEntities();
  }
  





  public Document build(InputSource in)
    throws JDOMException, IOException
  {
    try
    {
      saxParser.parse(in);
      
      return saxHandler.getDocument();
    } catch (SAXParseException e) {
      Document doc = saxHandler.getDocument();
      if (!doc.hasRootElement()) {
        doc = null;
      }
      
      String systemId = e.getSystemId();
      if (systemId != null) {
        throw new JDOMParseException("Error on line " + e.getLineNumber() + " of document " + systemId + ": " + e.getMessage(), e, doc);
      }
      

      throw new JDOMParseException("Error on line " + e.getLineNumber() + ": " + e.getMessage(), e, doc);
    }
    catch (SAXException e)
    {
      throw new JDOMParseException("Error in building: " + e.getMessage(), e, saxHandler.getDocument());

    }
    finally
    {

      saxHandler.reset();
    }
  }
  




  public Document build(InputStream in)
    throws JDOMException, IOException
  {
    return build(new InputSource(in));
  }
  



  public Document build(File file)
    throws JDOMException, IOException
  {
    try
    {
      return build(fileToURL(file));
    } catch (MalformedURLException e) {
      throw new JDOMException("Error in building", e);
    }
  }
  




  public Document build(URL url)
    throws JDOMException, IOException
  {
    return build(new InputSource(url.toExternalForm()));
  }
  







  public Document build(InputStream in, String systemId)
    throws JDOMException, IOException
  {
    InputSource src = new InputSource(in);
    src.setSystemId(systemId);
    return build(src);
  }
  





  public Document build(Reader characterStream)
    throws JDOMException, IOException
  {
    return build(new InputSource(characterStream));
  }
  







  public Document build(Reader characterStream, String systemId)
    throws JDOMException, IOException
  {
    InputSource src = new InputSource(characterStream);
    src.setSystemId(systemId);
    return build(src);
  }
  





  public Document build(String systemId)
    throws JDOMException, IOException
  {
    return build(new InputSource(systemId));
  }
  









  private static URL fileToURL(File file)
    throws MalformedURLException
  {
    return file.getAbsoluteFile().toURI().toURL();
  }
}
