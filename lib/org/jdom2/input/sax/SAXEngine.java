package org.jdom2.input.sax;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.JDOMFactory;
import org.xml.sax.DTDHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;

public abstract interface SAXEngine
{
  public abstract JDOMFactory getJDOMFactory();
  
  public abstract boolean isValidating();
  
  public abstract ErrorHandler getErrorHandler();
  
  public abstract EntityResolver getEntityResolver();
  
  public abstract DTDHandler getDTDHandler();
  
  public abstract boolean getIgnoringElementContentWhitespace();
  
  public abstract boolean getIgnoringBoundaryWhitespace();
  
  public abstract boolean getExpandEntities();
  
  public abstract Document build(InputSource paramInputSource)
    throws JDOMException, IOException;
  
  public abstract Document build(InputStream paramInputStream)
    throws JDOMException, IOException;
  
  public abstract Document build(File paramFile)
    throws JDOMException, IOException;
  
  public abstract Document build(URL paramURL)
    throws JDOMException, IOException;
  
  public abstract Document build(InputStream paramInputStream, String paramString)
    throws JDOMException, IOException;
  
  public abstract Document build(Reader paramReader)
    throws JDOMException, IOException;
  
  public abstract Document build(Reader paramReader, String paramString)
    throws JDOMException, IOException;
  
  public abstract Document build(String paramString)
    throws JDOMException, IOException;
}
