package org.jdom2.input.sax;

import org.jdom2.JDOMFactory;

public abstract interface SAXHandlerFactory
{
  public abstract SAXHandler createSAXHandler(JDOMFactory paramJDOMFactory);
}
