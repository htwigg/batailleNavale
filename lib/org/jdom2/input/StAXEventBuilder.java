package org.jdom2.input;

import org.jdom2.*;
import org.jdom2.input.stax.DTDParser;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import java.util.Iterator;































































































public class StAXEventBuilder
{
  public StAXEventBuilder() {}
  
  private static final Document process(JDOMFactory factory, XMLEventReader events)
    throws JDOMException
  {
    try
    {
      Document document = factory.document(null);
      Element current = null;
      
      XMLEvent event = events.peek();
      
      if (7 != event.getEventType()) {
        throw new JDOMException("JDOM requires that XMLStreamReaders are at their beginning when being processed.");
      }
      



      while (event.getEventType() != 8) {
        if (event.isStartDocument()) {
          document.setBaseURI(event.getLocation().getSystemId());
          document.setProperty("ENCODING_SCHEME", ((StartDocument)event).getCharacterEncodingScheme());
          
          document.setProperty("STANDALONE", String.valueOf(((StartDocument)event).isStandalone()));


        }
        else if ((event instanceof DTD))
        {

          DocType dtype = DTDParser.parse(((DTD)event).getDocumentTypeDeclaration(), factory);
          document.setDocType(dtype);
        } else if (event.isStartElement()) {
          Element emt = processElement(factory, event.asStartElement());
          if (current == null) {
            document.setRootElement(emt);
            DocType dt = document.getDocType();
            if (dt != null) {
              dt.setElementName(emt.getName());
            }
          } else {
            current.addContent(emt);
          }
          current = emt;
        } else if ((event.isCharacters()) && (current != null))
        {

          Characters chars = event.asCharacters();
          if (chars.isCData()) {
            current.addContent(factory.cdata(((Characters)event).getData()));
          }
          else {
            current.addContent(factory.text(((Characters)event).getData()));
          }
        }
        else if ((event instanceof javax.xml.stream.events.Comment)) {
          org.jdom2.Comment comment = factory.comment(((javax.xml.stream.events.Comment)event).getText());
          
          if (current == null) {
            document.addContent(comment);
          } else {
            current.addContent(comment);
          }
        } else if (event.isEntityReference()) {
          current.addContent(factory.entityRef(((EntityReference)event).getName()));
        }
        else if (event.isProcessingInstruction()) {
          org.jdom2.ProcessingInstruction pi = factory.processingInstruction(((javax.xml.stream.events.ProcessingInstruction)event).getTarget(), ((javax.xml.stream.events.ProcessingInstruction)event).getData());
          

          if (current == null) {
            document.addContent(pi);
          } else {
            current.addContent(pi);
          }
        } else if (event.isEndElement()) {
          current = current.getParentElement();
        }
        if (!events.hasNext()) break;
        event = events.nextEvent();
      }
      


      return document;
    } catch (XMLStreamException xse) {
      throw new JDOMException("Unable to process XMLStream. See Cause.", xse);
    }
  }
  
  private static final Element processElement(JDOMFactory factory, StartElement event)
  {
    QName qname = event.getName();
    
    Element element = factory.element(qname.getLocalPart(), org.jdom2.Namespace.getNamespace(qname.getPrefix(), qname.getNamespaceURI()));
    


    Iterator<?> it = event.getAttributes();
    while (it.hasNext())
    {
      Attribute att = (Attribute)it.next();
      

      QName aqname = att.getName();
      
      org.jdom2.Namespace attNs = org.jdom2.Namespace.getNamespace(aqname.getPrefix(), aqname.getNamespaceURI());
      

      factory.setAttribute(element, factory.attribute(aqname.getLocalPart(), att.getValue(), AttributeType.getAttributeType(att.getDTDType()), attNs));
    }
    


    for (Iterator<?> it = event.getNamespaces(); it.hasNext();) {
      javax.xml.stream.events.Namespace ns = (javax.xml.stream.events.Namespace)it.next();
      

      element.addNamespaceDeclaration(org.jdom2.Namespace.getNamespace(ns.getPrefix(), ns.getNamespaceURI()));
    }
    

    return element;
  }
  



  private JDOMFactory factory = new DefaultJDOMFactory();
  



  public JDOMFactory getFactory()
  {
    return factory;
  }
  





  public void setFactory(JDOMFactory factory)
  {
    this.factory = factory;
  }
  








  public Document build(XMLEventReader events)
    throws JDOMException
  {
    return process(factory, events);
  }
}
