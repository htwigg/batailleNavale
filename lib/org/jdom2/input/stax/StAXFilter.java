package org.jdom2.input.stax;

import org.jdom2.Namespace;

public abstract interface StAXFilter
{
  public abstract boolean includeDocType();
  
  public abstract boolean includeElement(int paramInt, String paramString, Namespace paramNamespace);
  
  public abstract String includeComment(int paramInt, String paramString);
  
  public abstract boolean includeEntityRef(int paramInt, String paramString);
  
  public abstract String includeCDATA(int paramInt, String paramString);
  
  public abstract String includeText(int paramInt, String paramString);
  
  public abstract boolean includeProcessingInstruction(int paramInt, String paramString);
  
  public abstract boolean pruneElement(int paramInt, String paramString, Namespace paramNamespace);
  
  public abstract String pruneComment(int paramInt, String paramString);
  
  public abstract boolean pruneEntityRef(int paramInt, String paramString);
  
  public abstract String pruneCDATA(int paramInt, String paramString);
  
  public abstract String pruneText(int paramInt, String paramString);
  
  public abstract boolean pruneProcessingInstruction(int paramInt, String paramString);
}
