package org.jdom2.input;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.xml.sax.SAXParseException;












































































public class JDOMParseException
  extends JDOMException
{
  private static final long serialVersionUID = 200L;
  private final Document partialDocument;
  
  public JDOMParseException(String message, Throwable cause)
  {
    this(message, cause, null);
  }
  













  public JDOMParseException(String message, Throwable cause, Document partialDocument)
  {
    super(message, cause);
    this.partialDocument = partialDocument;
  }
  





  public Document getPartialDocument()
  {
    return partialDocument;
  }
  






  public String getPublicId()
  {
    return (getCause() instanceof SAXParseException) ? ((SAXParseException)getCause()).getPublicId() : null;
  }
  







  public String getSystemId()
  {
    return (getCause() instanceof SAXParseException) ? ((SAXParseException)getCause()).getSystemId() : null;
  }
  









  public int getLineNumber()
  {
    return (getCause() instanceof SAXParseException) ? ((SAXParseException)getCause()).getLineNumber() : -1;
  }
  









  public int getColumnNumber()
  {
    return (getCause() instanceof SAXParseException) ? ((SAXParseException)getCause()).getColumnNumber() : -1;
  }
}
