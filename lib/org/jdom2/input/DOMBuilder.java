package org.jdom2.input;

import org.jdom2.*;

import java.util.HashMap;






















































































public class DOMBuilder
{
  private JDOMFactory factory = new DefaultJDOMFactory();
  





  public DOMBuilder() {}
  





  public void setFactory(JDOMFactory factory)
  {
    this.factory = factory;
  }
  



  public JDOMFactory getFactory()
  {
    return factory;
  }
  





  public org.jdom2.Document build(org.w3c.dom.Document domDocument)
  {
    org.jdom2.Document doc = factory.document(null);
    buildTree(domDocument, doc, null, true);
    return doc;
  }
  





  public org.jdom2.Element build(org.w3c.dom.Element domElement)
  {
    org.jdom2.Document doc = factory.document(null);
    buildTree(domElement, doc, null, true);
    return doc.getRootElement();
  }
  






  public CDATA build(CDATASection cdata)
  {
    return factory.cdata(cdata.getNodeValue());
  }
  






  public org.jdom2.Text build(org.w3c.dom.Text text)
  {
    return factory.text(text.getNodeValue());
  }
  






  public org.jdom2.Comment build(org.w3c.dom.Comment comment)
  {
    return factory.comment(comment.getNodeValue());
  }
  






  public org.jdom2.ProcessingInstruction build(org.w3c.dom.ProcessingInstruction pi)
  {
    return factory.processingInstruction(pi.getTarget(), pi.getData());
  }
  






  public EntityRef build(EntityReference er)
  {
    return factory.entityRef(er.getNodeName());
  }
  






  public DocType build(DocumentType doctype)
  {
    String publicID = doctype.getPublicId();
    String systemID = doctype.getSystemId();
    String internalDTD = doctype.getInternalSubset();
    
    DocType docType = factory.docType(doctype.getName());
    docType.setPublicID(publicID);
    docType.setSystemID(systemID);
    docType.setInternalSubset(internalDTD);
    return docType;
  }
  















  private void buildTree(Node node, org.jdom2.Document doc, org.jdom2.Element current, boolean atRoot)
  {
    switch (node.getNodeType()) {
    case 9: 
      NodeList nodes = node.getChildNodes();
      int i = 0; for (int size = nodes.getLength(); i < size; i++) {
        buildTree(nodes.item(i), doc, current, true);
      }
      break;
    
    case 1: 
      String nodeName = node.getNodeName();
      String prefix = "";
      String localName = nodeName;
      int colon = nodeName.indexOf(':');
      if (colon >= 0) {
        prefix = nodeName.substring(0, colon);
        localName = nodeName.substring(colon + 1);
      }
      

      Namespace ns = null;
      String uri = node.getNamespaceURI();
      if (uri == null) {
        ns = current == null ? Namespace.NO_NAMESPACE : current.getNamespace(prefix);
      }
      else
      {
        ns = Namespace.getNamespace(prefix, uri);
      }
      
      org.jdom2.Element element = factory.element(localName, ns);
      
      if (atRoot)
      {
        factory.setRoot(doc, element);
      }
      else {
        factory.addContent(current, element);
      }
      

      NamedNodeMap attributeList = node.getAttributes();
      int attsize = attributeList.getLength();
      
      for (int i = 0; i < attsize; i++) {
        Attr att = (Attr)attributeList.item(i);
        
        String attname = att.getName();
        if (attname.startsWith("xmlns")) {
          String attPrefix = "";
          colon = attname.indexOf(':');
          if (colon >= 0) {
            attPrefix = attname.substring(colon + 1);
          }
          
          String attvalue = att.getValue();
          
          Namespace declaredNS = Namespace.getNamespace(attPrefix, attvalue);
          






          if (prefix.equals(attPrefix))
          {



            element.setNamespace(declaredNS);
          }
          else {
            factory.addNamespaceDeclaration(element, declaredNS);
          }
        }
      }
      

      for (int i = 0; i < attsize; i++) {
        Attr att = (Attr)attributeList.item(i);
        
        String attname = att.getName();
        
        if (!attname.startsWith("xmlns")) {
          String attPrefix = "";
          String attLocalName = attname;
          colon = attname.indexOf(':');
          if (colon >= 0) {
            attPrefix = attname.substring(0, colon);
            attLocalName = attname.substring(colon + 1);
          }
          
          String attvalue = att.getValue();
          

          Namespace attNS = null;
          String attURI = att.getNamespaceURI();
          if ((attPrefix.isEmpty()) && ((attURI == null) || ("".equals(attURI)))) {
            attNS = Namespace.NO_NAMESPACE;









          }
          else if (attPrefix.length() > 0)
          {





            if (attURI == null)
            {





              attNS = element.getNamespace(attPrefix);
            } else {
              attNS = Namespace.getNamespace(attPrefix, attURI);
            }
            

          }
          else
          {
            HashMap<String, Namespace> tmpmap = new HashMap();
            for (Namespace nss : element.getNamespacesInScope()) {
              if ((nss.getPrefix().length() > 0) && (nss.getURI().equals(attURI))) {
                attNS = nss;
                break;
              }
              tmpmap.put(nss.getPrefix(), nss);
            }
            if (attNS == null)
            {










              int cnt = 0;
              String base = "attns";
              String pfx = base + cnt;
              while (tmpmap.containsKey(pfx)) {
                cnt++;
                pfx = base + cnt;
              }
              attNS = Namespace.getNamespace(pfx, attURI);
            }
          }
          

          Attribute attribute = factory.attribute(attLocalName, attvalue, attNS);
          
          factory.setAttribute(element, attribute);
        }
      }
      



      NodeList children = node.getChildNodes();
      if (children != null) {
        int size = children.getLength();
        for (int i = 0; i < size; i++) {
          Node item = children.item(i);
          if (item != null)
            buildTree(item, doc, element, false);
        }
      }
      break;
    

    case 3: 
      factory.addContent(current, build((org.w3c.dom.Text)node));
      break;
    
    case 4: 
      factory.addContent(current, build((CDATASection)node));
      break;
    

    case 7: 
      if (atRoot) {
        factory.addContent(doc, build((org.w3c.dom.ProcessingInstruction)node));
      } else {
        factory.addContent(current, build((org.w3c.dom.ProcessingInstruction)node));
      }
      break;
    
    case 8: 
      if (atRoot) {
        factory.addContent(doc, build((org.w3c.dom.Comment)node));
      } else {
        factory.addContent(current, build((org.w3c.dom.Comment)node));
      }
      break;
    
    case 5: 
      factory.addContent(current, build((EntityReference)node));
      break;
    

    case 6: 
      break;
    

    case 10: 
      factory.addContent(doc, build((DocumentType)node));
    }
  }
}
