package org.jdom2.input;

import org.jdom2.*;
import org.jdom2.input.stax.DTDParser;
import org.jdom2.input.stax.StAXFilter;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.ArrayList;
import java.util.List;
































































































public class StAXStreamBuilder
{
  public StAXStreamBuilder() {}
  
  private static final Document process(JDOMFactory factory, XMLStreamReader stream)
    throws JDOMException
  {
    try
    {
      int state = stream.getEventType();
      
      if (7 != state) {
        throw new JDOMException("JDOM requires that XMLStreamReaders are at their beginning when being processed.");
      }
      

      Document document = factory.document(null);
      
      while (state != 8) {
        switch (state)
        {

        case 7: 
          document.setBaseURI(stream.getLocation().getSystemId());
          document.setProperty("ENCODING_SCHEME", stream.getCharacterEncodingScheme());
          
          document.setProperty("STANDALONE", String.valueOf(stream.isStandalone()));
          
          document.setProperty("ENCODING", stream.getEncoding());
          
          break;
        
        case 11: 
          document.setDocType(DTDParser.parse(stream.getText(), factory));
          
          break;
        
        case 1: 
          document.setRootElement(processElementFragment(factory, stream));
          break;
        
        case 2: 
          throw new JDOMException("Unexpected XMLStream event at Document level: END_ELEMENT");
        case 9: 
          throw new JDOMException("Unexpected XMLStream event at Document level: ENTITY_REFERENCE");
        case 12: 
          throw new JDOMException("Unexpected XMLStream event at Document level: CDATA");
        
        case 6: 
          break;
        
        case 4: 
          String badtxt = stream.getText();
          if (!Verifier.isAllXMLWhitespace(badtxt)) {
            throw new JDOMException("Unexpected XMLStream event at Document level: CHARACTERS (" + badtxt + ")");
          }
          

          break;
        case 5: 
          document.addContent(factory.comment(stream.getText()));
          
          break;
        
        case 3: 
          document.addContent(factory.processingInstruction(stream.getPITarget(), stream.getPIData()));
          
          break;
        case 8: case 10: 
        default: 
          throw new JDOMException("Unexpected XMLStream event " + state);
        }
        
        if (stream.hasNext()) {
          state = stream.next();
        } else {
          throw new JDOMException("Unexpected end-of-XMLStreamReader");
        }
      }
      return document;
    } catch (XMLStreamException xse) {
      throw new JDOMException("Unable to process XMLStream. See Cause.", xse);
    }
  }
  
  private List<Content> processFragments(JDOMFactory factory, XMLStreamReader stream, StAXFilter filter) throws JDOMException
  {
    int state = stream.getEventType();
    
    if (7 != state) {
      throw new JDOMException("JDOM requires that XMLStreamReaders are at their beginning when being processed.");
    }
    
    List<Content> ret = new ArrayList();
    
    int depth = 0;
    String text = null;
    try
    {
      while ((stream.hasNext()) && ((state = stream.next()) != 8)) {
        switch (state) {
        case 7: 
          throw new JDOMException("Illegal state for XMLStreamReader. Cannot get XML Fragment for state START_DOCUMENT");
        case 8: 
          throw new JDOMException("Illegal state for XMLStreamReader. Cannot get XML Fragment for state END_DOCUMENT");
        case 2: 
          throw new JDOMException("Illegal state for XMLStreamReader. Cannot get XML Fragment for state END_ELEMENT");
        
        case 1: 
          QName qn = stream.getName();
          if (filter.includeElement(depth, qn.getLocalPart(), Namespace.getNamespace(qn.getPrefix(), qn.getNamespaceURI())))
          {
            ret.add(processPrunableElement(factory, stream, depth, filter));
          } else {
            int back = depth;
            depth++;
            
            while ((depth > back) && (stream.hasNext())) {
              state = stream.next();
              if (state == 1) {
                depth++;
              } else if (state == 2) {
                depth--;
              }
            }
          }
          break;
        
        case 11: 
          if (filter.includeDocType()) {
            ret.add(DTDParser.parse(stream.getText(), factory));
          }
          
          break;
        case 12: 
          if ((text = filter.includeCDATA(depth, stream.getText())) != null) {
            ret.add(factory.cdata(text));
          }
          
          break;
        case 4: 
        case 6: 
          if ((text = filter.includeText(depth, stream.getText())) != null) {
            ret.add(factory.text(text));
          }
          
          break;
        case 5: 
          if ((text = filter.includeComment(depth, stream.getText())) != null) {
            ret.add(factory.comment(text));
          }
          
          break;
        case 9: 
          if (filter.includeEntityRef(depth, stream.getLocalName())) {
            ret.add(factory.entityRef(stream.getLocalName()));
          }
          
          break;
        case 3: 
          if (filter.includeProcessingInstruction(depth, stream.getPITarget())) {
            ret.add(factory.processingInstruction(stream.getPITarget(), stream.getPIData()));
          }
          
          break;
        case 10: 
        default: 
          throw new JDOMException("Unexpected XMLStream event " + stream.getEventType());
        }
      }
    } catch (XMLStreamException e) {
      throw new JDOMException("Unable to process fragments from XMLStreamReader.", e);
    }
    
    return ret;
  }
  


  private static final Element processPrunableElement(JDOMFactory factory, XMLStreamReader reader, int topdepth, StAXFilter filter)
    throws XMLStreamException, JDOMException
  {
    if (1 != reader.getEventType()) {
      throw new JDOMException("JDOM requires that the XMLStreamReader is at the START_ELEMENT state when retrieving an Element Fragment.");
    }
    


    Element fragment = processElement(factory, reader);
    Element current = fragment;
    int depth = topdepth + 1;
    String text = null;
    while ((depth > topdepth) && (reader.hasNext())) {
      switch (reader.next()) {
      case 1: 
        QName qn = reader.getName();
        if (!filter.pruneElement(depth, qn.getLocalPart(), Namespace.getNamespace(qn.getPrefix(), qn.getNamespaceURI())))
        {

          Element tmp = processElement(factory, reader);
          current.addContent(tmp);
          current = tmp;
          depth++;
        } else {
          int edepth = depth;
          depth++;
          int state = 0;
          while ((depth > edepth) && (reader.hasNext()) && ((state = reader.next()) != 8))
          {
            if (state == 1) {
              depth++;
            } else if (state == 2) {
              depth--;
            }
          }
        }
        break;
      case 2: 
        current = current.getParentElement();
        depth--;
        break;
      case 12: 
        if ((text = filter.pruneCDATA(depth, reader.getText())) != null) {
          current.addContent(factory.cdata(text));
        }
        
        break;
      case 4: 
      case 6: 
        if ((text = filter.pruneText(depth, reader.getText())) != null) {
          current.addContent(factory.text(text));
        }
        
        break;
      case 5: 
        if ((text = filter.pruneComment(depth, reader.getText())) != null) {
          current.addContent(factory.comment(text));
        }
        
        break;
      case 9: 
        if (!filter.pruneEntityRef(depth, reader.getLocalName())) {
          current.addContent(factory.entityRef(reader.getLocalName()));
        }
        
        break;
      case 3: 
        if (!filter.pruneProcessingInstruction(depth, reader.getPITarget())) {
          current.addContent(factory.processingInstruction(reader.getPITarget(), reader.getPIData()));
        }
        break;
      case 7: case 8: 
      case 10: case 11: 
      default: 
        throw new JDOMException("Unexpected XMLStream event " + reader.getEventType());
      }
      
    }
    
    return fragment;
  }
  









  private static final Content processFragment(JDOMFactory factory, XMLStreamReader stream)
    throws JDOMException
  {
    try
    {
      switch (stream.getEventType())
      {
      case 7: 
        throw new JDOMException("Illegal state for XMLStreamReader. Cannot get XML Fragment for state START_DOCUMENT");
      case 8: 
        throw new JDOMException("Illegal state for XMLStreamReader. Cannot get XML Fragment for state END_DOCUMENT");
      case 2: 
        throw new JDOMException("Illegal state for XMLStreamReader. Cannot get XML Fragment for state END_ELEMENT");
      
      case 1: 
        Element emt = processElementFragment(factory, stream);
        stream.next();
        return emt;
      
      case 11: 
        Content dt = DTDParser.parse(stream.getText(), factory);
        stream.next();
        return dt;
      
      case 12: 
        Content cd = factory.cdata(stream.getText());
        stream.next();
        return cd;
      
      case 4: 
      case 6: 
        Content txt = factory.text(stream.getText());
        stream.next();
        return txt;
      
      case 5: 
        Content comment = factory.comment(stream.getText());
        stream.next();
        return comment;
      
      case 9: 
        Content er = factory.entityRef(stream.getLocalName());
        stream.next();
        return er;
      
      case 3: 
        Content pi = factory.processingInstruction(stream.getPITarget(), stream.getPIData());
        
        stream.next();
        return pi;
      }
      
      throw new JDOMException("Unexpected XMLStream event " + stream.getEventType());
    }
    catch (XMLStreamException xse)
    {
      throw new JDOMException("Unable to process XMLStream. See Cause.", xse);
    }
  }
  
  private static final Element processElementFragment(JDOMFactory factory, XMLStreamReader reader)
    throws XMLStreamException, JDOMException
  {
    if (1 != reader.getEventType()) {
      throw new JDOMException("JDOM requires that the XMLStreamReader is at the START_ELEMENT state when retrieving an Element Fragment.");
    }
    


    Element fragment = processElement(factory, reader);
    Element current = fragment;
    int depth = 1;
    while ((depth > 0) && (reader.hasNext())) {
      switch (reader.next()) {
      case 1: 
        Element tmp = processElement(factory, reader);
        current.addContent(tmp);
        current = tmp;
        depth++;
        break;
      case 2: 
        current = current.getParentElement();
        depth--;
        break;
      case 12: 
        current.addContent(factory.cdata(reader.getText()));
        break;
      
      case 4: 
      case 6: 
        current.addContent(factory.text(reader.getText()));
        break;
      
      case 5: 
        current.addContent(factory.comment(reader.getText()));
        break;
      
      case 9: 
        current.addContent(factory.entityRef(reader.getLocalName()));
        break;
      
      case 3: 
        current.addContent(factory.processingInstruction(reader.getPITarget(), reader.getPIData()));
        
        break;
      case 7: case 8: case 10: 
      case 11: default: 
        throw new JDOMException("Unexpected XMLStream event " + reader.getEventType());
      }
      
    }
    
    return fragment;
  }
  

  private static final Element processElement(JDOMFactory factory, XMLStreamReader reader)
  {
    Element element = factory.element(reader.getLocalName(), Namespace.getNamespace(reader.getPrefix(), reader.getNamespaceURI()));
    



    int i = 0; for (int len = reader.getAttributeCount(); i < len; i++) {
      factory.setAttribute(element, factory.attribute(reader.getAttributeLocalName(i), reader.getAttributeValue(i), AttributeType.getAttributeType(reader.getAttributeType(i)), Namespace.getNamespace(reader.getAttributePrefix(i), reader.getAttributeNamespace(i))));
    }
    






    int i = 0; for (int len = reader.getNamespaceCount(); i < len; i++) {
      element.addNamespaceDeclaration(Namespace.getNamespace(reader.getNamespacePrefix(i), reader.getNamespaceURI(i)));
    }
    

    return element;
  }
  


  private JDOMFactory builderfactory = new DefaultJDOMFactory();
  



  public JDOMFactory getFactory()
  {
    return builderfactory;
  }
  





  public void setFactory(JDOMFactory factory)
  {
    builderfactory = factory;
  }
  








  public Document build(XMLStreamReader reader)
    throws JDOMException
  {
    return process(builderfactory, reader);
  }
  






  public List<Content> buildFragments(XMLStreamReader reader, StAXFilter filter)
    throws JDOMException
  {
    return processFragments(builderfactory, reader, filter);
  }
  








  public Content fragment(XMLStreamReader reader)
    throws JDOMException
  {
    return processFragment(builderfactory, reader);
  }
}
