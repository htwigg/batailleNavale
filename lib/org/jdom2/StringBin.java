package org.jdom2;

import org.jdom2.internal.ArrayCopy;











































































































final class StringBin
{
  private static final int GROW = 4;
  private static final int DEFAULTCAP = 1023;
  private static final int MAXBUCKET = 64;
  private String[][] buckets;
  private int[] lengths;
  private int mask = 0;
  


  public StringBin()
  {
    this(1023);
  }
  



  public StringBin(int capacity)
  {
    if (capacity < 0) {
      throw new IllegalArgumentException("Can not have a negative capacity");
    }
    capacity--;
    if (capacity < 1023) {
      capacity = 1023;
    }
    
    capacity /= 3;
    int shift = 0;
    while (capacity != 0) {
      capacity >>>= 1;
      shift++;
    }
    mask = ((1 << shift) - 1);
    buckets = new String[mask + 1][];
    lengths = new int[buckets.length];
  }
  









  private final int locate(int hash, String value, String[] bucket, int length)
  {
    int left = 0;
    int right = length - 1;
    int mid = 0;
    while (left <= right) {
      mid = left + right >>> 1;
      if (bucket[mid].hashCode() > hash) {
        right = mid - 1;
      } else if (bucket[mid].hashCode() < hash) {
        left = mid + 1;
      }
      else
      {
        int cmp = value.compareTo(bucket[mid]);
        if (cmp == 0)
        {
          return mid; }
        if (cmp < 0)
        {
          do {
            mid--; if ((mid < left) || (bucket[mid].hashCode() != hash)) {
              break;
            }
            cmp = value.compareTo(bucket[mid]);
            if (cmp == 0)
            {
              return mid; }
          } while (cmp <= 0);
          





          return -(mid + 1) - 1;
          



          return -(mid + 1) - 1;
        }
        do
        {
          mid++; if ((mid > right) || (bucket[mid].hashCode() != hash))
            break;
          cmp = value.compareTo(bucket[mid]);
          if (cmp == 0)
          {
            return mid; }
        } while (cmp >= 0);
        





        return -mid - 1;
        



        return -mid - 1;
      }
    }
    

    return -left - 1;
  }
  







  public String reuse(String value)
  {
    if (value == null) {
      return null;
    }
    int hash = value.hashCode();
    





























    int bucketid = (hash >>> 16 ^ hash) & mask;
    
    int length = lengths[bucketid];
    if (length == 0)
    {
      String v = compact(value);
      buckets[bucketid] = new String[4];
      buckets[bucketid][0] = v;
      lengths[bucketid] = 1;
      return v;
    }
    

    String[] bucket = buckets[bucketid];
    

    int ip = -locate(hash, value, bucket, length) - 1;
    if (ip < 0)
    {
      return bucket[(-ip - 1)];
    }
    if (length >= 64)
    {
      rehash();
      return reuse(value);
    }
    if (length == bucket.length)
    {
      bucket = (String[])ArrayCopy.copyOf(bucket, length + 4);
      buckets[bucketid] = bucket;
    }
    System.arraycopy(bucket, ip, bucket, ip + 1, length - ip);
    String v = compact(value);
    bucket[ip] = v;
    lengths[bucketid] += 1;
    return v;
  }
  
















  private void rehash()
  {
    String[][] olddata = buckets;
    
    mask = ((mask + 1 << 2) - 1);
    buckets = new String[mask + 1][];
    lengths = new int[buckets.length];
    int hash = 0;int bucketid = 0;int length = 0;
    for (String[] ob : olddata) {
      if (ob != null)
      {


        for (String val : ob) {
          if (val == null) {
            break;
          }
          
          hash = val.hashCode();
          bucketid = (hash >>> 16 ^ hash) & mask;
          length = lengths[bucketid];
          if (length == 0) {
            buckets[bucketid] = new String[(ob.length + 4) / 4];
            buckets[bucketid][0] = val;
          } else {
            if (buckets[bucketid].length == length) {
              buckets[bucketid] = ((String[])ArrayCopy.copyOf(buckets[bucketid], lengths[bucketid] + 4));
            }
            
            buckets[bucketid][length] = val;
          }
          lengths[bucketid] += 1;
        }
      }
    }
  }
  








  private static final String compact(String input)
  {
    return new String(input.toCharArray());
  }
  



  public int size()
  {
    int sum = 0;
    for (int l : lengths) {
      sum += l;
    }
    return sum;
  }
}
