package org.jdom2;

import org.jdom2.internal.ArrayCopy;
import org.jdom2.util.IteratorIterable;

import java.util.Iterator;








































































final class DescendantIterator
  implements IteratorIterable<Content>
{
  private final Parent parent;
  private Object[] stack = new Object[16];
  private int ssize = 0;
  

  private Iterator<Content> current = null;
  
  private Iterator<Content> descending = null;
  
  private Iterator<Content> ascending = null;
  
  private boolean hasnext = true;
  




  DescendantIterator(Parent parent)
  {
    this.parent = parent;
    
    current = parent.getContent().iterator();
    hasnext = current.hasNext();
  }
  

  public DescendantIterator iterator()
  {
    return new DescendantIterator(parent);
  }
  





  public boolean hasNext()
  {
    return hasnext;
  }
  






  public Content next()
  {
    if (descending != null) {
      current = descending;
      descending = null;
    } else if (ascending != null) {
      current = ascending;
      ascending = null;
    }
    
    Content ret = (Content)current.next();
    


    if (((ret instanceof Element)) && (((Element)ret).getContentSize() > 0))
    {

      descending = ((Element)ret).getContent().iterator();
      if (ssize >= stack.length) {
        stack = ArrayCopy.copyOf(stack, ssize + 16);
      }
      stack[(ssize++)] = current;
      return ret;
    }
    
    if (current.hasNext())
    {
      return ret;
    }
    

    while (ssize > 0)
    {



      Iterator<Content> subit = (Iterator)stack[(--ssize)];
      ascending = subit;
      stack[ssize] = null;
      if (ascending.hasNext()) {
        return ret;
      }
    }
    
    ascending = null;
    hasnext = false;
    return ret;
  }
  






  public void remove()
  {
    current.remove();
    

    descending = null;
    if ((current.hasNext()) || (ascending != null))
    {
      return;
    }
    


    while (ssize > 0)
    {
      Iterator<Content> subit = (Iterator)stack[(--ssize)];
      stack[ssize] = null;
      ascending = subit;
      if (ascending.hasNext()) {
        return;
      }
    }
    ascending = null;
    hasnext = false;
  }
}
