package org.jdom2;




































public class DataConversionException
  extends JDOMException
{
  private static final long serialVersionUID = 200L;
  


































  public DataConversionException(String name, String dataType)
  {
    super("The XML construct " + name + " could not be converted to a " + dataType);
  }
}
