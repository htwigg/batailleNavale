package org.jdom2;





































































public enum AttributeType
{
  UNDECLARED, 
  





  CDATA, 
  





  ID, 
  






  IDREF, 
  






  IDREFS, 
  




  ENTITY, 
  






  ENTITIES, 
  










  NMTOKEN, 
  




  NMTOKENS, 
  




  NOTATION, 
  





  ENUMERATION;
  



  private AttributeType() {}
  



  @Deprecated
  public static final AttributeType byIndex(int index)
  {
    if (index < 0) {
      throw new IllegalDataException("No such AttributeType " + index);
    }
    if (index >= values().length) {
      throw new IllegalDataException("No such AttributeType " + index + ", max is " + (values().length - 1));
    }
    
    return values()[index];
  }
  











  public static final AttributeType getAttributeType(String typeName)
  {
    if (typeName == null) {
      return UNDECLARED;
    }
    try
    {
      return valueOf(typeName);
    } catch (IllegalArgumentException iae) {
      if ((typeName.length() > 0) && (typeName.trim().charAt(0) == '('))
      {



        return ENUMERATION; }
    }
    return UNDECLARED;
  }
}
