package org.jdom2.adapters;

import org.jdom2.JDOMException;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;




































































public class JAXPDOMAdapter
  extends AbstractDOMAdapter
{
  private static final ThreadLocal<DocumentBuilder> localbuilder = new ThreadLocal();
  




  public JAXPDOMAdapter() {}
  



  public Document createDocument()
    throws JDOMException
  {
    DocumentBuilder db = (DocumentBuilder)localbuilder.get();
    if (db == null) {
      try {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        db = dbf.newDocumentBuilder();
        localbuilder.set(db);
      } catch (ParserConfigurationException e) {
        throw new JDOMException("Unable to obtain a DOM parser. See cause:", e);
      }
    }
    return db.newDocument();
  }
}
