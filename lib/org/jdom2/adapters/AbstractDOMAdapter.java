package org.jdom2.adapters;

import org.jdom2.DocType;
import org.jdom2.JDOMException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;









































































public abstract class AbstractDOMAdapter
  implements DOMAdapter
{
  public AbstractDOMAdapter() {}
  
  public Document createDocument(DocType doctype)
    throws JDOMException
  {
    if (doctype == null) {
      return createDocument();
    }
    
    DOMImplementation domImpl = createDocument().getImplementation();
    DocumentType domDocType = domImpl.createDocumentType(doctype.getElementName(), doctype.getPublicID(), doctype.getSystemID());
    




    setInternalSubset(domDocType, doctype.getInternalSubset());
    
    Document ret = domImpl.createDocument("http://temporary", doctype.getElementName(), domDocType);
    


    Element root = ret.getDocumentElement();
    if (root != null) {
      ret.removeChild(root);
    }
    
    return ret;
  }
  








  protected void setInternalSubset(DocumentType dt, String s)
  {
    if ((dt == null) || (s == null)) { return;
    }
    

    try
    {
      Class<? extends DocumentType> dtclass = dt.getClass();
      Method setInternalSubset = dtclass.getMethod("setInternalSubset", new Class[] { String.class });
      
      setInternalSubset.invoke(dt, new Object[] { s });
    }
    catch (InvocationTargetException e) {}catch (IllegalAccessException e) {}catch (SecurityException e) {}catch (NoSuchMethodException e) {}
  }
}
