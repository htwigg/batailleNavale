package org.jdom2.adapters;

import org.jdom2.DocType;
import org.jdom2.JDOMException;
import org.w3c.dom.Document;

public abstract interface DOMAdapter
{
  public abstract Document createDocument()
    throws JDOMException;
  
  public abstract Document createDocument(DocType paramDocType)
    throws JDOMException;
}
