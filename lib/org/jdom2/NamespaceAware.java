package org.jdom2;

import java.util.List;

public abstract interface NamespaceAware
{
  public abstract List<Namespace> getNamespacesInScope();
  
  public abstract List<Namespace> getNamespacesIntroduced();
  
  public abstract List<Namespace> getNamespacesInherited();
}
