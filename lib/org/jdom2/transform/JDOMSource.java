package org.jdom2.transform;

import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.output.SAXOutputter;
import org.jdom2.output.XMLOutputter;

import javax.xml.transform.sax.SAXSource;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;












































































































public class JDOMSource
  extends SAXSource
{
  public static final String JDOM_FEATURE = "http://jdom.org/jdom2/transform/JDOMSource/feature";
  private XMLReader xmlReader = null;
  







  private EntityResolver resolver = null;
  








  public JDOMSource(Document source)
  {
    this(source, null);
  }
  








  public JDOMSource(List<? extends Content> source)
  {
    setNodes(source);
  }
  








  public JDOMSource(Element source)
  {
    List<Content> nodes = new ArrayList();
    nodes.add(source);
    
    setNodes(nodes);
  }
  












  public JDOMSource(Document source, EntityResolver resolver)
  {
    setDocument(source);
    this.resolver = resolver;
    if ((source != null) && (source.getBaseURI() != null)) {
      super.setSystemId(source.getBaseURI());
    }
  }
  










  public void setDocument(Document source)
  {
    super.setInputSource(new JDOMInputSource(source));
  }
  







  public Document getDocument()
  {
    Object src = ((JDOMInputSource)getInputSource()).getSource();
    Document doc = null;
    
    if ((src instanceof Document)) {
      doc = (Document)src;
    }
    return doc;
  }
  










  public void setNodes(List<? extends Content> source)
  {
    super.setInputSource(new JDOMInputSource(source));
  }
  







  public List<? extends Content> getNodes()
  {
    return ((JDOMInputSource)getInputSource()).getListSource();
  }
  

















  public void setInputSource(InputSource inputSource)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException();
  }
  

















  public void setXMLReader(XMLReader reader)
    throws UnsupportedOperationException
  {
    if ((reader instanceof XMLFilter))
    {
      XMLFilter filter = (XMLFilter)reader;
      while ((filter.getParent() instanceof XMLFilter)) {
        filter = (XMLFilter)filter.getParent();
      }
      filter.setParent(buildDocumentReader());
      

      xmlReader = reader;
    }
    else {
      throw new UnsupportedOperationException();
    }
  }
  










  public XMLReader getXMLReader()
  {
    if (xmlReader == null) {
      xmlReader = buildDocumentReader();
    }
    return xmlReader;
  }
  







  private XMLReader buildDocumentReader()
  {
    DocumentReader reader = new DocumentReader();
    if (resolver != null)
      reader.setEntityResolver(resolver);
    return reader;
  }
  















  private static class JDOMInputSource
    extends InputSource
  {
    private Document docsource = null;
    



    private List<? extends Content> listsource = null;
    



    public JDOMInputSource(Document document)
    {
      docsource = document;
    }
    




    public JDOMInputSource(List<? extends Content> nodes)
    {
      listsource = nodes;
    }
    




    public Object getSource()
    {
      return docsource == null ? listsource : docsource;
    }
    

















    public void setCharacterStream(Reader characterStream)
      throws UnsupportedOperationException
    {
      throw new UnsupportedOperationException();
    }
    













    public Reader getCharacterStream()
    {
      Reader reader = null;
      
      if (docsource != null)
      {

        reader = new StringReader(new XMLOutputter().outputString(docsource));

      }
      else if (listsource != null) {
        reader = new StringReader(new XMLOutputter().outputString(listsource));
      }
      

      return reader;
    }
    












    public void setByteStream(InputStream byteStream)
      throws UnsupportedOperationException
    {
      throw new UnsupportedOperationException();
    }
    
    public Document getDocumentSource() {
      return docsource;
    }
    
    public List<? extends Content> getListSource()
    {
      return listsource;
    }
  }
  
















  private static class DocumentReader
    extends SAXOutputter
    implements XMLReader
  {
    public DocumentReader() {}
    















    public void parse(String systemId)
      throws SAXNotSupportedException
    {
      throw new SAXNotSupportedException("Only JDOM Documents are supported as input");
    }
    
















    public void parse(InputSource input)
      throws SAXException
    {
      if ((input instanceof JDOMSource.JDOMInputSource)) {
        try {
          Document docsource = ((JDOMSource.JDOMInputSource)input).getDocumentSource();
          if (docsource != null) {
            output(docsource);
          }
          else {
            output(((JDOMSource.JDOMInputSource)input).getListSource());
          }
        }
        catch (JDOMException e) {
          throw new SAXException(e.getMessage(), e);
        }
        
      } else {
        throw new SAXNotSupportedException("Only JDOM Documents are supported as input");
      }
    }
  }
}
