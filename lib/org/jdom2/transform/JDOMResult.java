package org.jdom2.transform;

import org.jdom2.*;
import org.jdom2.input.sax.SAXHandler;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.XMLFilterImpl;

import javax.xml.transform.sax.SAXResult;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



































































































public class JDOMResult
  extends SAXResult
{
  public static final String JDOM_FEATURE = "http://jdom.org/jdom2/transform/JDOMResult/feature";
  private List<Content> resultlist = null;
  
  private Document resultdoc = null;
  




  private boolean queried = false;
  




  private JDOMFactory factory = null;
  



  public JDOMResult()
  {
    DocumentBuilder builder = new DocumentBuilder();
    

    super.setHandler(builder);
    super.setLexicalHandler(builder);
  }
  















  public void setResult(List<Content> result)
  {
    resultlist = result;
    queried = false;
  }
  











  public List<Content> getResult()
  {
    List<Content> nodes = Collections.emptyList();
    

    retrieveResult();
    
    if (resultlist != null) {
      nodes = resultlist;

    }
    else if ((resultdoc != null) && (!queried)) {
      List<Content> content = resultdoc.getContent();
      nodes = new ArrayList(content.size());
      
      while (content.size() != 0)
      {
        Content o = (Content)content.remove(0);
        nodes.add(o);
      }
      resultlist = nodes;
      resultdoc = null;
    }
    
    queried = true;
    
    return nodes;
  }
  














  public void setDocument(Document document)
  {
    resultdoc = document;
    resultlist = null;
    queried = false;
  }
  



















  public Document getDocument()
  {
    Document doc = null;
    

    retrieveResult();
    
    if (resultdoc != null) {
      doc = resultdoc;

    }
    else if ((resultlist != null) && (!queried)) {
      try
      {
        JDOMFactory f = getFactory();
        if (f == null) { f = new DefaultJDOMFactory();
        }
        doc = f.document(null);
        doc.setContent(resultlist);
        
        resultdoc = doc;
        resultlist = null;

      }
      catch (RuntimeException ex1)
      {
        return null;
      }
    }
    
    queried = true;
    
    return doc;
  }
  










  public void setFactory(JDOMFactory factory)
  {
    this.factory = factory;
  }
  









  public JDOMFactory getFactory()
  {
    return factory;
  }
  



  private void retrieveResult()
  {
    if ((resultlist == null) && (resultdoc == null)) {
      setResult(((DocumentBuilder)getHandler()).getResult());
    }
  }
  











  public void setHandler(ContentHandler handler) {}
  











  public void setLexicalHandler(LexicalHandler handler) {}
  










  private static class FragmentHandler
    extends SAXHandler
  {
    private Element dummyRoot = new Element("root", null, null);
    



    public FragmentHandler(JDOMFactory factory)
    {
      super();
      



      pushElement(dummyRoot);
    }
    






    public List<Content> getResult()
    {
      try
      {
        flushCharacters();
      }
      catch (SAXException e) {}
      return getDetachedContent(dummyRoot);
    }
    







    private List<Content> getDetachedContent(Element elt)
    {
      List<Content> content = elt.getContent();
      List<Content> nodes = new ArrayList(content.size());
      
      while (content.size() != 0)
      {
        Content o = (Content)content.remove(0);
        nodes.add(o);
      }
      return nodes;
    }
  }
  





  private class DocumentBuilder
    extends XMLFilterImpl
    implements LexicalHandler
  {
    private JDOMResult.FragmentHandler saxHandler = null;
    




    private boolean startDocumentReceived = false;
    





    public DocumentBuilder() {}
    





    public List<Content> getResult()
    {
      List<Content> mresult = null;
      
      if (saxHandler != null)
      {
        mresult = saxHandler.getResult();
        

        saxHandler = null;
        

        startDocumentReceived = false;
      }
      return mresult;
    }
    
    private void ensureInitialization()
      throws SAXException
    {
      if (!startDocumentReceived) {
        startDocument();
      }
    }
    













    public void startDocument()
      throws SAXException
    {
      startDocumentReceived = true;
      

      setResult(null);
      




      saxHandler = new JDOMResult.FragmentHandler(getFactory());
      super.setContentHandler(saxHandler);
      

      super.startDocument();
    }
    
























    public void startElement(String nsURI, String localName, String qName, Attributes atts)
      throws SAXException
    {
      ensureInitialization();
      super.startElement(nsURI, localName, qName, atts);
    }
    




    public void startPrefixMapping(String prefix, String uri)
      throws SAXException
    {
      ensureInitialization();
      super.startPrefixMapping(prefix, uri);
    }
    




    public void characters(char[] ch, int start, int length)
      throws SAXException
    {
      ensureInitialization();
      super.characters(ch, start, length);
    }
    




    public void ignorableWhitespace(char[] ch, int start, int length)
      throws SAXException
    {
      ensureInitialization();
      super.ignorableWhitespace(ch, start, length);
    }
    




    public void processingInstruction(String target, String data)
      throws SAXException
    {
      ensureInitialization();
      super.processingInstruction(target, data);
    }
    



    public void skippedEntity(String name)
      throws SAXException
    {
      ensureInitialization();
      super.skippedEntity(name);
    }
    


















    public void startDTD(String name, String publicId, String systemId)
      throws SAXException
    {
      ensureInitialization();
      saxHandler.startDTD(name, publicId, systemId);
    }
    





    public void endDTD()
      throws SAXException
    {
      saxHandler.endDTD();
    }
    









    public void startEntity(String name)
      throws SAXException
    {
      ensureInitialization();
      saxHandler.startEntity(name);
    }
    







    public void endEntity(String name)
      throws SAXException
    {
      saxHandler.endEntity(name);
    }
    





    public void startCDATA()
      throws SAXException
    {
      ensureInitialization();
      saxHandler.startCDATA();
    }
    





    public void endCDATA()
      throws SAXException
    {
      saxHandler.endCDATA();
    }
    










    public void comment(char[] ch, int start, int length)
      throws SAXException
    {
      ensureInitialization();
      saxHandler.comment(ch, start, length);
    }
  }
}
