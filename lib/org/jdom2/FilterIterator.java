package org.jdom2;

import org.jdom2.filter.Filter;
import org.jdom2.util.IteratorIterable;

import java.util.Iterator;
import java.util.NoSuchElementException;





























































final class FilterIterator<T>
  implements IteratorIterable<T>
{
  private final DescendantIterator iterator;
  private final Filter<T> filter;
  private T nextObject;
  private boolean canremove = false;
  
  public FilterIterator(DescendantIterator iterator, Filter<T> filter)
  {
    if (filter == null) {
      throw new NullPointerException("Cannot specify a null Filter for a FilterIterator");
    }
    
    this.iterator = iterator;
    this.filter = filter;
  }
  
  public Iterator<T> iterator()
  {
    return new FilterIterator(iterator.iterator(), filter);
  }
  

  public boolean hasNext()
  {
    canremove = false;
    
    if (nextObject != null) {
      return true;
    }
    
    while (iterator.hasNext()) {
      Object obj = iterator.next();
      T f = filter.filter(obj);
      if (f != null) {
        nextObject = f;
        return true;
      }
    }
    return false;
  }
  
  public T next()
  {
    if (!hasNext()) {
      throw new NoSuchElementException();
    }
    
    T obj = nextObject;
    nextObject = null;
    canremove = true;
    return obj;
  }
  
  public void remove()
  {
    if (!canremove) {
      throw new IllegalStateException("remove() can only be called on the FilterIterator immediately after a successful call to next(). A call to remove() immediately after a call to hasNext() or remove() will also fail.");
    }
    


    canremove = false;
    iterator.remove();
  }
}
