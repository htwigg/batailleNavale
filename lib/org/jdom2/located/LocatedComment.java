package org.jdom2.located;

import org.jdom2.Comment;



























































public class LocatedComment
  extends Comment
  implements Located
{
  private static final long serialVersionUID = 200L;
  private int line;
  private int col;
  
  public LocatedComment(String text)
  {
    super(text);
  }
  







  public int getLine()
  {
    return line;
  }
  
  public int getColumn()
  {
    return col;
  }
  
  public void setLine(int line)
  {
    this.line = line;
  }
  
  public void setColumn(int col)
  {
    this.col = col;
  }
}
