package org.jdom2.located;

import org.jdom2.ProcessingInstruction;

import java.util.Map;



































































public class LocatedProcessingInstruction
  extends ProcessingInstruction
  implements Located
{
  private static final long serialVersionUID = 200L;
  private int line;
  private int col;
  
  public LocatedProcessingInstruction(String target)
  {
    super(target);
  }
  









  public LocatedProcessingInstruction(String target, Map<String, String> data)
  {
    super(target, data);
  }
  








  public LocatedProcessingInstruction(String target, String data)
  {
    super(target, data);
  }
  








  public int getLine()
  {
    return line;
  }
  
  public int getColumn()
  {
    return col;
  }
  
  public void setLine(int line)
  {
    this.line = line;
  }
  
  public void setColumn(int col)
  {
    this.col = col;
  }
}
