package org.jdom2.located;

import org.jdom2.*;

import java.util.Map;


































































public class LocatedJDOMFactory
  extends DefaultJDOMFactory
{
  public LocatedJDOMFactory() {}
  
  public CDATA cdata(int line, int col, String text)
  {
    LocatedCDATA ret = new LocatedCDATA(text);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  
  public Text text(int line, int col, String text)
  {
    LocatedText ret = new LocatedText(text);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  
  public Comment comment(int line, int col, String text)
  {
    LocatedComment ret = new LocatedComment(text);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  

  public DocType docType(int line, int col, String elementName, String publicID, String systemID)
  {
    LocatedDocType ret = new LocatedDocType(elementName, publicID, systemID);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  

  public DocType docType(int line, int col, String elementName, String systemID)
  {
    LocatedDocType ret = new LocatedDocType(elementName, systemID);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  
  public DocType docType(int line, int col, String elementName)
  {
    LocatedDocType ret = new LocatedDocType(elementName);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  
  public Element element(int line, int col, String name, Namespace namespace)
  {
    LocatedElement ret = new LocatedElement(name, namespace);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  
  public Element element(int line, int col, String name)
  {
    LocatedElement ret = new LocatedElement(name);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  
  public Element element(int line, int col, String name, String uri)
  {
    LocatedElement ret = new LocatedElement(name, uri);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  

  public Element element(int line, int col, String name, String prefix, String uri)
  {
    LocatedElement ret = new LocatedElement(name, prefix, uri);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  

  public ProcessingInstruction processingInstruction(int line, int col, String target)
  {
    LocatedProcessingInstruction ret = new LocatedProcessingInstruction(target);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  

  public ProcessingInstruction processingInstruction(int line, int col, String target, Map<String, String> data)
  {
    LocatedProcessingInstruction ret = new LocatedProcessingInstruction(target, data);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  

  public ProcessingInstruction processingInstruction(int line, int col, String target, String data)
  {
    LocatedProcessingInstruction ret = new LocatedProcessingInstruction(target, data);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  
  public EntityRef entityRef(int line, int col, String name)
  {
    LocatedEntityRef ret = new LocatedEntityRef(name);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  

  public EntityRef entityRef(int line, int col, String name, String publicID, String systemID)
  {
    LocatedEntityRef ret = new LocatedEntityRef(name, publicID, systemID);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
  
  public EntityRef entityRef(int line, int col, String name, String systemID)
  {
    LocatedEntityRef ret = new LocatedEntityRef(name, systemID);
    ret.setLine(line);
    ret.setColumn(col);
    return ret;
  }
}
