package org.jdom2.located;

public abstract interface Located
{
  public abstract int getLine();
  
  public abstract int getColumn();
  
  public abstract void setLine(int paramInt);
  
  public abstract void setColumn(int paramInt);
}
