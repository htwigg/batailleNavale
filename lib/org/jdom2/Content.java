package org.jdom2;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;






































































































public abstract class Content
  extends CloneBase
  implements Serializable, NamespaceAware
{
  private static final long serialVersionUID = 200L;
  
  public static enum CType
  {
    Comment, 
    
    Element, 
    
    ProcessingInstruction, 
    
    EntityRef, 
    
    Text, 
    
    CDATA, 
    
    DocType;
    


    private CType() {}
  }
  

  protected transient Parent parent = null;
  



  protected final CType ctype;
  



  protected Content(CType type)
  {
    ctype = type;
  }
  





  public final CType getCType()
  {
    return ctype;
  }
  









  public Content detach()
  {
    if (parent != null) {
      parent.removeContent(this);
    }
    return this;
  }
  










  public Parent getParent()
  {
    return parent;
  }
  








  public final Element getParentElement()
  {
    Parent pnt = getParent();
    return (Element)((pnt instanceof Element) ? pnt : null);
  }
  










  protected Content setParent(Parent parent)
  {
    this.parent = parent;
    return this;
  }
  





  public Document getDocument()
  {
    if (parent == null) return null;
    return parent.getDocument();
  }
  



  public abstract String getValue();
  



  public Content clone()
  {
    Content c = (Content)super.clone();
    parent = null;
    return c;
  }
  










  public final boolean equals(Object ob)
  {
    return ob == this;
  }
  





  public final int hashCode()
  {
    return super.hashCode();
  }
  

  public List<Namespace> getNamespacesInScope()
  {
    Element emt = getParentElement();
    if (emt == null) {
      return Collections.singletonList(Namespace.XML_NAMESPACE);
    }
    return emt.getNamespacesInScope();
  }
  
  public List<Namespace> getNamespacesIntroduced()
  {
    return Collections.emptyList();
  }
  

  public List<Namespace> getNamespacesInherited()
  {
    return getNamespacesInScope();
  }
}
