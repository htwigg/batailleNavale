package org.jdom2;

import org.jdom2.filter.Filter;
import org.jdom2.util.IteratorIterable;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public abstract interface Parent
  extends Cloneable, NamespaceAware, Serializable
{
  public abstract int getContentSize();
  
  public abstract int indexOf(Content paramContent);
  
  public abstract List<Content> cloneContent();
  
  public abstract Content getContent(int paramInt);
  
  public abstract List<Content> getContent();
  
  public abstract <E extends Content> List<E> getContent(Filter<E> paramFilter);
  
  public abstract List<Content> removeContent();
  
  public abstract <E extends Content> List<E> removeContent(Filter<E> paramFilter);
  
  public abstract boolean removeContent(Content paramContent);
  
  public abstract Content removeContent(int paramInt);
  
  public abstract Object clone();
  
  public abstract IteratorIterable<Content> getDescendants();
  
  public abstract <E extends Content> IteratorIterable<E> getDescendants(Filter<E> paramFilter);
  
  public abstract Parent getParent();
  
  public abstract Document getDocument();
  
  public abstract void canContainContent(Content paramContent, int paramInt, boolean paramBoolean)
    throws IllegalAddException;
  
  public abstract Parent addContent(Content paramContent);
  
  public abstract Parent addContent(Collection<? extends Content> paramCollection);
  
  public abstract Parent addContent(int paramInt, Content paramContent);
  
  public abstract Parent addContent(int paramInt, Collection<? extends Content> paramCollection);
}
