package org.jdom2;

import java.util.Map;
































































public class SlimJDOMFactory
  extends DefaultJDOMFactory
{
  private StringBin cache = new StringBin();
  
  private final boolean cachetext;
  

  public SlimJDOMFactory()
  {
    this(true);
  }
  







  public SlimJDOMFactory(boolean cachetext)
  {
    this.cachetext = cachetext;
  }
  



  public void clearCache()
  {
    cache = new StringBin();
  }
  
  public Attribute attribute(String name, String value, Namespace namespace)
  {
    return super.attribute(cache.reuse(name), cachetext ? cache.reuse(value) : value, namespace);
  }
  



  @Deprecated
  public Attribute attribute(String name, String value, int type, Namespace namespace)
  {
    return super.attribute(cache.reuse(name), cachetext ? cache.reuse(value) : value, type, namespace);
  }
  



  public Attribute attribute(String name, String value, AttributeType type, Namespace namespace)
  {
    return super.attribute(cache.reuse(name), cachetext ? cache.reuse(value) : value, type, namespace);
  }
  


  public Attribute attribute(String name, String value)
  {
    return super.attribute(cache.reuse(name), cachetext ? cache.reuse(value) : value);
  }
  

  @Deprecated
  public Attribute attribute(String name, String value, int type)
  {
    return super.attribute(cache.reuse(name), cachetext ? cache.reuse(value) : value, type);
  }
  


  public Attribute attribute(String name, String value, AttributeType type)
  {
    return super.attribute(cache.reuse(name), cachetext ? cache.reuse(value) : value, type);
  }
  


  public CDATA cdata(int line, int col, String str)
  {
    return super.cdata(line, col, cachetext ? cache.reuse(str) : str);
  }
  
  public Text text(int line, int col, String str)
  {
    return super.text(line, col, cachetext ? cache.reuse(str) : str);
  }
  
  public Comment comment(int line, int col, String text)
  {
    return super.comment(line, col, cachetext ? cache.reuse(text) : text);
  }
  
  public DocType docType(int line, int col, String elementName, String publicID, String systemID)
  {
    return super.docType(line, col, cache.reuse(elementName), publicID, systemID);
  }
  
  public DocType docType(int line, int col, String elementName, String systemID)
  {
    return super.docType(line, col, cache.reuse(elementName), systemID);
  }
  
  public DocType docType(int line, int col, String elementName)
  {
    return super.docType(line, col, cache.reuse(elementName));
  }
  
  public Element element(int line, int col, String name, Namespace namespace)
  {
    return super.element(line, col, cache.reuse(name), namespace);
  }
  
  public Element element(int line, int col, String name)
  {
    return super.element(line, col, cache.reuse(name));
  }
  
  public Element element(int line, int col, String name, String uri)
  {
    return super.element(line, col, cache.reuse(name), uri);
  }
  
  public Element element(int line, int col, String name, String prefix, String uri)
  {
    return super.element(line, col, cache.reuse(name), prefix, uri);
  }
  

  public ProcessingInstruction processingInstruction(int line, int col, String target, Map<String, String> data)
  {
    return super.processingInstruction(line, col, cache.reuse(target), data);
  }
  

  public ProcessingInstruction processingInstruction(int line, int col, String target, String data)
  {
    return super.processingInstruction(line, col, cache.reuse(target), data);
  }
  
  public ProcessingInstruction processingInstruction(int line, int col, String target)
  {
    return super.processingInstruction(line, col, cache.reuse(target));
  }
  
  public EntityRef entityRef(int line, int col, String name)
  {
    return super.entityRef(line, col, cache.reuse(name));
  }
  
  public EntityRef entityRef(int line, int col, String name, String publicID, String systemID)
  {
    return super.entityRef(line, col, cache.reuse(name), publicID, systemID);
  }
  
  public EntityRef entityRef(int line, int col, String name, String systemID)
  {
    return super.entityRef(line, col, cache.reuse(name), systemID);
  }
}
