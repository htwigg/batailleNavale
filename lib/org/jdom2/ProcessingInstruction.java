package org.jdom2;

import org.jdom2.output.XMLOutputter;

import java.util.Map.Entry;




































































public class ProcessingInstruction
  extends Content
{
  private static final long serialVersionUID = 200L;
  protected String target;
  protected String rawData;
  protected transient Map<String, String> mapData = null;
  



  protected ProcessingInstruction()
  {
    super(Content.CType.ProcessingInstruction);
  }
  







  public ProcessingInstruction(String target)
  {
    this(target, "");
  }
  









  public ProcessingInstruction(String target, Map<String, String> data)
  {
    super(Content.CType.ProcessingInstruction);
    setTarget(target);
    setData(data);
  }
  








  public ProcessingInstruction(String target, String data)
  {
    super(Content.CType.ProcessingInstruction);
    setTarget(target);
    setData(data);
  }
  



  public ProcessingInstruction setTarget(String newTarget)
  {
    String reason;
    

    if ((reason = Verifier.checkProcessingInstructionTarget(newTarget)) != null)
    {
      throw new IllegalTargetException(newTarget, reason);
    }
    
    target = newTarget;
    return this;
  }
  






  public String getValue()
  {
    return rawData;
  }
  





  public String getTarget()
  {
    return target;
  }
  




  public String getData()
  {
    return rawData;
  }
  






  public List<String> getPseudoAttributeNames()
  {
    return new ArrayList(mapData.keySet());
  }
  





  public ProcessingInstruction setData(String data)
  {
    String reason = Verifier.checkProcessingInstructionData(data);
    if (reason != null) {
      throw new IllegalDataException(data, reason);
    }
    
    rawData = data;
    mapData = parseData(data);
    return this;
  }
  








  public ProcessingInstruction setData(Map<String, String> data)
  {
    String temp = toString(data);
    
    String reason = Verifier.checkProcessingInstructionData(temp);
    if (reason != null) {
      throw new IllegalDataException(temp, reason);
    }
    
    rawData = temp;
    mapData = new LinkedHashMap(data);
    return this;
  }
  









  public String getPseudoAttributeValue(String name)
  {
    return (String)mapData.get(name);
  }
  








  public ProcessingInstruction setPseudoAttribute(String name, String value)
  {
    String reason = Verifier.checkProcessingInstructionData(name);
    if (reason != null) {
      throw new IllegalDataException(name, reason);
    }
    
    reason = Verifier.checkProcessingInstructionData(value);
    if (reason != null) {
      throw new IllegalDataException(value, reason);
    }
    
    mapData.put(name, value);
    rawData = toString(mapData);
    return this;
  }
  







  public boolean removePseudoAttribute(String name)
  {
    if (mapData.remove(name) != null) {
      rawData = toString(mapData);
      return true;
    }
    
    return false;
  }
  





  private static final String toString(Map<String, String> pmapData)
  {
    StringBuilder stringData = new StringBuilder();
    
    for (Map.Entry<String, String> me : pmapData.entrySet()) {
      stringData.append((String)me.getKey()).append("=\"").append((String)me.getValue()).append("\" ");
    }
    



    if (stringData.length() > 0) {
      stringData.setLength(stringData.length() - 1);
    }
    
    return stringData.toString();
  }
  














  private Map<String, String> parseData(String prawData)
  {
    Map<String, String> data = new LinkedHashMap();
    



    String inputData = prawData.trim();
    

    while (!inputData.trim().equals(""))
    {


      String name = "";
      String value = "";
      int startName = 0;
      char previousChar = inputData.charAt(startName);
      for (int pos = 1; 
          pos < inputData.length(); pos++) {
        char currentChar = inputData.charAt(pos);
        if (currentChar == '=') {
          name = inputData.substring(startName, pos).trim();
          

          int[] bounds = extractQuotedString(inputData.substring(pos + 1));
          

          if (bounds == null) {
            return Collections.emptyMap();
          }
          value = inputData.substring(bounds[0] + pos + 1, bounds[1] + pos + 1);
          
          pos += bounds[1] + 1;
          break;
        }
        if ((Character.isWhitespace(previousChar)) && (!Character.isWhitespace(currentChar)))
        {
          startName = pos;
        }
        
        previousChar = currentChar;
      }
      

      inputData = inputData.substring(pos);
      






      if (name.length() > 0)
      {




        data.put(name, value);
      }
    }
    

    return data;
  }
  















  private static int[] extractQuotedString(String rawData)
  {
    boolean inQuotes = false;
    

    char quoteChar = '"';
    


    int start = 0;
    


    for (int pos = 0; pos < rawData.length(); pos++) {
      char currentChar = rawData.charAt(pos);
      if ((currentChar == '"') || (currentChar == '\'')) {
        if (!inQuotes)
        {
          quoteChar = currentChar;
          inQuotes = true;
          start = pos + 1;
        }
        else if (quoteChar == currentChar)
        {
          inQuotes = false;
          return new int[] { start, pos };
        }
      }
    }
    


    return null;
  }
  










  public String toString()
  {
    return "[ProcessingInstruction: " + new XMLOutputter().outputString(this) + "]";
  }
  




  public ProcessingInstruction clone()
  {
    ProcessingInstruction pi = (ProcessingInstruction)super.clone();
    




    mapData = parseData(rawData);
    return pi;
  }
  
  public ProcessingInstruction detach()
  {
    return (ProcessingInstruction)super.detach();
  }
  
  protected ProcessingInstruction setParent(Parent parent)
  {
    return (ProcessingInstruction)super.setParent(parent);
  }
}
