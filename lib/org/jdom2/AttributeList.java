package org.jdom2;

import org.jdom2.internal.ArrayCopy;






































































final class AttributeList
  extends AbstractList<Attribute>
  implements RandomAccess
{
  private static final int INITIAL_ARRAY_SIZE = 4;
  private Attribute[] attributeData;
  private int size;
  private final Element parent;
  private static final Comparator<Attribute> ATTRIBUTE_NATURAL = new Comparator()
  {
    public int compare(Attribute a1, Attribute a2)
    {
      int pcomp = a1.getNamespacePrefix().compareTo(a2.getNamespacePrefix());
      if (pcomp != 0) {
        return pcomp;
      }
      return a1.getName().compareTo(a2.getName());
    }
  };
  







  AttributeList(Element parent)
  {
    this.parent = parent;
  }
  






  final void uncheckedAddAttribute(Attribute a)
  {
    parent = parent;
    ensureCapacity(size + 1);
    attributeData[(size++)] = a;
    modCount += 1;
  }
  











  public boolean add(Attribute attribute)
  {
    if (attribute.getParent() != null) {
      throw new IllegalAddException("The attribute already has an existing parent \"" + attribute.getParent().getQualifiedName() + "\"");
    }
    


    if (Verifier.checkNamespaceCollision(attribute, parent) != null) {
      throw new IllegalAddException(parent, attribute, Verifier.checkNamespaceCollision(attribute, parent));
    }
    


    int duplicate = indexOfDuplicate(attribute);
    if (duplicate < 0) {
      attribute.setParent(parent);
      ensureCapacity(size + 1);
      attributeData[(size++)] = attribute;
      modCount += 1;
    } else {
      Attribute old = attributeData[duplicate];
      old.setParent(null);
      attributeData[duplicate] = attribute;
      attribute.setParent(parent);
    }
    return true;
  }
  










  public void add(int index, Attribute attribute)
  {
    if ((index < 0) || (index > size)) {
      throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
    }
    

    if (attribute.getParent() != null) {
      throw new IllegalAddException("The attribute already has an existing parent \"" + attribute.getParent().getQualifiedName() + "\"");
    }
    

    int duplicate = indexOfDuplicate(attribute);
    if (duplicate >= 0) {
      throw new IllegalAddException("Cannot add duplicate attribute");
    }
    
    String reason = Verifier.checkNamespaceCollision(attribute, parent);
    if (reason != null) {
      throw new IllegalAddException(parent, attribute, reason);
    }
    
    attribute.setParent(parent);
    
    ensureCapacity(size + 1);
    if (index == size) {
      attributeData[(size++)] = attribute;
    } else {
      System.arraycopy(attributeData, index, attributeData, index + 1, size - index);
      
      attributeData[index] = attribute;
      size += 1;
    }
    modCount += 1;
  }
  










  public boolean addAll(Collection<? extends Attribute> collection)
  {
    return addAll(size(), collection);
  }
  















  public boolean addAll(int index, Collection<? extends Attribute> collection)
  {
    if ((index < 0) || (index > size)) {
      throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
    }
    

    if (collection == null) {
      throw new NullPointerException("Can not add a null Collection to AttributeList");
    }
    
    int addcnt = collection.size();
    if (addcnt == 0) {
      return false;
    }
    if (addcnt == 1)
    {
      add(index, (Attribute)collection.iterator().next());
      return true;
    }
    
    ensureCapacity(size() + addcnt);
    
    int tmpmodcount = modCount;
    boolean ok = false;
    
    int count = 0;
    try
    {
      for (Attribute att : collection) {
        add(index + count, att);
        count++;
      }
      ok = true;
    } finally {
      if (!ok) {
        for (;;) { count--; if (count < 0) break;
          remove(index + count);
        }
        modCount = tmpmodcount;
      }
    }
    
    return true;
  }
  



  public void clear()
  {
    if (attributeData != null) {
      while (size > 0) {
        size -= 1;
        attributeData[size].setParent(null);
        attributeData[size] = null;
      }
    }
    modCount += 1;
  }
  







  void clearAndSet(Collection<? extends Attribute> collection)
  {
    if ((collection == null) || (collection.isEmpty())) {
      clear();
      return;
    }
    

    Attribute[] old = attributeData;
    int oldSize = size;
    int oldModCount = modCount;
    




    while (size > 0) {
      old[(--size)].setParent(null);
    }
    size = 0;
    attributeData = null;
    
    boolean ok = false;
    try {
      addAll(0, collection);
      ok = true;
    } finally {
      if (!ok)
      {


        attributeData = old;
        while (size < oldSize) {
          attributeData[(size++)].setParent(parent);
        }
        modCount = oldModCount;
      }
    }
  }
  








  private void ensureCapacity(int minCapacity)
  {
    if (attributeData == null) {
      attributeData = new Attribute[Math.max(minCapacity, 4)];
      
      return; }
    if (minCapacity < attributeData.length) {
      return;
    }
    


    attributeData = ((Attribute[])ArrayCopy.copyOf(attributeData, minCapacity + 4 >>> 1 << 1));
  }
  








  public Attribute get(int index)
  {
    if ((index < 0) || (index >= size)) {
      throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
    }
    

    return attributeData[index];
  }
  









  Attribute get(String name, Namespace namespace)
  {
    int index = indexOf(name, namespace);
    if (index < 0) {
      return null;
    }
    return attributeData[index];
  }
  










  int indexOf(String name, Namespace namespace)
  {
    if (attributeData != null) {
      if (namespace == null) {
        return indexOf(name, Namespace.NO_NAMESPACE);
      }
      String uri = namespace.getURI();
      for (int i = 0; i < size; i++) {
        Attribute att = attributeData[i];
        if ((uri.equals(att.getNamespaceURI())) && (name.equals(att.getName())))
        {
          return i;
        }
      }
    }
    return -1;
  }
  







  public Attribute remove(int index)
  {
    if ((index < 0) || (index >= size)) {
      throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
    }
    
    Attribute old = attributeData[index];
    old.setParent(null);
    System.arraycopy(attributeData, index + 1, attributeData, index, size - index - 1);
    
    attributeData[(--size)] = null;
    modCount += 1;
    return old;
  }
  










  boolean remove(String name, Namespace namespace)
  {
    int index = indexOf(name, namespace);
    if (index < 0) {
      return false;
    }
    remove(index);
    return true;
  }
  











  public Attribute set(int index, Attribute attribute)
  {
    if ((index < 0) || (index >= size)) {
      throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
    }
    
    if (attribute.getParent() != null) {
      throw new IllegalAddException("The attribute already has an existing parent \"" + attribute.getParent().getQualifiedName() + "\"");
    }
    


    int duplicate = indexOfDuplicate(attribute);
    if ((duplicate >= 0) && (duplicate != index)) {
      throw new IllegalAddException("Cannot set duplicate attribute");
    }
    
    String reason = Verifier.checkNamespaceCollision(attribute, parent, index);
    if (reason != null) {
      throw new IllegalAddException(parent, attribute, reason);
    }
    
    Attribute old = attributeData[index];
    old.setParent(null);
    
    attributeData[index] = attribute;
    attribute.setParent(parent);
    return old;
  }
  



  private int indexOfDuplicate(Attribute attribute)
  {
    return indexOf(attribute.getName(), attribute.getNamespace());
  }
  






  public Iterator<Attribute> iterator()
  {
    return new ALIterator(null);
  }
  





  public int size()
  {
    return size;
  }
  
  public boolean isEmpty()
  {
    return size == 0;
  }
  



  public String toString()
  {
    return super.toString();
  }
  










  private final int binarySearch(int[] indexes, int len, int val, Comparator<? super Attribute> comp)
  {
    int left = 0;int mid = 0;int right = len - 1;int cmp = 0;
    Attribute base = attributeData[val];
    while (left <= right) {
      mid = left + right >>> 1;
      cmp = comp.compare(base, attributeData[indexes[mid]]);
      if (cmp == 0) {
        while ((cmp == 0) && (mid < right) && (comp.compare(base, attributeData[indexes[(mid + 1)]]) == 0))
        {
          mid++;
        }
        return mid + 1; }
      if (cmp < 0) {
        right = mid - 1;
      } else {
        left = mid + 1;
      }
    }
    return left;
  }
  


  private void sortInPlace(int[] indexes)
  {
    int[] unsorted = ArrayCopy.copyOf(indexes, indexes.length);
    Arrays.sort(unsorted);
    Attribute[] usc = new Attribute[unsorted.length];
    for (int i = 0; i < usc.length; i++) {
      usc[i] = attributeData[indexes[i]];
    }
    
    for (int i = 0; i < indexes.length; i++) {
      attributeData[unsorted[i]] = usc[i];
    }
  }
  









  public void sort(Comparator<? super Attribute> comp)
  {
    if (comp == null) {
      comp = ATTRIBUTE_NATURAL;
    }
    int sz = size;
    int[] indexes = new int[sz];
    for (int i = 0; i < sz; i++) {
      int ip = binarySearch(indexes, i, i, comp);
      if (ip < i) {
        System.arraycopy(indexes, ip, indexes, ip + 1, i - ip);
      }
      indexes[ip] = i;
    }
    sortInPlace(indexes);
  }
  









  private final class ALIterator
    implements Iterator<Attribute>
  {
    private int expect = -1;
    
    private int cursor = 0;
    
    private boolean canremove = false;
    
    private ALIterator() {
      expect = modCount;
    }
    
    public boolean hasNext()
    {
      return cursor < size;
    }
    
    public Attribute next()
    {
      if (modCount != expect) {
        throw new ConcurrentModificationException("ContentList was modified outside of this Iterator");
      }
      
      if (cursor >= size) {
        throw new NoSuchElementException("Iterated beyond the end of the ContentList.");
      }
      
      canremove = true;
      return attributeData[(cursor++)];
    }
    
    public void remove()
    {
      if (modCount != expect) {
        throw new ConcurrentModificationException("ContentList was modified outside of this Iterator");
      }
      
      if (!canremove) {
        throw new IllegalStateException("Can only remove() content after a call to next()");
      }
      
      remove(--cursor);
      expect = modCount;
      canremove = false;
    }
  }
}
