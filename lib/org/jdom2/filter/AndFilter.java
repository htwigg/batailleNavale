package org.jdom2.filter;


















final class AndFilter<T>
  extends AbstractFilter<T>
{
  private static final long serialVersionUID = 200L;
  















  private final Filter<?> base;
  















  private final Filter<T> refiner;
  
















  public AndFilter(Filter<?> base, Filter<T> refiner)
  {
    if ((base == null) || (refiner == null)) {
      throw new NullPointerException("Cannot have a null base or refiner filter");
    }
    this.base = base;
    this.refiner = refiner;
  }
  
  public T filter(Object content)
  {
    Object o = base.filter(content);
    if (o != null) {
      return refiner.filter(content);
    }
    return null;
  }
  
  public int hashCode()
  {
    return base.hashCode() ^ refiner.hashCode();
  }
  
  public boolean equals(Object obj)
  {
    if (obj == this) {
      return true;
    }
    if ((obj instanceof AndFilter)) {
      AndFilter<?> them = (AndFilter)obj;
      return ((base.equals(base)) && (refiner.equals(refiner))) || ((refiner.equals(base)) && (base.equals(refiner)));
    }
    

    return false;
  }
  
  public String toString()
  {
    return 64 + "[AndFilter: " + base.toString() + ",\n" + "            " + refiner.toString() + "]";
  }
}
