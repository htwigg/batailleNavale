package org.jdom2.filter;

import org.jdom2.Element;
import org.jdom2.Namespace;










































































public class ElementFilter
  extends AbstractFilter<Element>
{
  private static final long serialVersionUID = 200L;
  private String name;
  private Namespace namespace;
  
  public ElementFilter() {}
  
  public ElementFilter(String name)
  {
    this.name = name;
  }
  




  public ElementFilter(Namespace namespace)
  {
    this.namespace = namespace;
  }
  





  public ElementFilter(String name, Namespace namespace)
  {
    this.name = name;
    this.namespace = namespace;
  }
  







  public Element filter(Object content)
  {
    if ((content instanceof Element)) {
      Element el = (Element)content;
      if (name == null) {
        if (namespace == null) {
          return el;
        }
        return namespace.equals(el.getNamespace()) ? el : null;
      }
      if (!name.equals(el.getName())) {
        return null;
      }
      if (namespace == null) {
        return el;
      }
      return namespace.equals(el.getNamespace()) ? el : null;
    }
    return null;
  }
  








  public boolean equals(Object obj)
  {
    if (this == obj) return true;
    if (!(obj instanceof ElementFilter)) { return false;
    }
    ElementFilter filter = (ElementFilter)obj;
    
    if (name != null ? !name.equals(name) : name != null) return false;
    if (namespace != null ? !namespace.equals(namespace) : namespace != null) { return false;
    }
    return true;
  }
  


  public int hashCode()
  {
    int result = name != null ? name.hashCode() : 0;
    result = 29 * result + (namespace != null ? namespace.hashCode() : 0);
    return result;
  }
  
  public String toString()
  {
    return "[ElementFilter: Name " + (name == null ? "*any*" : name) + " with Namespace " + namespace + "]";
  }
}
