package org.jdom2.filter;

import org.jdom2.Content;































































public abstract class AbstractFilter<T>
  implements Filter<T>
{
  private static final long serialVersionUID = 200L;
  
  public AbstractFilter() {}
  
  public final boolean matches(Object content)
  {
    return filter(content) != null;
  }
  
  public List<T> filter(List<?> content)
  {
    if (content == null) {
      return Collections.emptyList();
    }
    if ((content instanceof RandomAccess)) {
      int sz = content.size();
      ArrayList<T> ret = new ArrayList(sz);
      for (int i = 0; i < sz; i++) {
        T c = filter(content.get(i));
        if (c != null) {
          ret.add(c);
        }
      }
      if (ret.isEmpty()) {
        return Collections.emptyList();
      }
      return Collections.unmodifiableList(ret);
    }
    ArrayList<T> ret = new ArrayList(10);
    for (Iterator<?> it = content.iterator(); it.hasNext();) {
      T c = filter(it.next());
      if (c != null) {
        ret.add(c);
      }
    }
    if (ret.isEmpty()) {
      return Collections.emptyList();
    }
    return Collections.unmodifiableList(ret);
  }
  
  public final Filter<?> negate()
  {
    if ((this instanceof NegateFilter)) {
      return ((NegateFilter)this).getBaseFilter();
    }
    return new NegateFilter(this);
  }
  
  public final Filter<? extends Content> or(Filter<?> filter)
  {
    return new OrFilter(this, filter);
  }
  
  public final Filter<T> and(Filter<?> filter)
  {
    return new AndFilter(filter, this);
  }
  
  public <R> Filter<R> refine(Filter<R> filter)
  {
    return new AndFilter(this, filter);
  }
}
