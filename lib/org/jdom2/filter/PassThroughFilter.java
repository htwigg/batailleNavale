package org.jdom2.filter;

final class PassThroughFilter
  extends AbstractFilter<Object>
{
  private static final long serialVersionUID = 200L;
  
  PassThroughFilter() {}
  
  public Object filter(Object content)
  {
    return content;
  }
  
  public List<Object> filter(List<?> content)
  {
    if ((content == null) || (content.isEmpty())) {
      return Collections.emptyList();
    }
    if ((content instanceof RandomAccess)) {
      return Collections.unmodifiableList(content);
    }
    ArrayList<Object> ret = new ArrayList();
    for (Iterator<?> it = content.iterator(); it.hasNext();) {
      ret.add(it.next());
    }
    return Collections.unmodifiableList(ret);
  }
}
