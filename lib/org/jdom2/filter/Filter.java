package org.jdom2.filter;

import java.io.Serializable;
import java.util.List;

public abstract interface Filter<T>
  extends Serializable
{
  public abstract List<T> filter(List<?> paramList);
  
  public abstract T filter(Object paramObject);
  
  public abstract boolean matches(Object paramObject);
  
  public abstract Filter<? extends Object> negate();
  
  public abstract Filter<? extends Object> or(Filter<?> paramFilter);
  
  public abstract Filter<T> and(Filter<?> paramFilter);
  
  public abstract <R> Filter<R> refine(Filter<R> paramFilter);
}
