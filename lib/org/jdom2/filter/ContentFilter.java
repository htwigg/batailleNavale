package org.jdom2.filter;

import org.jdom2.*;





























































































public class ContentFilter
  extends AbstractFilter<Content>
{
  private static final long serialVersionUID = 200L;
  public static final int ELEMENT = 1;
  public static final int CDATA = 2;
  public static final int TEXT = 4;
  public static final int COMMENT = 8;
  public static final int PI = 16;
  public static final int ENTITYREF = 32;
  public static final int DOCUMENT = 64;
  public static final int DOCTYPE = 128;
  private int filterMask;
  
  public ContentFilter()
  {
    setDefaultMask();
  }
  





  public ContentFilter(boolean allVisible)
  {
    if (allVisible) {
      setDefaultMask();
    }
    else {
      filterMask &= (filterMask ^ 0xFFFFFFFF);
    }
  }
  




  public ContentFilter(int mask)
  {
    setFilterMask(mask);
  }
  




  public int getFilterMask()
  {
    return filterMask;
  }
  




  public void setFilterMask(int mask)
  {
    setDefaultMask();
    filterMask &= mask;
  }
  


  public void setDefaultMask()
  {
    filterMask = 255;
  }
  




  public void setDocumentContent()
  {
    filterMask = 153;
  }
  



  public void setElementContent()
  {
    filterMask = 63;
  }
  






  public void setElementVisible(boolean visible)
  {
    if (visible) {
      filterMask |= 0x1;
    }
    else {
      filterMask &= 0xFFFFFFFE;
    }
  }
  





  public void setCDATAVisible(boolean visible)
  {
    if (visible) {
      filterMask |= 0x2;
    }
    else {
      filterMask &= 0xFFFFFFFD;
    }
  }
  





  public void setTextVisible(boolean visible)
  {
    if (visible) {
      filterMask |= 0x4;
    }
    else {
      filterMask &= 0xFFFFFFFB;
    }
  }
  





  public void setCommentVisible(boolean visible)
  {
    if (visible) {
      filterMask |= 0x8;
    }
    else {
      filterMask &= 0xFFFFFFF7;
    }
  }
  





  public void setPIVisible(boolean visible)
  {
    if (visible) {
      filterMask |= 0x10;
    }
    else {
      filterMask &= 0xFFFFFFEF;
    }
  }
  





  public void setEntityRefVisible(boolean visible)
  {
    if (visible) {
      filterMask |= 0x20;
    }
    else {
      filterMask &= 0xFFFFFFDF;
    }
  }
  





  public void setDocTypeVisible(boolean visible)
  {
    if (visible) {
      filterMask |= 0x80;
    }
    else {
      filterMask &= 0xFF7F;
    }
  }
  







  public Content filter(Object obj)
  {
    if ((obj == null) || (!Content.class.isInstance(obj))) {
      return null;
    }
    
    Content content = (Content)obj;
    
    if ((content instanceof Element)) {
      return (filterMask & 0x1) != 0 ? content : null;
    }
    if ((content instanceof CDATA)) {
      return (filterMask & 0x2) != 0 ? content : null;
    }
    if ((content instanceof Text)) {
      return (filterMask & 0x4) != 0 ? content : null;
    }
    if ((content instanceof Comment)) {
      return (filterMask & 0x8) != 0 ? content : null;
    }
    if ((content instanceof ProcessingInstruction)) {
      return (filterMask & 0x10) != 0 ? content : null;
    }
    if ((content instanceof EntityRef)) {
      return (filterMask & 0x20) != 0 ? content : null;
    }
    


    if ((content instanceof DocType)) {
      return (filterMask & 0x80) != 0 ? content : null;
    }
    
    return null;
  }
  








  public boolean equals(Object obj)
  {
    if (this == obj) return true;
    if (!(obj instanceof ContentFilter)) { return false;
    }
    ContentFilter filter = (ContentFilter)obj;
    
    if (filterMask != filterMask) { return false;
    }
    return true;
  }
  

  public int hashCode()
  {
    return filterMask;
  }
}
