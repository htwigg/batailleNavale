package org.jdom2.filter;

import org.jdom2.Attribute;
import org.jdom2.Namespace;




































































public class AttributeFilter
  extends AbstractFilter<Attribute>
{
  private static final long serialVersionUID = 200L;
  private final String name;
  private final Namespace namespace;
  
  public AttributeFilter()
  {
    this(null, null);
  }
  




  public AttributeFilter(String name)
  {
    this(name, null);
  }
  




  public AttributeFilter(Namespace namespace)
  {
    this(null, namespace);
  }
  





  public AttributeFilter(String name, Namespace namespace)
  {
    this.name = name;
    this.namespace = namespace;
  }
  







  public Attribute filter(Object content)
  {
    if ((content instanceof Attribute)) {
      Attribute att = (Attribute)content;
      if (name == null) {
        if (namespace == null) {
          return att;
        }
        return namespace.equals(att.getNamespace()) ? att : null;
      }
      if (!name.equals(att.getName())) {
        return null;
      }
      if (namespace == null) {
        return att;
      }
      return namespace.equals(att.getNamespace()) ? att : null;
    }
    return null;
  }
  








  public boolean equals(Object obj)
  {
    if (this == obj) return true;
    if (!(obj instanceof AttributeFilter)) { return false;
    }
    AttributeFilter filter = (AttributeFilter)obj;
    
    if (name != null ? !name.equals(name) : name != null) return false;
    if (namespace != null ? !namespace.equals(namespace) : namespace != null) { return false;
    }
    return true;
  }
  

  public int hashCode()
  {
    int result = name != null ? name.hashCode() : 0;
    result = 29 * result + (namespace != null ? namespace.hashCode() : 0);
    return result;
  }
}
