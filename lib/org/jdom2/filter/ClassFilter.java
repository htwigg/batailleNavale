package org.jdom2.filter;
























final class ClassFilter<T>
  extends AbstractFilter<T>
{
  private static final long serialVersionUID = 200L;
  





















  private final Class<? extends T> fclass;
  






















  public ClassFilter(Class<? extends T> tclass)
  {
    fclass = tclass;
  }
  
  public T filter(Object content)
  {
    return fclass.isInstance(content) ? fclass.cast(content) : null;
  }
  
  public String toString()
  {
    return "[ClassFilter: Class " + fclass.getName() + "]";
  }
  
  public boolean equals(Object obj)
  {
    if (obj == this) {
      return true;
    }
    if ((obj instanceof ClassFilter)) {
      return fclass.equals(fclass);
    }
    return false;
  }
  
  public int hashCode()
  {
    return fclass.hashCode();
  }
}
