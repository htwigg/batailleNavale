package org.jdom2.filter;

import org.jdom2.Content;









































































final class OrFilter
  extends AbstractFilter<Content>
{
  private static final long serialVersionUID = 200L;
  private final Filter<?> left;
  private final Filter<?> right;
  
  public OrFilter(Filter<?> left, Filter<?> right)
  {
    if ((left == null) || (right == null)) {
      throw new IllegalArgumentException("null filter not allowed");
    }
    this.left = left;
    this.right = right;
  }
  
  public Content filter(Object obj)
  {
    if ((left.matches(obj)) || (right.matches(obj))) {
      return (Content)obj;
    }
    return null;
  }
  
  public boolean equals(Object obj)
  {
    if (this == obj) {
      return true;
    }
    
    if ((obj instanceof OrFilter)) {
      OrFilter filter = (OrFilter)obj;
      if (((left.equals(left)) && (right.equals(right))) || ((left.equals(right)) && (right.equals(left))))
      {
        return true;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    return left.hashCode() ^ 0xFFFFFFFF ^ right.hashCode();
  }
  
  public String toString()
  {
    return 64 + "[OrFilter: " + left.toString() + ",\n" + "           " + right.toString() + "]";
  }
}
