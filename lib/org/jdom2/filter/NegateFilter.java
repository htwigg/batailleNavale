package org.jdom2.filter;
























final class NegateFilter
  extends AbstractFilter<Object>
{
  private static final long serialVersionUID = 200L;
  






















  private final Filter<?> filter;
  























  public NegateFilter(Filter<?> filter)
  {
    this.filter = filter;
  }
  
  public Object filter(Object content)
  {
    if (filter.matches(content)) {
      return null;
    }
    return content;
  }
  
  Filter<?> getBaseFilter() {
    return filter;
  }
  
  public boolean equals(Object obj)
  {
    if (this == obj) {
      return true;
    }
    
    if ((obj instanceof NegateFilter)) {
      return filter.equals(filter);
    }
    return false;
  }
  
  public int hashCode()
  {
    return filter.hashCode() ^ 0xFFFFFFFF;
  }
  
  public String toString()
  {
    return 64 + "[NegateFilter: " + filter.toString() + "]";
  }
}
