package org.jdom2.filter;

import org.jdom2.Content.CType;
import org.jdom2.Text;






























































final class TextOnlyFilter
  extends AbstractFilter<Text>
{
  private static final long serialVersionUID = 200L;
  
  TextOnlyFilter() {}
  
  public Text filter(Object content)
  {
    if ((content instanceof Text)) {
      Text txt = (Text)content;
      if (txt.getCType() == Content.CType.Text) {
        return txt;
      }
    }
    return null;
  }
  
  public int hashCode()
  {
    return getClass().hashCode();
  }
  
  public boolean equals(Object obj)
  {
    return obj instanceof TextOnlyFilter;
  }
}
