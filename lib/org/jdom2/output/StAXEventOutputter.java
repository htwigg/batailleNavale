package org.jdom2.output;

import org.jdom2.*;
import org.jdom2.output.support.AbstractStAXEventProcessor;
import org.jdom2.output.support.StAXEventProcessor;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.util.XMLEventConsumer;
import java.util.List;





















































































































public final class StAXEventOutputter
  implements Cloneable
{
  private static final DefaultStAXEventProcessor DEFAULTPROCESSOR = new DefaultStAXEventProcessor(null);
  





  private static final XMLEventFactory DEFAULTEVENTFACTORY = XMLEventFactory.newInstance();
  








  private Format myFormat = null;
  

  private StAXEventProcessor myProcessor = null;
  
  private XMLEventFactory myEventFactory = null;
  

























  public StAXEventOutputter(Format format, StAXEventProcessor processor, XMLEventFactory eventfactory)
  {
    myFormat = (format == null ? Format.getRawFormat() : format.clone());
    myProcessor = (processor == null ? DEFAULTPROCESSOR : processor);
    myEventFactory = (eventfactory == null ? DEFAULTEVENTFACTORY : eventfactory);
  }
  



  public StAXEventOutputter()
  {
    this(null, null, null);
  }
  












  public StAXEventOutputter(Format format)
  {
    this(format, null, null);
  }
  







  public StAXEventOutputter(StAXEventProcessor processor)
  {
    this(null, processor, null);
  }
  






  public StAXEventOutputter(XMLEventFactory eventfactory)
  {
    this(null, null, eventfactory);
  }
  













  public void setFormat(Format newFormat)
  {
    myFormat = newFormat.clone();
  }
  







  public Format getFormat()
  {
    return myFormat;
  }
  





  public StAXEventProcessor getStAXStream()
  {
    return myProcessor;
  }
  






  public void setStAXEventProcessor(StAXEventProcessor processor)
  {
    myProcessor = processor;
  }
  




  public XMLEventFactory getEventFactory()
  {
    return myEventFactory;
  }
  


  public void setEventFactory(XMLEventFactory myEventFactory)
  {
    this.myEventFactory = myEventFactory;
  }
  























  public final void output(Document doc, XMLEventConsumer out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, myEventFactory, doc);
  }
  











  public final void output(DocType doctype, XMLEventConsumer out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, myEventFactory, doctype);
  }
  














  public final void output(Element element, XMLEventConsumer out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, myEventFactory, element);
  }
  
















  public final void outputElementContent(Element element, XMLEventConsumer out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, myEventFactory, element.getContent());
  }
  














  public final void output(List<? extends Content> list, XMLEventConsumer out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, myEventFactory, list);
  }
  











  public final void output(CDATA cdata, XMLEventConsumer out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, myEventFactory, cdata);
  }
  












  public final void output(Text text, XMLEventConsumer out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, myEventFactory, text);
  }
  











  public final void output(Comment comment, XMLEventConsumer out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, myEventFactory, comment);
  }
  












  public final void output(ProcessingInstruction pi, XMLEventConsumer out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, myEventFactory, pi);
  }
  











  public final void output(EntityRef entity, XMLEventConsumer out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, myEventFactory, entity);
  }
  














  public StAXEventOutputter clone()
  {
    try
    {
      return (StAXEventOutputter)super.clone();

    }
    catch (CloneNotSupportedException e)
    {

      throw new RuntimeException(e.toString());
    }
  }
  





  public String toString()
  {
    StringBuilder buffer = new StringBuilder();
    buffer.append("StAXStreamOutputter[omitDeclaration = ");
    buffer.append(myFormat.omitDeclaration);
    buffer.append(", ");
    buffer.append("encoding = ");
    buffer.append(myFormat.encoding);
    buffer.append(", ");
    buffer.append("omitEncoding = ");
    buffer.append(myFormat.omitEncoding);
    buffer.append(", ");
    buffer.append("indent = '");
    buffer.append(myFormat.indent);
    buffer.append("'");
    buffer.append(", ");
    buffer.append("expandEmptyElements = ");
    buffer.append(myFormat.expandEmptyElements);
    buffer.append(", ");
    buffer.append("lineSeparator = '");
    for (char ch : myFormat.lineSeparator.toCharArray()) {
      switch (ch) {
      case '\r': 
        buffer.append("\\r");
        break;
      case '\n': 
        buffer.append("\\n");
        break;
      case '\t': 
        buffer.append("\\t");
        break;
      case '\013': case '\f': default: 
        buffer.append("[" + ch + "]");
      }
      
    }
    buffer.append("', ");
    buffer.append("textMode = ");
    buffer.append(myFormat.mode + "]");
    return buffer.toString();
  }
  
  private static final class DefaultStAXEventProcessor
    extends AbstractStAXEventProcessor
  {
    private DefaultStAXEventProcessor() {}
  }
}
