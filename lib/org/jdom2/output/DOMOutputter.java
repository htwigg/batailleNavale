package org.jdom2.output;

import org.jdom2.*;
import org.jdom2.adapters.DOMAdapter;
import org.jdom2.adapters.JAXPDOMAdapter;
import org.jdom2.internal.ReflectionConstructor;
import org.jdom2.output.support.AbstractDOMOutputProcessor;
import org.jdom2.output.support.DOMOutputProcessor;

import java.util.List;






















































































public class DOMOutputter
{
  private static final DOMAdapter DEFAULT_ADAPTER = new JAXPDOMAdapter();
  
  private static final DOMOutputProcessor DEFAULT_PROCESSOR = new DefaultDOMOutputProcessor(null);
  

  private DOMAdapter adapter;
  

  private Format format;
  

  private DOMOutputProcessor processor;
  


  public DOMOutputter()
  {
    this(null, null, null);
  }
  







  public DOMOutputter(DOMOutputProcessor processor)
  {
    this(null, null, processor);
  }
  














  public DOMOutputter(DOMAdapter adapter, Format format, DOMOutputProcessor processor)
  {
    this.adapter = (adapter == null ? DEFAULT_ADAPTER : adapter);
    this.format = (format == null ? Format.getRawFormat() : format);
    this.processor = (processor == null ? DEFAULT_PROCESSOR : processor);
  }
  











  @Deprecated
  public DOMOutputter(String adapterClass)
  {
    if (adapterClass == null) {
      adapter = DEFAULT_ADAPTER;
    } else {
      adapter = ((DOMAdapter)ReflectionConstructor.construct(adapterClass, DOMAdapter.class));
    }
  }
  














  public DOMOutputter(DOMAdapter adapter)
  {
    this.adapter = (adapter == null ? DEFAULT_ADAPTER : adapter);
  }
  





  public DOMAdapter getDOMAdapter()
  {
    return adapter;
  }
  






  public void setDOMAdapter(DOMAdapter adapter)
  {
    this.adapter = (adapter == null ? DEFAULT_ADAPTER : adapter);
  }
  





  public Format getFormat()
  {
    return format;
  }
  






  public void setFormat(Format format)
  {
    this.format = (format == null ? Format.getRawFormat() : format);
  }
  





  public DOMOutputProcessor getDOMOutputProcessor()
  {
    return processor;
  }
  






  public void setDOMOutputProcessor(DOMOutputProcessor processor)
  {
    this.processor = (processor == null ? DEFAULT_PROCESSOR : processor);
  }
  








  @Deprecated
  public void setForceNamespaceAware(boolean flag) {}
  








  @Deprecated
  public boolean getForceNamespaceAware()
  {
    return true;
  }
  









  public org.w3c.dom.Document output(org.jdom2.Document document)
    throws JDOMException
  {
    return processor.process(adapter.createDocument(document.getDocType()), format, document);
  }
  















  public DocumentType output(DocType doctype)
    throws JDOMException
  {
    return adapter.createDocument(doctype).getDoctype();
  }
  









  public org.w3c.dom.Element output(org.jdom2.Element element)
    throws JDOMException
  {
    return processor.process(adapter.createDocument(), format, element);
  }
  










  public org.w3c.dom.Text output(org.jdom2.Text text)
    throws JDOMException
  {
    return processor.process(adapter.createDocument(), format, text);
  }
  










  public CDATASection output(CDATA cdata)
    throws JDOMException
  {
    return processor.process(adapter.createDocument(), format, cdata);
  }
  












  public org.w3c.dom.ProcessingInstruction output(org.jdom2.ProcessingInstruction pi)
    throws JDOMException
  {
    return processor.process(adapter.createDocument(), format, pi);
  }
  











  public org.w3c.dom.Comment output(org.jdom2.Comment comment)
    throws JDOMException
  {
    return processor.process(adapter.createDocument(), format, comment);
  }
  












  public EntityReference output(EntityRef entity)
    throws JDOMException
  {
    return processor.process(adapter.createDocument(), format, entity);
  }
  










  public Attr output(Attribute attribute)
    throws JDOMException
  {
    return processor.process(adapter.createDocument(), format, attribute);
  }
  











  public List<Node> output(List<? extends Content> list)
    throws JDOMException
  {
    return processor.process(adapter.createDocument(), format, list);
  }
  













  public org.w3c.dom.Element output(org.w3c.dom.Document basedoc, org.jdom2.Element element)
    throws JDOMException
  {
    return processor.process(basedoc, format, element);
  }
  













  public org.w3c.dom.Text output(org.w3c.dom.Document basedoc, org.jdom2.Text text)
    throws JDOMException
  {
    return processor.process(basedoc, format, text);
  }
  













  public CDATASection output(org.w3c.dom.Document basedoc, CDATA cdata)
    throws JDOMException
  {
    return processor.process(basedoc, format, cdata);
  }
  















  public org.w3c.dom.ProcessingInstruction output(org.w3c.dom.Document basedoc, org.jdom2.ProcessingInstruction pi)
    throws JDOMException
  {
    return processor.process(basedoc, format, pi);
  }
  














  public org.w3c.dom.Comment output(org.w3c.dom.Document basedoc, org.jdom2.Comment comment)
    throws JDOMException
  {
    return processor.process(basedoc, format, comment);
  }
  














  public EntityReference output(org.w3c.dom.Document basedoc, EntityRef entity)
    throws JDOMException
  {
    return processor.process(basedoc, format, entity);
  }
  













  public Attr output(org.w3c.dom.Document basedoc, Attribute attribute)
    throws JDOMException
  {
    return processor.process(basedoc, format, attribute);
  }
  













  public List<Node> output(org.w3c.dom.Document basedoc, List<? extends Content> list)
    throws JDOMException
  {
    return processor.process(basedoc, format, list);
  }
  
  private static final class DefaultDOMOutputProcessor
    extends AbstractDOMOutputProcessor
  {
    private DefaultDOMOutputProcessor() {}
  }
}
