package org.jdom2.output.support;

import org.jdom2.*;
import org.jdom2.output.Format;

import java.util.List;

public abstract interface SAXOutputProcessor
{
  public abstract void process(SAXTarget paramSAXTarget, Format paramFormat, Document paramDocument)
    throws JDOMException;
  
  public abstract void process(SAXTarget paramSAXTarget, Format paramFormat, DocType paramDocType)
    throws JDOMException;
  
  public abstract void process(SAXTarget paramSAXTarget, Format paramFormat, Element paramElement)
    throws JDOMException;
  
  public abstract void processAsDocument(SAXTarget paramSAXTarget, Format paramFormat, Element paramElement)
    throws JDOMException;
  
  public abstract void process(SAXTarget paramSAXTarget, Format paramFormat, List<? extends Content> paramList)
    throws JDOMException;
  
  public abstract void processAsDocument(SAXTarget paramSAXTarget, Format paramFormat, List<? extends Content> paramList)
    throws JDOMException;
  
  public abstract void process(SAXTarget paramSAXTarget, Format paramFormat, CDATA paramCDATA)
    throws JDOMException;
  
  public abstract void process(SAXTarget paramSAXTarget, Format paramFormat, Text paramText)
    throws JDOMException;
  
  public abstract void process(SAXTarget paramSAXTarget, Format paramFormat, Comment paramComment)
    throws JDOMException;
  
  public abstract void process(SAXTarget paramSAXTarget, Format paramFormat, ProcessingInstruction paramProcessingInstruction)
    throws JDOMException;
  
  public abstract void process(SAXTarget paramSAXTarget, Format paramFormat, EntityRef paramEntityRef)
    throws JDOMException;
}
