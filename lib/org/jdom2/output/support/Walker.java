package org.jdom2.output.support;

import org.jdom2.Content;

public abstract interface Walker
{
  public abstract boolean isAllText();
  
  public abstract boolean isAllWhitespace();
  
  public abstract boolean hasNext();
  
  public abstract Content next();
  
  public abstract String text();
  
  public abstract boolean isCDATA();
}
