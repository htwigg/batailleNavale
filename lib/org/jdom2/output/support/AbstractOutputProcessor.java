package org.jdom2.output.support;

import org.jdom2.Content;

import java.util.List;





















































































public abstract class AbstractOutputProcessor
{
  public AbstractOutputProcessor() {}
  
  protected Walker buildWalker(FormatStack fstack, List<? extends Content> content, boolean escape)
  {
    switch (1.$SwitchMap$org$jdom2$output$Format$TextMode[fstack.getTextMode().ordinal()]) {
    case 1: 
      return new WalkerPRESERVE(content);
    case 2: 
      return new WalkerNORMALIZE(content, fstack, escape);
    case 3: 
      return new WalkerTRIM(content, fstack, escape);
    case 4: 
      return new WalkerTRIM_FULL_WHITE(content, fstack, escape);
    }
    
    


    return new WalkerPRESERVE(content);
  }
}
