package org.jdom2.output.support;

import org.jdom2.Content;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
































































public class WalkerPRESERVE
  implements Walker
{
  private static final Iterator<Content> EMPTYIT = new Iterator()
  {
    public boolean hasNext() {
      return false;
    }
    
    public Content next()
    {
      throw new NoSuchElementException("Cannot call next() on an empty iterator.");
    }
    
    public void remove()
    {
      throw new UnsupportedOperationException("Cannot remove from an empty iterator.");
    }
  };
  

  private final Iterator<? extends Content> iter;
  

  private final boolean alltext;
  

  public WalkerPRESERVE(List<? extends Content> content)
  {
    if (content.isEmpty()) {
      alltext = true;
      iter = EMPTYIT;
    } else {
      iter = content.iterator();
      alltext = false;
    }
  }
  















  public boolean isAllText()
  {
    return alltext;
  }
  
  public boolean hasNext()
  {
    return iter.hasNext();
  }
  
  public Content next()
  {
    return (Content)iter.next();
  }
  
  public String text()
  {
    return null;
  }
  
  public boolean isCDATA()
  {
    return false;
  }
  
  public boolean isAllWhitespace()
  {
    return alltext;
  }
}
