package org.jdom2.output.support;

import org.jdom2.*;
import org.jdom2.Content.CType;
import org.jdom2.output.Format;
import org.jdom2.output.Format.TextMode;
import org.jdom2.util.NamespaceStack;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;










































































































public abstract class AbstractStAXStreamProcessor
  extends AbstractOutputProcessor
  implements StAXStreamProcessor
{
  public AbstractStAXStreamProcessor() {}
  
  public void process(XMLStreamWriter out, Format format, Document doc)
    throws XMLStreamException
  {
    printDocument(out, new FormatStack(format), new NamespaceStack(), doc);
    out.flush();
  }
  






  public void process(XMLStreamWriter out, Format format, DocType doctype)
    throws XMLStreamException
  {
    printDocType(out, new FormatStack(format), doctype);
    out.flush();
  }
  








  public void process(XMLStreamWriter out, Format format, Element element)
    throws XMLStreamException
  {
    printElement(out, new FormatStack(format), new NamespaceStack(), element);
    
    out.flush();
  }
  







  public void process(XMLStreamWriter out, Format format, List<? extends Content> list)
    throws XMLStreamException
  {
    FormatStack fstack = new FormatStack(format);
    Walker walker = buildWalker(fstack, list, false);
    printContent(out, fstack, new NamespaceStack(), walker);
    out.flush();
  }
  






  public void process(XMLStreamWriter out, Format format, CDATA cdata)
    throws XMLStreamException
  {
    List<CDATA> list = Collections.singletonList(cdata);
    FormatStack fstack = new FormatStack(format);
    Walker walker = buildWalker(fstack, list, false);
    if (walker.hasNext()) {
      Content c = walker.next();
      if (c == null) {
        printCDATA(out, fstack, new CDATA(walker.text()));
      } else if (c.getCType() == Content.CType.CDATA) {
        printCDATA(out, fstack, (CDATA)c);
      }
    }
    out.flush();
  }
  






  public void process(XMLStreamWriter out, Format format, Text text)
    throws XMLStreamException
  {
    List<Text> list = Collections.singletonList(text);
    FormatStack fstack = new FormatStack(format);
    Walker walker = buildWalker(fstack, list, false);
    if (walker.hasNext()) {
      Content c = walker.next();
      if (c == null) {
        printText(out, fstack, new Text(walker.text()));
      } else if (c.getCType() == Content.CType.Text) {
        printText(out, fstack, (Text)c);
      }
    }
    out.flush();
  }
  






  public void process(XMLStreamWriter out, Format format, Comment comment)
    throws XMLStreamException
  {
    printComment(out, new FormatStack(format), comment);
    out.flush();
  }
  






  public void process(XMLStreamWriter out, Format format, ProcessingInstruction pi)
    throws XMLStreamException
  {
    FormatStack fstack = new FormatStack(format);
    
    fstack.setIgnoreTrAXEscapingPIs(true);
    printProcessingInstruction(out, fstack, pi);
    out.flush();
  }
  






  public void process(XMLStreamWriter out, Format format, EntityRef entity)
    throws XMLStreamException
  {
    printEntityRef(out, new FormatStack(format), entity);
    out.flush();
  }
  






















  protected void printDocument(XMLStreamWriter out, FormatStack fstack, NamespaceStack nstack, Document doc)
    throws XMLStreamException
  {
    if (fstack.isOmitDeclaration())
    {
      out.writeStartDocument();
      if (fstack.getLineSeparator() != null) {
        out.writeCharacters(fstack.getLineSeparator());
      }
    } else if (fstack.isOmitEncoding()) {
      out.writeStartDocument("1.0");
      if (fstack.getLineSeparator() != null) {
        out.writeCharacters(fstack.getLineSeparator());
      }
    } else {
      out.writeStartDocument(fstack.getEncoding(), "1.0");
      if (fstack.getLineSeparator() != null) {
        out.writeCharacters(fstack.getLineSeparator());
      }
    }
    





    List<Content> list = doc.hasRootElement() ? doc.getContent() : new ArrayList(doc.getContentSize());
    
    if (list.isEmpty()) {
      int sz = doc.getContentSize();
      for (int i = 0; i < sz; i++) {
        list.add(doc.getContent(i));
      }
    }
    Walker walker = buildWalker(fstack, list, false);
    if (walker.hasNext()) {
      while (walker.hasNext())
      {
        Content c = walker.next();
        

        if (c == null)
        {
          String padding = walker.text();
          if ((padding != null) && (Verifier.isAllXMLWhitespace(padding)) && (!walker.isCDATA()))
          {



            out.writeCharacters(padding);
          }
        } else {
          switch (1.$SwitchMap$org$jdom2$Content$CType[c.getCType().ordinal()]) {
          case 1: 
            printComment(out, fstack, (Comment)c);
            break;
          case 2: 
            printDocType(out, fstack, (DocType)c);
            break;
          case 3: 
            printElement(out, fstack, nstack, (Element)c);
            break;
          case 4: 
            printProcessingInstruction(out, fstack, (ProcessingInstruction)c);
            
            break;
          case 5: 
            String padding = ((Text)c).getText();
            if ((padding != null) && (Verifier.isAllXMLWhitespace(padding)))
            {


              out.writeCharacters(padding);
            }
            
            break;
          }
          
        }
      }
      
      if (fstack.getLineSeparator() != null) {
        out.writeCharacters(fstack.getLineSeparator());
      }
    }
    
    out.writeEndDocument();
  }
  













  protected void printDocType(XMLStreamWriter out, FormatStack fstack, DocType docType)
    throws XMLStreamException
  {
    String publicID = docType.getPublicID();
    String systemID = docType.getSystemID();
    String internalSubset = docType.getInternalSubset();
    boolean hasPublic = false;
    



    StringWriter sw = new StringWriter();
    
    sw.write("<!DOCTYPE ");
    sw.write(docType.getElementName());
    if (publicID != null) {
      sw.write(" PUBLIC \"");
      sw.write(publicID);
      sw.write("\"");
      hasPublic = true;
    }
    if (systemID != null) {
      if (!hasPublic) {
        sw.write(" SYSTEM");
      }
      sw.write(" \"");
      sw.write(systemID);
      sw.write("\"");
    }
    if ((internalSubset != null) && (!internalSubset.equals(""))) {
      sw.write(" [");
      sw.write(fstack.getLineSeparator());
      sw.write(docType.getInternalSubset());
      sw.write("]");
    }
    sw.write(">");
    




    out.writeDTD(sw.toString());
  }
  












  protected void printProcessingInstruction(XMLStreamWriter out, FormatStack fstack, ProcessingInstruction pi)
    throws XMLStreamException
  {
    String target = pi.getTarget();
    String rawData = pi.getData();
    if ((rawData != null) && (rawData.trim().length() > 0)) {
      out.writeProcessingInstruction(target, rawData);
    } else {
      out.writeProcessingInstruction(target);
    }
  }
  











  protected void printComment(XMLStreamWriter out, FormatStack fstack, Comment comment)
    throws XMLStreamException
  {
    out.writeComment(comment.getText());
  }
  











  protected void printEntityRef(XMLStreamWriter out, FormatStack fstack, EntityRef entity)
    throws XMLStreamException
  {
    out.writeEntityRef(entity.getName());
  }
  












  protected void printCDATA(XMLStreamWriter out, FormatStack fstack, CDATA cdata)
    throws XMLStreamException
  {
    out.writeCData(cdata.getText());
  }
  











  protected void printText(XMLStreamWriter out, FormatStack fstack, Text text)
    throws XMLStreamException
  {
    out.writeCharacters(text.getText());
  }
  














  protected void printElement(XMLStreamWriter out, FormatStack fstack, NamespaceStack nstack, Element element)
    throws XMLStreamException
  {
    ArrayList<String> restore = new ArrayList();
    nstack.push(element);
    Iterator i$;
    String pfx; try { for (Namespace nsa : nstack.addedForward()) {
        restore.add(nsa.getPrefix());
        if ("".equals(nsa.getPrefix())) {
          out.setDefaultNamespace(nsa.getURI());
        } else {
          out.setPrefix(nsa.getPrefix(), nsa.getURI());
        }
      }
      
      List<Content> content = element.getContent();
      
      Format.TextMode textmode = fstack.getTextMode();
      








      Walker walker = null;
      
      if (!content.isEmpty())
      {

        String space = element.getAttributeValue("space", Namespace.XML_NAMESPACE);
        

        if ("default".equals(space)) {
          textmode = fstack.getDefaultMode();
        }
        else if ("preserve".equals(space)) {
          textmode = Format.TextMode.PRESERVE;
        }
        
        fstack.push();
        try {
          fstack.setTextMode(textmode);
          walker = buildWalker(fstack, content, false);
          if (!walker.hasNext())
          {
            walker = null;
          }
        } finally {
          fstack.pop();
        }
      }
      




      boolean expandit = (walker != null) || (fstack.isExpandEmptyElements());
      
      Namespace ns = element.getNamespace();
      if (expandit) {
        out.writeStartElement(ns.getPrefix(), element.getName(), ns.getURI());
        

        for (Namespace nsd : nstack.addedForward()) {
          printNamespace(out, fstack, nsd);
        }
        

        if (element.hasAttributes()) {
          for (Attribute attribute : element.getAttributes()) {
            printAttribute(out, fstack, attribute);
          }
        }
        



        out.writeCharacters("");
        

        if (walker != null)
        {
          fstack.push();
          try {
            fstack.setTextMode(textmode);
            if ((!walker.isAllText()) && (fstack.getPadBetween() != null))
            {
              String indent = fstack.getPadBetween();
              printText(out, fstack, new Text(indent));
            }
            
            printContent(out, fstack, nstack, walker);
            
            if ((!walker.isAllText()) && (fstack.getPadLast() != null))
            {
              String indent = fstack.getPadLast();
              printText(out, fstack, new Text(indent));
            }
          } finally {
            fstack.pop();
          }
        }
        
        out.writeEndElement();



      }
      else
      {



        out.writeEmptyElement(ns.getPrefix(), element.getName(), ns.getURI());
        

        for (Namespace nsd : nstack.addedForward()) {
          printNamespace(out, fstack, nsd);
        }
        

        for (Attribute attribute : element.getAttributes()) {
          printAttribute(out, fstack, attribute);
        }
        
        out.writeCharacters("");
      }
    }
    finally {
      nstack.pop();
      for (i$ = restore.iterator(); i$.hasNext();) { pfx = (String)i$.next();
        for (Namespace nsa : nstack) {
          if (nsa.getPrefix().equals(pfx)) {
            if ("".equals(nsa.getPrefix())) {
              out.setDefaultNamespace(nsa.getURI()); break;
            }
            out.setPrefix(nsa.getPrefix(), nsa.getURI());
            
            break;
          }
        }
      }
    }
  }
  

















  protected void printContent(XMLStreamWriter out, FormatStack fstack, NamespaceStack nstack, Walker walker)
    throws XMLStreamException
  {
    while (walker.hasNext()) {
      Content content = walker.next();
      
      if (content == null) {
        if (walker.isCDATA()) {
          printCDATA(out, fstack, new CDATA(walker.text()));
        } else {
          printText(out, fstack, new Text(walker.text()));
        }
      } else {
        switch (1.$SwitchMap$org$jdom2$Content$CType[content.getCType().ordinal()]) {
        case 6: 
          printCDATA(out, fstack, (CDATA)content);
          break;
        case 1: 
          printComment(out, fstack, (Comment)content);
          break;
        case 3: 
          printElement(out, fstack, nstack, (Element)content);
          break;
        case 7: 
          printEntityRef(out, fstack, (EntityRef)content);
          break;
        case 4: 
          printProcessingInstruction(out, fstack, (ProcessingInstruction)content);
          
          break;
        case 5: 
          printText(out, fstack, (Text)content);
          break;
        case 2: 
          printDocType(out, fstack, (DocType)content);
          break;
        default: 
          throw new IllegalStateException("Unexpected Content " + content.getCType());
        }
        
      }
    }
  }
  















  protected void printNamespace(XMLStreamWriter out, FormatStack fstack, Namespace ns)
    throws XMLStreamException
  {
    String prefix = ns.getPrefix();
    String uri = ns.getURI();
    
    out.writeNamespace(prefix, uri);
  }
  












  protected void printAttribute(XMLStreamWriter out, FormatStack fstack, Attribute attribute)
    throws XMLStreamException
  {
    if ((!attribute.isSpecified()) && (fstack.isSpecifiedAttributesOnly())) {
      return;
    }
    
    Namespace ns = attribute.getNamespace();
    if (ns == Namespace.NO_NAMESPACE) {
      out.writeAttribute(attribute.getName(), attribute.getValue());
    } else {
      out.writeAttribute(ns.getPrefix(), ns.getURI(), attribute.getName(), attribute.getValue());
    }
  }
}
