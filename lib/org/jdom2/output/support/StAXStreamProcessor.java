package org.jdom2.output.support;

import org.jdom2.*;
import org.jdom2.output.Format;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.util.List;

public abstract interface StAXStreamProcessor
{
  public abstract void process(XMLStreamWriter paramXMLStreamWriter, Format paramFormat, Document paramDocument)
    throws XMLStreamException;
  
  public abstract void process(XMLStreamWriter paramXMLStreamWriter, Format paramFormat, DocType paramDocType)
    throws XMLStreamException;
  
  public abstract void process(XMLStreamWriter paramXMLStreamWriter, Format paramFormat, Element paramElement)
    throws XMLStreamException;
  
  public abstract void process(XMLStreamWriter paramXMLStreamWriter, Format paramFormat, List<? extends Content> paramList)
    throws XMLStreamException;
  
  public abstract void process(XMLStreamWriter paramXMLStreamWriter, Format paramFormat, CDATA paramCDATA)
    throws XMLStreamException;
  
  public abstract void process(XMLStreamWriter paramXMLStreamWriter, Format paramFormat, Text paramText)
    throws XMLStreamException;
  
  public abstract void process(XMLStreamWriter paramXMLStreamWriter, Format paramFormat, Comment paramComment)
    throws XMLStreamException;
  
  public abstract void process(XMLStreamWriter paramXMLStreamWriter, Format paramFormat, ProcessingInstruction paramProcessingInstruction)
    throws XMLStreamException;
  
  public abstract void process(XMLStreamWriter paramXMLStreamWriter, Format paramFormat, EntityRef paramEntityRef)
    throws XMLStreamException;
}
