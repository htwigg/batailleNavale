package org.jdom2.output.support;

import org.jdom2.*;
import org.jdom2.Content.CType;
import org.jdom2.output.Format;
import org.jdom2.output.Format.TextMode;
import org.jdom2.util.NamespaceStack;
import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.EntityReference;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;











































































































public abstract class AbstractDOMOutputProcessor
  extends AbstractOutputProcessor
  implements DOMOutputProcessor
{
  public AbstractDOMOutputProcessor() {}
  
  private static String getXmlnsTagFor(Namespace ns)
  {
    String attrName = "xmlns";
    if (!ns.getPrefix().equals("")) {
      attrName = attrName + ":";
      attrName = attrName + ns.getPrefix();
    }
    return attrName;
  }
  






  public org.w3c.dom.Document process(org.w3c.dom.Document basedoc, Format format, org.jdom2.Document doc)
  {
    return printDocument(new FormatStack(format), new NamespaceStack(), basedoc, doc);
  }
  


  public org.w3c.dom.Element process(org.w3c.dom.Document basedoc, Format format, org.jdom2.Element element)
  {
    return printElement(new FormatStack(format), new NamespaceStack(), basedoc, element);
  }
  


  public List<Node> process(org.w3c.dom.Document basedoc, Format format, List<? extends Content> list)
  {
    List<Node> ret = new ArrayList(list.size());
    
    FormatStack fstack = new FormatStack(format);
    NamespaceStack nstack = new NamespaceStack();
    for (Content c : list) {
      fstack.push();
      try {
        Node node = helperContentDispatcher(fstack, nstack, basedoc, c);
        
        if (node != null) {
          ret.add(node);
        }
      } finally {
        fstack.pop();
      }
    }
    return ret;
  }
  

  public CDATASection process(org.w3c.dom.Document basedoc, Format format, CDATA cdata)
  {
    List<CDATA> list = Collections.singletonList(cdata);
    FormatStack fstack = new FormatStack(format);
    Walker walker = buildWalker(fstack, list, false);
    if (walker.hasNext()) {
      Content c = walker.next();
      if (c == null) {
        return printCDATA(fstack, basedoc, new CDATA(walker.text()));
      }
      if (c.getCType() == Content.CType.CDATA) {
        return printCDATA(fstack, basedoc, (CDATA)c);
      }
    }
    
    return null;
  }
  

  public org.w3c.dom.Text process(org.w3c.dom.Document basedoc, Format format, org.jdom2.Text text)
  {
    List<org.jdom2.Text> list = Collections.singletonList(text);
    FormatStack fstack = new FormatStack(format);
    Walker walker = buildWalker(fstack, list, false);
    if (walker.hasNext()) {
      Content c = walker.next();
      if (c == null) {
        return printText(fstack, basedoc, new org.jdom2.Text(walker.text()));
      }
      if (c.getCType() == Content.CType.Text) {
        return printText(fstack, basedoc, (org.jdom2.Text)c);
      }
    }
    
    return null;
  }
  

  public org.w3c.dom.Comment process(org.w3c.dom.Document basedoc, Format format, org.jdom2.Comment comment)
  {
    return printComment(new FormatStack(format), basedoc, comment);
  }
  


  public org.w3c.dom.ProcessingInstruction process(org.w3c.dom.Document basedoc, Format format, org.jdom2.ProcessingInstruction pi)
  {
    return printProcessingInstruction(new FormatStack(format), basedoc, pi);
  }
  

  public EntityReference process(org.w3c.dom.Document basedoc, Format format, EntityRef entity)
  {
    return printEntityRef(new FormatStack(format), basedoc, entity);
  }
  

  public Attr process(org.w3c.dom.Document basedoc, Format format, Attribute attribute)
  {
    return printAttribute(new FormatStack(format), basedoc, attribute);
  }
  























  protected org.w3c.dom.Document printDocument(FormatStack fstack, NamespaceStack nstack, org.w3c.dom.Document basedoc, org.jdom2.Document doc)
  {
    if (!fstack.isOmitDeclaration()) {
      basedoc.setXmlVersion("1.0");
    }
    
    int sz = doc.getContentSize();
    
    if (sz > 0) {
      for (int i = 0; i < sz; i++) {
        Content c = doc.getContent(i);
        Node n = null;
        switch (1.$SwitchMap$org$jdom2$Content$CType[c.getCType().ordinal()]) {
        case 1: 
          n = printComment(fstack, basedoc, (org.jdom2.Comment)c);
          break;
        
        case 2: 
          break;
        

        case 3: 
          n = printElement(fstack, nstack, basedoc, (org.jdom2.Element)c);
          break;
        case 4: 
          n = printProcessingInstruction(fstack, basedoc, (org.jdom2.ProcessingInstruction)c);
          
          break;
        }
        
        
        if (n != null) {
          basedoc.appendChild(n);
        }
      }
    }
    
    return basedoc;
  }
  













  protected org.w3c.dom.ProcessingInstruction printProcessingInstruction(FormatStack fstack, org.w3c.dom.Document basedoc, org.jdom2.ProcessingInstruction pi)
  {
    String target = pi.getTarget();
    String rawData = pi.getData();
    if ((rawData == null) || (rawData.trim().length() == 0)) {
      rawData = "";
    }
    return basedoc.createProcessingInstruction(target, rawData);
  }
  











  protected org.w3c.dom.Comment printComment(FormatStack fstack, org.w3c.dom.Document basedoc, org.jdom2.Comment comment)
  {
    return basedoc.createComment(comment.getText());
  }
  












  protected EntityReference printEntityRef(FormatStack fstack, org.w3c.dom.Document basedoc, EntityRef entity)
  {
    return basedoc.createEntityReference(entity.getName());
  }
  












  protected CDATASection printCDATA(FormatStack fstack, org.w3c.dom.Document basedoc, CDATA cdata)
  {
    return basedoc.createCDATASection(cdata.getText());
  }
  











  protected org.w3c.dom.Text printText(FormatStack fstack, org.w3c.dom.Document basedoc, org.jdom2.Text text)
  {
    return basedoc.createTextNode(text.getText());
  }
  











  protected Attr printAttribute(FormatStack fstack, org.w3c.dom.Document basedoc, Attribute attribute)
  {
    if ((!attribute.isSpecified()) && (fstack.isSpecifiedAttributesOnly())) {
      return null;
    }
    Attr attr = basedoc.createAttributeNS(attribute.getNamespaceURI(), attribute.getQualifiedName());
    
    attr.setValue(attribute.getValue());
    return attr;
  }
  






















  protected org.w3c.dom.Element printElement(FormatStack fstack, NamespaceStack nstack, org.w3c.dom.Document basedoc, org.jdom2.Element element)
  {
    nstack.push(element);
    try
    {
      Format.TextMode textmode = fstack.getTextMode();
      

      String space = element.getAttributeValue("space", Namespace.XML_NAMESPACE);
      

      if ("default".equals(space)) {
        textmode = fstack.getDefaultMode();
      } else if ("preserve".equals(space)) {
        textmode = Format.TextMode.PRESERVE;
      }
      
      org.w3c.dom.Element ret = basedoc.createElementNS(element.getNamespaceURI(), element.getQualifiedName());
      

      for (Namespace ns : nstack.addedForward()) {
        if (ns != Namespace.XML_NAMESPACE)
        {

          ret.setAttributeNS("http://www.w3.org/2000/xmlns/", getXmlnsTagFor(ns), ns.getURI());
        }
      }
      if (element.hasAttributes()) {
        for (Attribute att : element.getAttributes()) {
          Attr a = printAttribute(fstack, basedoc, att);
          if (a != null) {
            ret.setAttributeNodeNS(a);
          }
        }
      }
      
      List<Content> content = element.getContent();
      Walker walker;
      if (!content.isEmpty()) {
        fstack.push();
        try {
          fstack.setTextMode(textmode);
          walker = buildWalker(fstack, content, false);
          
          if ((!walker.isAllText()) && (fstack.getPadBetween() != null))
          {
            org.w3c.dom.Text n = basedoc.createTextNode(fstack.getPadBetween());
            
            ret.appendChild(n);
          }
          
          printContent(fstack, nstack, basedoc, ret, walker);
          
          if ((!walker.isAllText()) && (fstack.getPadLast() != null))
          {
            org.w3c.dom.Text n = basedoc.createTextNode(fstack.getPadLast());
            
            ret.appendChild(n);
          }
        }
        finally {}
      }
      


      return ret;
    }
    finally {
      nstack.pop();
    }
  }
  

















  protected void printContent(FormatStack fstack, NamespaceStack nstack, org.w3c.dom.Document basedoc, Node target, Walker walker)
  {
    while (walker.hasNext()) {
      Content c = walker.next();
      Node n = null;
      if (c == null)
      {
        String text = walker.text();
        if (walker.isCDATA()) {
          n = printCDATA(fstack, basedoc, new CDATA(text));
        } else {
          n = printText(fstack, basedoc, new org.jdom2.Text(text));
        }
      } else {
        n = helperContentDispatcher(fstack, nstack, basedoc, c);
      }
      
      if (n != null) {
        target.appendChild(n);
      }
    }
  }
  

















  protected Node helperContentDispatcher(FormatStack fstack, NamespaceStack nstack, org.w3c.dom.Document basedoc, Content content)
  {
    switch (1.$SwitchMap$org$jdom2$Content$CType[content.getCType().ordinal()]) {
    case 5: 
      return printCDATA(fstack, basedoc, (CDATA)content);
    case 1: 
      return printComment(fstack, basedoc, (org.jdom2.Comment)content);
    case 3: 
      return printElement(fstack, nstack, basedoc, (org.jdom2.Element)content);
    case 6: 
      return printEntityRef(fstack, basedoc, (EntityRef)content);
    case 4: 
      return printProcessingInstruction(fstack, basedoc, (org.jdom2.ProcessingInstruction)content);
    
    case 7: 
      return printText(fstack, basedoc, (org.jdom2.Text)content);
    case 2: 
      return null;
    }
    throw new IllegalStateException("Unexpected Content " + content.getCType());
  }
}
