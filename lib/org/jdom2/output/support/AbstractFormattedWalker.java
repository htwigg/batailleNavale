package org.jdom2.output.support;

import org.jdom2.CDATA;
import org.jdom2.Content;
import org.jdom2.internal.ArrayCopy;
import org.jdom2.output.EscapeStrategy;
import org.jdom2.output.Format;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;




















































































public abstract class AbstractFormattedWalker
  implements Walker
{
  private static final CDATA CDATATOKEN = new CDATA("");
  





  protected static enum Trim
  {
    LEFT, 
    
    RIGHT, 
    
    BOTH, 
    
    COMPACT, 
    
    NONE;
    
    private Trim() {} }
  private static final Iterator<Content> EMPTYIT = new Iterator()
  {
    public boolean hasNext() {
      return false;
    }
    
    public Content next()
    {
      throw new NoSuchElementException("Cannot call next() on an empty iterator.");
    }
    
    public void remove()
    {
      throw new UnsupportedOperationException("Cannot remove from an empty iterator.");
    }
  };
  







  protected final class MultiText
  {
    private MultiText() {}
    






    private void ensurespace()
    {
      if (mtsize >= mtdata.length) {
        mtdata = ((Content[])ArrayCopy.copyOf(mtdata, mtsize + 1 + mtsize / 2));
        mttext = ((String[])ArrayCopy.copyOf(mttext, mtdata.length));
      }
    }
    




    private void closeText()
    {
      if (mtbuffer.length() == 0)
      {
        return;
      }
      ensurespace();
      mtdata[mtsize] = null;
      mttext[AbstractFormattedWalker.access$008(AbstractFormattedWalker.this)] = mtbuffer.toString();
      mtbuffer.setLength(0);
    }
    








    public void appendText(AbstractFormattedWalker.Trim trim, String text)
    {
      int tlen = text.length();
      if (tlen == 0) {
        return;
      }
      String toadd = null;
      switch (AbstractFormattedWalker.2.$SwitchMap$org$jdom2$output$support$AbstractFormattedWalker$Trim[trim.ordinal()]) {
      case 1: 
        toadd = text;
        break;
      case 2: 
        toadd = Format.trimBoth(text);
        break;
      case 3: 
        toadd = Format.trimLeft(text);
        break;
      case 4: 
        toadd = Format.trimRight(text);
        break;
      case 5: 
        toadd = Format.compact(text);
      }
      
      if (toadd != null) {
        toadd = escapeText(toadd);
        mtbuffer.append(toadd);
        mtgottext = true;
      }
    }
    
    private String escapeText(String text) {
      if ((escape == null) || (!fstack.getEscapeOutput())) {
        return text;
      }
      return Format.escapeText(escape, endofline, text);
    }
    
    private String escapeCDATA(String text) {
      if (escape == null) {
        return text;
      }
      return text;
    }
    





    public void appendCDATA(AbstractFormattedWalker.Trim trim, String text)
    {
      closeText();
      String toadd = null;
      switch (AbstractFormattedWalker.2.$SwitchMap$org$jdom2$output$support$AbstractFormattedWalker$Trim[trim.ordinal()]) {
      case 1: 
        toadd = text;
        break;
      case 2: 
        toadd = Format.trimBoth(text);
        break;
      case 3: 
        toadd = Format.trimLeft(text);
        break;
      case 4: 
        toadd = Format.trimRight(text);
        break;
      case 5: 
        toadd = Format.compact(text);
      }
      
      
      toadd = escapeCDATA(toadd);
      ensurespace();
      
      mtdata[mtsize] = AbstractFormattedWalker.CDATATOKEN;
      mttext[AbstractFormattedWalker.access$008(AbstractFormattedWalker.this)] = toadd;
      
      mtgottext = true;
    }
    





    private void forceAppend(String text)
    {
      mtgottext = true;
      mtbuffer.append(text);
    }
    




    public void appendRaw(Content c)
    {
      closeText();
      ensurespace();
      mttext[mtsize] = null;
      mtdata[AbstractFormattedWalker.access$008(AbstractFormattedWalker.this)] = c;
      mtbuffer.setLength(0);
    }
    




    public void done()
    {
      if ((mtpostpad) && (newlineindent != null))
      {
        mtbuffer.append(newlineindent);
      }
      if (mtgottext) {
        closeText();
      }
      mtbuffer.setLength(0);
    }
  }
  




  private Content pending = null;
  private final Iterator<? extends Content> content;
  private final boolean alltext;
  private final boolean allwhite;
  private final String newlineindent;
  private final String endofline;
  private final EscapeStrategy escape;
  private final FormatStack fstack;
  private boolean hasnext = true;
  













  private MultiText multitext = null;
  private MultiText pendingmt = null;
  private final MultiText holdingmt = new MultiText(null);
  
  private final StringBuilder mtbuffer = new StringBuilder();
  
  private boolean mtpostpad;
  
  private boolean mtgottext = false;
  
  private int mtsize = 0;
  private int mtsourcesize = 0;
  private Content[] mtsource = new Content[8];
  
  private Content[] mtdata = new Content[8];
  
  private String[] mttext = new String[8];
  

  private int mtpos = -1;
  




  private Boolean mtwasescape;
  




  public AbstractFormattedWalker(List<? extends Content> xx, FormatStack fstack, boolean doescape)
  {
    this.fstack = fstack;
    content = (xx.isEmpty() ? EMPTYIT : xx.iterator());
    escape = (doescape ? fstack.getEscapeStrategy() : null);
    newlineindent = fstack.getPadBetween();
    endofline = fstack.getLevelEOL();
    if (!content.hasNext()) {
      alltext = true;
      allwhite = true;
    } else {
      boolean atext = false;
      boolean awhite = false;
      pending = ((Content)content.next());
      if (isTextLike(pending))
      {


        pendingmt = buildMultiText(true);
        analyzeMultiText(pendingmt, 0, mtsourcesize);
        pendingmt.done();
        
        if (pending == null) {
          atext = true;
          awhite = mtsize == 0;
        }
        if (mtsize == 0)
        {
          pendingmt = null;
        }
      }
      alltext = atext;
      allwhite = awhite;
    }
    hasnext = ((pendingmt != null) || (pending != null));
  }
  

  public final Content next()
  {
    if (!hasnext) {
      throw new NoSuchElementException("Cannot walk off end of Content");
    }
    
    if ((multitext != null) && (mtpos + 1 >= mtsize))
    {
      multitext = null;
      resetMultiText();
    }
    if (pendingmt != null)
    {

      if ((mtwasescape != null) && (fstack.getEscapeOutput() != mtwasescape.booleanValue()))
      {



        mtsize = 0;
        mtwasescape = Boolean.valueOf(fstack.getEscapeOutput());
        analyzeMultiText(pendingmt, 0, mtsourcesize);
        pendingmt.done();
      }
      multitext = pendingmt;
      pendingmt = null;
    }
    
    if (multitext != null)
    {



      mtpos += 1;
      
      Content ret = mttext[mtpos] == null ? mtdata[mtpos] : null;
      



      hasnext = ((mtpos + 1 < mtsize) || (pending != null));
      


      return ret;
    }
    

    Content ret = pending;
    pending = (content.hasNext() ? (Content)content.next() : null);
    



    if (pending == null) {
      hasnext = false;


    }
    else if (isTextLike(pending))
    {
      pendingmt = buildMultiText(false);
      analyzeMultiText(pendingmt, 0, mtsourcesize);
      pendingmt.done();
      
      if (mtsize > 0) {
        hasnext = true;


      }
      else if ((pending != null) && (newlineindent != null))
      {

        resetMultiText();
        pendingmt = holdingmt;
        pendingmt.forceAppend(newlineindent);
        pendingmt.done();
        hasnext = true;
      } else {
        pendingmt = null;
        hasnext = (pending != null);
      }
      
    }
    else
    {
      if (newlineindent != null) {
        resetMultiText();
        pendingmt = holdingmt;
        pendingmt.forceAppend(newlineindent);
        pendingmt.done();
      }
      hasnext = true;
    }
    
    return ret;
  }
  
  private void resetMultiText() {
    mtsourcesize = 0;
    mtpos = -1;
    mtsize = 0;
    mtgottext = false;
    mtpostpad = false;
    mtwasescape = null;
    mtbuffer.setLength(0);
  }
  






  protected abstract void analyzeMultiText(MultiText paramMultiText, int paramInt1, int paramInt2);
  





  protected final Content get(int index)
  {
    return mtsource[index];
  }
  
  public final boolean isAllText()
  {
    return alltext;
  }
  
  public final boolean hasNext()
  {
    return hasnext;
  }
  








  private final MultiText buildMultiText(boolean first)
  {
    if ((!first) && (newlineindent != null)) {
      mtbuffer.append(newlineindent);
    }
    mtsourcesize = 0;
    do {
      if (mtsourcesize >= mtsource.length) {
        mtsource = ((Content[])ArrayCopy.copyOf(mtsource, mtsource.length * 2));
      }
      mtsource[(mtsourcesize++)] = pending;
      pending = (content.hasNext() ? (Content)content.next() : null);
    } while ((pending != null) && (isTextLike(pending)));
    
    mtpostpad = (pending != null);
    mtwasescape = Boolean.valueOf(fstack.getEscapeOutput());
    return holdingmt;
  }
  
  public final String text()
  {
    if ((multitext == null) || (mtpos >= mtsize)) {
      return null;
    }
    return mttext[mtpos];
  }
  
  public final boolean isCDATA()
  {
    if ((multitext == null) || (mtpos >= mtsize)) {
      return false;
    }
    if (mttext[mtpos] == null) {
      return false;
    }
    
    return mtdata[mtpos] == CDATATOKEN;
  }
  
  public final boolean isAllWhitespace()
  {
    return allwhite;
  }
  
  private final boolean isTextLike(Content c) {
    switch (2.$SwitchMap$org$jdom2$Content$CType[c.getCType().ordinal()]) {
    case 1: 
    case 2: 
    case 3: 
      return true;
    }
    
    
    return false;
  }
}
