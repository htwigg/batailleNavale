package org.jdom2.output.support;

import org.jdom2.Content;
import org.jdom2.Text;
import org.jdom2.Verifier;

import java.util.List;



































































public class WalkerTRIM
  extends AbstractFormattedWalker
{
  public WalkerTRIM(List<? extends Content> content, FormatStack fstack, boolean escape)
  {
    super(content, fstack, escape);
  }
  


  protected void analyzeMultiText(AbstractFormattedWalker.MultiText mtext, int offset, int len)
  {
    while (len > 0) {
      Content c = get(offset);
      if (!(c instanceof Text))
        break;
      if (!Verifier.isAllXMLWhitespace(c.getValue())) {
        break;
      }
      


      offset++;
      len--;
    }
    
    while (len > 0) {
      Content c = get(offset + len - 1);
      if (!(c instanceof Text))
        break;
      if (!Verifier.isAllXMLWhitespace(c.getValue())) {
        break;
      }
      


      len--;
    }
    
    for (int i = 0; i < len; i++) {
      AbstractFormattedWalker.Trim trim = AbstractFormattedWalker.Trim.NONE;
      if (i + 1 == len) {
        trim = AbstractFormattedWalker.Trim.RIGHT;
      }
      if (i == 0) {
        trim = AbstractFormattedWalker.Trim.LEFT;
      }
      if (len == 1) {
        trim = AbstractFormattedWalker.Trim.BOTH;
      }
      Content c = get(offset + i);
      switch (1.$SwitchMap$org$jdom2$Content$CType[c.getCType().ordinal()]) {
      case 1: 
        mtext.appendText(trim, c.getValue());
        break;
      case 2: 
        mtext.appendCDATA(trim, c.getValue());
        break;
      

      case 3: 
      default: 
        mtext.appendRaw(c);
      }
    }
  }
}
