package org.jdom2.output.support;

import org.jdom2.*;
import org.jdom2.output.Format;
import org.jdom2.output.Format.TextMode;
import org.jdom2.util.NamespaceStack;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;




























































































































































public abstract class AbstractXMLOutputProcessor
  extends AbstractOutputProcessor
  implements XMLOutputProcessor
{
  protected static final String CDATAPRE = "<![CDATA[";
  protected static final String CDATAPOST = "]]>";
  
  public AbstractXMLOutputProcessor() {}
  
  public void process(Writer out, Format format, Document doc)
    throws IOException
  {
    printDocument(out, new FormatStack(format), new NamespaceStack(), doc);
    out.flush();
  }
  






  public void process(Writer out, Format format, DocType doctype)
    throws IOException
  {
    printDocType(out, new FormatStack(format), doctype);
    out.flush();
  }
  








  public void process(Writer out, Format format, Element element)
    throws IOException
  {
    printElement(out, new FormatStack(format), new NamespaceStack(), element);
    
    out.flush();
  }
  







  public void process(Writer out, Format format, List<? extends Content> list)
    throws IOException
  {
    FormatStack fstack = new FormatStack(format);
    Walker walker = buildWalker(fstack, list, true);
    printContent(out, fstack, new NamespaceStack(), walker);
    out.flush();
  }
  







  public void process(Writer out, Format format, CDATA cdata)
    throws IOException
  {
    List<CDATA> list = Collections.singletonList(cdata);
    FormatStack fstack = new FormatStack(format);
    Walker walker = buildWalker(fstack, list, true);
    if (walker.hasNext()) {
      printContent(out, fstack, new NamespaceStack(), walker);
    }
    out.flush();
  }
  







  public void process(Writer out, Format format, Text text)
    throws IOException
  {
    List<Text> list = Collections.singletonList(text);
    FormatStack fstack = new FormatStack(format);
    Walker walker = buildWalker(fstack, list, true);
    if (walker.hasNext()) {
      printContent(out, fstack, new NamespaceStack(), walker);
    }
    out.flush();
  }
  






  public void process(Writer out, Format format, Comment comment)
    throws IOException
  {
    printComment(out, new FormatStack(format), comment);
    out.flush();
  }
  






  public void process(Writer out, Format format, ProcessingInstruction pi)
    throws IOException
  {
    FormatStack fstack = new FormatStack(format);
    
    fstack.setIgnoreTrAXEscapingPIs(true);
    printProcessingInstruction(out, fstack, pi);
    out.flush();
  }
  






  public void process(Writer out, Format format, EntityRef entity)
    throws IOException
  {
    printEntityRef(out, new FormatStack(format), entity);
    out.flush();
  }
  
















  protected void write(Writer out, String str)
    throws IOException
  {
    if (str == null) {
      return;
    }
    out.write(str);
  }
  








  protected void write(Writer out, char c)
    throws IOException
  {
    out.write(c);
  }
  

































  protected void attributeEscapedEntitiesFilter(Writer out, FormatStack fstack, String value)
    throws IOException
  {
    if (!fstack.getEscapeOutput())
    {
      write(out, value);
      return;
    }
    
    write(out, Format.escapeAttribute(fstack.getEscapeStrategy(), value));
  }
  












  protected void textRaw(Writer out, String str)
    throws IOException
  {
    write(out, str);
  }
  











  protected void textRaw(Writer out, char ch)
    throws IOException
  {
    write(out, ch);
  }
  








  protected void textEntityRef(Writer out, String name)
    throws IOException
  {
    textRaw(out, '&');
    textRaw(out, name);
    textRaw(out, ';');
  }
  




  protected void textCDATA(Writer out, String text)
    throws IOException
  {
    textRaw(out, "<![CDATA[");
    textRaw(out, text);
    textRaw(out, "]]>");
  }
  


























  protected void printDocument(Writer out, FormatStack fstack, NamespaceStack nstack, Document doc)
    throws IOException
  {
    List<Content> list = doc.hasRootElement() ? doc.getContent() : new ArrayList(doc.getContentSize());
    
    if (list.isEmpty()) {
      int sz = doc.getContentSize();
      for (int i = 0; i < sz; i++) {
        list.add(doc.getContent(i));
      }
    }
    
    printDeclaration(out, fstack);
    
    Walker walker = buildWalker(fstack, list, true);
    if (walker.hasNext()) {
      while (walker.hasNext())
      {
        Content c = walker.next();
        

        if (c == null)
        {
          String padding = walker.text();
          if ((padding != null) && (Verifier.isAllXMLWhitespace(padding)) && (!walker.isCDATA()))
          {



            write(out, padding);
          }
        } else {
          switch (1.$SwitchMap$org$jdom2$Content$CType[c.getCType().ordinal()]) {
          case 1: 
            printComment(out, fstack, (Comment)c);
            break;
          case 2: 
            printDocType(out, fstack, (DocType)c);
            break;
          case 3: 
            printElement(out, fstack, nstack, (Element)c);
            break;
          case 4: 
            printProcessingInstruction(out, fstack, (ProcessingInstruction)c);
            
            break;
          case 5: 
            String padding = ((Text)c).getText();
            if ((padding != null) && (Verifier.isAllXMLWhitespace(padding)))
            {


              write(out, padding);
            }
            
            break;
          }
          
        }
      }
      
      if (fstack.getLineSeparator() != null) {
        write(out, fstack.getLineSeparator());
      }
    }
  }
  












  protected void printDeclaration(Writer out, FormatStack fstack)
    throws IOException
  {
    if (fstack.isOmitDeclaration()) {
      return;
    }
    



    if (fstack.isOmitEncoding()) {
      write(out, "<?xml version=\"1.0\"?>");
    } else {
      write(out, "<?xml version=\"1.0\"");
      write(out, " encoding=\"");
      write(out, fstack.getEncoding());
      write(out, "\"?>");
    }
    




    write(out, fstack.getLineSeparator());
  }
  












  protected void printDocType(Writer out, FormatStack fstack, DocType docType)
    throws IOException
  {
    String publicID = docType.getPublicID();
    String systemID = docType.getSystemID();
    String internalSubset = docType.getInternalSubset();
    boolean hasPublic = false;
    



    write(out, "<!DOCTYPE ");
    write(out, docType.getElementName());
    if (publicID != null) {
      write(out, " PUBLIC \"");
      write(out, publicID);
      write(out, "\"");
      hasPublic = true;
    }
    if (systemID != null) {
      if (!hasPublic) {
        write(out, " SYSTEM");
      }
      write(out, " \"");
      write(out, systemID);
      write(out, "\"");
    }
    if ((internalSubset != null) && (!internalSubset.equals(""))) {
      write(out, " [");
      write(out, fstack.getLineSeparator());
      write(out, docType.getInternalSubset());
      write(out, "]");
    }
    write(out, ">");
  }
  













  protected void printProcessingInstruction(Writer out, FormatStack fstack, ProcessingInstruction pi)
    throws IOException
  {
    String target = pi.getTarget();
    boolean piProcessed = false;
    
    if (!fstack.isIgnoreTrAXEscapingPIs()) {
      if (target.equals("javax.xml.transform.disable-output-escaping"))
      {
        fstack.setEscapeOutput(false);
        piProcessed = true;
      }
      else if (target.equals("javax.xml.transform.enable-output-escaping"))
      {
        fstack.setEscapeOutput(true);
        piProcessed = true;
      }
    }
    if (!piProcessed) {
      String rawData = pi.getData();
      

      if (!"".equals(rawData)) {
        write(out, "<?");
        write(out, target);
        write(out, " ");
        write(out, rawData);
        write(out, "?>");
      }
      else {
        write(out, "<?");
        write(out, target);
        write(out, "?>");
      }
    }
  }
  











  protected void printComment(Writer out, FormatStack fstack, Comment comment)
    throws IOException
  {
    write(out, "<!--");
    write(out, comment.getText());
    write(out, "-->");
  }
  












  protected void printEntityRef(Writer out, FormatStack fstack, EntityRef entity)
    throws IOException
  {
    textEntityRef(out, entity.getName());
  }
  












  protected void printCDATA(Writer out, FormatStack fstack, CDATA cdata)
    throws IOException
  {
    textCDATA(out, cdata.getText());
  }
  











  protected void printText(Writer out, FormatStack fstack, Text text)
    throws IOException
  {
    if (fstack.getEscapeOutput()) {
      textRaw(out, Format.escapeText(fstack.getEscapeStrategy(), fstack.getLineSeparator(), text.getText()));
      

      return;
    }
    textRaw(out, text.getText());
  }
  

















  protected void printElement(Writer out, FormatStack fstack, NamespaceStack nstack, Element element)
    throws IOException
  {
    nstack.push(element);
    try {
      List<Content> content = element.getContent();
      


      write(out, "<");
      
      write(out, element.getQualifiedName());
      

      for (Namespace ns : nstack.addedForward()) {
        printNamespace(out, fstack, ns);
      }
      

      if (element.hasAttributes()) {
        for (Attribute attribute : element.getAttributes()) {
          printAttribute(out, fstack, attribute);
        }
      }
      
      if (content.isEmpty())
      {
        if (fstack.isExpandEmptyElements()) {
          write(out, "></");
          write(out, element.getQualifiedName());
          write(out, ">");
        }
        else {
          write(out, " />");
        }
        
        return;
      }
      

      fstack.push();
      
      try
      {
        String space = element.getAttributeValue("space", Namespace.XML_NAMESPACE);
        

        if ("default".equals(space)) {
          fstack.setTextMode(fstack.getDefaultMode());
        }
        else if ("preserve".equals(space)) {
          fstack.setTextMode(Format.TextMode.PRESERVE);
        }
        

        Walker walker = buildWalker(fstack, content, true);
        
        if (!walker.hasNext())
        {
          if (fstack.isExpandEmptyElements()) {
            write(out, "></");
            write(out, element.getQualifiedName());
            write(out, ">");
          }
          else {
            write(out, " />");
          }
          
          return;
        }
        
        write(out, ">");
        if (!walker.isAllText())
        {
          textRaw(out, fstack.getPadBetween());
        }
        
        printContent(out, fstack, nstack, walker);
        
        if (!walker.isAllText())
        {
          textRaw(out, fstack.getPadLast());
        }
        write(out, "</");
        write(out, element.getQualifiedName());
        write(out, ">");
      }
      finally {
        fstack.pop();
      }
    } finally {
      nstack.pop();
    }
  }
  



































  protected void printContent(Writer out, FormatStack fstack, NamespaceStack nstack, Walker walker)
    throws IOException
  {
    while (walker.hasNext()) {
      Content c = walker.next();
      if (c == null)
      {
        String t = walker.text();
        if (walker.isCDATA()) {
          textCDATA(out, t);
        } else {
          textRaw(out, t);
        }
      } else {
        switch (1.$SwitchMap$org$jdom2$Content$CType[c.getCType().ordinal()]) {
        case 6: 
          printCDATA(out, fstack, (CDATA)c);
          break;
        case 1: 
          printComment(out, fstack, (Comment)c);
          break;
        case 2: 
          printDocType(out, fstack, (DocType)c);
          break;
        case 3: 
          printElement(out, fstack, nstack, (Element)c);
          break;
        case 7: 
          printEntityRef(out, fstack, (EntityRef)c);
          break;
        case 4: 
          printProcessingInstruction(out, fstack, (ProcessingInstruction)c);
          
          break;
        case 5: 
          printText(out, fstack, (Text)c);
        }
        
      }
    }
  }
  













  protected void printNamespace(Writer out, FormatStack fstack, Namespace ns)
    throws IOException
  {
    String prefix = ns.getPrefix();
    String uri = ns.getURI();
    
    write(out, " xmlns");
    if (!prefix.equals("")) {
      write(out, ":");
      write(out, prefix);
    }
    write(out, "=\"");
    attributeEscapedEntitiesFilter(out, fstack, uri);
    write(out, "\"");
  }
  












  protected void printAttribute(Writer out, FormatStack fstack, Attribute attribute)
    throws IOException
  {
    if ((!attribute.isSpecified()) && (fstack.isSpecifiedAttributesOnly())) {
      return;
    }
    write(out, " ");
    write(out, attribute.getQualifiedName());
    write(out, "=");
    
    write(out, "\"");
    attributeEscapedEntitiesFilter(out, fstack, attribute.getValue());
    write(out, "\"");
  }
}
