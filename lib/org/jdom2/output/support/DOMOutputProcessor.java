package org.jdom2.output.support;

import org.jdom2.Attribute;
import org.jdom2.CDATA;
import org.jdom2.Content;
import org.jdom2.EntityRef;
import org.jdom2.output.Format;
import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.EntityReference;
import org.w3c.dom.Node;

import java.util.List;

public abstract interface DOMOutputProcessor
{
  public abstract org.w3c.dom.Document process(org.w3c.dom.Document paramDocument, Format paramFormat, org.jdom2.Document paramDocument1);
  
  public abstract org.w3c.dom.Element process(org.w3c.dom.Document paramDocument, Format paramFormat, org.jdom2.Element paramElement);
  
  public abstract List<Node> process(org.w3c.dom.Document paramDocument, Format paramFormat, List<? extends Content> paramList);
  
  public abstract CDATASection process(org.w3c.dom.Document paramDocument, Format paramFormat, CDATA paramCDATA);
  
  public abstract org.w3c.dom.Text process(org.w3c.dom.Document paramDocument, Format paramFormat, org.jdom2.Text paramText);
  
  public abstract org.w3c.dom.Comment process(org.w3c.dom.Document paramDocument, Format paramFormat, org.jdom2.Comment paramComment);
  
  public abstract org.w3c.dom.ProcessingInstruction process(org.w3c.dom.Document paramDocument, Format paramFormat, org.jdom2.ProcessingInstruction paramProcessingInstruction);
  
  public abstract EntityReference process(org.w3c.dom.Document paramDocument, Format paramFormat, EntityRef paramEntityRef);
  
  public abstract Attr process(org.w3c.dom.Document paramDocument, Format paramFormat, Attribute paramAttribute);
}
