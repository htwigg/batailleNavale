package org.jdom2.output.support;

import org.jdom2.internal.ArrayCopy;
import org.jdom2.output.EscapeStrategy;
import org.jdom2.output.Format;
import org.jdom2.output.Format.TextMode;








































































public final class FormatStack
{
  private int capacity = 16;
  private int depth = 0;
  




  private final Format.TextMode defaultMode;
  




  private final String indent;
  




  private final String encoding;
  



  private final String lineSeparator;
  



  private final boolean omitDeclaration;
  



  private final boolean omitEncoding;
  



  private final boolean expandEmptyElements;
  



  private final boolean specifiedAttributesOnly;
  



  private final EscapeStrategy escapeStrategy;
  



  private String[] levelIndent = new String[capacity];
  

  private String[] levelEOL = new String[capacity];
  

  private String[] levelEOLIndent = new String[capacity];
  

  private String[] termEOLIndent = new String[capacity];
  




  private boolean[] ignoreTrAXEscapingPIs = new boolean[capacity];
  

  private Format.TextMode[] mode = new Format.TextMode[capacity];
  

  private boolean[] escapeOutput = new boolean[capacity];
  





  public FormatStack(Format format)
  {
    indent = format.getIndent();
    lineSeparator = format.getLineSeparator();
    
    encoding = format.getEncoding();
    omitDeclaration = format.getOmitDeclaration();
    omitEncoding = format.getOmitEncoding();
    expandEmptyElements = format.getExpandEmptyElements();
    escapeStrategy = format.getEscapeStrategy();
    defaultMode = format.getTextMode();
    specifiedAttributesOnly = format.isSpecifiedAttributesOnly();
    
    mode[depth] = format.getTextMode();
    if (mode[depth] == Format.TextMode.PRESERVE)
    {
      levelIndent[depth] = null;
      levelEOL[depth] = null;
      levelEOLIndent[depth] = null;
      termEOLIndent[depth] = null;
    } else {
      levelIndent[depth] = (format.getIndent() == null ? null : "");
      
      levelEOL[depth] = format.getLineSeparator();
      levelEOLIndent[depth] = (levelIndent[depth] == null ? null : levelEOL[depth]);
      
      termEOLIndent[depth] = levelEOLIndent[depth];
    }
    
    ignoreTrAXEscapingPIs[depth] = format.getIgnoreTrAXEscapingPIs();
    escapeOutput[depth] = true;
  }
  



  private final void resetReusableIndents()
  {
    int d = depth + 1;
    while ((d < levelIndent.length) && (levelIndent[d] != null))
    {
      levelIndent[d] = null;
      d++;
    }
  }
  


  public String getIndent()
  {
    return indent;
  }
  


  public String getLineSeparator()
  {
    return lineSeparator;
  }
  


  public String getEncoding()
  {
    return encoding;
  }
  


  public boolean isOmitDeclaration()
  {
    return omitDeclaration;
  }
  






  public boolean isSpecifiedAttributesOnly()
  {
    return specifiedAttributesOnly;
  }
  


  public boolean isOmitEncoding()
  {
    return omitEncoding;
  }
  


  public boolean isExpandEmptyElements()
  {
    return expandEmptyElements;
  }
  


  public EscapeStrategy getEscapeStrategy()
  {
    return escapeStrategy;
  }
  


  public boolean isIgnoreTrAXEscapingPIs()
  {
    return ignoreTrAXEscapingPIs[depth];
  }
  





  public void setIgnoreTrAXEscapingPIs(boolean ignoreTrAXEscapingPIs)
  {
    this.ignoreTrAXEscapingPIs[depth] = ignoreTrAXEscapingPIs;
  }
  






  public boolean getEscapeOutput()
  {
    return escapeOutput[depth];
  }
  







  public void setEscapeOutput(boolean escape)
  {
    escapeOutput[depth] = escape;
  }
  



  public Format.TextMode getDefaultMode()
  {
    return defaultMode;
  }
  


  public String getLevelIndent()
  {
    return levelIndent[depth];
  }
  





  public String getPadBetween()
  {
    return levelEOLIndent[depth];
  }
  





  public String getPadLast()
  {
    return termEOLIndent[depth];
  }
  





  public void setLevelIndent(String indent)
  {
    levelIndent[depth] = indent;
    levelEOLIndent[depth] = (levelEOL[depth] + indent);
    
    resetReusableIndents();
  }
  


  public String getLevelEOL()
  {
    return levelEOL[depth];
  }
  





  public void setLevelEOL(String newline)
  {
    levelEOL[depth] = newline;
    resetReusableIndents();
  }
  


  public Format.TextMode getTextMode()
  {
    return mode[depth];
  }
  





  public void setTextMode(Format.TextMode mode)
  {
    if (this.mode[depth] == mode) {
      return;
    }
    this.mode[depth] = mode;
    switch (1.$SwitchMap$org$jdom2$output$Format$TextMode[mode.ordinal()]) {
    case 1: 
      levelEOL[depth] = null;
      levelIndent[depth] = null;
      levelEOLIndent[depth] = null;
      termEOLIndent[depth] = null;
      break;
    default: 
      levelEOL[depth] = lineSeparator;
      if ((indent == null) || (lineSeparator == null)) {
        levelEOLIndent[depth] = null;
        termEOLIndent[depth] = null;
      } else {
        if (depth > 0) {
          StringBuilder sb = new StringBuilder(indent.length() * depth);
          for (int i = 1; i < depth; i++) {
            sb.append(indent);
          }
          

          termEOLIndent[depth] = (lineSeparator + sb.toString());
          
          sb.append(indent);
          levelIndent[depth] = sb.toString();
        } else {
          termEOLIndent[depth] = lineSeparator;
          levelIndent[depth] = "";
        }
        levelEOLIndent[depth] = (lineSeparator + levelIndent[depth]);
      }
      break; }
    resetReusableIndents();
  }
  




  public void push()
  {
    int prev = depth++;
    if (depth >= capacity) {
      capacity *= 2;
      levelIndent = ((String[])ArrayCopy.copyOf(levelIndent, capacity));
      levelEOL = ((String[])ArrayCopy.copyOf(levelEOL, capacity));
      levelEOLIndent = ((String[])ArrayCopy.copyOf(levelEOLIndent, capacity));
      termEOLIndent = ((String[])ArrayCopy.copyOf(termEOLIndent, capacity));
      ignoreTrAXEscapingPIs = ArrayCopy.copyOf(ignoreTrAXEscapingPIs, capacity);
      mode = ((Format.TextMode[])ArrayCopy.copyOf(mode, capacity));
      escapeOutput = ArrayCopy.copyOf(escapeOutput, capacity);
    }
    
    ignoreTrAXEscapingPIs[depth] = ignoreTrAXEscapingPIs[prev];
    mode[depth] = mode[prev];
    escapeOutput[depth] = escapeOutput[prev];
    
    if ((levelIndent[prev] == null) || (levelEOL[prev] == null)) {
      levelIndent[depth] = null;
      levelEOL[depth] = null;
      levelEOLIndent[depth] = null;
      termEOLIndent[depth] = null;
    } else if (levelIndent[depth] == null)
    {

      levelEOL[depth] = levelEOL[prev];
      termEOLIndent[depth] = (levelEOL[depth] + levelIndent[prev]);
      levelIndent[depth] = (levelIndent[prev] + indent);
      levelEOLIndent[depth] = (levelEOL[depth] + levelIndent[depth]);
    }
  }
  






  public void pop()
  {
    depth -= 1;
  }
}
