package org.jdom2.output.support;

import org.jdom2.*;
import org.jdom2.output.Format;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

public abstract interface XMLOutputProcessor
{
  public abstract void process(Writer paramWriter, Format paramFormat, Document paramDocument)
    throws IOException;
  
  public abstract void process(Writer paramWriter, Format paramFormat, DocType paramDocType)
    throws IOException;
  
  public abstract void process(Writer paramWriter, Format paramFormat, Element paramElement)
    throws IOException;
  
  public abstract void process(Writer paramWriter, Format paramFormat, List<? extends Content> paramList)
    throws IOException;
  
  public abstract void process(Writer paramWriter, Format paramFormat, CDATA paramCDATA)
    throws IOException;
  
  public abstract void process(Writer paramWriter, Format paramFormat, Text paramText)
    throws IOException;
  
  public abstract void process(Writer paramWriter, Format paramFormat, Comment paramComment)
    throws IOException;
  
  public abstract void process(Writer paramWriter, Format paramFormat, ProcessingInstruction paramProcessingInstruction)
    throws IOException;
  
  public abstract void process(Writer paramWriter, Format paramFormat, EntityRef paramEntityRef)
    throws IOException;
}
