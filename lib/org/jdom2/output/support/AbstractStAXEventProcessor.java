package org.jdom2.output.support;

import org.jdom2.*;
import org.jdom2.Content.CType;
import org.jdom2.output.Format;
import org.jdom2.output.Format.TextMode;
import org.jdom2.util.NamespaceStack;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.util.XMLEventConsumer;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;





























































































public abstract class AbstractStAXEventProcessor
  extends AbstractOutputProcessor
  implements StAXEventProcessor
{
  public AbstractStAXEventProcessor() {}
  
  private static final class NSIterator
    implements Iterator<javax.xml.stream.events.Namespace>
  {
    private final Iterator<org.jdom2.Namespace> source;
    private final XMLEventFactory fac;
    
    public NSIterator(Iterator<org.jdom2.Namespace> source, XMLEventFactory fac)
    {
      this.source = source;
      this.fac = fac;
    }
    
    public boolean hasNext()
    {
      return source.hasNext();
    }
    
    public javax.xml.stream.events.Namespace next()
    {
      org.jdom2.Namespace ns = (org.jdom2.Namespace)source.next();
      return fac.createNamespace(ns.getPrefix(), ns.getURI());
    }
    
    public void remove()
    {
      throw new UnsupportedOperationException("Cannot remove Namespaces");
    }
  }
  

  private static final class AttIterator
    implements Iterator<javax.xml.stream.events.Attribute>
  {
    private final Iterator<org.jdom2.Attribute> source;
    private final XMLEventFactory fac;
    
    public AttIterator(Iterator<org.jdom2.Attribute> source, XMLEventFactory fac, boolean specifiedAttributesOnly)
    {
      this.source = (specifiedAttributesOnly ? specified(source) : source);
      this.fac = fac;
    }
    
    private Iterator<org.jdom2.Attribute> specified(Iterator<org.jdom2.Attribute> src) {
      if (src == null) {
        return null;
      }
      ArrayList<org.jdom2.Attribute> al = new ArrayList();
      while (src.hasNext()) {
        org.jdom2.Attribute att = (org.jdom2.Attribute)src.next();
        if (att.isSpecified()) {
          al.add(att);
        }
      }
      return al.isEmpty() ? null : al.iterator();
    }
    
    public boolean hasNext()
    {
      return (source != null) && (source.hasNext());
    }
    
    public javax.xml.stream.events.Attribute next()
    {
      org.jdom2.Attribute att = (org.jdom2.Attribute)source.next();
      org.jdom2.Namespace ns = att.getNamespace();
      if (ns == org.jdom2.Namespace.NO_NAMESPACE) {
        return fac.createAttribute(att.getName(), att.getValue());
      }
      return fac.createAttribute(ns.getPrefix(), ns.getURI(), att.getName(), att.getValue());
    }
    

    public void remove()
    {
      throw new UnsupportedOperationException("Cannot remove attributes");
    }
  }
  















  public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, Document doc)
    throws XMLStreamException
  {
    printDocument(out, new FormatStack(format), new NamespaceStack(), eventfactory, doc);
  }
  






  public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, DocType doctype)
    throws XMLStreamException
  {
    printDocType(out, new FormatStack(format), eventfactory, doctype);
  }
  








  public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, Element element)
    throws XMLStreamException
  {
    printElement(out, new FormatStack(format), new NamespaceStack(), eventfactory, element);
  }
  








  public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, List<? extends Content> list)
    throws XMLStreamException
  {
    FormatStack fstack = new FormatStack(format);
    Walker walker = buildWalker(fstack, list, false);
    printContent(out, new FormatStack(format), new NamespaceStack(), eventfactory, walker);
  }
  






  public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, CDATA cdata)
    throws XMLStreamException
  {
    List<CDATA> list = Collections.singletonList(cdata);
    FormatStack fstack = new FormatStack(format);
    Walker walker = buildWalker(fstack, list, false);
    if (walker.hasNext()) {
      Content c = walker.next();
      if (c == null) {
        printCDATA(out, fstack, eventfactory, new CDATA(walker.text()));
      } else if (c.getCType() == Content.CType.CDATA) {
        printCDATA(out, fstack, eventfactory, (CDATA)c);
      }
    }
  }
  






  public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, Text text)
    throws XMLStreamException
  {
    List<Text> list = Collections.singletonList(text);
    FormatStack fstack = new FormatStack(format);
    Walker walker = buildWalker(fstack, list, false);
    if (walker.hasNext()) {
      Content c = walker.next();
      if (c == null) {
        printText(out, fstack, eventfactory, new Text(walker.text()));
      } else if (c.getCType() == Content.CType.Text) {
        printText(out, fstack, eventfactory, (Text)c);
      }
    }
  }
  






  public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, Comment comment)
    throws XMLStreamException
  {
    printComment(out, new FormatStack(format), eventfactory, comment);
  }
  






  public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, ProcessingInstruction pi)
    throws XMLStreamException
  {
    FormatStack fstack = new FormatStack(format);
    
    fstack.setIgnoreTrAXEscapingPIs(true);
    printProcessingInstruction(out, fstack, eventfactory, pi);
  }
  






  public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, EntityRef entity)
    throws XMLStreamException
  {
    printEntityRef(out, new FormatStack(format), eventfactory, entity);
  }
  
























  protected void printDocument(XMLEventConsumer out, FormatStack fstack, NamespaceStack nstack, XMLEventFactory eventfactory, Document doc)
    throws XMLStreamException
  {
    if (fstack.isOmitDeclaration())
    {
      out.add(eventfactory.createStartDocument(null, null));
    } else if (fstack.isOmitEncoding()) {
      out.add(eventfactory.createStartDocument(null, "1.0"));
      if (fstack.getLineSeparator() != null) {
        out.add(eventfactory.createCharacters(fstack.getLineSeparator()));
      }
    } else {
      out.add(eventfactory.createStartDocument(fstack.getEncoding(), "1.0"));
      if (fstack.getLineSeparator() != null) {
        out.add(eventfactory.createCharacters(fstack.getLineSeparator()));
      }
    }
    



    List<Content> list = doc.hasRootElement() ? doc.getContent() : new ArrayList(doc.getContentSize());
    
    if (list.isEmpty()) {
      int sz = doc.getContentSize();
      for (int i = 0; i < sz; i++) {
        list.add(doc.getContent(i));
      }
    }
    
    Walker walker = buildWalker(fstack, list, false);
    if (walker.hasNext()) {
      while (walker.hasNext())
      {
        Content c = walker.next();
        


        if (c == null)
        {
          String padding = walker.text();
          if ((padding != null) && (Verifier.isAllXMLWhitespace(padding)) && (!walker.isCDATA()))
          {



            out.add(eventfactory.createCharacters(padding));
          }
        } else {
          switch (1.$SwitchMap$org$jdom2$Content$CType[c.getCType().ordinal()]) {
          case 1: 
            printComment(out, fstack, eventfactory, (Comment)c);
            break;
          case 2: 
            printDocType(out, fstack, eventfactory, (DocType)c);
            break;
          case 3: 
            printElement(out, fstack, nstack, eventfactory, (Element)c);
            
            break;
          case 4: 
            printProcessingInstruction(out, fstack, eventfactory, (ProcessingInstruction)c);
          }
          
        }
      }
      




      if (fstack.getLineSeparator() != null) {
        out.add(eventfactory.createCharacters(fstack.getLineSeparator()));
      }
    }
    
    out.add(eventfactory.createEndDocument());
  }
  















  protected void printDocType(XMLEventConsumer out, FormatStack fstack, XMLEventFactory eventfactory, DocType docType)
    throws XMLStreamException
  {
    String publicID = docType.getPublicID();
    String systemID = docType.getSystemID();
    String internalSubset = docType.getInternalSubset();
    boolean hasPublic = false;
    



    StringWriter sw = new StringWriter();
    
    sw.write("<!DOCTYPE ");
    sw.write(docType.getElementName());
    if (publicID != null) {
      sw.write(" PUBLIC \"");
      sw.write(publicID);
      sw.write("\"");
      hasPublic = true;
    }
    if (systemID != null) {
      if (!hasPublic) {
        sw.write(" SYSTEM");
      }
      sw.write(" \"");
      sw.write(systemID);
      sw.write("\"");
    }
    if ((internalSubset != null) && (!internalSubset.equals(""))) {
      sw.write(" [");
      sw.write(fstack.getLineSeparator());
      sw.write(docType.getInternalSubset());
      sw.write("]");
    }
    sw.write(">");
    




    out.add(eventfactory.createDTD(sw.toString()));
  }
  














  protected void printProcessingInstruction(XMLEventConsumer out, FormatStack fstack, XMLEventFactory eventfactory, ProcessingInstruction pi)
    throws XMLStreamException
  {
    String target = pi.getTarget();
    String rawData = pi.getData();
    if ((rawData != null) && (rawData.trim().length() > 0)) {
      out.add(eventfactory.createProcessingInstruction(target, rawData));
    } else {
      out.add(eventfactory.createProcessingInstruction(target, ""));
    }
  }
  













  protected void printComment(XMLEventConsumer out, FormatStack fstack, XMLEventFactory eventfactory, Comment comment)
    throws XMLStreamException
  {
    out.add(eventfactory.createComment(comment.getText()));
  }
  













  protected void printEntityRef(XMLEventConsumer out, FormatStack fstack, XMLEventFactory eventfactory, EntityRef entity)
    throws XMLStreamException
  {
    out.add(eventfactory.createEntityReference(entity.getName(), null));
  }
  














  protected void printCDATA(XMLEventConsumer out, FormatStack fstack, XMLEventFactory eventfactory, CDATA cdata)
    throws XMLStreamException
  {
    out.add(eventfactory.createCData(cdata.getText()));
  }
  













  protected void printText(XMLEventConsumer out, FormatStack fstack, XMLEventFactory eventfactory, Text text)
    throws XMLStreamException
  {
    out.add(eventfactory.createCharacters(text.getText()));
  }
  





















  protected void printElement(XMLEventConsumer out, FormatStack fstack, NamespaceStack nstack, XMLEventFactory eventfactory, Element element)
    throws XMLStreamException
  {
    nstack.push(element);
    try
    {
      org.jdom2.Namespace ns = element.getNamespace();
      Iterator<org.jdom2.Attribute> ait = element.hasAttributes() ? element.getAttributes().iterator() : null;
      

      if (ns == org.jdom2.Namespace.NO_NAMESPACE) {
        out.add(eventfactory.createStartElement("", "", element.getName(), new AttIterator(ait, eventfactory, fstack.isSpecifiedAttributesOnly()), new NSIterator(nstack.addedForward().iterator(), eventfactory)));

      }
      else if ("".equals(ns.getPrefix())) {
        out.add(eventfactory.createStartElement("", ns.getURI(), element.getName(), new AttIterator(ait, eventfactory, fstack.isSpecifiedAttributesOnly()), new NSIterator(nstack.addedForward().iterator(), eventfactory)));
      }
      else
      {
        out.add(eventfactory.createStartElement(ns.getPrefix(), ns.getURI(), element.getName(), new AttIterator(ait, eventfactory, fstack.isSpecifiedAttributesOnly()), new NSIterator(nstack.addedForward().iterator(), eventfactory)));
      }
      

      ait = null;
      
      List<Content> content = element.getContent();
      
      if (!content.isEmpty()) {
        Format.TextMode textmode = fstack.getTextMode();
        

        String space = element.getAttributeValue("space", org.jdom2.Namespace.XML_NAMESPACE);
        

        if ("default".equals(space)) {
          textmode = fstack.getDefaultMode();
        }
        else if ("preserve".equals(space)) {
          textmode = Format.TextMode.PRESERVE;
        }
        
        fstack.push();
        try
        {
          fstack.setTextMode(textmode);
          
          Walker walker = buildWalker(fstack, content, false);
          if (walker.hasNext()) {
            if ((!walker.isAllText()) && (fstack.getPadBetween() != null))
            {
              String indent = fstack.getPadBetween();
              printText(out, fstack, eventfactory, new Text(indent));
            }
            
            printContent(out, fstack, nstack, eventfactory, walker);
            
            if ((!walker.isAllText()) && (fstack.getPadLast() != null))
            {
              String indent = fstack.getPadLast();
              printText(out, fstack, eventfactory, new Text(indent));
            }
          }
        }
        finally {}
      }
      


      out.add(eventfactory.createEndElement(element.getNamespacePrefix(), element.getNamespaceURI(), element.getName(), new NSIterator(nstack.addedReverse().iterator(), eventfactory)));

    }
    finally
    {

      nstack.pop();
    }
  }
  



















  protected void printContent(XMLEventConsumer out, FormatStack fstack, NamespaceStack nstack, XMLEventFactory eventfactory, Walker walker)
    throws XMLStreamException
  {
    while (walker.hasNext())
    {
      Content content = walker.next();
      
      if (content == null) {
        if (walker.isCDATA()) {
          printCDATA(out, fstack, eventfactory, new CDATA(walker.text()));
        } else {
          printText(out, fstack, eventfactory, new Text(walker.text()));
        }
      } else {
        switch (1.$SwitchMap$org$jdom2$Content$CType[content.getCType().ordinal()]) {
        case 5: 
          printCDATA(out, fstack, eventfactory, (CDATA)content);
          break;
        case 1: 
          printComment(out, fstack, eventfactory, (Comment)content);
          break;
        case 3: 
          printElement(out, fstack, nstack, eventfactory, (Element)content);
          break;
        case 6: 
          printEntityRef(out, fstack, eventfactory, (EntityRef)content);
          break;
        case 4: 
          printProcessingInstruction(out, fstack, eventfactory, (ProcessingInstruction)content);
          
          break;
        case 7: 
          printText(out, fstack, eventfactory, (Text)content);
          break;
        case 2: 
          printDocType(out, fstack, eventfactory, (DocType)content);
          break;
        default: 
          throw new IllegalStateException("Unexpected Content " + content.getCType());
        }
      }
    }
  }
}
