package org.jdom2.output.support;

import org.jdom2.Content;
import org.jdom2.Verifier;

import java.util.List;



































































public class WalkerNORMALIZE
  extends AbstractFormattedWalker
{
  public WalkerNORMALIZE(List<? extends Content> content, FormatStack fstack, boolean escape)
  {
    super(content, fstack, escape);
  }
  
  private boolean isSpaceFirst(String text) {
    if (text.length() > 0) {
      return Verifier.isXMLWhitespace(text.charAt(0));
    }
    return false;
  }
  
  private boolean isSpaceLast(String text) {
    int tlen = text.length();
    if ((tlen > 0) && (Verifier.isXMLWhitespace(text.charAt(tlen - 1)))) {
      return true;
    }
    return false;
  }
  

  protected void analyzeMultiText(AbstractFormattedWalker.MultiText mtext, int offset, int len)
  {
    boolean needspace = false;
    boolean between = false;
    
    String ttext = null;
    for (int i = 0; i < len; i++) {
      Content c = get(offset + i);
      switch (1.$SwitchMap$org$jdom2$Content$CType[c.getCType().ordinal()]) {
      case 1: 
        ttext = c.getValue();
        if (Verifier.isAllXMLWhitespace(ttext)) {
          if ((between) && (ttext.length() > 0)) {
            needspace = true;
          }
        } else {
          if ((between) && ((needspace) || (isSpaceFirst(ttext)))) {
            mtext.appendText(AbstractFormattedWalker.Trim.NONE, " ");
          }
          mtext.appendText(AbstractFormattedWalker.Trim.COMPACT, ttext);
          between = true;
          needspace = isSpaceLast(ttext);
        }
        break;
      case 2: 
        ttext = c.getValue();
        if (Verifier.isAllXMLWhitespace(ttext)) {
          if ((between) && (ttext.length() > 0)) {
            needspace = true;
          }
        } else {
          if ((between) && ((needspace) || (isSpaceFirst(ttext)))) {
            mtext.appendText(AbstractFormattedWalker.Trim.NONE, " ");
          }
          mtext.appendCDATA(AbstractFormattedWalker.Trim.COMPACT, ttext);
          between = true;
          needspace = isSpaceLast(ttext);
        }
        break;
      

      case 3: 
      default: 
        ttext = null;
        if ((between) && (needspace)) {
          mtext.appendText(AbstractFormattedWalker.Trim.NONE, " ");
        }
        mtext.appendRaw(c);
        between = true;
        needspace = false;
      }
    }
  }
}
