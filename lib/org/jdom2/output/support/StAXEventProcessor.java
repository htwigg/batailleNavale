package org.jdom2.output.support;

import org.jdom2.*;
import org.jdom2.output.Format;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.util.XMLEventConsumer;
import java.util.List;

public abstract interface StAXEventProcessor
{
  public abstract void process(XMLEventConsumer paramXMLEventConsumer, Format paramFormat, XMLEventFactory paramXMLEventFactory, Document paramDocument)
    throws XMLStreamException;
  
  public abstract void process(XMLEventConsumer paramXMLEventConsumer, Format paramFormat, XMLEventFactory paramXMLEventFactory, DocType paramDocType)
    throws XMLStreamException;
  
  public abstract void process(XMLEventConsumer paramXMLEventConsumer, Format paramFormat, XMLEventFactory paramXMLEventFactory, Element paramElement)
    throws XMLStreamException;
  
  public abstract void process(XMLEventConsumer paramXMLEventConsumer, Format paramFormat, XMLEventFactory paramXMLEventFactory, List<? extends Content> paramList)
    throws XMLStreamException;
  
  public abstract void process(XMLEventConsumer paramXMLEventConsumer, Format paramFormat, XMLEventFactory paramXMLEventFactory, CDATA paramCDATA)
    throws XMLStreamException;
  
  public abstract void process(XMLEventConsumer paramXMLEventConsumer, Format paramFormat, XMLEventFactory paramXMLEventFactory, Text paramText)
    throws XMLStreamException;
  
  public abstract void process(XMLEventConsumer paramXMLEventConsumer, Format paramFormat, XMLEventFactory paramXMLEventFactory, Comment paramComment)
    throws XMLStreamException;
  
  public abstract void process(XMLEventConsumer paramXMLEventConsumer, Format paramFormat, XMLEventFactory paramXMLEventFactory, ProcessingInstruction paramProcessingInstruction)
    throws XMLStreamException;
  
  public abstract void process(XMLEventConsumer paramXMLEventConsumer, Format paramFormat, XMLEventFactory paramXMLEventFactory, EntityRef paramEntityRef)
    throws XMLStreamException;
}
