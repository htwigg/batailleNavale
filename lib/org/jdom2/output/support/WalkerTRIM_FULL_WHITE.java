package org.jdom2.output.support;

import org.jdom2.Content;
import org.jdom2.Text;
import org.jdom2.Verifier;

import java.util.List;



































































public class WalkerTRIM_FULL_WHITE
  extends AbstractFormattedWalker
{
  public WalkerTRIM_FULL_WHITE(List<? extends Content> content, FormatStack fstack, boolean escape)
  {
    super(content, fstack, escape);
  }
  

  protected void analyzeMultiText(AbstractFormattedWalker.MultiText mtext, int offset, int len)
  {
    int ln = len;
    for (;;) { ln--; if (ln < 0) break;
      Content c = get(offset + ln);
      if (!(c instanceof Text))
        break;
      if (!Verifier.isAllXMLWhitespace(c.getValue())) {
        break;
      }
    }
    


    if (ln < 0)
    {
      return;
    }
    

    for (int i = 0; i < len; i++) {
      Content c = get(offset + i);
      switch (1.$SwitchMap$org$jdom2$Content$CType[c.getCType().ordinal()]) {
      case 1: 
        mtext.appendText(AbstractFormattedWalker.Trim.NONE, c.getValue());
        break;
      case 2: 
        mtext.appendCDATA(AbstractFormattedWalker.Trim.NONE, c.getValue());
        break;
      

      case 3: 
      default: 
        mtext.appendRaw(c);
      }
    }
  }
}
