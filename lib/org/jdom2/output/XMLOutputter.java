package org.jdom2.output;

import org.jdom2.*;
import org.jdom2.output.support.AbstractXMLOutputProcessor;
import org.jdom2.output.support.FormatStack;
import org.jdom2.output.support.XMLOutputProcessor;

import java.util.List;










































































































































public final class XMLOutputter
  implements Cloneable
{
  private static final Writer makeWriter(OutputStream out, Format format)
    throws UnsupportedEncodingException
  {
    return new BufferedWriter(new OutputStreamWriter(new BufferedOutputStream(out), format.getEncoding()));
  }
  









  private static final class DefaultXMLProcessor
    extends AbstractXMLOutputProcessor
  {
    private DefaultXMLProcessor() {}
    








    public String escapeAttributeEntities(String str, Format format)
    {
      StringWriter sw = new StringWriter();
      try {
        super.attributeEscapedEntitiesFilter(sw, new FormatStack(format), str);
      }
      catch (IOException e) {}
      
      return sw.toString();
    }
    










    public final String escapeElementEntities(String str, Format format)
    {
      return Format.escapeText(format.getEscapeStrategy(), format.getLineSeparator(), str);
    }
  }
  






  private static final DefaultXMLProcessor DEFAULTPROCESSOR = new DefaultXMLProcessor(null);
  








  private Format myFormat = null;
  

  private XMLOutputProcessor myProcessor = null;
  























  public XMLOutputter(Format format, XMLOutputProcessor processor)
  {
    myFormat = (format == null ? Format.getRawFormat() : format.clone());
    myProcessor = (processor == null ? DEFAULTPROCESSOR : processor);
  }
  



  public XMLOutputter()
  {
    this(null, null);
  }
  








  public XMLOutputter(XMLOutputter that)
  {
    this(myFormat, null);
  }
  












  public XMLOutputter(Format format)
  {
    this(format, null);
  }
  







  public XMLOutputter(XMLOutputProcessor processor)
  {
    this(null, processor);
  }
  













  public void setFormat(Format newFormat)
  {
    myFormat = newFormat.clone();
  }
  







  public Format getFormat()
  {
    return myFormat;
  }
  





  public XMLOutputProcessor getXMLOutputProcessor()
  {
    return myProcessor;
  }
  






  public void setXMLOutputProcessor(XMLOutputProcessor processor)
  {
    myProcessor = processor;
  }
  




















  public final void output(Document doc, OutputStream out)
    throws IOException
  {
    output(doc, makeWriter(out, myFormat));
  }
  











  public final void output(DocType doctype, OutputStream out)
    throws IOException
  {
    output(doctype, makeWriter(out, myFormat));
  }
  











  public final void output(Element element, OutputStream out)
    throws IOException
  {
    output(element, makeWriter(out, myFormat));
  }
  















  public final void outputElementContent(Element element, OutputStream out)
    throws IOException
  {
    outputElementContent(element, makeWriter(out, myFormat));
  }
  



















  public final void output(List<? extends Content> list, OutputStream out)
    throws IOException
  {
    output(list, makeWriter(out, myFormat));
  }
  










  public final void output(CDATA cdata, OutputStream out)
    throws IOException
  {
    output(cdata, makeWriter(out, myFormat));
  }
  











  public final void output(Text text, OutputStream out)
    throws IOException
  {
    output(text, makeWriter(out, myFormat));
  }
  










  public final void output(Comment comment, OutputStream out)
    throws IOException
  {
    output(comment, makeWriter(out, myFormat));
  }
  











  public final void output(ProcessingInstruction pi, OutputStream out)
    throws IOException
  {
    output(pi, makeWriter(out, myFormat));
  }
  










  public void output(EntityRef entity, OutputStream out)
    throws IOException
  {
    output(entity, makeWriter(out, myFormat));
  }
  



















  public final String outputString(Document doc)
  {
    StringWriter out = new StringWriter();
    try {
      output(doc, out);
    }
    catch (IOException e) {}
    
    return out.toString();
  }
  











  public final String outputString(DocType doctype)
  {
    StringWriter out = new StringWriter();
    try {
      output(doctype, out);
    }
    catch (IOException e) {}
    
    return out.toString();
  }
  











  public final String outputString(Element element)
  {
    StringWriter out = new StringWriter();
    try {
      output(element, out);
    }
    catch (IOException e) {}
    
    return out.toString();
  }
  
















  public final String outputString(List<? extends Content> list)
  {
    StringWriter out = new StringWriter();
    try {
      output(list, out);
    }
    catch (IOException e) {}
    
    return out.toString();
  }
  











  public final String outputString(CDATA cdata)
  {
    StringWriter out = new StringWriter();
    try {
      output(cdata, out);
    }
    catch (IOException e) {}
    
    return out.toString();
  }
  











  public final String outputString(Text text)
  {
    StringWriter out = new StringWriter();
    try {
      output(text, out);
    }
    catch (IOException e) {}
    
    return out.toString();
  }
  











  public final String outputString(Comment comment)
  {
    StringWriter out = new StringWriter();
    try {
      output(comment, out);
    }
    catch (IOException e) {}
    
    return out.toString();
  }
  











  public final String outputString(ProcessingInstruction pi)
  {
    StringWriter out = new StringWriter();
    try {
      output(pi, out);
    }
    catch (IOException e) {}
    
    return out.toString();
  }
  











  public final String outputString(EntityRef entity)
  {
    StringWriter out = new StringWriter();
    try {
      output(entity, out);
    }
    catch (IOException e) {}
    
    return out.toString();
  }
  















  public final String outputElementContentString(Element element)
  {
    StringWriter out = new StringWriter();
    try {
      outputElementContent(element, out);
    }
    catch (IOException e) {}
    
    return out.toString();
  }
  























  public final void output(Document doc, Writer out)
    throws IOException
  {
    myProcessor.process(out, myFormat, doc);
    out.flush();
  }
  










  public final void output(DocType doctype, Writer out)
    throws IOException
  {
    myProcessor.process(out, myFormat, doctype);
    out.flush();
  }
  













  public final void output(Element element, Writer out)
    throws IOException
  {
    myProcessor.process(out, myFormat, element);
    out.flush();
  }
  















  public final void outputElementContent(Element element, Writer out)
    throws IOException
  {
    myProcessor.process(out, myFormat, element.getContent());
    out.flush();
  }
  













  public final void output(List<? extends Content> list, Writer out)
    throws IOException
  {
    myProcessor.process(out, myFormat, list);
    out.flush();
  }
  










  public final void output(CDATA cdata, Writer out)
    throws IOException
  {
    myProcessor.process(out, myFormat, cdata);
    out.flush();
  }
  











  public final void output(Text text, Writer out)
    throws IOException
  {
    myProcessor.process(out, myFormat, text);
    out.flush();
  }
  










  public final void output(Comment comment, Writer out)
    throws IOException
  {
    myProcessor.process(out, myFormat, comment);
    out.flush();
  }
  











  public final void output(ProcessingInstruction pi, Writer out)
    throws IOException
  {
    myProcessor.process(out, myFormat, pi);
    out.flush();
  }
  










  public final void output(EntityRef entity, Writer out)
    throws IOException
  {
    myProcessor.process(out, myFormat, entity);
    out.flush();
  }
  













  public String escapeAttributeEntities(String str)
  {
    return DEFAULTPROCESSOR.escapeAttributeEntities(str, myFormat);
  }
  







  public String escapeElementEntities(String str)
  {
    return DEFAULTPROCESSOR.escapeElementEntities(str, myFormat);
  }
  













  public XMLOutputter clone()
  {
    try
    {
      return (XMLOutputter)super.clone();

    }
    catch (CloneNotSupportedException e)
    {

      throw new RuntimeException(e.toString());
    }
  }
  





  public String toString()
  {
    StringBuilder buffer = new StringBuilder();
    buffer.append("XMLOutputter[omitDeclaration = ");
    buffer.append(myFormat.omitDeclaration);
    buffer.append(", ");
    buffer.append("encoding = ");
    buffer.append(myFormat.encoding);
    buffer.append(", ");
    buffer.append("omitEncoding = ");
    buffer.append(myFormat.omitEncoding);
    buffer.append(", ");
    buffer.append("indent = '");
    buffer.append(myFormat.indent);
    buffer.append("'");
    buffer.append(", ");
    buffer.append("expandEmptyElements = ");
    buffer.append(myFormat.expandEmptyElements);
    buffer.append(", ");
    buffer.append("lineSeparator = '");
    for (char ch : myFormat.lineSeparator.toCharArray()) {
      switch (ch) {
      case '\r': 
        buffer.append("\\r");
        break;
      case '\n': 
        buffer.append("\\n");
        break;
      case '\t': 
        buffer.append("\\t");
        break;
      case '\013': case '\f': default: 
        buffer.append("[" + ch + "]");
      }
      
    }
    buffer.append("', ");
    buffer.append("textMode = ");
    buffer.append(myFormat.mode + "]");
    return buffer.toString();
  }
}
