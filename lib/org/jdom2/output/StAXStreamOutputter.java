package org.jdom2.output;

import org.jdom2.*;
import org.jdom2.output.support.AbstractStAXStreamProcessor;
import org.jdom2.output.support.StAXStreamProcessor;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.util.List;





















































































































public final class StAXStreamOutputter
  implements Cloneable
{
  private static final DefaultStAXStreamProcessor DEFAULTPROCESSOR = new DefaultStAXStreamProcessor(null);
  








  private Format myFormat = null;
  

  private StAXStreamProcessor myProcessor = null;
  























  public StAXStreamOutputter(Format format, StAXStreamProcessor processor)
  {
    myFormat = (format == null ? Format.getRawFormat() : format.clone());
    myProcessor = (processor == null ? DEFAULTPROCESSOR : processor);
  }
  



  public StAXStreamOutputter()
  {
    this(null, null);
  }
  












  public StAXStreamOutputter(Format format)
  {
    this(format, null);
  }
  







  public StAXStreamOutputter(StAXStreamProcessor processor)
  {
    this(null, processor);
  }
  













  public void setFormat(Format newFormat)
  {
    myFormat = newFormat.clone();
  }
  







  public Format getFormat()
  {
    return myFormat;
  }
  





  public StAXStreamProcessor getStAXStream()
  {
    return myProcessor;
  }
  






  public void setStAXStreamProcessor(StAXStreamProcessor processor)
  {
    myProcessor = processor;
  }
  























  public final void output(Document doc, XMLStreamWriter out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, doc);
    out.flush();
  }
  










  public final void output(DocType doctype, XMLStreamWriter out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, doctype);
    out.flush();
  }
  













  public final void output(Element element, XMLStreamWriter out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, element);
    out.flush();
  }
  















  public final void outputElementContent(Element element, XMLStreamWriter out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, element.getContent());
    out.flush();
  }
  













  public final void output(List<? extends Content> list, XMLStreamWriter out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, list);
    out.flush();
  }
  










  public final void output(CDATA cdata, XMLStreamWriter out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, cdata);
    out.flush();
  }
  











  public final void output(Text text, XMLStreamWriter out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, text);
    out.flush();
  }
  










  public final void output(Comment comment, XMLStreamWriter out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, comment);
    out.flush();
  }
  











  public final void output(ProcessingInstruction pi, XMLStreamWriter out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, pi);
    out.flush();
  }
  










  public final void output(EntityRef entity, XMLStreamWriter out)
    throws XMLStreamException
  {
    myProcessor.process(out, myFormat, entity);
    out.flush();
  }
  













  public StAXStreamOutputter clone()
  {
    try
    {
      return (StAXStreamOutputter)super.clone();

    }
    catch (CloneNotSupportedException e)
    {

      throw new RuntimeException(e.toString());
    }
  }
  





  public String toString()
  {
    StringBuilder buffer = new StringBuilder();
    buffer.append("StAXStreamOutputter[omitDeclaration = ");
    buffer.append(myFormat.omitDeclaration);
    buffer.append(", ");
    buffer.append("encoding = ");
    buffer.append(myFormat.encoding);
    buffer.append(", ");
    buffer.append("omitEncoding = ");
    buffer.append(myFormat.omitEncoding);
    buffer.append(", ");
    buffer.append("indent = '");
    buffer.append(myFormat.indent);
    buffer.append("'");
    buffer.append(", ");
    buffer.append("expandEmptyElements = ");
    buffer.append(myFormat.expandEmptyElements);
    buffer.append(", ");
    buffer.append("lineSeparator = '");
    for (char ch : myFormat.lineSeparator.toCharArray()) {
      switch (ch) {
      case '\r': 
        buffer.append("\\r");
        break;
      case '\n': 
        buffer.append("\\n");
        break;
      case '\t': 
        buffer.append("\\t");
        break;
      case '\013': case '\f': default: 
        buffer.append("[" + ch + "]");
      }
      
    }
    buffer.append("', ");
    buffer.append("textMode = ");
    buffer.append(myFormat.mode + "]");
    return buffer.toString();
  }
  
  private static final class DefaultStAXStreamProcessor
    extends AbstractStAXStreamProcessor
  {
    private DefaultStAXStreamProcessor() {}
  }
}
