package org.jdom2.output;

import org.jdom2.IllegalDataException;
import org.jdom2.Verifier;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;





















































































public class Format
  implements Cloneable
{
  private static final class EscapeStrategyUTF
    implements EscapeStrategy
  {
    private EscapeStrategyUTF() {}
    
    public final boolean shouldEscape(char ch)
    {
      return Verifier.isHighSurrogate(ch);
    }
  }
  



  private static final EscapeStrategy UTFEscapeStrategy = new EscapeStrategyUTF(null);
  
  private static final class EscapeStrategy8Bits
    implements EscapeStrategy
  {
    private EscapeStrategy8Bits() {}
    
    public boolean shouldEscape(char ch)
    {
      return ch >>> '\b' != 0;
    }
  }
  



  private static final EscapeStrategy Bits8EscapeStrategy = new EscapeStrategy8Bits(null);
  
  private static final class EscapeStrategy7Bits
    implements EscapeStrategy
  {
    private EscapeStrategy7Bits() {}
    
    public boolean shouldEscape(char ch)
    {
      return ch >>> '\007' != 0;
    }
  }
  



  private static final EscapeStrategy Bits7EscapeStrategy = new EscapeStrategy7Bits(null);
  




  private static final EscapeStrategy DefaultEscapeStrategy = new EscapeStrategy()
  {
    public boolean shouldEscape(char ch) {
      if (Verifier.isHighSurrogate(ch)) {
        return true;
      }
      
      return false;
    }
  };
  private static final String STANDARD_INDENT = "  ";
  
  private static final class DefaultCharsetEscapeStrategy
    implements EscapeStrategy
  {
    private final CharsetEncoder encoder;
    
    public DefaultCharsetEscapeStrategy(CharsetEncoder cse)
    {
      encoder = cse;
    }
    

    public boolean shouldEscape(char ch)
    {
      if (Verifier.isHighSurrogate(ch)) {
        return true;
      }
      
      return !encoder.canEncode(ch);
    }
  }
  









  public static Format getRawFormat()
  {
    return new Format();
  }
  









  public static Format getPrettyFormat()
  {
    Format f = new Format();
    f.setIndent("  ");
    f.setTextMode(TextMode.TRIM);
    return f;
  }
  








  public static Format getCompactFormat()
  {
    Format f = new Format();
    f.setTextMode(TextMode.NORMALIZE);
    return f;
  }
  







  public static final String compact(String str)
  {
    int right = str.length() - 1;
    int left = 0;
    while ((left <= right) && (Verifier.isXMLWhitespace(str.charAt(left))))
    {
      left++;
    }
    while ((right > left) && (Verifier.isXMLWhitespace(str.charAt(right))))
    {
      right--;
    }
    
    if (left > right) {
      return "";
    }
    
    boolean space = true;
    StringBuilder buffer = new StringBuilder(right - left + 1);
    while (left <= right) {
      char c = str.charAt(left);
      if (Verifier.isXMLWhitespace(c)) {
        if (space) {
          buffer.append(' ');
          space = false;
        }
      } else {
        buffer.append(c);
        space = true;
      }
      left++;
    }
    return buffer.toString();
  }
  






  public static final String trimRight(String str)
  {
    int right = str.length() - 1;
    while ((right >= 0) && (Verifier.isXMLWhitespace(str.charAt(right)))) {
      right--;
    }
    if (right < 0) {
      return "";
    }
    return str.substring(0, right + 1);
  }
  






  public static final String trimLeft(String str)
  {
    int right = str.length();
    int left = 0;
    while ((left < right) && (Verifier.isXMLWhitespace(str.charAt(left)))) {
      left++;
    }
    if (left >= right) {
      return "";
    }
    
    return str.substring(left);
  }
  






  public static final String trimBoth(String str)
  {
    int right = str.length() - 1;
    while ((right > 0) && (Verifier.isXMLWhitespace(str.charAt(right)))) {
      right--;
    }
    int left = 0;
    while ((left <= right) && (Verifier.isXMLWhitespace(str.charAt(left)))) {
      left++;
    }
    if (left > right) {
      return "";
    }
    return str.substring(left, right + 1);
  }
  


















  public static final String escapeAttribute(EscapeStrategy strategy, String value)
  {
    int len = value.length();
    int idx = 0;
    
    while (idx < len) {
      char ch = value.charAt(idx);
      if ((ch == '<') || (ch == '>') || (ch == '&') || (ch == '\r') || (ch == '\n') || (ch == '"') || (ch == '\t') || (strategy.shouldEscape(ch))) {
        break;
      }
      
      idx++;
    }
    
    if (idx == len) {
      return value;
    }
    
    char highsurrogate = '\000';
    StringBuilder sb = new StringBuilder(len + 5);
    sb.append(value, 0, idx);
    while (idx < len) {
      char ch = value.charAt(idx++);
      if (highsurrogate > 0) {
        if (!Verifier.isLowSurrogate(ch)) {
          throw new IllegalDataException("Could not decode surrogate pair 0x" + Integer.toHexString(highsurrogate) + " / 0x" + Integer.toHexString(ch));
        }
        


        int chp = Verifier.decodeSurrogatePair(highsurrogate, ch);
        sb.append("&#x");
        sb.append(Integer.toHexString(chp));
        sb.append(';');
        highsurrogate = '\000';
      }
      else {
        switch (ch) {
        case '<': 
          sb.append("&lt;");
          break;
        case '>': 
          sb.append("&gt;");
          break;
        case '&': 
          sb.append("&amp;");
          break;
        case '\r': 
          sb.append("&#xD;");
          break;
        case '"': 
          sb.append("&quot;");
          break;
        case '\t': 
          sb.append("&#x9;");
          break;
        case '\n': 
          sb.append("&#xA;");
          break;
        
        default: 
          if (strategy.shouldEscape(ch))
          {

            if (Verifier.isHighSurrogate(ch))
            {
              highsurrogate = ch;
            } else {
              sb.append("&#x");
              sb.append(Integer.toHexString(ch));
              sb.append(';');
            }
          } else
            sb.append(ch);
          break;
        }
      }
    }
    if (highsurrogate > 0) {
      throw new IllegalDataException("Surrogate pair 0x" + Integer.toHexString(highsurrogate) + "truncated");
    }
    

    return sb.toString();
  }
  























  public static final String escapeText(EscapeStrategy strategy, String eol, String value)
  {
    int right = value.length();
    int idx = 0;
    while (idx < right) {
      char ch = value.charAt(idx);
      if ((ch == '<') || (ch == '>') || (ch == '&') || (ch == '\r') || (ch == '\n') || (strategy.shouldEscape(ch))) {
        break;
      }
      
      idx++;
    }
    
    if (idx == right)
    {
      return value;
    }
    
    StringBuilder sb = new StringBuilder();
    if (idx > 0) {
      sb.append(value, 0, idx);
    }
    char highsurrogate = '\000';
    while (idx < right) {
      char ch = value.charAt(idx++);
      if (highsurrogate > 0) {
        if (!Verifier.isLowSurrogate(ch)) {
          throw new IllegalDataException("Could not decode surrogate pair 0x" + Integer.toHexString(highsurrogate) + " / 0x" + Integer.toHexString(ch));
        }
        


        int chp = Verifier.decodeSurrogatePair(highsurrogate, ch);
        sb.append("&#x" + Integer.toHexString(chp) + ";");
        highsurrogate = '\000';
      }
      else {
        switch (ch) {
        case '<': 
          sb.append("&lt;");
          break;
        case '>': 
          sb.append("&gt;");
          break;
        case '&': 
          sb.append("&amp;");
          break;
        case '\r': 
          sb.append("&#xD;");
          break;
        case '\n': 
          if (eol != null) {
            sb.append(eol);
          } else {
            sb.append('\n');
          }
          break;
        
        default: 
          if (strategy.shouldEscape(ch))
          {

            if (Verifier.isHighSurrogate(ch))
            {
              highsurrogate = ch;
            } else {
              sb.append("&#x" + Integer.toHexString(ch) + ";");
            }
          } else
            sb.append(ch);
          break;
        }
      }
    }
    if (highsurrogate > 0) {
      throw new IllegalDataException("Surrogate pair 0x" + Integer.toHexString(highsurrogate) + "truncated");
    }
    

    return sb.toString();
  }
  

  private static final EscapeStrategy chooseStrategy(String encoding)
  {
    if (("UTF-8".equalsIgnoreCase(encoding)) || ("UTF-16".equalsIgnoreCase(encoding)))
    {
      return UTFEscapeStrategy;
    }
    
    if ((encoding.toUpperCase().startsWith("ISO-8859-")) || ("Latin1".equalsIgnoreCase(encoding)))
    {
      return Bits8EscapeStrategy;
    }
    
    if (("US-ASCII".equalsIgnoreCase(encoding)) || ("ASCII".equalsIgnoreCase(encoding)))
    {
      return Bits7EscapeStrategy;
    }
    try
    {
      CharsetEncoder cse = Charset.forName(encoding).newEncoder();
      return new DefaultCharsetEscapeStrategy(cse);
    }
    catch (Exception e) {}
    
    return DefaultEscapeStrategy;
  }
  





  private static final String STANDARD_LINE_SEPARATOR = LineSeparator.DEFAULT.value();
  


  private static final String STANDARD_ENCODING = "UTF-8";
  

  String indent = null;
  

  String lineSeparator = STANDARD_LINE_SEPARATOR;
  

  String encoding = "UTF-8";
  


  boolean omitDeclaration = false;
  


  boolean omitEncoding = false;
  


  boolean specifiedAttributesOnly = false;
  


  boolean expandEmptyElements = false;
  


  boolean ignoreTrAXEscapingPIs = false;
  

  TextMode mode = TextMode.PRESERVE;
  

  EscapeStrategy escapeStrategy = DefaultEscapeStrategy;
  


  private Format()
  {
    setEncoding("UTF-8");
  }
  





  public Format setEscapeStrategy(EscapeStrategy strategy)
  {
    escapeStrategy = strategy;
    return this;
  }
  




  public EscapeStrategy getEscapeStrategy()
  {
    return escapeStrategy;
  }
  
























































  public Format setLineSeparator(String separator)
  {
    lineSeparator = ("".equals(separator) ? null : separator);
    return this;
  }
  














  public Format setLineSeparator(LineSeparator separator)
  {
    return setLineSeparator(separator == null ? STANDARD_LINE_SEPARATOR : separator.value());
  }
  






  public String getLineSeparator()
  {
    return lineSeparator;
  }
  










  public Format setOmitEncoding(boolean omitEncoding)
  {
    this.omitEncoding = omitEncoding;
    return this;
  }
  




  public boolean getOmitEncoding()
  {
    return omitEncoding;
  }
  









  public Format setOmitDeclaration(boolean omitDeclaration)
  {
    this.omitDeclaration = omitDeclaration;
    return this;
  }
  




  public boolean getOmitDeclaration()
  {
    return omitDeclaration;
  }
  








  public Format setExpandEmptyElements(boolean expandEmptyElements)
  {
    this.expandEmptyElements = expandEmptyElements;
    return this;
  }
  




  public boolean getExpandEmptyElements()
  {
    return expandEmptyElements;
  }
  


























  public void setIgnoreTrAXEscapingPIs(boolean ignoreTrAXEscapingPIs)
  {
    this.ignoreTrAXEscapingPIs = ignoreTrAXEscapingPIs;
  }
  





  public boolean getIgnoreTrAXEscapingPIs()
  {
    return ignoreTrAXEscapingPIs;
  }
  






  public Format setTextMode(TextMode mode)
  {
    this.mode = mode;
    return this;
  }
  




  public TextMode getTextMode()
  {
    return mode;
  }
  









  public Format setIndent(String indent)
  {
    this.indent = indent;
    return this;
  }
  




  public String getIndent()
  {
    return indent;
  }
  







  public Format setEncoding(String encoding)
  {
    this.encoding = encoding;
    escapeStrategy = chooseStrategy(encoding);
    return this;
  }
  




  public String getEncoding()
  {
    return encoding;
  }
  





  public boolean isSpecifiedAttributesOnly()
  {
    return specifiedAttributesOnly;
  }
  






  public void setSpecifiedAttributesOnly(boolean specifiedAttributesOnly)
  {
    this.specifiedAttributesOnly = specifiedAttributesOnly;
  }
  
  public Format clone()
  {
    Format format = null;
    try
    {
      format = (Format)super.clone();
    }
    catch (CloneNotSupportedException ce) {}
    


    return format;
  }
  









































































  public static enum TextMode
  {
    PRESERVE, 
    



    TRIM, 
    





    NORMALIZE, 
    




    TRIM_FULL_WHITE;
    
    private TextMode() {}
  }
}
