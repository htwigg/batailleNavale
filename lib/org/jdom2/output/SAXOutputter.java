package org.jdom2.output;

import org.jdom2.*;
import org.jdom2.output.support.AbstractSAXOutputProcessor;
import org.jdom2.output.support.SAXOutputProcessor;
import org.jdom2.output.support.SAXTarget;
import org.xml.sax.ext.DeclHandler;
import org.xml.sax.ext.LexicalHandler;

import java.util.List;























































































public class SAXOutputter
{
  private static final SAXOutputProcessor DEFAULT_PROCESSOR = new DefaultSAXOutputProcessor(null);
  


  private ContentHandler contentHandler;
  


  private ErrorHandler errorHandler;
  


  private DTDHandler dtdHandler;
  


  private EntityResolver entityResolver;
  


  private LexicalHandler lexicalHandler;
  


  private DeclHandler declHandler;
  

  private boolean declareNamespaces = false;
  




  private boolean reportDtdEvents = true;
  



  private SAXOutputProcessor processor = DEFAULT_PROCESSOR;
  



  private Format format = Format.getRawFormat();
  






  public SAXOutputter() {}
  






  public SAXOutputter(ContentHandler contentHandler)
  {
    this(contentHandler, null, null, null, null);
  }
  















  public SAXOutputter(ContentHandler contentHandler, ErrorHandler errorHandler, DTDHandler dtdHandler, EntityResolver entityResolver)
  {
    this(contentHandler, errorHandler, dtdHandler, entityResolver, null);
  }
  

















  public SAXOutputter(ContentHandler contentHandler, ErrorHandler errorHandler, DTDHandler dtdHandler, EntityResolver entityResolver, LexicalHandler lexicalHandler)
  {
    this.contentHandler = contentHandler;
    this.errorHandler = errorHandler;
    this.dtdHandler = dtdHandler;
    this.entityResolver = entityResolver;
    this.lexicalHandler = lexicalHandler;
  }
  






















  public SAXOutputter(SAXOutputProcessor processor, Format format, ContentHandler contentHandler, ErrorHandler errorHandler, DTDHandler dtdHandler, EntityResolver entityResolver, LexicalHandler lexicalHandler)
  {
    this.processor = (processor == null ? DEFAULT_PROCESSOR : processor);
    this.format = (format == null ? Format.getRawFormat() : format);
    this.contentHandler = contentHandler;
    this.errorHandler = errorHandler;
    this.dtdHandler = dtdHandler;
    this.entityResolver = entityResolver;
    this.lexicalHandler = lexicalHandler;
  }
  





  public void setContentHandler(ContentHandler contentHandler)
  {
    this.contentHandler = contentHandler;
  }
  





  public ContentHandler getContentHandler()
  {
    return contentHandler;
  }
  





  public void setErrorHandler(ErrorHandler errorHandler)
  {
    this.errorHandler = errorHandler;
  }
  





  public ErrorHandler getErrorHandler()
  {
    return errorHandler;
  }
  





  public void setDTDHandler(DTDHandler dtdHandler)
  {
    this.dtdHandler = dtdHandler;
  }
  





  public DTDHandler getDTDHandler()
  {
    return dtdHandler;
  }
  





  public void setEntityResolver(EntityResolver entityResolver)
  {
    this.entityResolver = entityResolver;
  }
  





  public EntityResolver getEntityResolver()
  {
    return entityResolver;
  }
  





  public void setLexicalHandler(LexicalHandler lexicalHandler)
  {
    this.lexicalHandler = lexicalHandler;
  }
  





  public LexicalHandler getLexicalHandler()
  {
    return lexicalHandler;
  }
  





  public void setDeclHandler(DeclHandler declHandler)
  {
    this.declHandler = declHandler;
  }
  





  public DeclHandler getDeclHandler()
  {
    return declHandler;
  }
  






  public boolean getReportNamespaceDeclarations()
  {
    return declareNamespaces;
  }
  








  public void setReportNamespaceDeclarations(boolean declareNamespaces)
  {
    this.declareNamespaces = declareNamespaces;
  }
  




  public boolean getReportDTDEvents()
  {
    return reportDtdEvents;
  }
  







  public void setReportDTDEvents(boolean reportDtdEvents)
  {
    this.reportDtdEvents = reportDtdEvents;
  }
  





































  public void setFeature(String name, boolean value)
    throws SAXNotRecognizedException, SAXNotSupportedException
  {
    if ("http://xml.org/sax/features/namespace-prefixes".equals(name))
    {
      setReportNamespaceDeclarations(value);
    }
    else if ("http://xml.org/sax/features/namespaces".equals(name)) {
      if (value != true)
      {
        throw new SAXNotSupportedException(name);
      }
      
    }
    else if ("http://xml.org/sax/features/validation".equals(name))
    {
      setReportDTDEvents(value);
    }
    else {
      throw new SAXNotRecognizedException(name);
    }
  }
  















  public boolean getFeature(String name)
    throws SAXNotRecognizedException, SAXNotSupportedException
  {
    if ("http://xml.org/sax/features/namespace-prefixes".equals(name))
    {
      return declareNamespaces;
    }
    if ("http://xml.org/sax/features/namespaces".equals(name))
    {
      return true;
    }
    if ("http://xml.org/sax/features/validation".equals(name))
    {
      return reportDtdEvents;
    }
    
    throw new SAXNotRecognizedException(name);
  }
  






























  public void setProperty(String name, Object value)
    throws SAXNotRecognizedException, SAXNotSupportedException
  {
    if (("http://xml.org/sax/properties/lexical-handler".equals(name)) || ("http://xml.org/sax/handlers/LexicalHandler".equals(name)))
    {
      setLexicalHandler((LexicalHandler)value);
    }
    else if (("http://xml.org/sax/properties/declaration-handler".equals(name)) || ("http://xml.org/sax/handlers/DeclHandler".equals(name)))
    {
      setDeclHandler((DeclHandler)value);
    } else {
      throw new SAXNotRecognizedException(name);
    }
  }
  













  public Object getProperty(String name)
    throws SAXNotRecognizedException, SAXNotSupportedException
  {
    if (("http://xml.org/sax/properties/lexical-handler".equals(name)) || ("http://xml.org/sax/handlers/LexicalHandler".equals(name)))
    {
      return getLexicalHandler();
    }
    if (("http://xml.org/sax/properties/declaration-handler".equals(name)) || ("http://xml.org/sax/handlers/DeclHandler".equals(name)))
    {
      return getDeclHandler();
    }
    throw new SAXNotRecognizedException(name);
  }
  




  public SAXOutputProcessor getSAXOutputProcessor()
  {
    return processor;
  }
  





  public void setSAXOutputProcessor(SAXOutputProcessor processor)
  {
    this.processor = (processor == null ? DEFAULT_PROCESSOR : processor);
  }
  




  public Format getFormat()
  {
    return format;
  }
  





  public void setFormat(Format format)
  {
    this.format = (format == null ? Format.getRawFormat() : format);
  }
  
  private final SAXTarget buildTarget(Document doc) {
    String publicID = null;
    String systemID = null;
    if (doc != null) {
      DocType dt = doc.getDocType();
      if (dt != null) {
        publicID = dt.getPublicID();
        systemID = dt.getSystemID();
      }
    }
    return new SAXTarget(contentHandler, errorHandler, dtdHandler, entityResolver, lexicalHandler, declHandler, declareNamespaces, reportDtdEvents, publicID, systemID);
  }
  









  public void output(Document document)
    throws JDOMException
  {
    processor.process(buildTarget(document), format, document);
  }
  















  public void output(List<? extends Content> nodes)
    throws JDOMException
  {
    processor.processAsDocument(buildTarget(null), format, nodes);
  }
  







  public void output(Element node)
    throws JDOMException
  {
    processor.processAsDocument(buildTarget(null), format, node);
  }
  

















  public void outputFragment(List<? extends Content> nodes)
    throws JDOMException
  {
    if (nodes == null) {
      return;
    }
    processor.process(buildTarget(null), format, nodes);
  }
  
















  public void outputFragment(Content node)
    throws JDOMException
  {
    if (node == null) {
      return;
    }
    
    SAXTarget out = buildTarget(null);
    
    switch (1.$SwitchMap$org$jdom2$Content$CType[node.getCType().ordinal()]) {
    case 1: 
      processor.process(out, format, (CDATA)node);
      break;
    case 2: 
      processor.process(out, format, (Comment)node);
      break;
    case 3: 
      processor.process(out, format, (Element)node);
      break;
    case 4: 
      processor.process(out, format, (EntityRef)node);
      break;
    case 5: 
      processor.process(out, format, (ProcessingInstruction)node);
      break;
    case 6: 
      processor.process(out, format, (Text)node);
      break;
    default: 
      handleError(new JDOMException("Invalid element content: " + node));
    }
    
  }
  














  private void handleError(JDOMException exception)
    throws JDOMException
  {
    if (errorHandler != null) {
      try {
        errorHandler.error(new SAXParseException(exception.getMessage(), null, exception));
      }
      catch (SAXException se)
      {
        if ((se.getException() instanceof JDOMException)) {
          throw ((JDOMException)se.getException());
        }
        throw new JDOMException(se.getMessage(), se);
      }
      
    } else {
      throw exception;
    }
  }
  








  @Deprecated
  public JDOMLocator getLocator()
  {
    return null;
  }
  
  private static final class DefaultSAXOutputProcessor
    extends AbstractSAXOutputProcessor
  {
    private DefaultSAXOutputProcessor() {}
  }
}
