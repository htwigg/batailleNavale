package org.jdom2.output;

public abstract interface EscapeStrategy
{
  public abstract boolean shouldEscape(char paramChar);
}
