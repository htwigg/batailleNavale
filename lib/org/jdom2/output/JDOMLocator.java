package org.jdom2.output;

import org.xml.sax.Locator;

public abstract interface JDOMLocator
  extends Locator
{
  public abstract Object getNode();
}
