package org.jdom2.util;

import org.jdom2.Attribute;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.internal.ArrayCopy;





























































































public final class NamespaceStack
  implements Iterable<Namespace>
{
  private static final class ForwardWalker
    implements Iterator<Namespace>
  {
    private final Namespace[] namespaces;
    int cursor = 0;
    
    public ForwardWalker(Namespace[] namespaces) {
      this.namespaces = namespaces;
    }
    
    public boolean hasNext()
    {
      return cursor < namespaces.length;
    }
    
    public Namespace next()
    {
      if (cursor >= namespaces.length) {
        throw new NoSuchElementException("Cannot over-iterate...");
      }
      return namespaces[(cursor++)];
    }
    
    public void remove()
    {
      throw new UnsupportedOperationException("Cannot remove Namespaces from iterator");
    }
  }
  




  private static final class BackwardWalker
    implements Iterator<Namespace>
  {
    private final Namespace[] namespaces;
    


    int cursor = -1;
    
    public BackwardWalker(Namespace[] namespaces) {
      this.namespaces = namespaces;
      cursor = (namespaces.length - 1);
    }
    
    public boolean hasNext()
    {
      return cursor >= 0;
    }
    
    public Namespace next()
    {
      if (cursor < 0) {
        throw new NoSuchElementException("Cannot over-iterate...");
      }
      return namespaces[(cursor--)];
    }
    
    public void remove()
    {
      throw new UnsupportedOperationException("Cannot remove Namespaces from iterator");
    }
  }
  


  private static final class NamespaceIterable
    implements Iterable<Namespace>
  {
    private final boolean forward;
    

    private final Namespace[] namespaces;
    

    public NamespaceIterable(Namespace[] data, boolean forward)
    {
      this.forward = forward;
      namespaces = data;
    }
    
    public Iterator<Namespace> iterator() {
      return forward ? new NamespaceStack.ForwardWalker(namespaces) : new NamespaceStack.BackwardWalker(namespaces);
    }
  }
  


  private static final class EmptyIterable
    implements Iterable<Namespace>, Iterator<Namespace>
  {
    private EmptyIterable() {}
    

    public Iterator<Namespace> iterator()
    {
      return this;
    }
    
    public boolean hasNext()
    {
      return false;
    }
    
    public Namespace next()
    {
      throw new NoSuchElementException("Can not call next() on an empty Iterator.");
    }
    

    public void remove()
    {
      throw new UnsupportedOperationException("Cannot remove Namespaces from iterator");
    }
  }
  


  private static final Namespace[] EMPTY = new Namespace[0];
  
  private static final Iterable<Namespace> EMPTYITER = new EmptyIterable(null);
  

  private static final Comparator<Namespace> NSCOMP = new Comparator()
  {
    public int compare(Namespace ns1, Namespace ns2) {
      return ns1.getPrefix().compareTo(ns2.getPrefix());
    }
  };
  private static final Namespace[] DEFAULTSEED = { Namespace.NO_NAMESPACE, Namespace.XML_NAMESPACE };
  
























  private static final int binarySearch(Namespace[] data, int left, int right, Namespace key)
  {
    
    






















    while (left <= right)
    {


      int mid = left + right >>> 1;
      if (data[mid] == key)
      {
        return mid;
      }
      int cmp = NSCOMP.compare(data[mid], key);
      
      if (cmp < 0) {
        left = mid + 1;
      } else if (cmp > 0) {
        right = mid - 1;
      }
      else {
        return mid;
      }
    }
    return -left - 1;
  }
  

  private Namespace[][] added = new Namespace[10][];
  
  private Namespace[][] scope = new Namespace[10][];
  
  private int depth = -1;
  




  public NamespaceStack()
  {
    this(DEFAULTSEED);
  }
  





  public NamespaceStack(Namespace[] seed)
  {
    depth += 1;
    added[depth] = seed;
    
    scope[depth] = added[depth];
  }
  
















  private static final Namespace[] checkNamespace(List<Namespace> store, Namespace namespace, Namespace[] scope)
  {
    if (namespace == scope[0])
    {
      return scope;
    }
    if (namespace.getPrefix().equals(scope[0].getPrefix()))
    {

      store.add(namespace);
      Namespace[] nscope = (Namespace[])ArrayCopy.copyOf(scope, scope.length);
      nscope[0] = namespace;
      return nscope;
    }
    
    int ip = binarySearch(scope, 1, scope.length, namespace);
    if ((ip >= 0) && (namespace == scope[ip]))
    {
      return scope;
    }
    store.add(namespace);
    if (ip >= 0)
    {

      Namespace[] nscope = (Namespace[])ArrayCopy.copyOf(scope, scope.length);
      nscope[ip] = namespace;
      return nscope;
    }
    
    Namespace[] nscope = (Namespace[])ArrayCopy.copyOf(scope, scope.length + 1);
    ip = -ip - 1;
    System.arraycopy(nscope, ip, nscope, ip + 1, nscope.length - ip - 1);
    nscope[ip] = namespace;
    return nscope;
  }
  
































  public void push(Element element)
  {
    List<Namespace> toadd = new ArrayList(8);
    Namespace mns = element.getNamespace();
    
    Namespace[] newscope = checkNamespace(toadd, mns, scope[depth]);
    if (element.hasAdditionalNamespaces()) {
      for (Namespace ns : element.getAdditionalNamespaces()) {
        if (ns != mns)
        {


          newscope = checkNamespace(toadd, ns, newscope); }
      }
    }
    if (element.hasAttributes()) {
      for (Attribute a : element.getAttributes()) {
        Namespace ns = a.getNamespace();
        if ((ns != Namespace.NO_NAMESPACE) && 
        



          (ns != mns))
        {


          newscope = checkNamespace(toadd, ns, newscope);
        }
      }
    }
    pushStack(mns, newscope, toadd);
  }
  





  public void push(Attribute att)
  {
    List<Namespace> toadd = new ArrayList(1);
    Namespace mns = att.getNamespace();
    
    Namespace[] newscope = checkNamespace(toadd, mns, scope[depth]);
    
    pushStack(mns, newscope, toadd);
  }
  


  private final void pushStack(Namespace mns, Namespace[] newscope, List<Namespace> toadd)
  {
    depth += 1;
    
    if (depth >= scope.length)
    {
      scope = ((Namespace[][])ArrayCopy.copyOf(scope, scope.length * 2));
      added = ((Namespace[][])ArrayCopy.copyOf(added, scope.length));
    }
    

    if (toadd.isEmpty())
    {
      added[depth] = EMPTY;
    } else {
      added[depth] = ((Namespace[])toadd.toArray(new Namespace[toadd.size()]));
      if (added[depth][0] == mns) {
        Arrays.sort(added[depth], 1, added[depth].length, NSCOMP);
      } else {
        Arrays.sort(added[depth], NSCOMP);
      }
    }
    
    if (mns != newscope[0]) {
      if (toadd.isEmpty())
      {

        newscope = (Namespace[])ArrayCopy.copyOf(newscope, newscope.length);
      }
      



      Namespace tmp = newscope[0];
      int ip = -binarySearch(newscope, 1, newscope.length, tmp) - 1;
      


      ip--;
      System.arraycopy(newscope, 1, newscope, 0, ip);
      newscope[ip] = tmp;
      
      ip = binarySearch(newscope, 0, newscope.length, mns);
      
      System.arraycopy(newscope, 0, newscope, 1, ip);
      newscope[0] = mns;
    }
    
    scope[depth] = newscope;
  }
  



  public void pop()
  {
    if (depth <= 0) {
      throw new IllegalStateException("Cannot over-pop the stack.");
    }
    scope[depth] = null;
    added[depth] = null;
    depth -= 1;
  }
  





  public Iterable<Namespace> addedForward()
  {
    if (added[depth].length == 0) {
      return EMPTYITER;
    }
    return new NamespaceIterable(added[depth], true);
  }
  





  public Iterable<Namespace> addedReverse()
  {
    if (added[depth].length == 0) {
      return EMPTYITER;
    }
    return new NamespaceIterable(added[depth], false);
  }
  





  public Iterator<Namespace> iterator()
  {
    return new ForwardWalker(scope[depth]);
  }
  




  public Namespace[] getScope()
  {
    return (Namespace[])ArrayCopy.copyOf(scope[depth], scope[depth].length);
  }
  





  public boolean isInScope(Namespace ns)
  {
    if (ns == scope[depth][0]) {
      return true;
    }
    int ip = binarySearch(scope[depth], 1, scope[depth].length, ns);
    if (ip >= 0)
    {
      return ns == scope[depth][ip];
    }
    return false;
  }
}
