package org.jdom2.util;

import java.util.Iterator;

public abstract interface IteratorIterable<T>
  extends Iterable<T>, Iterator<T>
{}
