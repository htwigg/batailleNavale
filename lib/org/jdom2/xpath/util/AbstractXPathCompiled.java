package org.jdom2.xpath.util;

import org.jdom2.Namespace;
import org.jdom2.Verifier;
import org.jdom2.filter.Filter;
import org.jdom2.xpath.XPathDiagnostic;
import org.jdom2.xpath.XPathExpression;

import java.util.Map.Entry;



























































public abstract class AbstractXPathCompiled<T>
  implements XPathExpression<T>
{
  private static final class NamespaceComparator
    implements Comparator<Namespace>
  {
    private NamespaceComparator() {}
    
    public int compare(Namespace ns1, Namespace ns2)
    {
      return ns1.getPrefix().compareTo(ns2.getPrefix());
    }
  }
  
  private static final NamespaceComparator NSSORT = new NamespaceComparator(null);
  





  private static final String getPrefixForURI(String uri, Namespace[] nsa)
  {
    for (Namespace ns : nsa) {
      if (ns.getURI().equals(uri)) {
        return ns.getPrefix();
      }
    }
    throw new IllegalStateException("No namespace defined with URI " + uri);
  }
  
  private final Map<String, Namespace> xnamespaces = new HashMap();
  
  private Map<String, Map<String, Object>> xvariables = new HashMap();
  




  private final String xquery;
  




  private final Filter<T> xfilter;
  




  public AbstractXPathCompiled(String query, Filter<T> filter, Map<String, Object> variables, Namespace[] namespaces)
  {
    if (query == null) {
      throw new NullPointerException("Null query");
    }
    if (filter == null) {
      throw new NullPointerException("Null filter");
    }
    xnamespaces.put(Namespace.NO_NAMESPACE.getPrefix(), Namespace.NO_NAMESPACE);
    
    if (namespaces != null) {
      for (Namespace ns : namespaces) {
        if (ns == null) {
          throw new NullPointerException("Null namespace");
        }
        Namespace oldns = (Namespace)xnamespaces.put(ns.getPrefix(), ns);
        if ((oldns != null) && (oldns != ns)) {
          if (oldns == Namespace.NO_NAMESPACE) {
            throw new IllegalArgumentException("The default (no prefix) Namespace URI for XPath queries is always '' and it cannot be redefined to '" + ns.getURI() + "'.");
          }
          

          throw new IllegalArgumentException("A Namespace with the prefix '" + ns.getPrefix() + "' has already been declared.");
        }
      }
    }
    


    if (variables != null) {
      for (Map.Entry<String, Object> me : variables.entrySet()) {
        String qname = (String)me.getKey();
        if (qname == null) {
          throw new NullPointerException("Variable with a null name");
        }
        int p = qname.indexOf(':');
        String pfx = p < 0 ? "" : qname.substring(0, p);
        String lname = p < 0 ? qname : qname.substring(p + 1);
        
        String vpfxmsg = Verifier.checkNamespacePrefix(pfx);
        if (vpfxmsg != null) {
          throw new IllegalArgumentException("Prefix '" + pfx + "' for variable " + qname + " is illegal: " + vpfxmsg);
        }
        

        String vnamemsg = Verifier.checkXMLName(lname);
        if (vnamemsg != null) {
          throw new IllegalArgumentException("Variable name '" + lname + "' for variable " + qname + " is illegal: " + vnamemsg);
        }
        


        Namespace ns = (Namespace)xnamespaces.get(pfx);
        if (ns == null) {
          throw new IllegalArgumentException("Prefix '" + pfx + "' for variable " + qname + " has not been assigned a Namespace.");
        }
        


        Map<String, Object> vmap = (Map)xvariables.get(ns.getURI());
        if (vmap == null) {
          vmap = new HashMap();
          xvariables.put(ns.getURI(), vmap);
        }
        
        if (vmap.put(lname, me.getValue()) != null) {
          throw new IllegalArgumentException("Variable with name " + (String)me.getKey() + "' has already been defined.");
        }
      }
    }
    
    xquery = query;
    xfilter = filter;
  }
  





















  public XPathExpression<T> clone()
  {
    AbstractXPathCompiled<T> ret = null;
    try
    {
      AbstractXPathCompiled<T> c = (AbstractXPathCompiled)super.clone();
      
      ret = c;
    } catch (CloneNotSupportedException cnse) {
      throw new IllegalStateException("Should never be getting a CloneNotSupportedException!", cnse);
    }
    

    Map<String, Map<String, Object>> vmt = new HashMap();
    for (Map.Entry<String, Map<String, Object>> me : xvariables.entrySet()) {
      Map<String, Object> cmap = new HashMap();
      for (Map.Entry<String, Object> ne : ((Map)me.getValue()).entrySet()) {
        cmap.put(ne.getKey(), ne.getValue());
      }
      vmt.put(me.getKey(), cmap);
    }
    xvariables = vmt;
    return ret;
  }
  
  public final String getExpression()
  {
    return xquery;
  }
  
  public final Namespace getNamespace(String prefix)
  {
    Namespace ns = (Namespace)xnamespaces.get(prefix);
    if (ns == null) {
      throw new IllegalArgumentException("Namespace with prefix '" + prefix + "' has not been declared.");
    }
    
    return ns;
  }
  
  public Namespace[] getNamespaces()
  {
    Namespace[] nsa = (Namespace[])xnamespaces.values().toArray(new Namespace[xnamespaces.size()]);
    
    Arrays.sort(nsa, NSSORT);
    return nsa;
  }
  
  public final Object getVariable(String name, Namespace uri)
  {
    Map<String, Object> vmap = (Map)xvariables.get(uri == null ? "" : uri.getURI());
    
    if (vmap == null) {
      throw new IllegalArgumentException("Variable with name '" + name + "' in namespace '" + uri.getURI() + "' has not been declared.");
    }
    
    Object ret = vmap.get(name);
    if (ret == null) {
      if (!vmap.containsKey(name)) {
        throw new IllegalArgumentException("Variable with name '" + name + "' in namespace '" + uri.getURI() + "' has not been declared.");
      }
      


      return null;
    }
    return ret;
  }
  
  public Object getVariable(String qname)
  {
    if (qname == null) {
      throw new NullPointerException("Cannot get variable value for null qname");
    }
    
    int pos = qname.indexOf(':');
    if (pos >= 0) {
      return getVariable(qname.substring(pos + 1), getNamespace(qname.substring(0, pos)));
    }
    
    return getVariable(qname, Namespace.NO_NAMESPACE);
  }
  
  public Object setVariable(String name, Namespace uri, Object value)
  {
    Object ret = getVariable(name, uri);
    
    ((Map)xvariables.get(uri.getURI())).put(name, value);
    return ret;
  }
  
  public Object setVariable(String qname, Object value)
  {
    if (qname == null) {
      throw new NullPointerException("Cannot get variable value for null qname");
    }
    
    int pos = qname.indexOf(':');
    if (pos >= 0) {
      return setVariable(qname.substring(pos + 1), getNamespace(qname.substring(0, pos)), value);
    }
    
    return setVariable(qname, Namespace.NO_NAMESPACE, value);
  }
  




  protected Map<String, Object> getVariables()
  {
    HashMap<String, Object> vars = new HashMap();
    Namespace[] nsa = getNamespaces();
    for (Map.Entry<String, Map<String, Object>> ue : xvariables.entrySet()) {
      String uri = (String)ue.getKey();
      pfx = getPrefixForURI(uri, nsa);
      for (Map.Entry<String, Object> ve : ((Map)ue.getValue()).entrySet()) {
        if ("".equals(pfx)) {
          vars.put(ve.getKey(), ve.getValue());
        } else
          vars.put(pfx + ":" + (String)ve.getKey(), ve.getValue());
      }
    }
    String pfx;
    return vars;
  }
  
  public final Filter<T> getFilter()
  {
    return xfilter;
  }
  
  public List<T> evaluate(Object context)
  {
    return xfilter.filter(evaluateRawAll(context));
  }
  



  public T evaluateFirst(Object context)
  {
    Object raw = evaluateRawFirst(context);
    if (raw == null) {
      return null;
    }
    return xfilter.filter(raw);
  }
  
  public XPathDiagnostic<T> diagnose(Object context, boolean firstonly)
  {
    List<?> result = firstonly ? Collections.singletonList(evaluateRawFirst(context)) : evaluateRawAll(context);
    

    return new XPathDiagnosticImpl(context, this, result, firstonly);
  }
  
  public String toString()
  {
    int nscnt = xnamespaces.size();
    int vcnt = 0;
    for (Map<String, Object> cmap : xvariables.values()) {
      vcnt += cmap.size();
    }
    return String.format("[XPathExpression: %d namespaces and %d variables for query %s]", new Object[] { Integer.valueOf(nscnt), Integer.valueOf(vcnt), getExpression() });
  }
  
  protected abstract List<?> evaluateRawAll(Object paramObject);
  
  protected abstract Object evaluateRawFirst(Object paramObject);
}
