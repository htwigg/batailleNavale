package org.jdom2.xpath.util;

import org.jdom2.filter.Filter;
import org.jdom2.xpath.XPathDiagnostic;
import org.jdom2.xpath.XPathExpression;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;














































































public class XPathDiagnosticImpl<T>
  implements XPathDiagnostic<T>
{
  private final Object dcontext;
  private final XPathExpression<T> dxpath;
  private final List<Object> draw;
  private final List<Object> dfiltered;
  private final List<T> dresult;
  private final boolean dfirstonly;
  
  public XPathDiagnosticImpl(Object dcontext, XPathExpression<T> dxpath, List<?> inraw, boolean dfirstonly)
  {
    int sz = inraw.size();
    List<Object> raw = new ArrayList(sz);
    List<Object> filtered = new ArrayList(sz);
    List<T> result = new ArrayList(sz);
    Filter<T> filter = dxpath.getFilter();
    
    for (Object o : inraw) {
      raw.add(o);
      T t = filter.filter(o);
      if (t == null) {
        filtered.add(o);
      } else {
        result.add(t);
      }
    }
    
    this.dcontext = dcontext;
    this.dxpath = dxpath;
    this.dfirstonly = dfirstonly;
    
    dfiltered = Collections.unmodifiableList(filtered);
    draw = Collections.unmodifiableList(raw);
    dresult = Collections.unmodifiableList(result);
  }
  

  public Object getContext()
  {
    return dcontext;
  }
  
  public XPathExpression<T> getXPathExpression()
  {
    return dxpath;
  }
  
  public List<T> getResult()
  {
    return dresult;
  }
  
  public List<Object> getFilteredResults()
  {
    return dfiltered;
  }
  
  public List<Object> getRawResults()
  {
    return draw;
  }
  
  public boolean isFirstOnly()
  {
    return dfirstonly;
  }
  
  public String toString()
  {
    return String.format("[XPathDiagnostic: '%s' evaluated (%s) against %s produced  raw=%d discarded=%d returned=%d]", new Object[] { dxpath.getExpression(), dfirstonly ? "first" : "all", dcontext.getClass().getName(), Integer.valueOf(draw.size()), Integer.valueOf(dfiltered.size()), Integer.valueOf(dresult.size()) });
  }
}
