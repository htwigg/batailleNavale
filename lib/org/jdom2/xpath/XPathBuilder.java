package org.jdom2.xpath;

import org.jdom2.Namespace;
import org.jdom2.filter.Filter;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;







































































public class XPathBuilder<T>
{
  private final Filter<T> filter;
  private final String expression;
  private Map<String, Object> variables;
  private Map<String, Namespace> namespaces;
  
  public XPathBuilder(String expression, Filter<T> filter)
  {
    if (expression == null) {
      throw new NullPointerException("Null expression");
    }
    if (filter == null) {
      throw new NullPointerException("Null filter");
    }
    this.filter = filter;
    this.expression = expression;
  }
  






































  public boolean setVariable(String qname, Object value)
  {
    if (qname == null) {
      throw new NullPointerException("Null variable name");
    }
    if (variables == null) {
      variables = new HashMap();
    }
    return variables.put(qname, value) == null;
  }
  











  public boolean setNamespace(String prefix, String uri)
  {
    if (prefix == null) {
      throw new NullPointerException("Null prefix");
    }
    if (uri == null) {
      throw new NullPointerException("Null URI");
    }
    return setNamespace(Namespace.getNamespace(prefix, uri));
  }
  









  public boolean setNamespace(Namespace namespace)
  {
    if (namespace == null) {
      throw new NullPointerException("Null Namespace");
    }
    if ("".equals(namespace.getPrefix())) {
      if (Namespace.NO_NAMESPACE != namespace) {
        throw new IllegalArgumentException("Cannot set a Namespace URI in XPath for the \"\" prefix.");
      }
      

      return false;
    }
    
    if (namespaces == null) {
      namespaces = new HashMap();
    }
    return namespaces.put(namespace.getPrefix(), namespace) == null;
  }
  








  public boolean setNamespaces(Collection<Namespace> namespaces)
  {
    if (namespaces == null) {
      throw new NullPointerException("Null namespaces Collection");
    }
    boolean ret = false;
    for (Namespace ns : namespaces) {
      if (setNamespace(ns)) {
        ret = true;
      }
    }
    return ret;
  }
  












  public Object getVariable(String qname)
  {
    if (qname == null) {
      throw new NullPointerException("Null qname");
    }
    if (variables == null) {
      return null;
    }
    return variables.get(qname);
  }
  







  public Namespace getNamespace(String prefix)
  {
    if (prefix == null) {
      throw new NullPointerException("Null prefix");
    }
    if ("".equals(prefix)) {
      return Namespace.NO_NAMESPACE;
    }
    if (namespaces == null) {
      return null;
    }
    return (Namespace)namespaces.get(prefix);
  }
  




  public Filter<T> getFilter()
  {
    return filter;
  }
  




  public String getExpression()
  {
    return expression;
  }
  









  public XPathExpression<T> compileWith(XPathFactory factory)
  {
    if (namespaces == null) {
      return factory.compile(expression, filter, variables, new Namespace[0]);
    }
    return factory.compile(expression, filter, variables, (Namespace[])namespaces.values().toArray(new Namespace[namespaces.size()]));
  }
}
