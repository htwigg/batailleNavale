package org.jdom2.xpath;

import java.util.List;

public abstract interface XPathDiagnostic<T>
{
  public abstract Object getContext();
  
  public abstract XPathExpression<T> getXPathExpression();
  
  public abstract List<T> getResult();
  
  public abstract List<Object> getFilteredResults();
  
  public abstract List<Object> getRawResults();
  
  public abstract boolean isFirstOnly();
}
