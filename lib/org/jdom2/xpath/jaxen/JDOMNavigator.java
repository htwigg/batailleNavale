package org.jdom2.xpath.jaxen;

import org.jaxen.NamespaceContext;
import org.jdom2.Namespace;
import org.jdom2.NamespaceAware;

import java.util.HashMap;
import java.util.List;


























































final class JDOMNavigator
  extends JDOMCoreNavigator
  implements NamespaceContext
{
  private static final long serialVersionUID = 200L;
  private final HashMap<String, String> nsFromContext = new HashMap();
  private final HashMap<String, String> nsFromUser = new HashMap();
  
  JDOMNavigator() {}
  
  void reset() { super.reset();
    nsFromContext.clear();
  }
  
  void setContext(Object node) {
    nsFromContext.clear();
    
    List<Namespace> nsl = null;
    if ((node instanceof NamespaceAware)) {
      nsl = ((NamespaceAware)node).getNamespacesInScope();
    } else if ((node instanceof NamespaceContainer)) {
      nsl = ((NamespaceContainer)node).getParentElement().getNamespacesInScope();
    }
    if (nsl != null) {
      for (Namespace ns : nsl) {
        nsFromContext.put(ns.getPrefix(), ns.getURI());
      }
    }
  }
  
  void includeNamespace(Namespace namespace) {
    nsFromUser.put(namespace.getPrefix(), namespace.getURI());
  }
  
  public String translateNamespacePrefixToUri(String prefix)
  {
    if (prefix == null) {
      return null;
    }
    String uri = (String)nsFromUser.get(prefix);
    if (uri != null) {
      return uri;
    }
    return (String)nsFromContext.get(prefix);
  }
}
