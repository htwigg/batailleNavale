package org.jdom2.xpath.jaxen;

import org.jaxen.BaseXPath;
import org.jaxen.JaxenException;
import org.jaxen.SimpleVariableContext;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



































































































@Deprecated
public class JDOMXPath
  extends org.jdom2.xpath.XPath
{
  private static final long serialVersionUID = 200L;
  private transient org.jaxen.XPath xPath;
  private final JDOMNavigator navigator = new JDOMNavigator();
  








  private static final Object unWrapNS(Object o)
  {
    if ((o instanceof NamespaceContainer)) {
      return ((NamespaceContainer)o).getNamespace();
    }
    return o;
  }
  








  private static final List<Object> unWrap(List<?> results)
  {
    ArrayList<Object> ret = new ArrayList(results.size());
    for (Iterator<?> it = results.iterator(); it.hasNext();) {
      ret.add(unWrapNS(it.next()));
    }
    return ret;
  }
  








  public JDOMXPath(String expr)
    throws JDOMException
  {
    setXPath(expr);
  }
  












  public List<?> selectNodes(Object context)
    throws JDOMException
  {
    try
    {
      navigator.setContext(context);
      
      return unWrap(xPath.selectNodes(context));
    } catch (JaxenException ex1) {
      throw new JDOMException("XPath error while evaluating \"" + xPath.toString() + "\": " + ex1.getMessage(), ex1);
    }
    finally
    {
      navigator.reset();
    }
  }
  













  public Object selectSingleNode(Object context)
    throws JDOMException
  {
    try
    {
      navigator.setContext(context);
      
      return unWrapNS(xPath.selectSingleNode(context));
    } catch (JaxenException ex1) {
      throw new JDOMException("XPath error while evaluating \"" + xPath.toString() + "\": " + ex1.getMessage(), ex1);
    }
    finally
    {
      navigator.reset();
    }
  }
  










  public String valueOf(Object context)
    throws JDOMException
  {
    try
    {
      navigator.setContext(context);
      
      return xPath.stringValueOf(context);
    } catch (JaxenException ex1) {
      throw new JDOMException("XPath error while evaluating \"" + xPath.toString() + "\": " + ex1.getMessage(), ex1);
    }
    finally
    {
      navigator.reset();
    }
  }
  













  public Number numberValueOf(Object context)
    throws JDOMException
  {
    try
    {
      navigator.setContext(context);
      
      return xPath.numberValueOf(context);
    } catch (JaxenException ex1) {
      throw new JDOMException("XPath error while evaluating \"" + xPath.toString() + "\": " + ex1.getMessage(), ex1);
    }
    finally
    {
      navigator.reset();
    }
  }
  











  public void setVariable(String name, Object value)
    throws IllegalArgumentException
  {
    Object o = xPath.getVariableContext();
    if ((o instanceof SimpleVariableContext)) {
      ((SimpleVariableContext)o).setVariableValue(null, name, value);
    }
  }
  












  public void addNamespace(Namespace namespace)
  {
    navigator.includeNamespace(namespace);
  }
  





  public String getXPath()
  {
    return xPath.toString();
  }
  





  private void setXPath(String expr)
    throws JDOMException
  {
    try
    {
      xPath = new BaseXPath(expr, navigator);
      xPath.setNamespaceContext(navigator);
    } catch (Exception ex1) {
      throw new JDOMException("Invalid XPath expression: \"" + expr + "\"", ex1);
    }
  }
  

  public String toString()
  {
    return String.format("[XPath: %s]", new Object[] { xPath.toString() });
  }
}
