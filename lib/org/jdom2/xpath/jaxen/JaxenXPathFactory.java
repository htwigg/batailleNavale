package org.jdom2.xpath.jaxen;

import org.jdom2.Namespace;
import org.jdom2.filter.Filter;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

import java.util.Map;



































































public class JaxenXPathFactory
  extends XPathFactory
{
  public JaxenXPathFactory() {}
  
  public <T> XPathExpression<T> compile(String expression, Filter<T> filter, Map<String, Object> variables, Namespace... namespaces)
  {
    return new JaxenCompiled(expression, filter, variables, namespaces);
  }
}
