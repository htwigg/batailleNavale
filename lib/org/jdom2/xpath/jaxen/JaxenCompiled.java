package org.jdom2.xpath.jaxen;

import org.jaxen.*;
import org.jdom2.Namespace;
import org.jdom2.filter.Filter;
import org.jdom2.xpath.util.AbstractXPathCompiled;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;





































































class JaxenCompiled<T>
  extends AbstractXPathCompiled<T>
  implements NamespaceContext, VariableContext
{
  private final XPath xPath;
  
  private static final Object unWrapNS(Object o)
  {
    if ((o instanceof NamespaceContainer)) {
      return ((NamespaceContainer)o).getNamespace();
    }
    return o;
  }
  








  private static final List<Object> unWrap(List<?> results)
  {
    ArrayList<Object> ret = new ArrayList(results.size());
    for (Iterator<?> it = results.iterator(); it.hasNext();) {
      ret.add(unWrapNS(it.next()));
    }
    return ret;
  }
  






















  private final JDOM2Navigator navigator = new JDOM2Navigator();
  






  public JaxenCompiled(String expression, Filter<T> filter, Map<String, Object> variables, Namespace[] namespaces)
  {
    super(expression, filter, variables, namespaces);
    try {
      xPath = new BaseXPath(expression, navigator);
    } catch (JaxenException e) {
      throw new IllegalArgumentException("Unable to compile '" + expression + "'. See Cause.", e);
    }
    
    xPath.setNamespaceContext(this);
    xPath.setVariableContext(this);
  }
  





  private JaxenCompiled(JaxenCompiled<T> toclone)
  {
    this(toclone.getExpression(), toclone.getFilter(), toclone.getVariables(), toclone.getNamespaces());
  }
  
  public String translateNamespacePrefixToUri(String prefix)
  {
    return getNamespace(prefix).getURI();
  }
  
  public Object getVariableValue(String namespaceURI, String prefix, String localName)
    throws UnresolvableException
  {
    if (namespaceURI == null) {
      namespaceURI = "";
    }
    if (prefix == null) {
      prefix = "";
    }
    try {
      if ("".equals(namespaceURI)) {
        namespaceURI = getNamespace(prefix).getURI();
      }
      return getVariable(localName, Namespace.getNamespace(namespaceURI));
    } catch (IllegalArgumentException e) {
      throw new UnresolvableException("Unable to resolve variable " + localName + " in namespace '" + namespaceURI + "' to a vaulue.");
    }
  }
  

  protected List<?> evaluateRawAll(Object context)
  {
    try
    {
      return unWrap(xPath.selectNodes(context));
    } catch (JaxenException e) {
      throw new IllegalStateException("Unable to evaluate expression. See cause", e);
    }
  }
  
  protected Object evaluateRawFirst(Object context)
  {
    try
    {
      return unWrapNS(xPath.selectSingleNode(context));
    } catch (JaxenException e) {
      throw new IllegalStateException("Unable to evaluate expression. See cause", e);
    }
  }
  





  public JaxenCompiled<T> clone()
  {
    return new JaxenCompiled(this);
  }
}
