package org.jdom2.xpath;

import org.jdom2.Namespace;
import org.jdom2.filter.Filter;
import org.jdom2.filter.Filters;
import org.jdom2.internal.ReflectionConstructor;
import org.jdom2.internal.SystemProperty;
import org.jdom2.xpath.jaxen.JaxenXPathFactory;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

















































































public abstract class XPathFactory
{
  private static final Namespace[] EMPTYNS = new Namespace[0];
  



  private static final AtomicReference<XPathFactory> defaultreference = new AtomicReference();
  
  private static final String DEFAULTFACTORY = SystemProperty.get("org.jdom2.xpath.XPathFactory", null);
  






  public XPathFactory() {}
  





  public static final XPathFactory instance()
  {
    XPathFactory ret = (XPathFactory)defaultreference.get();
    if (ret != null) {
      return ret;
    }
    XPathFactory fac = DEFAULTFACTORY == null ? new JaxenXPathFactory() : newInstance(DEFAULTFACTORY);
    
    if (defaultreference.compareAndSet(null, fac)) {
      return fac;
    }
    

    return (XPathFactory)defaultreference.get();
  }
  












  public static final XPathFactory newInstance(String factoryclass)
  {
    return (XPathFactory)ReflectionConstructor.construct(factoryclass, XPathFactory.class);
  }
  






















































  public abstract <T> XPathExpression<T> compile(String paramString, Filter<T> paramFilter, Map<String, Object> paramMap, Namespace... paramVarArgs);
  





















































  public <T> XPathExpression<T> compile(String expression, Filter<T> filter, Map<String, Object> variables, Collection<Namespace> namespaces)
  {
    return compile(expression, filter, variables, (Namespace[])namespaces.toArray(EMPTYNS));
  }
  
















  public <T> XPathExpression<T> compile(String expression, Filter<T> filter)
  {
    return compile(expression, filter, null, EMPTYNS);
  }
  










  public XPathExpression<Object> compile(String expression)
  {
    return compile(expression, Filters.fpassthrough(), null, EMPTYNS);
  }
}
