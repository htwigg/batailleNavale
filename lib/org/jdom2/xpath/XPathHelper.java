package org.jdom2.xpath;

import org.jdom2.*;
import org.jdom2.filter.Filters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;































































































































public final class XPathHelper
{
  private XPathHelper() {}
  
  private static StringBuilder getPositionPath(Object node, List<?> siblings, String pathToken, StringBuilder buffer)
  {
    buffer.append(pathToken);
    
    if (siblings != null) {
      int position = 0;
      Iterator<?> i = siblings.iterator();
      while (i.hasNext()) {
        position++;
        if (i.next() == node)
          break;
      }
      if ((position > 1) || (i.hasNext()))
      {

        buffer.append('[').append(position).append(']');
      }
    }
    return buffer;
  }
  









  private static final StringBuilder getSingleStep(NamespaceAware nsa, StringBuilder buffer)
  {
    if ((nsa instanceof Content))
    {
      Content content = (Content)nsa;
      
      Parent pnt = content.getParent();
      
      if ((content instanceof Text))
      {
        List<?> sibs = pnt == null ? null : pnt.getContent(Filters.text());
        
        return getPositionPath(content, sibs, "text()", buffer);
      }
      if ((content instanceof Comment))
      {
        List<?> sibs = pnt == null ? null : pnt.getContent(Filters.comment());
        
        return getPositionPath(content, sibs, "comment()", buffer);
      }
      if ((content instanceof ProcessingInstruction))
      {
        List<?> sibs = pnt == null ? null : pnt.getContent(Filters.processinginstruction());
        
        return getPositionPath(content, sibs, "processing-instruction()", buffer);
      }
      
      if (((content instanceof Element)) && (((Element)content).getNamespace() == Namespace.NO_NAMESPACE))
      {



        String ename = ((Element)content).getName();
        List<?> sibs = (pnt instanceof Element) ? ((Element)pnt).getChildren(ename) : null;
        
        return getPositionPath(content, sibs, ename, buffer);
      }
      if ((content instanceof Element))
      {



        Element emt = (Element)content;
        


        List<?> sibs = (pnt instanceof Element) ? ((Element)pnt).getChildren(emt.getName(), emt.getNamespace()) : null;
        
        String xps = "*[local-name() = '" + emt.getName() + "' and namespace-uri() = '" + emt.getNamespaceURI() + "']";
        

        return getPositionPath(content, sibs, xps, buffer);
      }
      
      List<?> sibs = pnt == null ? Collections.singletonList(nsa) : pnt.getContent();
      
      return getPositionPath(content, sibs, "node()", buffer);
    }
    
    if ((nsa instanceof Attribute)) {
      Attribute att = (Attribute)nsa;
      if (att.getNamespace() == Namespace.NO_NAMESPACE) {
        buffer.append("@").append(att.getName());
      } else {
        buffer.append("@*[local-name() = '").append(att.getName());
        buffer.append("' and namespace-uri() = '");
        buffer.append(att.getNamespaceURI()).append("']");
      }
    }
    

    return buffer;
  }
  
















  private static StringBuilder getRelativeElementPath(Element from, Parent to, StringBuilder sb)
  {
    if (from == to) {
      sb.append(".");
      return sb;
    }
    



    ArrayList<Parent> tostack = new ArrayList();
    Parent p = to;
    while ((p != null) && (p != from)) {
      tostack.add(p);
      p = p.getParent();
    }
    

    int pos = tostack.size();
    
    if (p != from)
    {



      Parent f = from;
      int fcnt = 0;
      
      while ((f != null) && ((pos = locate(f, tostack)) < 0))
      {

        fcnt++;
        f = f.getParent();
      }
      if (f == null) {
        throw new IllegalArgumentException("The 'from' and 'to' Element have no common ancestor.");
      }
      
      for (;;)
      {
        fcnt--; if (fcnt < 0) break;
        sb.append("../");
      }
    }
    for (;;)
    {
      pos--; if (pos < 0) break;
      getSingleStep((NamespaceAware)tostack.get(pos), sb);
      sb.append("/");
    }
    
    sb.setLength(sb.length() - 1);
    return sb;
  }
  









  private static int locate(Parent f, List<Parent> tostack)
  {
    int ret = tostack.size();
    do { ret--; if (ret < 0) break;
    } while (f != tostack.get(ret));
    return ret;
    

    return -1;
  }
  












  public static String getRelativePath(Content from, Content to)
  {
    if (from == null) {
      throw new NullPointerException("Cannot create a path from a null target");
    }
    
    if (to == null) {
      throw new NullPointerException("Cannot create a path to a null target");
    }
    
    StringBuilder sb = new StringBuilder();
    if (from == to) {
      return ".";
    }
    Element efrom = (from instanceof Element) ? (Element)from : from.getParentElement();
    
    if (from != efrom) {
      sb.append("../");
    }
    if ((to instanceof Element)) {
      getRelativeElementPath(efrom, (Element)to, sb);
    } else {
      Parent telement = to.getParent();
      if (telement == null) {
        throw new IllegalArgumentException("Cannot get a relative XPath to detached content.");
      }
      
      getRelativeElementPath(efrom, telement, sb);
      sb.append("/");
      getSingleStep(to, sb);
    }
    return sb.toString();
  }
  












  public static String getRelativePath(Content from, Attribute to)
  {
    if (from == null) {
      throw new NullPointerException("Cannot create a path from a null Content");
    }
    
    if (to == null) {
      throw new NullPointerException("Cannot create a path to a null Attribute");
    }
    
    Element t = to.getParent();
    if (t == null) {
      throw new IllegalArgumentException("Cannot create a path to detached Attribute");
    }
    
    StringBuilder sb = new StringBuilder(getRelativePath(from, t));
    sb.append("/");
    getSingleStep(to, sb);
    return sb.toString();
  }
  













  public static String getRelativePath(Attribute from, Attribute to)
  {
    if (from == null) {
      throw new NullPointerException("Cannot create a path from a null 'from'");
    }
    
    if (to == null) {
      throw new NullPointerException("Cannot create a path to a null target");
    }
    
    if (from == to) {
      return ".";
    }
    
    Element f = from.getParent();
    if (f == null) {
      throw new IllegalArgumentException("Cannot create a path from a detached attrbibute");
    }
    

    return "../" + getRelativePath(f, to);
  }
  












  public static String getRelativePath(Attribute from, Content to)
  {
    if (from == null) {
      throw new NullPointerException("Cannot create a path from a null 'from'");
    }
    
    if (to == null) {
      throw new NullPointerException("Cannot create a path to a null target");
    }
    
    Element f = from.getParent();
    if (f == null) {
      throw new IllegalArgumentException("Cannot create a path from a detached attrbibute");
    }
    
    if (f == to) {
      return "src/main";
    }
    return "../" + getRelativePath(f, to);
  }
  








  public static String getAbsolutePath(Content to)
  {
    if (to == null) {
      throw new NullPointerException("Cannot create a path to a null target");
    }
    

    StringBuilder sb = new StringBuilder();
    
    Element t = (to instanceof Element) ? (Element)to : to.getParentElement();
    
    if (t == null) {
      if (to.getParent() == null) {
        throw new IllegalArgumentException("Cannot create a path to detached target");
      }
      

      sb.append("/");
      getSingleStep(to, sb);
      return sb.toString();
    }
    Element r = t;
    while (r.getParentElement() != null) {
      r = r.getParentElement();
    }
    sb.append("/");
    getSingleStep(r, sb);
    if (r != t) {
      sb.append("/");
      getRelativeElementPath(r, t, sb);
    }
    if (t != to) {
      sb.append("/");
      getSingleStep(to, sb);
    }
    return sb.toString();
  }
  








  public static String getAbsolutePath(Attribute to)
  {
    if (to == null) {
      throw new NullPointerException("Cannot create a path to a null target");
    }
    

    Element t = to.getParent();
    if (t == null) {
      throw new IllegalArgumentException("Cannot create a path to detached target");
    }
    
    Element r = t;
    while (r.getParentElement() != null) {
      r = r.getParentElement();
    }
    StringBuilder sb = new StringBuilder();
    
    sb.append("/");
    getSingleStep(r, sb);
    if (t != r) {
      sb.append("/");
      getRelativeElementPath(r, t, sb);
    }
    sb.append("/");
    getSingleStep(to, sb);
    return sb.toString();
  }
}
