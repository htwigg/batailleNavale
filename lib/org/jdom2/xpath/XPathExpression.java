package org.jdom2.xpath;

import org.jdom2.Namespace;
import org.jdom2.filter.Filter;

import java.util.List;

public abstract interface XPathExpression<T>
  extends Cloneable
{
  public abstract XPathExpression<T> clone();
  
  public abstract String getExpression();
  
  public abstract Namespace getNamespace(String paramString);
  
  public abstract Namespace[] getNamespaces();
  
  public abstract Object setVariable(String paramString, Namespace paramNamespace, Object paramObject);
  
  public abstract Object setVariable(String paramString, Object paramObject);
  
  public abstract Object getVariable(String paramString, Namespace paramNamespace);
  
  public abstract Object getVariable(String paramString);
  
  public abstract Filter<T> getFilter();
  
  public abstract List<T> evaluate(Object paramObject);
  
  public abstract T evaluateFirst(Object paramObject);
  
  public abstract XPathDiagnostic<T> diagnose(Object paramObject, boolean paramBoolean);
}
