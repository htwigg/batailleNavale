package org.jdom2;

import org.jdom2.output.XMLOutputter;



































































public class Comment
  extends Content
{
  private static final long serialVersionUID = 200L;
  protected String text;
  
  protected Comment()
  {
    super(Content.CType.Comment);
  }
  




  public Comment(String text)
  {
    super(Content.CType.Comment);
    setText(text);
  }
  







  public String getValue()
  {
    return text;
  }
  




  public String getText()
  {
    return text;
  }
  




  public Comment setText(String text)
  {
    String reason;
    


    if ((reason = Verifier.checkCommentData(text)) != null) {
      throw new IllegalDataException(text, "comment", reason);
    }
    
    this.text = text;
    return this;
  }
  
  public Comment clone()
  {
    return (Comment)super.clone();
  }
  
  public Comment detach()
  {
    return (Comment)super.detach();
  }
  
  protected Comment setParent(Parent parent)
  {
    return (Comment)super.setParent(parent);
  }
  










  public String toString()
  {
    return "[Comment: " + new XMLOutputter().outputString(this) + "]";
  }
}
