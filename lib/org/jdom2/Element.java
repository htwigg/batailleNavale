package org.jdom2;

import org.jdom2.filter.ElementFilter;
import org.jdom2.filter.Filter;
import org.jdom2.util.IteratorIterable;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.net.URISyntaxException;



























































































public class Element
  extends Content
  implements Parent
{
  private static final int INITIAL_ARRAY_SIZE = 5;
  protected String name;
  protected Namespace namespace;
  transient List<Namespace> additionalNamespaces = null;
  




  transient AttributeList attributes = null;
  




  transient ContentList content = new ContentList(this);
  






  private static final long serialVersionUID = 200L;
  






  protected Element()
  {
    super(Content.CType.Element);
  }
  








  public Element(String name, Namespace namespace)
  {
    super(Content.CType.Element);
    setName(name);
    setNamespace(namespace);
  }
  






  public Element(String name)
  {
    this(name, (Namespace)null);
  }
  










  public Element(String name, String uri)
  {
    this(name, Namespace.getNamespace("", uri));
  }
  











  public Element(String name, String prefix, String uri)
  {
    this(name, Namespace.getNamespace(prefix, uri));
  }
  




  public String getName()
  {
    return name;
  }
  







  public Element setName(String name)
  {
    String reason = Verifier.checkElementName(name);
    if (reason != null) {
      throw new IllegalNameException(name, "element", reason);
    }
    this.name = name;
    return this;
  }
  




  public Namespace getNamespace()
  {
    return namespace;
  }
  







  public Element setNamespace(Namespace namespace)
  {
    if (namespace == null) {
      namespace = Namespace.NO_NAMESPACE;
    }
    
    if (additionalNamespaces != null) {
      String reason = Verifier.checkNamespaceCollision(namespace, getAdditionalNamespaces());
      
      if (reason != null) {
        throw new IllegalAddException(this, namespace, reason);
      }
    }
    if (hasAttributes()) {
      for (Attribute a : getAttributes()) {
        String reason = Verifier.checkNamespaceCollision(namespace, a);
        
        if (reason != null) {
          throw new IllegalAddException(this, namespace, reason);
        }
      }
    }
    
    this.namespace = namespace;
    return this;
  }
  





  public String getNamespacePrefix()
  {
    return namespace.getPrefix();
  }
  






  public String getNamespaceURI()
  {
    return namespace.getURI();
  }
  










  public Namespace getNamespace(String prefix)
  {
    if (prefix == null) {
      return null;
    }
    
    if ("xml".equals(prefix))
    {
      return Namespace.XML_NAMESPACE;
    }
    

    if (prefix.equals(getNamespacePrefix())) {
      return getNamespace();
    }
    

    if (additionalNamespaces != null) {
      for (int i = 0; i < additionalNamespaces.size(); i++) {
        Namespace ns = (Namespace)additionalNamespaces.get(i);
        if (prefix.equals(ns.getPrefix())) {
          return ns;
        }
      }
    }
    
    if (attributes != null) {
      for (Attribute a : attributes) {
        if (prefix.equals(a.getNamespacePrefix())) {
          return a.getNamespace();
        }
      }
    }
    

    if ((parent instanceof Element)) {
      return ((Element)parent).getNamespace(prefix);
    }
    
    return null;
  }
  









  public String getQualifiedName()
  {
    if ("".equals(namespace.getPrefix())) {
      return getName();
    }
    
    return namespace.getPrefix() + ':' + name;
  }
  
















  public boolean addNamespaceDeclaration(Namespace additionalNamespace)
  {
    if (additionalNamespaces == null) {
      additionalNamespaces = new ArrayList(5);
    }
    
    for (Namespace ns : additionalNamespaces) {
      if (ns == additionalNamespace) {
        return false;
      }
    }
    


    String reason = Verifier.checkNamespaceCollision(additionalNamespace, this);
    if (reason != null) {
      throw new IllegalAddException(this, additionalNamespace, reason);
    }
    
    return additionalNamespaces.add(additionalNamespace);
  }
  









  public void removeNamespaceDeclaration(Namespace additionalNamespace)
  {
    if (additionalNamespaces == null) {
      return;
    }
    additionalNamespaces.remove(additionalNamespace);
  }
  












  public List<Namespace> getAdditionalNamespaces()
  {
    if (additionalNamespaces == null) {
      return Collections.emptyList();
    }
    return Collections.unmodifiableList(additionalNamespaces);
  }
  








  public String getValue()
  {
    StringBuilder buffer = new StringBuilder();
    
    for (Content child : getContent()) {
      if (((child instanceof Element)) || ((child instanceof Text))) {
        buffer.append(child.getValue());
      }
    }
    return buffer.toString();
  }
  










  public boolean isRootElement()
  {
    return parent instanceof Document;
  }
  
  public int getContentSize()
  {
    return content.size();
  }
  
  public int indexOf(Content child)
  {
    return content.indexOf(child);
  }
  





















  public String getText()
  {
    if (content.size() == 0) {
      return "";
    }
    

    if (content.size() == 1) {
      Object obj = content.get(0);
      if ((obj instanceof Text)) {
        return ((Text)obj).getText();
      }
      return "";
    }
    

    StringBuilder textContent = new StringBuilder();
    boolean hasText = false;
    
    for (int i = 0; i < content.size(); i++) {
      Object obj = content.get(i);
      if ((obj instanceof Text)) {
        textContent.append(((Text)obj).getText());
        hasText = true;
      }
    }
    
    if (!hasText) {
      return "";
    }
    return textContent.toString();
  }
  







  public String getTextTrim()
  {
    return getText().trim();
  }
  








  public String getTextNormalize()
  {
    return Text.normalizeString(getText());
  }
  








  public String getChildText(String cname)
  {
    Element child = getChild(cname);
    if (child == null) {
      return null;
    }
    return child.getText();
  }
  








  public String getChildTextTrim(String cname)
  {
    Element child = getChild(cname);
    if (child == null) {
      return null;
    }
    return child.getTextTrim();
  }
  








  public String getChildTextNormalize(String cname)
  {
    Element child = getChild(cname);
    if (child == null) {
      return null;
    }
    return child.getTextNormalize();
  }
  









  public String getChildText(String cname, Namespace ns)
  {
    Element child = getChild(cname, ns);
    if (child == null) {
      return null;
    }
    return child.getText();
  }
  










  public String getChildTextTrim(String cname, Namespace ns)
  {
    Element child = getChild(cname, ns);
    if (child == null) {
      return null;
    }
    return child.getTextTrim();
  }
  










  public String getChildTextNormalize(String cname, Namespace ns)
  {
    Element child = getChild(cname, ns);
    if (child == null) {
      return null;
    }
    return child.getTextNormalize();
  }
  













  public Element setText(String text)
  {
    content.clear();
    
    if (text != null) {
      addContent(new Text(text));
    }
    
    return this;
  }
  








  public boolean coalesceText(boolean recursively)
  {
    Iterator<Content> it = recursively ? getDescendants() : content.iterator();
    
    Text tfirst = null;
    boolean changed = false;
    while (it.hasNext()) {
      Content c = (Content)it.next();
      if (c.getCType() == Content.CType.Text)
      {
        Text text = (Text)c;
        if ("".equals(text.getValue())) {
          it.remove();
          changed = true;
        } else if ((tfirst == null) || (tfirst.getParent() != text.getParent()))
        {


          tfirst = text;
        }
        else {
          tfirst.append(text.getValue());
          
          it.remove();
          changed = true;
        }
      }
      else {
        tfirst = null;
      }
    }
    return changed;
  }
  






















  public List<Content> getContent()
  {
    return content;
  }
  












  public <E extends Content> List<E> getContent(Filter<E> filter)
  {
    return content.getView(filter);
  }
  





  public List<Content> removeContent()
  {
    List<Content> old = new ArrayList(content);
    content.clear();
    return old;
  }
  






  public <F extends Content> List<F> removeContent(Filter<F> filter)
  {
    List<F> old = new ArrayList();
    Iterator<F> iter = content.getView(filter).iterator();
    while (iter.hasNext()) {
      F child = (Content)iter.next();
      old.add(child);
      iter.remove();
    }
    return old;
  }
  


































  public Element setContent(Collection<? extends Content> newContent)
  {
    content.clearAndSet(newContent);
    return this;
  }
  














  public Element setContent(int index, Content child)
  {
    content.set(index, child);
    return this;
  }
  















  public Parent setContent(int index, Collection<? extends Content> newContent)
  {
    content.remove(index);
    content.addAll(index, newContent);
    return this;
  }
  









  public Element addContent(String str)
  {
    return addContent(new Text(str));
  }
  






  public Element addContent(Content child)
  {
    content.add(child);
    return this;
  }
  











  public Element addContent(Collection<? extends Content> newContent)
  {
    content.addAll(newContent);
    return this;
  }
  










  public Element addContent(int index, Content child)
  {
    content.add(index, child);
    return this;
  }
  














  public Element addContent(int index, Collection<? extends Content> newContent)
  {
    content.addAll(index, newContent);
    return this;
  }
  
  public List<Content> cloneContent()
  {
    int size = getContentSize();
    List<Content> list = new ArrayList(size);
    for (int i = 0; i < size; i++) {
      Content child = getContent(i);
      list.add(child.clone());
    }
    return list;
  }
  
  public Content getContent(int index)
  {
    return content.get(index);
  }
  





  public boolean removeContent(Content child)
  {
    return content.remove(child);
  }
  
  public Content removeContent(int index)
  {
    return content.remove(index);
  }
  


























  public Element setContent(Content child)
  {
    content.clear();
    content.add(child);
    return this;
  }
  







  public boolean isAncestor(Element element)
  {
    Parent p = element.getParent();
    while ((p instanceof Element)) {
      if (p == this) {
        return true;
      }
      p = p.getParent();
    }
    return false;
  }
  







  public boolean hasAttributes()
  {
    return (attributes != null) && (!attributes.isEmpty());
  }
  








  public boolean hasAdditionalNamespaces()
  {
    return (additionalNamespaces != null) && (!additionalNamespaces.isEmpty());
  }
  



  AttributeList getAttributeList()
  {
    if (attributes == null) {
      attributes = new AttributeList(this);
    }
    return attributes;
  }
  










  public List<Attribute> getAttributes()
  {
    return getAttributeList();
  }
  








  public Attribute getAttribute(String attname)
  {
    return getAttribute(attname, Namespace.NO_NAMESPACE);
  }
  









  public Attribute getAttribute(String attname, Namespace ns)
  {
    if (attributes == null) {
      return null;
    }
    return getAttributeList().get(attname, ns);
  }
  









  public String getAttributeValue(String attname)
  {
    if (attributes == null) {
      return null;
    }
    return getAttributeValue(attname, Namespace.NO_NAMESPACE);
  }
  










  public String getAttributeValue(String attname, String def)
  {
    if (attributes == null) {
      return def;
    }
    return getAttributeValue(attname, Namespace.NO_NAMESPACE, def);
  }
  










  public String getAttributeValue(String attname, Namespace ns)
  {
    if (attributes == null) {
      return null;
    }
    return getAttributeValue(attname, ns, null);
  }
  











  public String getAttributeValue(String attname, Namespace ns, String def)
  {
    if (attributes == null) {
      return def;
    }
    Attribute attribute = getAttributeList().get(attname, ns);
    if (attribute == null) {
      return def;
    }
    
    return attribute.getValue();
  }
  










































  public Element setAttributes(Collection<? extends Attribute> newAttributes)
  {
    getAttributeList().clearAndSet(newAttributes);
    return this;
  }
  














  public Element setAttribute(String name, String value)
  {
    Attribute attribute = getAttribute(name);
    if (attribute == null) {
      Attribute newAttribute = new Attribute(name, value);
      setAttribute(newAttribute);
    } else {
      attribute.setValue(value);
    }
    
    return this;
  }
  


















  public Element setAttribute(String name, String value, Namespace ns)
  {
    Attribute attribute = getAttribute(name, ns);
    if (attribute == null) {
      Attribute newAttribute = new Attribute(name, value, ns);
      setAttribute(newAttribute);
    } else {
      attribute.setValue(value);
    }
    
    return this;
  }
  











  public Element setAttribute(Attribute attribute)
  {
    getAttributeList().add(attribute);
    return this;
  }
  








  public boolean removeAttribute(String attname)
  {
    return removeAttribute(attname, Namespace.NO_NAMESPACE);
  }
  










  public boolean removeAttribute(String attname, Namespace ns)
  {
    if (attributes == null) {
      return false;
    }
    return getAttributeList().remove(attname, ns);
  }
  







  public boolean removeAttribute(Attribute attribute)
  {
    if (attributes == null) {
      return false;
    }
    return getAttributeList().remove(attribute);
  }
  












  public String toString()
  {
    StringBuilder stringForm = new StringBuilder(64).append("[Element: <").append(getQualifiedName());
    


    String nsuri = getNamespaceURI();
    if (!"".equals(nsuri)) {
      stringForm.append(" [Namespace: ").append(nsuri).append("]");
    }
    


    stringForm.append("/>]");
    
    return stringForm.toString();
  }
  












  public Element clone()
  {
    Element element = (Element)super.clone();
    











    content = new ContentList(element);
    attributes = (attributes == null ? null : new AttributeList(element));
    

    if (attributes != null) {
      for (int i = 0; i < attributes.size(); i++) {
        Attribute attribute = attributes.get(i);
        attributes.add(attribute.clone());
      }
    }
    

    if (additionalNamespaces != null) {
      additionalNamespaces = new ArrayList(additionalNamespaces);
    }
    

    for (int i = 0; i < content.size(); i++) {
      Content c = content.get(i);
      content.add(c.clone());
    }
    
    return element;
  }
  






  public IteratorIterable<Content> getDescendants()
  {
    return new DescendantIterator(this);
  }
  









  public <F extends Content> IteratorIterable<F> getDescendants(Filter<F> filter)
  {
    return new FilterIterator(new DescendantIterator(this), filter);
  }
  






























  public List<Element> getChildren()
  {
    return content.getView(new ElementFilter());
  }
  















  public List<Element> getChildren(String cname)
  {
    return getChildren(cname, Namespace.NO_NAMESPACE);
  }
  
















  public List<Element> getChildren(String cname, Namespace ns)
  {
    return content.getView(new ElementFilter(cname, ns));
  }
  









  public Element getChild(String cname, Namespace ns)
  {
    List<Element> elements = content.getView(new ElementFilter(cname, ns));
    Iterator<Element> iter = elements.iterator();
    if (iter.hasNext()) {
      return (Element)iter.next();
    }
    return null;
  }
  








  public Element getChild(String cname)
  {
    return getChild(cname, Namespace.NO_NAMESPACE);
  }
  









  public boolean removeChild(String cname)
  {
    return removeChild(cname, Namespace.NO_NAMESPACE);
  }
  










  public boolean removeChild(String cname, Namespace ns)
  {
    ElementFilter filter = new ElementFilter(cname, ns);
    List<Element> old = content.getView(filter);
    Iterator<Element> iter = old.iterator();
    if (iter.hasNext()) {
      iter.next();
      iter.remove();
      return true;
    }
    
    return false;
  }
  









  public boolean removeChildren(String cname)
  {
    return removeChildren(cname, Namespace.NO_NAMESPACE);
  }
  










  public boolean removeChildren(String cname, Namespace ns)
  {
    boolean deletedSome = false;
    
    ElementFilter filter = new ElementFilter(cname, ns);
    List<Element> old = content.getView(filter);
    Iterator<Element> iter = old.iterator();
    while (iter.hasNext()) {
      iter.next();
      iter.remove();
      deletedSome = true;
    }
    
    return deletedSome;
  }
  









































  public List<Namespace> getNamespacesInScope()
  {
    TreeMap<String, Namespace> namespaces = new TreeMap();
    namespaces.put(Namespace.XML_NAMESPACE.getPrefix(), Namespace.XML_NAMESPACE);
    namespaces.put(getNamespacePrefix(), getNamespace());
    if (additionalNamespaces != null) {
      for (Namespace ns : getAdditionalNamespaces()) {
        if (!namespaces.containsKey(ns.getPrefix())) {
          namespaces.put(ns.getPrefix(), ns);
        }
      }
    }
    if (attributes != null) {
      for (Attribute att : getAttributes()) {
        Namespace ns = att.getNamespace();
        if (!namespaces.containsKey(ns.getPrefix())) {
          namespaces.put(ns.getPrefix(), ns);
        }
      }
    }
    

    Element pnt = getParentElement();
    if (pnt != null) {
      for (Namespace ns : pnt.getNamespacesInScope()) {
        if (!namespaces.containsKey(ns.getPrefix())) {
          namespaces.put(ns.getPrefix(), ns);
        }
      }
    }
    
    if ((pnt == null) && (!namespaces.containsKey("")))
    {
      namespaces.put(Namespace.NO_NAMESPACE.getPrefix(), Namespace.NO_NAMESPACE);
    }
    
    ArrayList<Namespace> al = new ArrayList(namespaces.size());
    al.add(getNamespace());
    namespaces.remove(getNamespacePrefix());
    al.addAll(namespaces.values());
    
    return Collections.unmodifiableList(al);
  }
  
  public List<Namespace> getNamespacesInherited()
  {
    if (getParentElement() == null) {
      ArrayList<Namespace> ret = new ArrayList(getNamespacesInScope());
      for (Iterator<Namespace> it = ret.iterator(); it.hasNext();) {
        Namespace ns = (Namespace)it.next();
        if ((ns != Namespace.NO_NAMESPACE) && (ns != Namespace.XML_NAMESPACE))
        {

          it.remove(); }
      }
      return Collections.unmodifiableList(ret);
    }
    


    HashMap<String, Namespace> parents = new HashMap();
    for (Namespace ns : getParentElement().getNamespacesInScope()) {
      parents.put(ns.getPrefix(), ns);
    }
    
    ArrayList<Namespace> al = new ArrayList();
    for (Namespace ns : getNamespacesInScope()) {
      if (ns == parents.get(ns.getPrefix()))
      {
        al.add(ns);
      }
    }
    
    return Collections.unmodifiableList(al);
  }
  
  public List<Namespace> getNamespacesIntroduced()
  {
    if (getParentElement() == null)
    {
      List<Namespace> ret = new ArrayList(getNamespacesInScope());
      for (Iterator<Namespace> it = ret.iterator(); it.hasNext();) {
        Namespace ns = (Namespace)it.next();
        if ((ns == Namespace.XML_NAMESPACE) || (ns == Namespace.NO_NAMESPACE)) {
          it.remove();
        }
      }
      return Collections.unmodifiableList(ret);
    }
    


    HashMap<String, Namespace> parents = new HashMap();
    for (Namespace ns : getParentElement().getNamespacesInScope()) {
      parents.put(ns.getPrefix(), ns);
    }
    
    ArrayList<Namespace> al = new ArrayList();
    for (Namespace ns : getNamespacesInScope()) {
      if ((!parents.containsKey(ns.getPrefix())) || (ns != parents.get(ns.getPrefix())))
      {
        al.add(ns);
      }
    }
    
    return Collections.unmodifiableList(al);
  }
  
  public Element detach()
  {
    return (Element)super.detach();
  }
  
  public void canContainContent(Content child, int index, boolean replace) throws IllegalAddException
  {
    if ((child instanceof DocType)) {
      throw new IllegalAddException("A DocType is not allowed except at the document level");
    }
  }
  
















  public void sortContent(Comparator<? super Content> comparator)
  {
    content.sort(comparator);
  }
  















  public void sortChildren(Comparator<? super Element> comparator)
  {
    ((ContentList.FilterList)getChildren()).sort(comparator);
  }
  


















  public void sortAttributes(Comparator<? super Attribute> comparator)
  {
    if (attributes != null) {
      attributes.sort(comparator);
    }
  }
  






























  public <E extends Content> void sortContent(Filter<E> filter, Comparator<? super E> comparator)
  {
    ContentList.FilterList<E> list = (ContentList.FilterList)getContent(filter);
    list.sort(comparator);
  }
  








  private final URI resolve(String uri, URI relative)
    throws URISyntaxException
  {
    if (uri == null) {
      return relative;
    }
    URI n = new URI(uri);
    if (relative == null) {
      return n;
    }
    return n.resolve(relative);
  }
  




















  public URI getXMLBaseURI()
    throws URISyntaxException
  {
    Parent p = this;
    URI ret = null;
    while (p != null) {
      if ((p instanceof Element)) {
        ret = resolve(((Element)p).getAttributeValue("base", Namespace.XML_NAMESPACE), ret);
      }
      else {
        ret = resolve(((Document)p).getBaseURI(), ret);
      }
      if ((ret != null) && (ret.isAbsolute())) {
        return ret;
      }
      p = p.getParent();
    }
    return ret;
  }
  
























  private void writeObject(ObjectOutputStream out)
    throws IOException
  {
    out.defaultWriteObject();
    if (hasAdditionalNamespaces()) {
      int ans = additionalNamespaces.size();
      out.writeInt(ans);
      for (int i = 0; i < ans; i++) {
        out.writeObject(additionalNamespaces.get(i));
      }
    } else {
      out.writeInt(0);
    }
    if (hasAttributes()) {
      int ans = attributes.size();
      out.writeInt(ans);
      for (int i = 0; i < ans; i++) {
        out.writeObject(attributes.get(i));
      }
    } else {
      out.writeInt(0);
    }
    
    int cs = content.size();
    out.writeInt(cs);
    for (int i = 0; i < cs; i++) {
      out.writeObject(content.get(i));
    }
  }
  









  private void readObject(ObjectInputStream in)
    throws IOException, ClassNotFoundException
  {
    in.defaultReadObject();
    
    content = new ContentList(this);
    
    int nss = in.readInt();
    for (;;) {
      nss--; if (nss < 0) break;
      addNamespaceDeclaration((Namespace)in.readObject());
    }
    
    int ats = in.readInt();
    for (;;) { ats--; if (ats < 0) break;
      setAttribute((Attribute)in.readObject());
    }
    
    int cs = in.readInt();
    for (;;) { cs--; if (cs < 0) break;
      addContent((Content)in.readObject());
    }
  }
}
