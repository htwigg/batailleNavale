package org.jdom2;

import org.jdom2.output.Format;












































































public class Text
  extends Content
{
  private static final long serialVersionUID = 200L;
  static final String EMPTY_STRING = "";
  protected String value;
  
  protected Text(Content.CType ctype)
  {
    super(ctype);
  }
  




  protected Text()
  {
    this(Content.CType.Text);
  }
  








  public Text(String str)
  {
    this(Content.CType.Text);
    setText(str);
  }
  





  public String getText()
  {
    return value;
  }
  





  public String getTextTrim()
  {
    return Format.trimBoth(getText());
  }
  






  public String getTextNormalize()
  {
    return normalizeString(getText());
  }
  










  public static String normalizeString(String str)
  {
    if (str == null) {
      return "";
    }
    return Format.compact(str);
  }
  










  public Text setText(String str)
  {
    if (str == null) {
      value = "";
      return this;
    }
    String reason;
    if ((reason = Verifier.checkCharacterData(str)) != null) {
      throw new IllegalDataException(str, "character content", reason);
    }
    value = str;
    return this;
  }
  










  public void append(String str)
  {
    if (str == null)
      return;
    String reason;
    if ((reason = Verifier.checkCharacterData(str)) != null) {
      throw new IllegalDataException(str, "character content", reason);
    }
    
    if (str.length() > 0) {
      value += str;
    }
  }
  





  public void append(Text text)
  {
    if (text == null) {
      return;
    }
    value += text.getText();
  }
  






  public String getValue()
  {
    return value;
  }
  










  public String toString()
  {
    return 64 + "[Text: " + getText() + "]";
  }
  




  public Text clone()
  {
    Text text = (Text)super.clone();
    value = value;
    return text;
  }
  
  public Text detach()
  {
    return (Text)super.detach();
  }
  
  protected Text setParent(Parent parent)
  {
    return (Text)super.setParent(parent);
  }
  

  public Element getParent()
  {
    return (Element)super.getParent();
  }
}
