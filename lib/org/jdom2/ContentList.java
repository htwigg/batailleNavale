package org.jdom2;

import org.jdom2.filter.Filter;
import org.jdom2.internal.ArrayCopy;

































































final class ContentList
  extends AbstractList<Content>
  implements RandomAccess
{
  private static final int INITIAL_ARRAY_SIZE = 4;
  private Content[] elementData = null;
  





  private int size;
  




  private transient int sizeModCount = Integer.MIN_VALUE;
  




  private transient int dataModiCount = Integer.MIN_VALUE;
  



  private final Parent parent;
  



  ContentList(Parent parent)
  {
    this.parent = parent;
  }
  






  final void uncheckedAddContent(Content c)
  {
    parent = parent;
    ensureCapacity(size + 1);
    elementData[(size++)] = c;
    incModCount();
  }
  










  private final void setModCount(int sizemod, int datamod)
  {
    sizeModCount = sizemod;
    dataModiCount = datamod;
  }
  







  private final int getModCount()
  {
    return sizeModCount;
  }
  






  private final void incModCount()
  {
    dataModiCount += 1;
    
    sizeModCount += 1;
  }
  
  private final void incDataModOnly() {
    dataModiCount += 1;
  }
  



  private final int getDataModCount()
  {
    return dataModiCount;
  }
  
  private final void checkIndex(int index, boolean excludes) {
    int max = excludes ? size - 1 : size;
    
    if ((index < 0) || (index > max)) {
      throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size);
    }
  }
  


  private final void checkPreConditions(Content child, int index, boolean replace)
  {
    if (child == null) {
      throw new NullPointerException("Cannot add null object");
    }
    
    checkIndex(index, replace);
    
    if (child.getParent() != null)
    {
      Parent p = child.getParent();
      if ((p instanceof Document)) {
        throw new IllegalAddException((Element)child, "The Content already has an existing parent document");
      }
      
      throw new IllegalAddException("The Content already has an existing parent \"" + ((Element)p).getQualifiedName() + "\"");
    }
    


    if (child == parent) {
      throw new IllegalAddException("The Element cannot be added to itself");
    }
    


    if (((parent instanceof Element)) && ((child instanceof Element)) && (((Element)child).isAncestor((Element)parent)))
    {
      throw new IllegalAddException("The Element cannot be added as a descendent of itself");
    }
  }
  














  public void add(int index, Content child)
  {
    checkPreConditions(child, index, false);
    
    parent.canContainContent(child, index, false);
    
    child.setParent(parent);
    
    ensureCapacity(size + 1);
    if (index == size) {
      elementData[(size++)] = child;
    } else {
      System.arraycopy(elementData, index, elementData, index + 1, size - index);
      elementData[index] = child;
      size += 1;
    }
    
    incModCount();
  }
  








  public boolean addAll(Collection<? extends Content> collection)
  {
    return addAll(size, collection);
  }
  














  public boolean addAll(int index, Collection<? extends Content> collection)
  {
    if (collection == null) {
      throw new NullPointerException("Can not add a null collection to the ContentList");
    }
    

    checkIndex(index, false);
    
    if (collection.isEmpty())
    {

      return false;
    }
    int addcnt = collection.size();
    if (addcnt == 1)
    {
      add(index, (Content)collection.iterator().next());
      return true;
    }
    
    ensureCapacity(size() + addcnt);
    
    int tmpmodcount = getModCount();
    int tmpdmc = getDataModCount();
    boolean ok = false;
    
    int count = 0;
    try
    {
      for (Content c : collection) {
        add(index + count, c);
        count++;
      }
      ok = true;
    } finally {
      if (!ok) {
        for (;;) {
          count--; if (count < 0) break;
          remove(index + count);
        }
        
        setModCount(tmpmodcount, tmpdmc);
      }
    }
    
    return true;
  }
  



  public void clear()
  {
    if (elementData != null) {
      for (int i = 0; i < size; i++) {
        Content obj = elementData[i];
        removeParent(obj);
      }
      elementData = null;
      size = 0;
    }
    incModCount();
  }
  






  void clearAndSet(Collection<? extends Content> collection)
  {
    if ((collection == null) || (collection.isEmpty())) {
      clear();
      return;
    }
    

    Content[] old = elementData;
    int oldSize = size;
    int oldModCount = getModCount();
    int oldDataModCount = getDataModCount();
    




    while (size > 0) {
      old[(--size)].setParent(null);
    }
    size = 0;
    elementData = null;
    
    boolean ok = false;
    try {
      addAll(0, collection);
      ok = true;
    } finally {
      if (!ok)
      {




        elementData = old;
        while (size < oldSize) {
          elementData[(size++)].setParent(parent);
        }
        setModCount(oldModCount, oldDataModCount);
      }
    }
  }
  








  void ensureCapacity(int minCapacity)
  {
    if (elementData == null) {
      elementData = new Content[Math.max(minCapacity, 4)];
      return; }
    if (minCapacity < elementData.length) {
      return;
    }
    




    int newcap = size * 3 / 2 + 1;
    elementData = ((Content[])ArrayCopy.copyOf(elementData, newcap < minCapacity ? minCapacity : newcap));
  }
  








  public Content get(int index)
  {
    checkIndex(index, true);
    return elementData[index];
  }
  








  <E extends Content> List<E> getView(Filter<E> filter)
  {
    return new FilterList(filter);
  }
  






  int indexOfFirstElement()
  {
    if (elementData != null) {
      for (int i = 0; i < size; i++) {
        if ((elementData[i] instanceof Element)) {
          return i;
        }
      }
    }
    return -1;
  }
  





  int indexOfDocType()
  {
    if (elementData != null) {
      for (int i = 0; i < size; i++) {
        if ((elementData[i] instanceof DocType)) {
          return i;
        }
      }
    }
    return -1;
  }
  







  public Content remove(int index)
  {
    checkIndex(index, true);
    
    Content old = elementData[index];
    removeParent(old);
    System.arraycopy(elementData, index + 1, elementData, index, size - index - 1);
    elementData[(--size)] = null;
    incModCount();
    return old;
  }
  
  private static void removeParent(Content c)
  {
    c.setParent(null);
  }
  











  public Content set(int index, Content child)
  {
    checkPreConditions(child, index, true);
    

    parent.canContainContent(child, index, true);
    






    Content old = elementData[index];
    removeParent(old);
    child.setParent(parent);
    elementData[index] = child;
    

    incDataModOnly();
    return old;
  }
  





  public int size()
  {
    return size;
  }
  
  public Iterator<Content> iterator()
  {
    return new CLIterator(null);
  }
  
  public ListIterator<Content> listIterator()
  {
    return new CLListIterator(0);
  }
  
  public ListIterator<Content> listIterator(int start)
  {
    return new CLListIterator(start);
  }
  





  public String toString()
  {
    return super.toString();
  }
  


  private void sortInPlace(int[] indexes)
  {
    int[] unsorted = ArrayCopy.copyOf(indexes, indexes.length);
    Arrays.sort(unsorted);
    Content[] usc = new Content[unsorted.length];
    for (int i = 0; i < usc.length; i++) {
      usc[i] = elementData[indexes[i]];
    }
    
    for (int i = 0; i < indexes.length; i++) {
      elementData[unsorted[i]] = usc[i];
    }
  }
  










  private final int binarySearch(int[] indexes, int len, int val, Comparator<? super Content> comp)
  {
    int left = 0;int mid = 0;int right = len - 1;int cmp = 0;
    Content base = elementData[val];
    while (left <= right) {
      mid = left + right >>> 1;
      cmp = comp.compare(base, elementData[indexes[mid]]);
      if (cmp == 0) {
        while ((cmp == 0) && (mid < right) && (comp.compare(base, elementData[indexes[(mid + 1)]]) == 0))
        {
          mid++;
        }
        return mid + 1; }
      if (cmp < 0) {
        right = mid - 1;
      } else {
        left = mid + 1;
      }
    }
    return left;
  }
  







  public final void sort(Comparator<? super Content> comp)
  {
    if (comp == null)
    {


      return;
    }
    
    int sz = size;
    int[] indexes = new int[sz];
    for (int i = 0; i < sz; i++) {
      int ip = binarySearch(indexes, i, i, comp);
      if (ip < i) {
        System.arraycopy(indexes, ip, indexes, ip + 1, i - ip);
      }
      indexes[ip] = i;
    }
    sortInPlace(indexes);
  }
  









  private final class CLIterator
    implements Iterator<Content>
  {
    private int expect = -1;
    private int cursor = 0;
    private boolean canremove = false;
    
    private CLIterator() {
      expect = ContentList.this.getModCount();
    }
    
    public boolean hasNext()
    {
      return cursor < size;
    }
    
    public Content next()
    {
      if (ContentList.this.getModCount() != expect) {
        throw new ConcurrentModificationException("ContentList was modified outside of this Iterator");
      }
      
      if (cursor >= size) {
        throw new NoSuchElementException("Iterated beyond the end of the ContentList.");
      }
      
      canremove = true;
      return elementData[(cursor++)];
    }
    
    public void remove()
    {
      if (ContentList.this.getModCount() != expect) {
        throw new ConcurrentModificationException("ContentList was modified outside of this Iterator");
      }
      
      if (!canremove) {
        throw new IllegalStateException("Can only remove() content after a call to next()");
      }
      
      canremove = false;
      remove(--cursor);
      expect = ContentList.this.getModCount();
    }
  }
  











  private final class CLListIterator
    implements ListIterator<Content>
  {
    private boolean forward = false;
    
    private boolean canremove = false;
    
    private boolean canset = false;
    

    private int expectedmod = -1;
    
    private int cursor = -1;
    







    CLListIterator(int start)
    {
      expectedmod = ContentList.this.getModCount();
      

      forward = false;
      
      ContentList.this.checkIndex(start, false);
      
      cursor = start;
    }
    
    private void checkConcurrent() {
      if (expectedmod != ContentList.this.getModCount()) {
        throw new ConcurrentModificationException("The ContentList supporting this iterator has been modified bysomething other than this Iterator.");
      }
    }
    





    public boolean hasNext()
    {
      return (forward ? cursor + 1 : cursor) < size;
    }
    




    public boolean hasPrevious()
    {
      return (forward ? cursor : cursor - 1) >= 0;
    }
    




    public int nextIndex()
    {
      return forward ? cursor + 1 : cursor;
    }
    





    public int previousIndex()
    {
      return forward ? cursor : cursor - 1;
    }
    



    public Content next()
    {
      checkConcurrent();
      int next = forward ? cursor + 1 : cursor;
      
      if (next >= size) {
        throw new NoSuchElementException("next() is beyond the end of the Iterator");
      }
      
      cursor = next;
      forward = true;
      canremove = true;
      canset = true;
      return elementData[cursor];
    }
    



    public Content previous()
    {
      checkConcurrent();
      int prev = forward ? cursor : cursor - 1;
      
      if (prev < 0) {
        throw new NoSuchElementException("previous() is beyond the beginning of the Iterator");
      }
      
      cursor = prev;
      forward = false;
      canremove = true;
      canset = true;
      return elementData[cursor];
    }
    



    public void add(Content obj)
    {
      checkConcurrent();
      
      int next = forward ? cursor + 1 : cursor;
      
      add(next, obj);
      
      expectedmod = ContentList.this.getModCount();
      
      canremove = (this.canset = 0);
      





      cursor = next;
      forward = true;
    }
    




    public void remove()
    {
      checkConcurrent();
      if (!canremove) {
        throw new IllegalStateException("Can not remove an element unless either next() or previous() has been called since the last remove()");
      }
      






      remove(cursor);
      forward = false;
      expectedmod = ContentList.this.getModCount();
      
      canremove = false;
      canset = false;
    }
    




    public void set(Content obj)
    {
      checkConcurrent();
      if (!canset) {
        throw new IllegalStateException("Can not set an element unless either next() or previous() has been called since the last remove() or set()");
      }
      


      set(cursor, obj);
      expectedmod = ContentList.this.getModCount();
    }
  }
  









  class FilterList<F extends Content>
    extends AbstractList<F>
  {
    final Filter<F> filter;
    








    int[] backingpos = new int[size + 4];
    int backingsize = 0;
    
    int xdata = -1;
    





    FilterList()
    {
      this.filter = filter;
    }
    











    public boolean isEmpty()
    {
      return resync(0) == size;
    }
    









    private final int resync(int index)
    {
      if (xdata != ContentList.this.getDataModCount())
      {

        xdata = ContentList.this.getDataModCount();
        backingsize = 0;
        if (size >= backingpos.length) {
          backingpos = new int[size + 1];
        }
      }
      
      if ((index >= 0) && (index < backingsize))
      {

        return backingpos[index];
      }
      

      int bpi = 0;
      if (backingsize > 0) {
        bpi = backingpos[(backingsize - 1)] + 1;
      }
      
      while (bpi < size) {
        F gotit = (Content)filter.filter(elementData[bpi]);
        if (gotit != null) {
          backingpos[backingsize] = bpi;
          if (backingsize++ == index) {
            return bpi;
          }
        }
        bpi++;
      }
      return size;
    }
    











    public void add(int index, Content obj)
    {
      if (index < 0) {
        throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
      }
      int adj = resync(index);
      if ((adj == size) && (index > size())) {
        throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
      }
      if (filter.matches(obj)) {
        ContentList.this.add(adj, obj);
        



        if (backingpos.length <= size) {
          backingpos = ArrayCopy.copyOf(backingpos, backingpos.length + 1);
        }
        backingpos[index] = adj;
        backingsize = (index + 1);
        xdata = ContentList.this.getDataModCount();
      }
      else {
        throw new IllegalAddException("Filter won't allow the " + obj.getClass().getName() + " '" + obj + "' to be added to the list");
      }
    }
    



    public boolean addAll(int index, Collection<? extends F> collection)
    {
      if (collection == null) {
        throw new NullPointerException("Cannot add a null collection");
      }
      
      if (index < 0) {
        throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
      }
      
      int adj = resync(index);
      if ((adj == size) && (index > size())) {
        throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
      }
      
      int addcnt = collection.size();
      if (addcnt == 0) {
        return false;
      }
      
      ensureCapacity(ContentList.this.size() + addcnt);
      
      int tmpmodcount = ContentList.this.getModCount();
      int tmpdmc = ContentList.this.getDataModCount();
      boolean ok = false;
      
      int count = 0;
      try
      {
        for (Content c : collection) {
          if (c == null) {
            throw new NullPointerException("Cannot add null content");
          }
          
          if (filter.matches(c)) {
            ContentList.this.add(adj + count, c);
            




            if (backingpos.length <= size) {
              backingpos = ArrayCopy.copyOf(backingpos, backingpos.length + addcnt);
            }
            backingpos[(index + count)] = (adj + count);
            backingsize = (index + count + 1);
            xdata = ContentList.this.getDataModCount();
            
            count++;
          } else {
            throw new IllegalAddException("Filter won't allow the " + c.getClass().getName() + " '" + c + "' to be added to the list");
          }
        }
        


        ok = true;
      } finally {
        if (!ok) {
          for (;;) {
            count--; if (count < 0) break;
            ContentList.this.remove(adj + count);
          }
          
          ContentList.this.setModCount(tmpmodcount, tmpdmc);
          

          backingsize = index;
          xdata = tmpmodcount;
        }
      }
      
      return true;
    }
    







    public F get(int index)
    {
      if (index < 0) {
        throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
      }
      int adj = resync(index);
      if (adj == size) {
        throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
      }
      return (Content)filter.filter(ContentList.this.get(adj));
    }
    
    public Iterator<F> iterator()
    {
      return new ContentList.FilterListIterator(ContentList.this, this, 0);
    }
    
    public ListIterator<F> listIterator()
    {
      return new ContentList.FilterListIterator(ContentList.this, this, 0);
    }
    
    public ListIterator<F> listIterator(int index)
    {
      return new ContentList.FilterListIterator(ContentList.this, this, index);
    }
    







    public F remove(int index)
    {
      if (index < 0) {
        throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
      }
      int adj = resync(index);
      if (adj == size) {
        throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
      }
      Content oldc = ContentList.this.remove(adj);
      
      backingsize = index;
      xdata = ContentList.this.getDataModCount();
      
      return (Content)filter.filter(oldc);
    }
    










    public F set(int index, F obj)
    {
      if (index < 0) {
        throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
      }
      int adj = resync(index);
      if (adj == size) {
        throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
      }
      F ins = (Content)filter.filter(obj);
      if (ins != null) {
        F oldc = (Content)filter.filter(ContentList.this.set(adj, ins));
        
        xdata = ContentList.this.getDataModCount();
        return oldc;
      }
      throw new IllegalAddException("Filter won't allow index " + index + " to be set to " + obj.getClass().getName());
    }
    







    public int size()
    {
      resync(-1);
      return backingsize;
    }
    











    private final int fbinarySearch(int[] indexes, int len, int val, Comparator<? super F> comp)
    {
      int left = 0;int mid = 0;int right = len - 1;int cmp = 0;
      F base = elementData[backingpos[val]];
      while (left <= right) {
        mid = left + right >>> 1;
        cmp = comp.compare(base, elementData[indexes[mid]]);
        if (cmp == 0) {
          while ((cmp == 0) && (mid < right) && (comp.compare(base, elementData[indexes[(mid + 1)]]) == 0))
          {
            mid++;
          }
          return mid + 1; }
        if (cmp < 0) {
          right = mid - 1;
        } else {
          left = mid + 1;
        }
      }
      return left;
    }
    








    public final void sort(Comparator<? super F> comp)
    {
      if (comp == null)
      {


        return;
      }
      int sz = size();
      int[] indexes = new int[sz];
      for (int i = 0; i < sz; i++) {
        int ip = fbinarySearch(indexes, i, i, comp);
        if (ip < i) {
          System.arraycopy(indexes, ip, indexes, ip + 1, i - ip);
        }
        indexes[ip] = backingpos[i];
      }
      ContentList.this.sortInPlace(indexes);
    }
  }
  



  final class FilterListIterator<F extends Content>
    implements ListIterator<F>
  {
    private final ContentList.FilterList<F> filterlist;
    


    private boolean forward = false;
    
    private boolean canremove = false;
    
    private boolean canset = false;
    

    private int expectedmod = -1;
    
    private int cursor = -1;
    







    FilterListIterator(int flist)
    {
      filterlist = flist;
      expectedmod = ContentList.this.getModCount();
      

      forward = false;
      
      if (start < 0) {
        throw new IndexOutOfBoundsException("Index: " + start + " Size: " + filterlist.size());
      }
      
      int adj = ContentList.FilterList.access$800(filterlist, start);
      
      if ((adj == size) && (start > filterlist.size()))
      {

        throw new IndexOutOfBoundsException("Index: " + start + " Size: " + filterlist.size());
      }
      
      cursor = start;
    }
    
    private void checkConcurrent() {
      if (expectedmod != ContentList.this.getModCount()) {
        throw new ConcurrentModificationException("The ContentList supporting the FilterList this iterator is processing has been modified by something other than this Iterator.");
      }
    }
    






    public boolean hasNext()
    {
      return ContentList.FilterList.access$800(filterlist, forward ? cursor + 1 : cursor) < size;
    }
    




    public boolean hasPrevious()
    {
      return (forward ? cursor : cursor - 1) >= 0;
    }
    




    public int nextIndex()
    {
      return forward ? cursor + 1 : cursor;
    }
    





    public int previousIndex()
    {
      return forward ? cursor : cursor - 1;
    }
    



    public F next()
    {
      checkConcurrent();
      int next = forward ? cursor + 1 : cursor;
      
      if (ContentList.FilterList.access$800(filterlist, next) >= size) {
        throw new NoSuchElementException("next() is beyond the end of the Iterator");
      }
      
      cursor = next;
      forward = true;
      canremove = true;
      canset = true;
      return filterlist.get(cursor);
    }
    



    public F previous()
    {
      checkConcurrent();
      int prev = forward ? cursor : cursor - 1;
      
      if (prev < 0) {
        throw new NoSuchElementException("previous() is beyond the beginning of the Iterator");
      }
      
      cursor = prev;
      forward = false;
      canremove = true;
      canset = true;
      return filterlist.get(cursor);
    }
    



    public void add(Content obj)
    {
      checkConcurrent();
      
      int next = forward ? cursor + 1 : cursor;
      
      filterlist.add(next, obj);
      
      expectedmod = ContentList.this.getModCount();
      
      canremove = (this.canset = 0);
      





      cursor = next;
      forward = true;
    }
    




    public void remove()
    {
      checkConcurrent();
      if (!canremove) {
        throw new IllegalStateException("Can not remove an element unless either next() or previous() has been called since the last remove()");
      }
      






      filterlist.remove(cursor);
      forward = false;
      expectedmod = ContentList.this.getModCount();
      
      canremove = false;
      canset = false;
    }
    




    public void set(F obj)
    {
      checkConcurrent();
      if (!canset) {
        throw new IllegalStateException("Can not set an element unless either next() or previous() has been called since the last remove() or set()");
      }
      


      filterlist.set(cursor, obj);
      expectedmod = ContentList.this.getModCount();
    }
  }
}
