package org.jdom2;






































public class CDATA
  extends Text
{
  private static final long serialVersionUID = 200L;
  





































  protected CDATA()
  {
    super(Content.CType.CDATA);
  }
  









  public CDATA(String string)
  {
    super(Content.CType.CDATA);
    setText(string);
  }
  














  public CDATA setText(String str)
  {
    if ((str == null) || ("".equals(str))) {
      value = "";
      return this;
    }
    
    String reason = Verifier.checkCDATASection(str);
    if (reason != null) {
      throw new IllegalDataException(str, "CDATA section", reason);
    }
    
    value = str;
    
    return this;
  }
  














  public void append(String str)
  {
    if ((str == null) || ("".equals(str))) {
      return;
    }
    
    String tmpValue;
    
    String tmpValue;
    if (value == "") {
      tmpValue = str;
    } else {
      tmpValue = value + str;
    }
    






    String reason = Verifier.checkCDATASection(tmpValue);
    if (reason != null) {
      throw new IllegalDataException(str, "CDATA section", reason);
    }
    
    value = tmpValue;
  }
  










  public void append(Text text)
  {
    if (text == null) {
      return;
    }
    append(text.getText());
  }
  










  public String toString()
  {
    return 64 + "[CDATA: " + getText() + "]";
  }
  




  public CDATA clone()
  {
    return (CDATA)super.clone();
  }
  
  public CDATA detach()
  {
    return (CDATA)super.detach();
  }
  
  protected CDATA setParent(Parent parent)
  {
    return (CDATA)super.setParent(parent);
  }
}
