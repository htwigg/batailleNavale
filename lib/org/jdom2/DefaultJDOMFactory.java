package org.jdom2;

import java.util.Map;






































































public class DefaultJDOMFactory
  implements JDOMFactory
{
  public DefaultJDOMFactory() {}
  
  public Attribute attribute(String name, String value, Namespace namespace)
  {
    return new Attribute(name, value, namespace);
  }
  

  @Deprecated
  public Attribute attribute(String name, String value, int type, Namespace namespace)
  {
    return new Attribute(name, value, AttributeType.byIndex(type), namespace);
  }
  


  public Attribute attribute(String name, String value, AttributeType type, Namespace namespace)
  {
    return new Attribute(name, value, type, namespace);
  }
  
  public Attribute attribute(String name, String value)
  {
    return new Attribute(name, value);
  }
  
  @Deprecated
  public Attribute attribute(String name, String value, int type)
  {
    return new Attribute(name, value, type);
  }
  
  public Attribute attribute(String name, String value, AttributeType type)
  {
    return new Attribute(name, value, type);
  }
  
  public final CDATA cdata(String str)
  {
    return cdata(-1, -1, str);
  }
  
  public CDATA cdata(int line, int col, String text)
  {
    return new CDATA(text);
  }
  
  public final Text text(String str)
  {
    return text(-1, -1, str);
  }
  
  public Text text(int line, int col, String text)
  {
    return new Text(text);
  }
  
  public final Comment comment(String text)
  {
    return comment(-1, -1, text);
  }
  
  public Comment comment(int line, int col, String text)
  {
    return new Comment(text);
  }
  
  public final DocType docType(String elementName, String publicID, String systemID)
  {
    return docType(-1, -1, elementName, publicID, systemID);
  }
  

  public DocType docType(int line, int col, String elementName, String publicID, String systemID)
  {
    return new DocType(elementName, publicID, systemID);
  }
  
  public final DocType docType(String elementName, String systemID)
  {
    return docType(-1, -1, elementName, systemID);
  }
  

  public DocType docType(int line, int col, String elementName, String systemID)
  {
    return new DocType(elementName, systemID);
  }
  
  public final DocType docType(String elementName)
  {
    return docType(-1, -1, elementName);
  }
  

  public DocType docType(int line, int col, String elementName)
  {
    return new DocType(elementName);
  }
  
  public Document document(Element rootElement, DocType docType)
  {
    return new Document(rootElement, docType);
  }
  

  public Document document(Element rootElement, DocType docType, String baseURI)
  {
    return new Document(rootElement, docType, baseURI);
  }
  
  public Document document(Element rootElement)
  {
    return new Document(rootElement);
  }
  
  public final Element element(String name, Namespace namespace)
  {
    return element(-1, -1, name, namespace);
  }
  

  public Element element(int line, int col, String name, Namespace namespace)
  {
    return new Element(name, namespace);
  }
  
  public final Element element(String name)
  {
    return element(-1, -1, name);
  }
  
  public Element element(int line, int col, String name)
  {
    return new Element(name);
  }
  
  public final Element element(String name, String uri)
  {
    return element(-1, -1, name, uri);
  }
  

  public Element element(int line, int col, String name, String uri)
  {
    return new Element(name, uri);
  }
  
  public final Element element(String name, String prefix, String uri)
  {
    return element(-1, -1, name, prefix, uri);
  }
  

  public Element element(int line, int col, String name, String prefix, String uri)
  {
    return new Element(name, prefix, uri);
  }
  
  public final ProcessingInstruction processingInstruction(String target)
  {
    return processingInstruction(-1, -1, target);
  }
  

  public ProcessingInstruction processingInstruction(int line, int col, String target)
  {
    return new ProcessingInstruction(target);
  }
  

  public final ProcessingInstruction processingInstruction(String target, Map<String, String> data)
  {
    return processingInstruction(-1, -1, target, data);
  }
  

  public ProcessingInstruction processingInstruction(int line, int col, String target, Map<String, String> data)
  {
    return new ProcessingInstruction(target, data);
  }
  

  public final ProcessingInstruction processingInstruction(String target, String data)
  {
    return processingInstruction(-1, -1, target, data);
  }
  

  public ProcessingInstruction processingInstruction(int line, int col, String target, String data)
  {
    return new ProcessingInstruction(target, data);
  }
  
  public final EntityRef entityRef(String name)
  {
    return entityRef(-1, -1, name);
  }
  
  public EntityRef entityRef(int line, int col, String name)
  {
    return new EntityRef(name);
  }
  
  public final EntityRef entityRef(String name, String publicID, String systemID)
  {
    return entityRef(-1, -1, name, publicID, systemID);
  }
  

  public EntityRef entityRef(int line, int col, String name, String publicID, String systemID)
  {
    return new EntityRef(name, publicID, systemID);
  }
  
  public final EntityRef entityRef(String name, String systemID)
  {
    return entityRef(-1, -1, name, systemID);
  }
  

  public EntityRef entityRef(int line, int col, String name, String systemID)
  {
    return new EntityRef(name, systemID);
  }
  




  public void addContent(Parent parent, Content child)
  {
    if ((parent instanceof Document)) {
      ((Document)parent).addContent(child);
    } else {
      ((Element)parent).addContent(child);
    }
  }
  
  public void setAttribute(Element parent, Attribute a)
  {
    parent.setAttribute(a);
  }
  
  public void addNamespaceDeclaration(Element parent, Namespace additional)
  {
    parent.addNamespaceDeclaration(additional);
  }
  
  public void setRoot(Document doc, Element root)
  {
    doc.setRootElement(root);
  }
}
