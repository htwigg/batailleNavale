package org.jdom2;

import org.jdom2.filter.Filter;
import org.jdom2.util.IteratorIterable;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
































































public class Document
  extends CloneBase
  implements Parent
{
  transient ContentList content = new ContentList(this);
  



  protected String baseURI = null;
  

  private transient HashMap<String, Object> propertyMap = null;
  





  private static final long serialVersionUID = 200L;
  






  public Document() {}
  






  public Document(Element rootElement, DocType docType, String baseURI)
  {
    if (rootElement != null) {
      setRootElement(rootElement);
    }
    if (docType != null) {
      setDocType(docType);
    }
    if (baseURI != null) {
      setBaseURI(baseURI);
    }
  }
  











  public Document(Element rootElement, DocType docType)
  {
    this(rootElement, docType, null);
  }
  









  public Document(Element rootElement)
  {
    this(rootElement, null, null);
  }
  










  public Document(List<? extends Content> content)
  {
    setContent(content);
  }
  
  public int getContentSize()
  {
    return content.size();
  }
  
  public int indexOf(Content child)
  {
    return content.indexOf(child);
  }
  























  public boolean hasRootElement()
  {
    return content.indexOfFirstElement() >= 0;
  }
  






  public Element getRootElement()
  {
    int index = content.indexOfFirstElement();
    if (index < 0) {
      throw new IllegalStateException("Root element not set");
    }
    return (Element)content.get(index);
  }
  









  public Document setRootElement(Element rootElement)
  {
    int index = content.indexOfFirstElement();
    if (index < 0) {
      content.add(rootElement);
    }
    else {
      content.set(index, rootElement);
    }
    return this;
  }
  




  public Element detachRootElement()
  {
    int index = content.indexOfFirstElement();
    if (index < 0)
      return null;
    return (Element)removeContent(index);
  }
  






  public DocType getDocType()
  {
    int index = content.indexOfDocType();
    if (index < 0) {
      return null;
    }
    return (DocType)content.get(index);
  }
  












  public Document setDocType(DocType docType)
  {
    if (docType == null)
    {
      int docTypeIndex = content.indexOfDocType();
      if (docTypeIndex >= 0) content.remove(docTypeIndex);
      return this;
    }
    
    if (docType.getParent() != null) {
      throw new IllegalAddException(docType, "The DocType already is attached to a document");
    }
    


    int docTypeIndex = content.indexOfDocType();
    if (docTypeIndex < 0) {
      content.add(0, docType);
    }
    else {
      content.set(docTypeIndex, docType);
    }
    
    return this;
  }
  







  public Document addContent(Content child)
  {
    content.add(child);
    return this;
  }
  











  public Document addContent(Collection<? extends Content> c)
  {
    content.addAll(c);
    return this;
  }
  










  public Document addContent(int index, Content child)
  {
    content.add(index, child);
    return this;
  }
  














  public Document addContent(int index, Collection<? extends Content> c)
  {
    content.addAll(index, c);
    return this;
  }
  
  public List<Content> cloneContent()
  {
    int size = getContentSize();
    List<Content> list = new ArrayList(size);
    for (int i = 0; i < size; i++) {
      Content child = getContent(i);
      list.add(child.clone());
    }
    return list;
  }
  
  public Content getContent(int index)
  {
    return content.get(index);
  }
  



















  public List<Content> getContent()
  {
    if (!hasRootElement())
      throw new IllegalStateException("Root element not set");
    return content;
  }
  













  public <F extends Content> List<F> getContent(Filter<F> filter)
  {
    if (!hasRootElement())
      throw new IllegalStateException("Root element not set");
    return content.getView(filter);
  }
  





  public List<Content> removeContent()
  {
    List<Content> old = new ArrayList(content);
    content.clear();
    return old;
  }
  






  public <F extends Content> List<F> removeContent(Filter<F> filter)
  {
    List<F> old = new ArrayList();
    Iterator<F> itr = content.getView(filter).iterator();
    while (itr.hasNext()) {
      F child = (Content)itr.next();
      old.add(child);
      itr.remove();
    }
    return old;
  }
  

































  public Document setContent(Collection<? extends Content> newContent)
  {
    content.clearAndSet(newContent);
    return this;
  }
  








  public final void setBaseURI(String uri)
  {
    baseURI = uri;
  }
  







  public final String getBaseURI()
  {
    return baseURI;
  }
  














  public Document setContent(int index, Content child)
  {
    content.set(index, child);
    return this;
  }
  















  public Document setContent(int index, Collection<? extends Content> collection)
  {
    content.remove(index);
    content.addAll(index, collection);
    return this;
  }
  
  public boolean removeContent(Content child)
  {
    return content.remove(child);
  }
  
  public Content removeContent(int index)
  {
    return content.remove(index);
  }
  


























  public Document setContent(Content child)
  {
    content.clear();
    content.add(child);
    return this;
  }
  










  public String toString()
  {
    StringBuilder stringForm = new StringBuilder().append("[Document: ");
    

    DocType docType = getDocType();
    if (docType != null) {
      stringForm.append(docType.toString()).append(", ");
    }
    else {
      stringForm.append(" No DOCTYPE declaration, ");
    }
    
    Element rootElement = hasRootElement() ? getRootElement() : null;
    if (rootElement != null) {
      stringForm.append("Root is ").append(rootElement.toString());
    }
    else {
      stringForm.append(" No root element");
    }
    
    stringForm.append("]");
    
    return stringForm.toString();
  }
  








  public final boolean equals(Object ob)
  {
    return ob == this;
  }
  





  public final int hashCode()
  {
    return super.hashCode();
  }
  





  public Document clone()
  {
    Document doc = (Document)super.clone();
    


    content = new ContentList(doc);
    


    for (int i = 0; i < content.size(); i++) {
      Object obj = content.get(i);
      if ((obj instanceof Element)) {
        Element element = ((Element)obj).clone();
        content.add(element);
      }
      else if ((obj instanceof Comment)) {
        Comment comment = ((Comment)obj).clone();
        content.add(comment);
      }
      else if ((obj instanceof ProcessingInstruction)) {
        ProcessingInstruction pi = ((ProcessingInstruction)obj).clone();
        content.add(pi);
      }
      else if ((obj instanceof DocType)) {
        DocType dt = ((DocType)obj).clone();
        content.add(dt);
      }
    }
    
    return doc;
  }
  





  public IteratorIterable<Content> getDescendants()
  {
    return new DescendantIterator(this);
  }
  









  public <F extends Content> IteratorIterable<F> getDescendants(Filter<F> filter)
  {
    return new FilterIterator(new DescendantIterator(this), filter);
  }
  




  public Parent getParent()
  {
    return null;
  }
  






  public Document getDocument()
  {
    return this;
  }
  









  public void setProperty(String id, Object value)
  {
    if (propertyMap == null) {
      propertyMap = new HashMap();
    }
    propertyMap.put(id, value);
  }
  







  public Object getProperty(String id)
  {
    if (propertyMap == null) {
      return null;
    }
    return propertyMap.get(id);
  }
  
  public void canContainContent(Content child, int index, boolean replace)
  {
    if ((child instanceof Element)) {
      int cre = content.indexOfFirstElement();
      if ((replace) && (cre == index)) {
        return;
      }
      if (cre >= 0) {
        throw new IllegalAddException("Cannot add a second root element, only one is allowed");
      }
      
      if (content.indexOfDocType() >= index) {
        throw new IllegalAddException("A root element cannot be added before the DocType");
      }
    }
    
    if ((child instanceof DocType)) {
      int cdt = content.indexOfDocType();
      if ((replace) && (cdt == index))
      {
        return;
      }
      if (cdt >= 0) {
        throw new IllegalAddException("Cannot add a second doctype, only one is allowed");
      }
      
      int firstElt = content.indexOfFirstElement();
      if ((firstElt != -1) && (firstElt < index)) {
        throw new IllegalAddException("A DocType cannot be added after the root element");
      }
    }
    

    if ((child instanceof CDATA)) {
      throw new IllegalAddException("A CDATA is not allowed at the document root");
    }
    
    if ((child instanceof Text)) {
      throw new IllegalAddException("A Text is not allowed at the document root");
    }
    
    if ((child instanceof EntityRef)) {
      throw new IllegalAddException("An EntityRef is not allowed at the document root");
    }
  }
  
















  public List<Namespace> getNamespacesInScope()
  {
    return Collections.unmodifiableList(Arrays.asList(new Namespace[] { Namespace.NO_NAMESPACE, Namespace.XML_NAMESPACE }));
  }
  

  public List<Namespace> getNamespacesIntroduced()
  {
    return Collections.unmodifiableList(Arrays.asList(new Namespace[] { Namespace.NO_NAMESPACE, Namespace.XML_NAMESPACE }));
  }
  

  public List<Namespace> getNamespacesInherited()
  {
    return Collections.emptyList();
  }
  




















  private void writeObject(ObjectOutputStream out)
    throws IOException
  {
    out.defaultWriteObject();
    int cs = content.size();
    out.writeInt(cs);
    for (int i = 0; i < cs; i++) {
      out.writeObject(getContent(i));
    }
  }
  








  private void readObject(ObjectInputStream in)
    throws IOException, ClassNotFoundException
  {
    in.defaultReadObject();
    
    content = new ContentList(this);
    
    int cs = in.readInt();
    for (;;) { cs--; if (cs < 0) break;
      addContent((Content)in.readObject());
    }
  }
}
