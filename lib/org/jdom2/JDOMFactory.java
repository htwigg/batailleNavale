package org.jdom2;

import java.util.Map;

public abstract interface JDOMFactory
{
  public abstract Attribute attribute(String paramString1, String paramString2, Namespace paramNamespace);
  
  @Deprecated
  public abstract Attribute attribute(String paramString1, String paramString2, int paramInt, Namespace paramNamespace);
  
  public abstract Attribute attribute(String paramString1, String paramString2, AttributeType paramAttributeType, Namespace paramNamespace);
  
  public abstract Attribute attribute(String paramString1, String paramString2);
  
  @Deprecated
  public abstract Attribute attribute(String paramString1, String paramString2, int paramInt);
  
  public abstract Attribute attribute(String paramString1, String paramString2, AttributeType paramAttributeType);
  
  public abstract CDATA cdata(String paramString);
  
  public abstract CDATA cdata(int paramInt1, int paramInt2, String paramString);
  
  public abstract Text text(int paramInt1, int paramInt2, String paramString);
  
  public abstract Text text(String paramString);
  
  public abstract Comment comment(String paramString);
  
  public abstract Comment comment(int paramInt1, int paramInt2, String paramString);
  
  public abstract DocType docType(String paramString1, String paramString2, String paramString3);
  
  public abstract DocType docType(String paramString1, String paramString2);
  
  public abstract DocType docType(String paramString);
  
  public abstract DocType docType(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3);
  
  public abstract DocType docType(int paramInt1, int paramInt2, String paramString1, String paramString2);
  
  public abstract DocType docType(int paramInt1, int paramInt2, String paramString);
  
  public abstract Document document(Element paramElement, DocType paramDocType);
  
  public abstract Document document(Element paramElement, DocType paramDocType, String paramString);
  
  public abstract Document document(Element paramElement);
  
  public abstract Element element(String paramString, Namespace paramNamespace);
  
  public abstract Element element(String paramString);
  
  public abstract Element element(String paramString1, String paramString2);
  
  public abstract Element element(String paramString1, String paramString2, String paramString3);
  
  public abstract Element element(int paramInt1, int paramInt2, String paramString, Namespace paramNamespace);
  
  public abstract Element element(int paramInt1, int paramInt2, String paramString);
  
  public abstract Element element(int paramInt1, int paramInt2, String paramString1, String paramString2);
  
  public abstract Element element(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3);
  
  public abstract ProcessingInstruction processingInstruction(String paramString, Map<String, String> paramMap);
  
  public abstract ProcessingInstruction processingInstruction(String paramString1, String paramString2);
  
  public abstract ProcessingInstruction processingInstruction(String paramString);
  
  public abstract ProcessingInstruction processingInstruction(int paramInt1, int paramInt2, String paramString, Map<String, String> paramMap);
  
  public abstract ProcessingInstruction processingInstruction(int paramInt1, int paramInt2, String paramString1, String paramString2);
  
  public abstract ProcessingInstruction processingInstruction(int paramInt1, int paramInt2, String paramString);
  
  public abstract EntityRef entityRef(String paramString);
  
  public abstract EntityRef entityRef(String paramString1, String paramString2, String paramString3);
  
  public abstract EntityRef entityRef(String paramString1, String paramString2);
  
  public abstract EntityRef entityRef(int paramInt1, int paramInt2, String paramString);
  
  public abstract EntityRef entityRef(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3);
  
  public abstract EntityRef entityRef(int paramInt1, int paramInt2, String paramString1, String paramString2);
  
  public abstract void addContent(Parent paramParent, Content paramContent);
  
  public abstract void setAttribute(Element paramElement, Attribute paramAttribute);
  
  public abstract void addNamespaceDeclaration(Element paramElement, Namespace paramNamespace);
  
  public abstract void setRoot(Document paramDocument, Element paramElement);
}
